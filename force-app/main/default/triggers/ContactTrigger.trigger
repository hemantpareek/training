trigger ContactTrigger on Contact (after insert,before update, after update, after delete, after undelete) {
    if(CheckRecursive.preventTriggerFire == false){ 
        
        if(Trigger.isAfter && Trigger.isInsert){
            CheckRecursive.insertcount++;
            System.debug('insert trigger ==>> '+CheckRecursive.insertcount);
            ContactTriggerHandler.insertRecordHandler(Trigger.newMap); 
        
        }else if(Trigger.isAfter && Trigger.isDelete){
            CheckRecursive.deletecount++;
            System.debug('delete trigger ==>> '+CheckRecursive.deletecount);
            ContactTriggerHandler.deleteRecordHandler(Trigger.oldMap);

        }else if(Trigger.isAfter && Trigger.isUndelete){
            CheckRecursive.undeletecount++;
            System.debug('undelete trigger ==>> '+CheckRecursive.undeletecount);
            ContactTriggerHandler.undeleteRecordHandler(Trigger.newMap);

        }else if(Trigger.isAfter && Trigger.isUpdate && CheckRecursive.run == true){
            CheckRecursive.updatecount++;
            System.debug('update trigger ==>> '+CheckRecursive.updatecount);
            ContactTriggerHandler.updateRecordHandler(Trigger.oldMap,Trigger.newMap);
        }
    }
}