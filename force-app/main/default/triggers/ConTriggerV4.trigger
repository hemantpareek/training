trigger ConTriggerV4 on Contact (after insert, after update) {
    if(CheckRecursive.run){
            List<Contact> cons = [SELECT Id, LastName, Account.Name FROM Contact WHERE Id IN:Trigger.new];
            if(Trigger.oldMap!=null){
                System.debug('@@@ oldcon:' + Trigger.oldMap.get(cons[0].Id));
            }
            System.debug('@@@ newcon:' + Trigger.newMap.get(cons[0].Id));
            System.debug('@@@ con:' + cons[0]);
            cons[0].LastName = 'Test';
            CheckRecursive.run = false;
            //update cons;
            Contact con = Trigger.new[0];
            update con;
            CheckRecursive.run = true;
        
    }
}