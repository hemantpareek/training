trigger ContactTriggerV2 on Contact (before insert, before update, after delete, after undelete) {

    if(Trigger.isBefore && Trigger.isInsert && ContactTriggerV2Handler.fireTrigger){
        ContactTriggerV2Handler.handleInsert(Trigger.New);
    }else if(Trigger.isBefore && Trigger.isUpdate && ContactTriggerV2Handler.fireTrigger){
        ContactTriggerV2Handler.handleUpdate(Trigger.newMap, Trigger.oldMap);
    }else if(Trigger.isAfter && Trigger.isDelete && ContactTriggerV2Handler.fireTrigger){
        ContactTriggerV2Handler.handleDelete(Trigger.old);
    }else if(Trigger.isAfter && Trigger.isUndelete && ContactTriggerV2Handler.fireTrigger){
        ContactTriggerV2Handler.handleUndelete(Trigger.new);
    }
}