trigger ContactTrigger_V3 on Contact (before update) {
    
    if(Trigger.isBefore && Trigger.isUpdate && ContactTriggerHandler_V3.fireTrigger){
        ContactTriggerHandler_V3.handleUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
}