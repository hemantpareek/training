global class ContactStatusUpdaterBatch implements Database.Batchable<sObject>, Database.Stateful {  
    @testVisible List<Contact> contactRecords;
    public ContactStatusUpdaterBatch(){
        contactRecords = new List<Contact>();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {  
        String query = 'SELECT id, lastname, status__c, sample_boolean__c, sample_date__c, sample_date_time__c, sample_text_area__c FROM Contact WHERE createdDate=yesterday';
        return Database.getQueryLocator(query);
    }  
    global void execute(Database.BatchableContext bc, List<Contact> contacts){  
        List<Contact> conToUpdate = new List<Contact>();
        for(Contact con : contacts){
            con.status__c = 'Ready for approval';
            conToUpdate.add(con);
        }
        update conToUpdate;
        contactRecords.addAll(contacts);
    }      
    global void finish(Database.BatchableContext bc){  
        // execute any post-processing operations  
        // generate csv and mail to the user
            Folder f = [SELECT name FROM Folder where name='Public Docs' LIMIT 1];
            Datetime now = Datetime.now();
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);
            Document doc = new Document(name='YesterdayCreatedRecords_'+local, Body = Blob.valueOf(generateCsvString()), Type = 'csv', FolderId = f.id);
            insert doc;
            sendEmail(doc.id);
    }   

    global void sendEmail(String docId){
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();  
        message.setEntityAttachments(new List<String>{docId});  
        message.setPlainTextBody('Please approve these records');  
        message.setSubject('Records waiting for approval');  
        message.setToAddresses(new List<String> {'er.hemantpareek@gmail.com'});  
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);  
        if (results[0].success) {  
            System.debug('The email was sent successfully.');  
        } else {  
            System.debug('The email failed to send: '+ results[0].errors[0].message);  
        }  
    }

    public String generateCsvString(){
        String generatedCSVFile ='';
        String colums = '';
        List<String> fields = new List<String>{'lastname', 'status__c', 'sample_boolean__c', 'sample_date__c', 'sample_date_time__c', 'sample_text_area__c'};
        for(String field : fields){
            colums+='\"'+field+'\"'+',';
        }
        colums=colums.removeEnd(',');
        colums+='\n';
        generatedCSVFile+=colums;
        for(Contact con: contactRecords){
            String row = '';
            for(String field : fields){
                row += '\"';
                row += con.get(field)!=null ? con.get(field) : '' ;
                row += '\"'+',';
            }
            row=row.removeEnd(',');
            row+='\n';
            generatedCSVFile+=row;
        }
        return generatedCSVFile;
    } 
}