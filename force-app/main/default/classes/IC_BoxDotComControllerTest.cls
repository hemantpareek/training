@isTest
private class IC_BoxDotComControllerTest {
     @TestSetup
    private static void makeData(){
        Test.startTest();
        Box_com_Integration_Data__c bdci = new Box_com_Integration_Data__c();
        bdci.access_Token__c = 'SAMPLE_ACCESS_TOKEN';
        bdci.refresh_token__c = 'SAMPLE_REFRESH_TOKEN';
        bdci.expires_in__c = system.now().addSeconds(3600);
        bdci.user_email__c = UserInfo.getUserEmail();
        database.insert(bdci);
        Box_Com_Integration_Credentials__c bdcic = new Box_Com_Integration_Credentials__c();
        bdcic.client_id__c = 'SAMPLE_CLIENT_ID';
        bdcic.client_secret__c = 'SAMPLE_CLIENT_SECRET';
        database.insert(bdcic);
        Test.stopTest();
    }

    //1. TEST IF THERE EXISTS VALID DATA IN CUSTOM SETTING OR NOT
    @isTest
    private static void retrieveDataFromCustomSettingTest(){
        Test.startTest();
        //POSITIVE TEST
        system.assertEquals(null,IC_BoxDotComController.access_token);
        system.assertEquals(null,IC_BoxDotComController.refresh_token);
        system.assertEquals(null,IC_BoxDotComController.expires_in);
        String redirect_uri = 'https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/box';
        system.assertEquals(redirect_uri,IC_BoxDotComController.redirect_uri);
        system.assertEquals(null,IC_BoxDotComController.bdci);
        system.assertEquals(true,IC_BoxDotComController.retrieveDataFromCustomSetting());
        system.assertEquals('SAMPLE_ACCESS_TOKEN',IC_BoxDotComController.access_token);
        system.assertEquals('SAMPLE_REFRESH_TOKEN',IC_BoxDotComController.refresh_token);
        system.assert(IC_BoxDotComController.expires_in > system.now());
        system.assert(IC_BoxDotComController.bdci != null);
        //NEGATIVE TEST
        IC_BoxDotComController.bdci.expires_in__c = system.now().addSeconds(-3600);
        database.update(IC_BoxDotComController.bdci);
        system.assertEquals(true,IC_BoxDotComController.retrieveDataFromCustomSetting());
        system.assertEquals(null,IC_BoxDotComController.access_token);
        //NO RECORD TEST
        database.delete(IC_BoxDotComController.bdci);
        system.assertEquals(false,IC_BoxDotComController.retrieveDataFromCustomSetting());
        Test.stopTest();
    }

    //2. TEST IF THERE EXISTS VALID DATA IN CUSTOM METADATA TYPES OR NOT 
    @isTest
    private static void retrieveDataFromCustomMetadataTest(){
        Test.startTest();
        system.assertEquals(null,IC_BoxDotComController.clientID);
        system.assertEquals(null,IC_BoxDotComController.clientSecret);
        system.assertEquals(true,IC_BoxDotComController.retrieveDataFromCustomMetadata());
        system.assert(IC_BoxDotComController.clientID != null); // this assertion will fail if you have no data in custom settings
        system.assert(IC_BoxDotComController.clientSecret != null); // this assertion will fail if you have no data in custom settings
        //IF NO RECORDS
        database.delete([SELECT ID FROM Box_Com_Integration_Credentials__c]);
        system.assertEquals(false,IC_BoxDotComController.retrieveDataFromCustomMetadata());
        Test.stopTest();
    }

    //3. TEST FOR AUTH URI RETRIVAL
    @isTest
    private static void doGetAuthURITest(){
        Test.startTest();
        String expectedOutput = '{"status":"authenticateWithCode",'+
                                '"authURI":"https://app.box.com/api/oauth2/authorize?'+
                                'response_type=code&client_id=SAMPLE_CLIENT_ID&'+
                                'redirect_uri=https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/box"'+
                                '}';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doGetAuthURI());
        //WHEN CREDENTIALS DOES NOT EXISTS
        database.delete([SELECT ID FROM Box_Com_Integration_Credentials__c]);
        expectedOutput = '{"status":"CUSTOM_METADATA_CREDENTIAL_NOT_FOUND"}';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doGetAuthURI());
        Test.stopTest();
    }

    //4. TEST FOR PROCESS WHEN COMPONENT LOADS FIRST TIME
    @isTest
    private static void doInitApexTest(){
        Test.startTest();
        //IF CUSTOM SETTING CREDENTIALS EXISTS && EXPIRES_IN > SYSTEM.NOW()
        String expectedOutput = '{"status":"readyToLoadData"}';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doInitApex());
        system.assertEquals('SAMPLE_ACCESS_TOKEN', IC_BoxDotComController.access_token);
        //IF CUSTOM SETTING CREDENTIALS EXISTS && EXPIRES_IN < SYSTEM.NOW() && REFRESH TOKEN != NULL
        IC_BoxDotComController.bdci.expires_in__c = system.now().addSeconds(-3600);
        update(IC_BoxDotComController.bdci);
        expectedOutput = '{"status":"getAccessTokenFromRefreshToken"}';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doInitApex());
        //IF CUSTOM SETTING CREDENTIALS EXISTS && EXPIRES_IN < SYSTEM.NOW() && REFRESH TOKEN IS NULL
        IC_BoxDotComController.bdci.refresh_token__c = null;
        update(IC_BoxDotComController.bdci);
        expectedOutput = '{"status":"authenticateWithCode",'+
                         '"authURI":"https://app.box.com/api/oauth2/authorize?'+
                         'response_type=code&client_id=SAMPLE_CLIENT_ID&'+
                         'redirect_uri=https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/box"'+
                         '}';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doInitApex());
        //IF CUSTOM SETTING CREDENTIALS NOT EXISTS
        delete(IC_BoxDotComController.bdci);
        system.assertEquals(expectedOutput, IC_BoxDotComController.doInitApex());
        Test.stopTest();
    }

    //5. TEST FOR GETTING ACCESS TOKEN FROM REFRESH TOKEN WHEN ALL CREDENTIALS EXISTS IN CUSTOM SETTINGS 
    @isTest
    private static void doGetAccessTokenFromRefreshTokenTest_1(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        String expectedOutput = 'SUCCESS';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doGetAccessTokenFromRefreshToken());
        system.assertEquals('NEW_ACCESS_TOKEN', IC_BoxDotComController.bdci.access_token__c);
        system.assertEquals('NEW_REFRESH_TOKEN', IC_BoxDotComController.bdci.refresh_token__c);
        Test.stopTest();
    }

    //6. TEST FOR GETTING ACCESS TOKEN FROM REFRESH TOKEN WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doGetAccessTokenFromRefreshTokenTest_2(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_BoxDotComController.doGetAccessTokenFromRefreshToken());
        Test.stopTest();
    }

    //7. TEST FOR GETTING ACCESS TOKEN FROM REFRESH TOKEN WHEN CREDENTIAL NOT EXISTS IN DATABASE
    @isTest
    private static void doGetAccessTokenFromRefreshTokenTest_3(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        delete([SELECT Id FROM Box_com_Integration_Data__c]);
        delete([SELECT Id FROM Box_Com_Integration_Credentials__c]);
        Test.startTest();
        system.assertEquals(null, IC_BoxDotComController.doGetAccessTokenFromRefreshToken());
        Test.stopTest();
    }

    //8. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN NO CLIENT ID AND SECRET ARE PRESENT
    @isTest
    private static void doGetAccessTokenTest_1(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        delete([SELECT Id FROM Box_Com_Integration_Credentials__c]);
        Test.startTest();
        system.assertEquals(null, IC_BoxDotComController.doGetAccessToken('SAMPLE_CODE'));
        Test.stopTest();
    }

    //9. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN CLIENT ID AND SECRET ARE PRESENT
    @isTest
    private static void doGetAccessTokenTest_2(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        system.assertEquals('SUCCESS', IC_BoxDotComController.doGetAccessToken('SAMPLE_CODE'));
        system.assertEquals('NEW_ACCESS_TOKEN', IC_BoxDotComController.bdci.access_Token__c);
        Test.stopTest();
    }

    //10. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN THERE IS NO EXISTING RECORD OF GOOGLE DRIVE INTEGRATION DATA
    @isTest
    private static void doGetAccessTokenTest_3(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        delete([SELECT Id FROM Box_com_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals('SUCCESS', IC_BoxDotComController.doGetAccessToken('SAMPLE_CODE'));
        system.assert(IC_BoxDotComController.bdci != null);
        system.assertEquals('NEW_ACCESS_TOKEN', IC_BoxDotComController.bdci.access_Token__c);
        Test.stopTest();
    }

    //11. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doGetAccessTokenTest_4(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_BoxDotComController.doGetAccessToken('SAMPLE_CODE'));
        Test.stopTest();
    }

    //12. TEST FOR FETCH DATA FIRST TIME
    @isTest
    private static void doFetchDataFirstTimeTest(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        //FETCH DATA TEST FOR ROOT FOLDER
        String expectedOutput = '[{'+
                                '"id":"SAMPLE_ID",'+
                                '"name":"SAMPLE_NAME",'+
                                '"type":"SAMPLE_TYPE",'+
                                '"isLast":"false"'+
                                '}]';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doFetchDataFirstTime());
        //FETCH DATA TEST FOR NULL RESPONSE
        BoxDotComMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_BoxDotComController.doFetchDataFirstTime());
        Test.stopTest();
    }

    //13. TEST FOR CHANGING DIRECTORY
    @isTest
    private static void doChangeDirectoryTest(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        String expectedOutput = '[{'+
                                '"id":"SAMPLE_ID",'+
                                '"name":"SAMPLE_NAME",'+
                                '"type":"SAMPLE_TYPE",'+
                                '"isLast":"false"'+
                                '}]';
        BoxDotComMockCallOut.parentId = 'SAMPLE_PARENT_ID';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doChangeDirectory('SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //14. TEST FOR DELETING SELECTED FILE WHEN ALL INTEGRATION DATA RECORDS DOESNOT EXISTS
    @isTest
    private static void deleteSelectedFileTest_1(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        delete([SELECT Id FROM Box_com_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals(null, IC_BoxDotComController.deleteSelectedFile('file','SAMPLE_FILE_ID'));
        Test.stopTest();
    }

    //15. TEST FOR DELETING SELECTED FILE WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void deleteSelectedFileTest_2(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 204;
        system.assertEquals('DELETE_SUCCESSFUL', IC_BoxDotComController.deleteSelectedFile('file','SAMPLE_FILE_ID'));
        Test.stopTest();
    }

    //16. TEST FOR DELETING SELECTED FILE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void deleteSelectedFileTest_3(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_BoxDotComController.deleteSelectedFile('file','SAMPLE_FILE_ID'));
        Test.stopTest();
    }

    //17. TEST FOR DELETING SELECTED FOLDER WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void deleteSelectedFileTest_4(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 204;
        system.assertEquals('DELETE_SUCCESSFUL', IC_BoxDotComController.deleteSelectedFile('folder','SAMPLE_FOLDER_ID'));
        Test.stopTest();
    }

    //18. TEST FOR UPLOAD FILE WHEN ALL INTEGRATION DATA RECORDS DOESNOT EXISTS
    @isTest
    private static void doUploadFileTest_1(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        delete([SELECT Id FROM Box_com_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 201;
        BoxDotComMockCallOut.parentId = 'SAMPLE_PARENT_ID';
        system.assertEquals(null, IC_BoxDotComController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLEBASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //19. TEST FOR UPLOAD FILE/CREATE FOLDER WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void doUploadFileTest_2(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 201;
        BoxDotComMockCallOut.parentId = 'SAMPLE_PARENT_ID';
        String expectedOutput = '{"name":"SAMPLE_FILE_NAME","id":"SAMPLE_FILE_ID","type":"SAMPLE_FILE_TYPE"}';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLEBASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PARENT_ID'));
        Test.stopTest();
    }   

    //20. TEST FOR UPLOAD FILE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doUploadFileTest_3(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 400;
        BoxDotComMockCallOut.parentId = 'SAMPLE_PARENT_ID';
        system.assertEquals(null, IC_BoxDotComController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLEBASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //21. TEST FOR CREATE FOLDER WHEN CREDENTIALS NOT AVAILABLE
    @isTest
    private static void doCreateFolderTest_1(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        delete([SELECT Id FROM Box_com_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 201;
        BoxDotComMockCallOut.parentId = 'SAMPLE_PARENT_ID';
        system.assertEquals(null, IC_BoxDotComController.doCreateFolder('SAMPLE_FOLDER_NAME','SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //22. TEST FOR CREATE FOLDER WHEN CREDENTIALS AVAILABLE
    @isTest
    private static void doCreateFolderTest_2(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 201;
        BoxDotComMockCallOut.parentId = 'SAMPLE_PARENT_ID';
        String expectedOutput = 'SAMPLE_ID';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doCreateFolder('SAMPLE_FOLDER_NAME','SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //23. TEST FOR CREATE FOLDER WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doCreateFolderTest_3(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 400;
        BoxDotComMockCallOut.parentId = 'SAMPLE_PARENT_ID';
        system.assertEquals(null, IC_BoxDotComController.doCreateFolder('SAMPLE_FOLDER_NAME','SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //24. TEST FOR DOWNLOAD FILE WHEN CREDENTIALS NOT AVAILABLE
    @isTest
    private static void doFileDownloadTest_1(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        delete([SELECT Id FROM Box_com_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 302;
        system.assertEquals(null, IC_BoxDotComController.doFileDownload('SAMPLE_FILE_ID'));
        Test.stopTest();
    }

    //25. TEST FOR DOWNLOAD FILE WHEN CREDENTIALS AVAILABLE
    @isTest
    private static void doFileDownloadTest_2(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 302;
        String expectedOutput = 'SAMPLE_DOWNLOAD_LINK';
        system.assertEquals(expectedOutput, IC_BoxDotComController.doFileDownload('SAMPLE_FILE_ID'));
        Test.stopTest();
    }

    //26. TEST FOR DOWNLOAD FILE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doFileDownloadTest_3(){
        Test.setMock(HttpCalloutMock.class, new BoxDotComMockCallOut());
        Test.startTest();
        BoxDotComMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_BoxDotComController.doFileDownload('SAMPLE_FILE_ID'));
        Test.stopTest();
    }
}