@isTest
private class IC_GoogleDriveControllerTest {
    
    @TestSetup
    private static void makeData(){
        Test.startTest();
        Google_Drive_Integration_Data__c gdi = new Google_Drive_Integration_Data__c();
        gdi.access_Token__c = 'SAMPLE_ACCESS_TOKEN';
        gdi.refresh_token__c = 'SAMPLE_REFRESH_TOKEN';
        gdi.expires_in__c = system.now().addSeconds(3600);
        gdi.user_email__c = UserInfo.getUserEmail();
        database.insert(gdi);
        Google_Drive_Integration_Credentials__c gdic = new Google_Drive_Integration_Credentials__c();
        gdic.client_id__c = 'SAMPLE_CLIENT_ID';
        gdic.client_secret__c = 'SAMPLE_CLIENT_SECRET';
        database.insert(gdic);
        Test.stopTest();
    }

    //1. TEST IF THERE EXISTS VALID DATA IN CUSTOM SETTING OR NOT
    @isTest
    private static void retrieveDataFromCustomSettingTest(){
        Test.startTest();
        //POSITIVE TEST
        system.assertEquals(null,IC_GoogleDriveController.access_token);
        system.assertEquals(null,IC_GoogleDriveController.refresh_token);
        system.assertEquals(null,IC_GoogleDriveController.expires_in);
        String redirect_uri = 'https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/google-drive';
        system.assertEquals(redirect_uri,IC_GoogleDriveController.redirect_uri);
        system.assertEquals(null,IC_GoogleDriveController.gdi);
        system.assertEquals(true,IC_GoogleDriveController.retrieveDataFromCustomSetting());
        system.assertEquals('SAMPLE_ACCESS_TOKEN',IC_GoogleDriveController.access_token);
        system.assertEquals('SAMPLE_REFRESH_TOKEN',IC_GoogleDriveController.refresh_token);
        system.assert(IC_GoogleDriveController.expires_in > system.now());
        system.assert(IC_GoogleDriveController.gdi != null);
        //NEGATIVE TEST
        IC_GoogleDriveController.gdi.expires_in__c = system.now().addSeconds(-3600);
        database.update(IC_GoogleDriveController.gdi);
        system.assertEquals(true,IC_GoogleDriveController.retrieveDataFromCustomSetting());
        system.assertEquals(null,IC_GoogleDriveController.access_token);
        //NO RECORD TEST
        database.delete(IC_GoogleDriveController.gdi);
        system.assertEquals(false,IC_GoogleDriveController.retrieveDataFromCustomSetting());
        Test.stopTest();
    }

    //2. TEST IF THERE EXISTS VALID DATA IN CUSTOM METADATA TYPES OR NOT 
    @isTest
    private static void retrieveDataFromCustomMetadataTest(){
        Test.startTest();
        system.assertEquals(null,IC_GoogleDriveController.clientID);
        system.assertEquals(null,IC_GoogleDriveController.clientSecret);
        system.assertEquals(true,IC_GoogleDriveController.retrieveDataFromCustomMetadata());
        system.assert(IC_GoogleDriveController.clientID != null); // this assertion will fail if you have no data in custom settings
        system.assert(IC_GoogleDriveController.clientSecret != null); // this assertion will fail if you have no data in custom settings
        //IF NO RECORDS
        database.delete([SELECT ID FROM Google_Drive_Integration_Credentials__c]);
        system.assertEquals(false,IC_GoogleDriveController.retrieveDataFromCustomMetadata());
        Test.stopTest();
    }

    //3. TEST FOR AUTH URI RETRIVAL
    @isTest
    private static void doGetAuthURITest(){
        Test.startTest();
        String expectedOutput = '{"'+
                                'status":"authenticateWithCode",'+
                                '"authURI":"https://accounts.google.com/o/oauth2/auth?'+
                                'client_id=SAMPLE_CLIENT_ID&response_type=code&'+
                                'scope=https://www.googleapis.com/auth/drive&'+
                                'redirect_uri=https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/google-drive&'+
                                'state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome&login_hint=jsmith@example.com&'+
                                'access_type=offline"}';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doGetAuthURI());
        //WHEN CREDENTIALS DOES NOT EXISTS
        database.delete([SELECT ID FROM Google_Drive_Integration_Credentials__c]);
        expectedOutput = '{"status":"CUSTOM_METADATA_CREDENTIAL_NOT_FOUND"}';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doGetAuthURI());
        Test.stopTest();
    }

    //4. TEST FOR PROCESS WHEN COMPONENT LOADS FIRST TIME
    @isTest
    private static void doInitApexTest(){
        Test.startTest();
        //IF CUSTOM SETTING CREDENTIALS EXISTS && EXPIRES_IN > SYSTEM.NOW()
        String expectedOutput = '{"status":"readyToLoadData"}';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doInitApex());
        system.assertEquals('SAMPLE_ACCESS_TOKEN', IC_GoogleDriveController.access_token);
        //IF CUSTOM SETTING CREDENTIALS EXISTS && EXPIRES_IN < SYSTEM.NOW() && REFRESH TOKEN != NULL
        IC_GoogleDriveController.gdi.expires_in__c = system.now().addSeconds(-3600);
        update(IC_GoogleDriveController.gdi);
        expectedOutput = '{"status":"getAccessTokenFromRefreshToken"}';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doInitApex());
        //IF CUSTOM SETTING CREDENTIALS EXISTS && EXPIRES_IN < SYSTEM.NOW() && REFRESH TOKEN IS NULL
        IC_GoogleDriveController.gdi.refresh_token__c = null;
        update(IC_GoogleDriveController.gdi);
        expectedOutput = '{"'+
                         'status":"authenticateWithCode",'+
                         '"authURI":"https://accounts.google.com/o/oauth2/auth?'+
                         'client_id=SAMPLE_CLIENT_ID&response_type=code&'+
                         'scope=https://www.googleapis.com/auth/drive&'+
                          'redirect_uri=https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/google-drive&'+
                         'state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome&login_hint=jsmith@example.com&'+
                         'access_type=offline"}';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doInitApex());
        //IF CUSTOM SETTING CREDENTIALS NOT EXISTS
        delete(IC_GoogleDriveController.gdi);
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doInitApex());
        Test.stopTest();
    }

    //5. TEST FOR GETTING ACCESS TOKEN FROM REFRESH TOKEN WHEN ALL CREDENTIALS EXISTS IN CUSTOM SETTINGS 
    @isTest
    private static void doGetAccessTokenFromRefreshTokenTest_1(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        String expectedOutput = 'SUCCESS';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doGetAccessTokenFromRefreshToken());
        system.assertEquals('NEW_ACCESS_TOKEN', IC_GoogleDriveController.gdi.access_token__c);
        Test.stopTest();
    }

    //6. TEST FOR GETTING ACCESS TOKEN FROM REFRESH TOKEN WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doGetAccessTokenFromRefreshTokenTest_2(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        GoogleDriveMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_GoogleDriveController.doGetAccessTokenFromRefreshToken());
        Test.stopTest();
    }

    //7. TEST FOR GETTING ACCESS TOKEN FROM REFRESH TOKEN WHEN CREDENTIAL NOT EXISTS IN DATABASE
    @isTest
    private static void doGetAccessTokenFromRefreshTokenTest_3(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        delete([SELECT Id FROM Google_Drive_Integration_Data__c]);
        delete([SELECT Id FROM Google_Drive_Integration_Credentials__c]);
        Test.startTest();
        system.assertEquals(null, IC_GoogleDriveController.doGetAccessTokenFromRefreshToken());
        Test.stopTest();
    }

    //8. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN NO CLIENT ID AND SECRET ARE PRESENT
    @isTest
    private static void doGetAccessTokenTest_1(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        delete([SELECT Id FROM Google_Drive_Integration_Credentials__c]);
        Test.startTest();
        system.assertEquals(null, IC_GoogleDriveController.doGetAccessToken('SAMPLE_CODE'));
        Test.stopTest();
    }

    //9. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN CLIENT ID AND SECRET ARE PRESENT
    @isTest
    private static void doGetAccessTokenTest_2(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        system.assertEquals('SUCCESS', IC_GoogleDriveController.doGetAccessToken('SAMPLE_CODE'));
        system.assertEquals('NEW_ACCESS_TOKEN', IC_GoogleDriveController.gdi.access_Token__c);
        Test.stopTest();
    }

    //10. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN THERE IS NO EXISTING RECORD OF GOOGLE DRIVE INTEGRATION DATA
    @isTest
    private static void doGetAccessTokenTest_3(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        delete([SELECT Id FROM Google_Drive_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals('SUCCESS', IC_GoogleDriveController.doGetAccessToken('SAMPLE_CODE'));
        system.assert(IC_GoogleDriveController.gdi != null);
        system.assertEquals('NEW_ACCESS_TOKEN', IC_GoogleDriveController.gdi.access_Token__c);
        Test.stopTest();
    }

    //11. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doGetAccessTokenTest_4(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        GoogleDriveMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_GoogleDriveController.doGetAccessToken('SAMPLE_CODE'));
        Test.stopTest();
    }

    //12. TEST FOR FETCH DATA FIRST TIME
    @isTest
    private static void doFetchDataFirstTimeTest(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        //FETCH DATA TEST FOR ROOT FOLDER
        String expectedOutput = '[{'+
                                '"id":"SAMPLE_ID",'+
                                '"name":"SAMPLE_NAME",'+
                                '"type":"SAMPLE_TYPE",'+
                                '"shared":true,'+
                                '"webContentLink":"SAMPLE_WEB_CONTENT_LINK",'+
                                '"isLast":"false"},{'+
                                '"id":"SAMPLE_FOLDER_ID",'+
                                '"name":"SAMPLE_FOLDER_NAME",'+
                                '"type":"application/vnd.google-apps.folder",'+
                                '"shared":null,'+
                                '"webContentLink":"null",'+
                                '"isLast":"false"'+
                                '}]';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doFetchDataFirstTime());
        //FETCH DATA TEST FOR NULL RESPONSE
        GoogleDriveMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_GoogleDriveController.doFetchDataFirstTime());
        Test.stopTest();
    }

    //13. TEST FOR CHANGING DIRECTORY
    @isTest
    private static void doChangeDirectoryTest(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        String expectedOutput = '[{'+
                                '"id":"SAMPLE_ID",'+
                                '"name":"SAMPLE_NAME",'+
                                '"type":"SAMPLE_TYPE",'+
                                '"shared":true,'+
                                '"webContentLink":"SAMPLE_WEB_CONTENT_LINK",'+
                                '"isLast":"false"},{'+
                                '"id":"SAMPLE_FOLDER_ID",'+
                                '"name":"SAMPLE_FOLDER_NAME",'+
                                '"type":"application/vnd.google-apps.folder",'+
                                '"shared":null,'+
                                '"webContentLink":"null",'+
                                '"isLast":"false"'+
                                '}]';
        GoogleDriveMockCallOut.parentId = 'SAMPLE_PARENT_ID';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doChangeDirectory('SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //14. TEST FOR DELETING SELECTED FILE WHEN ALL INTEGRATION DATA RECORDS DOESNOT EXISTS
    @isTest
    private static void deleteSelectedFileTest_1(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        delete([SELECT Id FROM Google_Drive_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals(null, IC_GoogleDriveController.deleteSelectedFile('SAMPLE_FILE_ID'));
        Test.stopTest();
    }

    //15. TEST FOR DELETING SELECTED FILE WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void deleteSelectedFileTest_2(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        GoogleDriveMockCallOut.responseCode = 204;
        system.assertEquals('DELETE_SUCCESSFUL', IC_GoogleDriveController.deleteSelectedFile('SAMPLE_FILE_ID'));
        Test.stopTest();
    }

    //16. TEST FOR DELETING SELECTED FILE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void deleteSelectedFileTest_3(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        GoogleDriveMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_GoogleDriveController.deleteSelectedFile('SAMPLE_FILE_ID'));
        Test.stopTest();
    }

    //17. TEST FOR UPLOAD FILE WHEN ALL INTEGRATION DATA RECORDS DOESNOT EXISTS
    @isTest
    private static void doUploadFileTest_1(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        delete([SELECT Id FROM Google_Drive_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals(null, IC_GoogleDriveController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLE_BASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //18. TEST FOR UPLOAD FILE/CREATE FOLDER WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void doUploadFileTest_2(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        String expectedOutput = '{"name":"SAMPLE_FILE_NAME","id":"SAMPLE_FILE_ID","type":"SAMPLE_FILE_TYPE"}';
        system.assertEquals(expectedOutput, IC_GoogleDriveController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLE_BASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PARENT_ID'));
        Test.stopTest();
    }   

    //19. TEST FOR UPLOAD FILE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doUploadFileTest_3(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        GoogleDriveMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_GoogleDriveController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLE_BASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PARENT_ID'));
        Test.stopTest();
    }
}