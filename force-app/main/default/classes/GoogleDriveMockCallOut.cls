@isTest
global class GoogleDriveMockCallOut implements HttpCalloutMock {
    public static Integer responseCode = 200;
    public static String parentId = 'root';
    global HTTPResponse respond(HTTPRequest req) {
        
        //1. FAKE RESPONSE FOR GET ACCESS TOKEN
        if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://accounts.google.com/o/oauth2/token'){            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"access_token":"NEW_ACCESS_TOKEN","refresh_token":"NEW_REFRESH_TOKEN","expires_in":3600}');
            res.setStatusCode(responseCode);
            return res;

        //2. FAKE RESPONSE FOR FETCH DATA
        } else if (req.getMethod() == 'GET' && req.getEndpoint() == 'https://www.googleapis.com/drive/v2/files?q=%27'+parentId+'%27%20in%20parents'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String body = '{"items":['+
                          '{"id":"SAMPLE_ID",'+
                          '"title":"SAMPLE_NAME",'+
                          '"mimeType":"SAMPLE_TYPE",'+
                          '"shared":true,'+
                          '"webContentLink":"SAMPLE_WEB_CONTENT_LINK"},'+
                          '{"id":"SAMPLE_FOLDER_ID",'+
                          '"title":"SAMPLE_FOLDER_NAME",'+
                          '"mimeType":"application/vnd.google-apps.folder"}'+
                          ']}';
            res.setBody(body);
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR UPLOAD FILE/CREATE FOLDER
        } else if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String body = '{"name":"SAMPLE_FILE_NAME","id":"SAMPLE_FILE_ID","mimeType":"SAMPLE_FILE_TYPE"}';
            res.setBody(body);
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR DELETE FILE
        } else if (req.getMethod() == 'DELETE' && req.getEndpoint() == 'https://www.googleapis.com/drive/v2/files/SAMPLE_FILE_ID'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(responseCode);
            return res;
        }
        return null;
    }
}