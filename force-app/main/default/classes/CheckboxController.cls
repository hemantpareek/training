public class CheckboxController {
    public List<AccountWrapper> wrapperList{get;set;}
    public boolean mastercheckbox {get;set;}
    
    public CheckboxController(){
        wrapperList = new List<AccountWrapper>();
        mastercheckbox = false;
        for(Account acc : [SELECT Id, NAME, TYPE, Industry FROM Account LIMIT 10]){
         	wrapperList.add(new AccountWrapper(acc));   
        }
    }
    
    public class AccountWrapper{
        public boolean isSelected {get;set;}
        public Account record {get;set;}
        public AccountWrapper(Account record){
            this.isSelected = false;
            this.record = record;
        }
    }
}