@isTest
class InboundEmailServiceInsertRecordTest {
    
    @isTest static void insertRecord_singleSobject_SingleField_test(){  
        Test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;  
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
        email.subject = 'Create record from Email';  
        email.plainTextBody = 'account#name#TestAccount';  
        env.fromAddress = 'test@gmail.com';  
        System.assertEquals(0, [SELECT id FROM Account].size());
        Messaging.InboundEmailResult result = new InboundEmailService_InsertRecord().handleInboundEmail(email, env);  
        System.assert(result.success);  
        System.assertEquals(1, [SELECT id FROM Account].size());
        Test.stopTest();
    }  
    
    @isTest static void insertRecord_singleSobject_multiField_test(){  
        Test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;  
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
        email.subject = 'Create record from Email';  
        email.plainTextBody = 'account#name#TestAccount#industry#Electronics#type#prospect#rating#cold';  
        env.fromAddress = 'test@gmail.com';  
        System.assertEquals(0, [SELECT id FROM Account].size());
        Messaging.InboundEmailResult result = new InboundEmailService_InsertRecord().handleInboundEmail(email, env);  
        System.assert(result.success);  
        System.assertEquals(1, [SELECT id FROM Account].size());
        Account a = [SELECT id,name,type,rating,industry FROM Account];
        system.assertEquals('testaccount', a.Name);
        system.assertEquals('Electronics',a.Industry);
        system.assertEquals('Prospect',a.Type);
        system.assertEquals('Cold', a.Rating);
        Test.stopTest();
    }
    
    @isTest static void insertRecord_multiSobject_multiField_test(){  
        Test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;  
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
        email.subject = 'Create record from Email';  
        email.plainTextBody = 'account#name#TestAccount#industry#Electronics#type#prospect#rating#cold'+
            '##contact#lastName#c1#firstname#james#title#contactrecord';  
        env.fromAddress = 'test@gmail.com';  
        System.assertEquals(0, [SELECT id FROM Account].size());  
        System.assertEquals(0, [SELECT id FROM Contact].size()); 
        Messaging.InboundEmailResult result = new InboundEmailService_InsertRecord().handleInboundEmail(email, env); 
        System.assert(result.success);  
        System.assertEquals(1, [SELECT id FROM Account].size());  
        System.assertEquals(1, [SELECT id FROM Contact].size());  
        Account a = [SELECT ID,Name,Industry,Type,Rating FROM Account];
        Contact c = [SELECT LastName,FirstName,title FROM Contact];
        system.assertEquals('testaccount',a.Name);
        system.assertEquals('Prospect',a.Type);
        system.assertEquals('Electronics',a.Industry);
        system.assertEquals('Cold',a.Rating);
        system.assertEquals('c1',c.LastName);
        system.assertEquals('james',c.FirstName);
        system.assertEquals('contactrecord',c.title);
        Test.stopTest();
    }  
    
    @isTest 
    static void invalid_record_test(){  
        Test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;  
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
        email.subject = 'Create record from Email';  
        email.plainTextBody = 'account#name#a1#industry##Contact#lastName#c1';  // industry value is not provided
        env.fromAddress = 'test@gmail.com';  
        Messaging.InboundEmailResult result = new InboundEmailService_InsertRecord().handleInboundEmail(email, env);  
        System.assert(result.success);  
        system.assertEquals(1,[select name,industry from account].size());
        system.assertEquals(null, [select industry from account].industry);
        system.assertEquals(1,[select lastname from contact].size());
        Test.stopTest();
    }  
    
     @isTest 
    static void invalid_fieldName_test(){  
        Test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;  
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
        email.subject = 'Create record from Email';  
        email.plainTextBody = 'account#name#a1#indus#electronics##Contact#lastName#c1';  // indus in a invalid field name from account
        env.fromAddress = 'test@gmail.com';  
        Messaging.InboundEmailResult result = new InboundEmailService_InsertRecord().handleInboundEmail(email, env);  
        System.assert(result.success);  
        system.assertEquals(0,[select name,industry from account].size());
        system.assertEquals(1,[select lastname from contact].size());
        Test.stopTest();
    }  

     @isTest 
    static void invalid_fieldvalue_test(){  
        Test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;  
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
        email.subject = 'Create record from Email';  
        email.plainTextBody = 'account#name#a1#industry#xxxxxxxx##Contact#lastName#c1';  // industry value is invalid
        env.fromAddress = 'test@gmail.com';  
        Messaging.InboundEmailResult result = new InboundEmailService_InsertRecord().handleInboundEmail(email, env);  
        System.assert(result.success);  
        system.assertEquals(1,[select name,industry from account].size());
        system.assertEquals('xxxxxxxx', [select industry from account].industry); // industry is not a restricted picklist 
        system.assertEquals(1,[select lastname from contact].size());
        Test.stopTest();
    }  
    
    @isTest 
    static void invalid_fieldvalueType_test(){  
        Test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;  
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
        email.subject = 'Create record from Email';  
        email.plainTextBody = 'account#name#a1##Contact#lastName#c1#sample_boolean__c#true';  // string value to boolean field
        env.fromAddress = 'test@gmail.com';  
        Messaging.InboundEmailResult result = new InboundEmailService_InsertRecord().handleInboundEmail(email, env);  
        System.assert(result.success);  
        system.assertEquals(1,[select name,industry from account].size());
        system.assertEquals(0,[select lastname from contact].size());   // contact with invalid field value type will not be inserted
        Test.stopTest();
    } 
    
    @isTest 
    static void required_field_missing_test(){  
        Test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;  
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
        email.subject = 'Create record from Email';  
        email.plainTextBody = 'account#type#prospect##Contact#lastName#c1#sample_boolean__c#true';  // string value to boolean field
        env.fromAddress = 'test@gmail.com';  
        Messaging.InboundEmailResult result = new InboundEmailService_InsertRecord().handleInboundEmail(email, env);  
        System.assert(result.success);  
        system.assertEquals(0,[select name,industry from account].size()); // account can't be inserted without name
        system.assertEquals(0,[select lastname from contact].size());   
        Test.stopTest();
    } 
}