global class GoogleDriveService {

    //1. REDIRECTS TO AUTHENTICATION PAGE
    public static String authenticate(String client_id, String redirect_uri){
    return  'https://accounts.google.com/o/oauth2/auth?'+
            'client_id='+EncodingUtil.urlEncode(client_id,'UTF-8')+
            '&response_type=code'+
            '&scope=https://www.googleapis.com/auth/drive'+
            '&redirect_uri='+redirect_uri+
            '&state=security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome'+
            '&login_hint=jsmith@example.com&'+
            'access_type=offline';
    }

    //2. RETRIEVE/UPLOAD DATA FROM/TO GOOGLE DRIVE ACCORDING TO PARAMETERS RECEIVED
    public static Object accessDrive(Map<String,String> parameters){
        HttpRequest req = new HttpRequest();//Getting access token from google
        if(parameters.get('setMethod') != null){
            req.setMethod(parameters.get('setMethod'));
        }
        if(parameters.get('setEndpoint') != null){
            req.setEndpoint(parameters.get('setEndpoint'));
        }
        if(parameters.get('content-type') != null){
            req.setHeader('content-type',parameters.get('content-type'));
        }
        if(parameters.get('Authorization') != null){
            req.setHeader('Authorization',parameters.get('Authorization'));
        }
        if(parameters.get('content-length') != null){
            req.setHeader('content-length',parameters.get('content-length'));
        }
        if(parameters.get('setBody') != null){
            req.setBody(parameters.get('setBody'));
        }
        if(parameters.get('setTimeout') != null){
            req.setTimeout(Integer.valueOf(parameters.get('setTimeout')));
        }

        system.debug('method  ==>> '+req.getMethod());
        system.debug('endpoint  ==>> '+req.getEndpoint());
        system.debug('content type ==>> '+req.getHeader('content-type'));
        system.debug('authorization ==>> '+req.getHeader('Authorization'));
        system.debug('contant-length ==>> '+req.getHeader('content-length'));
        system.debug('body ==>> '+req.getBody());

        Http h = new Http();        
        HttpResponse res = h.send(req);

        system.debug(res.getStatusCode());
        system.debug(res.getBody());
        
        Integer statusCode = res.getStatusCode();
        if(statusCode == 200){
            String request_type = parameters.get('request_type');
            if(request_type == 'getAccessToken'){
                return parseAuthResponse(res.getBody());
            }else if(request_type == 'fileUpload'){
                return parseFileResponse(res.getBody());
            }else if(request_type == 'fetchData'){
                return parseFetchDataResponse(res.getBody());
            }
        }else if(statusCode == 204){
            return 'DELETE_SUCCESSFUL';
        }
        return null;        
    }
    
    //3. PARSES AUTH JSON RESPONSE
    public static Object parseAuthResponse(String json) {
        return System.JSON.deserialize(json, GoogleDriveWrapper.AccessDrive.class);
	}

    //4. PARSES FETCH DATA RESPONSE
    public static Object parseFetchDataResponse(String json) {
        return System.JSON.deserialize(json, GoogleDriveWrapper.class);
	}

    //5. PARSES FILE RESPONSE
    public static Object parseFileResponse(String json) {
        return System.JSON.deserialize(json, GoogleDriveWrapper.File.class);
	}
}