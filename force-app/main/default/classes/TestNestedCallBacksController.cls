public class TestNestedCallBacksController {
	@auraenabled
    public static String actionA_Apex(){
        return 'response from actionA_Apex';
    }
    
    @auraenabled
    public static String actionB_Apex(){
        return 'response from actionB_Apex';
    }
    
    @auraenabled
    public static String actionC_Apex(){
        return 'response from actionC_Apex';
    }
    
    @auraenabled
    public static String actionD_Apex(){
        return 'response from actionD_Apex';
    }
    
    @auraenabled
    public static String actionE_Apex(){
        return 'response from actionE_Apex';
    }
}