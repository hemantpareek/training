public class KanbanViewController {
    //------------------------------------------------------WRAPPER TO HOLD OBJECT API NAME WITH ITS LABEL---------------------------------------------------------
    public class SObjectTypeWrapper implements Comparable { //WORKS FOR BOTH SOBJECTS AND SOBJECT FIELDS
        public String value;
        public String label;
        public String pickListValues;
        boolean isNillable;

        public SObjectTypeWrapper(String value, String label){
            this.value = value;
            this.label = label;
        }

        public SObjectTypeWrapper(String value, String label, String pickListValues, boolean isNillable){
            this.value = value;
            this.label = label;
            this.pickListValues = pickListValues;
            this.isNillable = isNillable;
        }

        public Integer compareTo(Object compareTo) {
            SObjectTypeWrapper sow = (SObjectTypeWrapper)compareTo;
            if(label > sow.label){
                return 1;
            }else if(label < sow.label){
                return -1;
            }else{
                return 0;
            }
        }
    }
    //--------------------------------------------------------------------- METHODS ---------------------------------------------------------------------------------
    //1. FETCHS SOBJECT LIST FROM OBJECT SCHEMA
    @AuraEnabled
    public static String doGetObjectList(){
        Map<String, Schema.sObjectType> schemaMap = Schema.getGlobalDescribe();
        List<SObjectTypeWrapper> objList = new List<SObjectTypeWrapper>();
        objList.add(new SobjectTypeWrapper('none','-None-'));
        List<SObjectTypeWrapper> result = new List<SObjectTypeWrapper>();
        for(String value : schemaMap.keySet()){
            DescribeSObjectResult dsr = schemaMap.get(value).getDescribe();
            if(dsr.isCreateable() && dsr.isDeletable() && dsr.isUpdateable()){
                result.add(new SObjectTypeWrapper(value, dsr.getLabel())); 
            }
        }
        result.sort();
        objList.addAll(result); //Map<value,label>
        return JSON.serialize(objList);
    }

    //2. FETCHS SOBJECT FIELDS LIST FROM SOBJECT SCHEMA
    @AuraEnabled
    public static string doGetPickListFieldsWithValues(String sObjectvalue){
        Map<String, Schema.sObjectField> fieldMap = Schema.getGlobalDescribe().get(sObjectvalue).getDescribe().fields.getMap();        
        List<SObjectTypeWrapper> fieldList = new List<SObjectTypeWrapper>();
        for(String value : fieldMap.keySet()){
            DescribeFieldResult dfr = fieldMap.get(value).getDescribe();
            if(dfr.isAccessible() && dfr.getType() == Schema.DisplayType.PICKLIST && dfr.isUpdateable()){
                List<Schema.PicklistEntry> pickListEntries = dfr.getPicklistValues();
                String pickListValues = '';
                for(Schema.PicklistEntry pickListEntry : pickListEntries){
                    pickListValues += pickListEntry.getValue() + ',';
                }
                pickListValues = pickListValues.removeEnd(',');
                fieldList.add(new SObjectTypeWrapper(value, dfr.getLabel(), pickListValues, dfr.isNillable()));
            }
        }
        fieldList.sort(); //Map<value,label>
        return JSON.serialize(fieldList);
    }

    @AuraEnabled
    public static string doGetRecords(String objectName, String picklistFieldName){
        String nameField = 'name';
        if(objectName == 'case'){
            nameField = 'caseNumber';
        }
        String fields = 'id,' + nameField + ',' + picklistFieldName;
        String query = 'SELECT ' + fields + ' FROM ' + objectName + ' LIMIT 10000';
        List<Sobject> records = Database.query(query);
        String result = '[';
        for(Sobject sobj : records){
            result += '{';
            for(String fld : fields.split(',')){
                result += '"' + fld + '":"' + sobj.get(fld) + '",';
            }
            result = result.removeEnd(',');
            result += '},';
        }
        result = result.removeEnd(',');
        result += ']';
        return result;
    }

    @AuraEnabled
    public static string updateRecord(String objectName, String recordId, String pickListfield, String pickListFieldValve){
        String query = 'SELECT ' + pickListfield + ' FROM ' + objectName + ' WHERE ID = \'' + recordId + '\'';
        Sobject rcdToUpdate = Database.query(query)[0];
        rcdToUpdate.put(pickListfield, pickListFieldValve);
        Database.SaveResult res = Database.update(rcdToUpdate,false);
        if(res.isSuccess()){
            return 'true';
        }else{
            return 'false';
        }
    }
}