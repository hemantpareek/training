public class HitAPIonContactChange {
    public static Integer count = 0;
    /*
	@InvocableMethod
    public static void func(List<ID> records){
        count++;
        System.debug('Called FROM Process Builder => '+ count + ' => ' + records);
    }
    */
    @InvocableMethod
    public static void func(List<Contact> records){
        count++;
        System.debug('Called FROM Process Builder => '+ count + ' => ' + records.size());
        for(Contact con : records){
            system.debug(con);
        }
    }
    
}