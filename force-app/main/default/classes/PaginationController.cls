//******************************* PAGINATION VISUALFORCE PAGE CONTROLLER *****************************************
public class PaginationController {
    
    //************************************** F I E L D S *******************************************************
    Map<String, Schema.sObjectType> schemaMap;          //map to hold results given by Schema.getGlobalDescribe
    Map <String, Schema.SObjectField> fieldMap;         //map to hold results given by sObjectResult.fields.getMap
    public List<SelectOption> objectList{get; set;}     //holds list of objects to be displayed on UI picklist
    public List<SelectOption> fieldList{get; set;}      //holds the default list of fields to be displayed on object selection
    public List<SelectOption> SelectedfieldList{get; set;} //holds fields that a selected from fieldList
    public String selectedObject{get; set;}             //holds the object selected from objectlist
    public String[] fieldsToAdd{get; set;}              //holds fields to be added to selectedFieldsList
    public String[] fieldsToRemove{get; set;}           //holds fields to be removed from selectedFieldsList
    public String[] fieldsToDisplay{get; set;}          //holds fields to be processed to vf component
    public String objectToDisplay{get; set;}            //holds object to be displayed on vf component
    public boolean renderComponent{get;set;}            //decides if component will render or not
    public String keyPrefixProcessed{get;set;}          //holds the process sobject key prefix
    public String nameField{get;set;}                   //holds processed nameField 
    
    //********************************** C O N S T R U C T O R ***************************************************
    public PaginationController(){
        initObjLst();
        keyPrefixProcessed='001';
        fieldsToDisplay = new List<String>();
    }
    
    //************************************* M E T H O D S *********************************************************
    //1.  INITIALIZES OBJECT LIST FROM SCHEMA CLASS ALSO APPLY FILTERS ON IT.
    public void initObjLst(){
        objectList = new List<SelectOption>();
        objectList.add(new SelectOption('-none-','-none-',true));
        schemaMap = Schema.getGlobalDescribe();
        List<String> objectApiNames = new List<String>(schemaMap.keySet());
        objectApiNames = removeUnSupportedObjects(objectApiNames); // remove unsupported objects with setCtrl
        Map<String,String> objectLabelsWithValues = new Map<String,String>();   //label,value
        for(String objectApiName : objectApiNames){
            DescribeSObjectResult dsr = schemaMap.get(objectApiName).getDescribe();
            if(dsr.isCreateable() && dsr.isDeletable() && dsr.isUpdateable()){
                objectLabelsWithValues.put(dsr.getLabel(),objectApiName);
            }
        }
        List<String> objectLabels = new List<String>(objectLabelsWithValues.keySet());
        objectLabels.sort();
        for(String objectLabel : objectLabels){
            ObjectList.add(new SelectOption(objectLabelsWithValues.get(objectLabel),objectLabel));
        }
        selectedObject=ObjectList[0].getValue();
        initFldlst();
    }
    
    //2. REMOVED OBJECTS WHICH ARE NOT SUPPORTED BY STDSETCONTROLLER
    public List<String> removeUnSupportedObjects(List<String> objectApiNames){
        List<String> supportedStdObjects = new List<String>{'account','asset','campaign','case','contact','contract','idea','lead',
            'opportunity','order','product2','solution','user'};
                List<String> customObjects = new List<String>();
        for(String objectApiName : objectApiNames){
            if(objectApiName.endsWithIgnoreCase('__c')){
                customObjects.add(objectApiName);
            }
        }        
        List<String> supportedObjectList = new List<String>();
        supportedObjectList.addAll(supportedStdObjects);
        supportedObjectList.addAll(customObjects);
        supportedObjectList.sort();
        return supportedObjectList;    
    }
    
    //3. INITIALIZES FIELDLIST BASED UPON SELECTED OBJECT
    public void initFldLst(){  
        fieldList = new List<SelectOption>();
        if(selectedObject!='-none-'){
            Schema.DescribeSObjectResult sObjectResult = schemaMap.get(selectedObject).getDescribe();        
            keyPrefixProcessed = sObjectResult.getKeyPrefix();
            fieldMap = sObjectResult.fields.getMap();    
            List<String> fieldApiNames = new List<String>(fieldMap.KeySet());
            Map<String,String> fieldLabelsWithValues = new Map<String,String>();   //label,value
            for(String fieldApiName : fieldApiNames){
                DescribeFieldResult dfr = fieldMap.get(fieldApiName).getDescribe();
                if(dfr.isNameField()){
                    nameField = fieldApiName;
                }
                if(dfr.isAccessible() && dfr.isSortable()){
                    fieldLabelsWithValues.put(dfr.getLabel(),fieldApiName);
                }
            }
            List<String> fieldLabels = new List<String>(fieldLabelsWithValues.keySet());
            fieldLabels.sort();
            for(String fieldLabel : fieldLabels){
                fieldList.add(new SelectOption(fieldLabelsWithValues.get(fieldLabel),fieldLabel));
            }
            SelectedfieldList= new List<SelectOption>();
        }
    }
    
    //4. ADDS FIELDS TO SELELCTEDFIELDLIST AND REMOVES FROM FIELDLIST
    public void addToSelectedFldLst(){
        for(String fieldToAdd : fieldsToAdd){
            if(SelectedfieldList.size()==10){
                break;
            }
            SelectOption so = new SelectOption(fieldToAdd,fieldMap.get(fieldToAdd).getDescribe().getLabel());
            SelectedfieldList.add(so);
            fieldList.remove(fieldList.indexOf(so));
        }
        fieldsToAdd.clear();
    }
    
    //5. ADDS FIELDS TO FIELDLIST AND REMOVES FROM SELECTEDFIELDS LIST
    public void removeFromSelectedFldLst(){
        for(String fieldToRemove : fieldsToRemove){
            SelectOption so = new SelectOption(fieldToRemove,fieldMap.get(fieldToRemove).getDescribe().getLabel());
            fieldList.add(so);
            SelectedfieldList.remove(selectedfieldList.indexOf(so));
        }
        fieldList.sort();
        fieldsToRemove.clear();
    }
    
    /*6. FUNCTION TO INITILIZE OBJECTTODISPLAY AND FIELDSTODISPLAY ALSO CALLS INIT() OF COMPONENT WHICH INITIATES SETCTRL
         PROCESSING BASED ON SELECEDOBJECT AND FIELDS PROCESSED USING ACTION FUNCTION ON COMPONENT. */
    public void processRecords(){
        fieldsToDisplay.clear();
        objectToDisplay = selectedObject;
        if(!selectedfieldList.isEmpty()){
            for(SelectOption fieldToDisplay : SelectedfieldList){
                fieldsToDisplay.add(fieldToDisplay.getValue());
            }
        }   
        renderComponent=true;
    }
    
    //7. PERFORM OPERATIONS WHEN A OBJECT CHANGES
    public void objectChangeAction(){
        renderComponent=false;
        initFldLst();
    }
}