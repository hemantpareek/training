global class NotesAndAttachmentsController {
    
    @RemoteAction
    global static void createAttachment(Blob fileBody, String fileName, String parentId){
        Attachment attach=new Attachment();
        attach.Body=fileBody;
        attach.Name=fileName;
        attach.ParentID=parentId;
        insert attach;
    }
}