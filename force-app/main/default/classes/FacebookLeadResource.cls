@RestResource(urlMapping='/FacebookLeadResource/*') //endpoint => (SFDC base url)/services/apexrest/className
global class FacebookLeadResource {
    
    @HttpGet // called when a get request is received
    global static Integer doGet(){
        RestRequest req = RestContext.request;
        system.debug('@@@@Getreq:  ' + req);
        Integer startIndex = (''+req).indexOf('hub.challenge=') + 'hub.challenge='.length();
        Integer endIndex = (''+req).indexOf(',', startIndex);
        String verifyToken = (''+req).substring( startIndex , endIndex );
        RestResponse res = RestContext.response;
        system.debug('@@@@res:  ' + res);
        system.debug('@@@@res:  ' + verifyToken);
        return Integer.valueOf(verifyToken);
    }
    
    @HttpPost // called when a post request is received
    global static void doPost(){
        RestRequest req = RestContext.request;
        system.debug('@@@@Postreq:  ' + req);
        system.debug('############# reqbody : ' + req.requestBody.toString());
        /*Attachment attachment = new Attachment();
        attachment.Body = 
        attachment.Name = String.valueOf('Lead_Details_'+system.now()+'.txt');
        attachment.ParentId = '0012v00002lkX50'; 
        insert attachment;*/
    }
}