@RestResource(urlMapping='/FacebookMessangerResource/*') //endpoint => (SFDC base url)/services/apexrest/className
global class FacebookMessangerResource {

    static String pageAccessToken = 'EAAUpiIiiEvMBABvJEUD2ADYfTZC9hy6InKGyZBlIvxQjI5cIi8rvZAUt2blw3XZBC2mox63KEge58sWljYjxTmMNnT4yba9TgHx3KZB3ZBVUR7wya2JiKzZBewzVy7NVmpc8kgrrpBZAO66eo00jhMUQL2vtGPOgljlM2Eh3vPEf0ZAZCWXgvLd6Ri';
    
    @HttpPost // called when a post request is sent
    global static void doPost(){
        RestRequest req = RestContext.request;
        system.debug('@@@@req:  ' + req);
        RestResponse res = RestContext.response;
        system.debug('@@@@res:  ' + res);
        String body = req.requestBody.toString();
        system.debug('@@@@body : ' + body);
        createPageRecord(body);
    }
    
    @HttpGet // called when a post request is received
    global static Integer doGet(){
        RestRequest req = RestContext.request;
        system.debug('@@@@req:  ' + req);
        Integer startIndex = (''+req).indexOf('hub.challenge=') + 'hub.challenge='.length();
        Integer endIndex = (''+req).indexOf(',', startIndex);
        String verifyToken = (''+req).substring( startIndex , endIndex );
        RestResponse res = RestContext.response;
        system.debug('@@@@res:  ' + res);
        system.debug('@@@@res:  ' + verifyToken);
        return Integer.valueOf(verifyToken);
    }
    
    public static void createPageRecord(String body){
        psidWrapper pw = (psidWrapper)parsePsid(body);
        String pageId = pw.entry[0].id;
        String pageRecordId;
        List<Facebook_Pages__c> fbpLst = [Select Name, page_id__c FROM Facebook_Pages__c WHERE page_id__c = :pageId];
        if(fbpLst.isEmpty()){
            //Hit for getting page Name
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint('https://graph.facebook.com/v5.0/' + pageId + '?fields=id,name&access_token=' + pageAccessToken);
            http h = new http();
            HttpResponse res = h.send(req);
            if(res.getStatusCode() == 200){
                fbpage pg = (fbpage) parsePage(res.getBody());
                Facebook_Pages__c fbp = new Facebook_Pages__c(name = pg.name , page_id__c = pg.id);
                insert fbp;
                pageRecordId = fbp.id;
            }
        }else{
            pageRecordId = fbpLst[0].id;
        }
        createUserRecord(body, pageRecordId);
    }

    @future(callout=true)
    public static void createUserRecord(String body, String pageRecordId){
        psidWrapper pw = (psidWrapper)parsePsid(body);
        String psid = pw.entry[0].messaging[0].sender.id;
        String userRecordId;
        List<Facebook_Messanger_User__c> fmu = [Select Name, psid__c FROM Facebook_Messanger_User__c WHERE psid__c = :psid LIMIT 1];
        if(fmu.isEmpty()){
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint('https://graph.facebook.com/v5.0/' + psid + '?fields=id,name&access_token=' + pageAccessToken);
            http h = new http();
            HttpResponse res = h.send(req);
            if(res.getStatusCode() == 200){
                user u = (user) parseUser(res.getBody());
                Facebook_Messanger_User__c fbu =  new Facebook_Messanger_User__c(name = u.name ,psid__c = u.id, Facebook_Page__c = pageRecordId);   
                insert fbu;
                userRecordId = fbu.id;
            }
        }else{
            userRecordId = fmu[0].id;
        }
        createMessageRecord(userRecordId, body);
    }

    public static void createMessageRecord(String userRecordId, String body){
        psidWrapper pw = (psidWrapper)parsePsid(body);
        String msgId = pw.entry[0].messaging[0].message.mid;
        String text = pw.entry[0].messaging[0].message.text;
        insert new Facebook_Messages__c(Facebook_User__c = userRecordId, Message_Id__c = msgId, Message__c = text);
    }

    //wrapper to parse psid
    public class psidWrapper{
        public list<entry> entry;
    }

    public user user;

    //wrapper to parse user
    public class user{
        public string name;
        public string id;
    }
    
    public static Object parseUser(String jsonString){
        return System.JSON.deserialize(jsonString, FacebookMessangerResource.user.class);
    }
    
    //wrapper class to parse psid
    
    public static Object parsePsid(String jsonString){
        return System.JSON.deserialize(jsonString, FacebookMessangerResource.psidWrapper.class);
    }

    //parse page
    public static Object parsePage(String jsonString){
        return System.JSON.deserialize(jsonString, FacebookMessangerResource.fbpage.class);
    }
    
    public class entry{
        public string id;
        public list<messaging> messaging;
    }
    
    public class messaging{
        public sender sender;
        public recipient recipient;
        public message message;
    }
    
    public class sender{
        public string id;
    }
    
    public class recipient{
        public string id;
    }
    
    public class message{
        public string mid;
        public string text;
    }

    public class fbpage{
        public string id;
        public string name;
    }
}