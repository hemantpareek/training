@isTest
public class TriggerSequenceTestingControllerTest {
	@isTest
    private static void validate(){
        Test.startTest();
        TriggerSequenceTestingController ctrl = new TriggerSequenceTestingController();
        ctrl.init();
        ctrl.generateParentIds();
        ctrl.toggleEditMode();
        ctrl.setTriggerFire();
        ctrl.prepareWrapperList();
        ctrl.resetAll();
        ctrl.deletePermanently();
        ctrl.save();
        ctrl.deleteSeleted();
        ctrl.undeleteAll();
        try{
            ctrl.deleteSingle();
        }catch(Exception e){}
        ctrl.bulkInsert();
        ctrl.singleInsert();
        ctrl.emptyRecycleBin();
        ctrl.retrieveContacts();
        Test.stopTest();
    }
    
    @isTest
    private static void validate2(){
        Test.startTest();
		Account acc1 = new Account(name='Parent A');
		Account acc2 = new Account(name='Parent B');
		insert acc1;
		insert acc2;
        List<Contact> conToInsert = new List<Contact>();
        for(integer i=1; i<=20; i++){
            if(i<=10){
                conToInsert.add(new Contact(lastName='c'+i, accountId=acc1.id));
            }else{
                conToInsert.add(new Contact(lastName='c'+i, accountId=acc2.id));
            }
        }        
        insert conToInsert;
        TriggerSequenceTestingController ctrl = new TriggerSequenceTestingController();
        ctrl.init();
        ctrl.generateParentIds();
        ctrl.toggleEditMode();
        ctrl.setTriggerFire();
        ctrl.prepareWrapperList();
        ctrl.resetAll();
        ctrl.deletePermanently();
        ctrl.save();
        ctrl.deleteSeleted();
        ctrl.undeleteAll();
        try{
            ctrl.deleteSingle();
        }catch(Exception e){}
        ctrl.bulkInsert();
        ctrl.singleInsert();
        ctrl.emptyRecycleBin();
        ctrl.retrieveContacts();
        Test.stopTest();
    }
}