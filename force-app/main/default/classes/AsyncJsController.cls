public with sharing class AsyncJsController {
    
    @AuraEnabled
    public static string m1(){
        return 'response from m1';
    }

    @AuraEnabled
    public static string m2(){
        return 'response from m2';
    }

    @AuraEnabled
    public static string m3(){
        return 'response from m3';
    }

    @AuraEnabled
    public static string m4(){
        // String abc = null;
        // abc.capitalize();
        return 'response from m4';
    }

    @AuraEnabled
    public static string m5(){
        return 'response from m5';
    }

    @AuraEnabled
    public static string m6(String param){
        return 'response from m6' + param;
    }

    @AuraEnabled
    public static string m7(String param){
        return 'response from m7' + param;
    }

    @AuraEnabled
    public static string m8(String param){
        return 'response from m8' + param;
    }

    @AuraEnabled
    public static string m9(String param){
        return 'response from m9' + param;
    }

    @AuraEnabled
    public static string m10(String param){
        return 'response from m10' + param;
    }
}