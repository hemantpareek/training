@isTest
global class DropBoxMockCallOut implements HttpCalloutMock {
    public static Integer responseCode = 200;
    global HTTPResponse respond(HTTPRequest req) {
        
        //1. FAKE RESPONSE FOR GET ACCESS TOKEN
        if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://api.dropbox.com/1/oauth2/token'){            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"access_token":"NEW_ACCESS_TOKEN"}');
            res.setStatusCode(responseCode);
            return res;

        //2. FAKE RESPONSE FOR FETCH DATA
        } else if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://api.dropboxapi.com/2/files/list_folder'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String body = '{"entries":['+
                          '{"id":"SAMPLE_ID",'+
                          '"name":"SAMPLE_NAME",'+
                          '"type":"SAMPLE_TYPE",'+
                          '"path_display":""'+
                          '}]}';
            res.setBody(body);
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR UPLOAD FILE
        } else if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://content.dropboxapi.com/2/files/upload'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String body = '{"name":"SAMPLE_FILE_NAME","path_display":"SAMPLE_PATH_DISPLAY"}';
            res.setBody(body);
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR DELETE FILE
        } else if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://api.dropboxapi.com/2/files/delete_v2'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR DOWNLOAD FILE
        } else if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://api.dropboxapi.com/2/files/get_temporary_link'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(responseCode);
            res.setBody('{"link":"SAMPLE_FILEDOWNLOAD_LINK"}');
            return res;

        //FAKE RESPONSE FOR CRETAE FOLDER
        } else if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://api.dropboxapi.com/2/files/create_folder_v2'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(responseCode);
            res.setBody('{"metadata":{"path_display":"SAMPLE_CREATED_FOLDER_PATH"}}');
            return res;
        }
        return null;
    }
}