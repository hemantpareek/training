public class GoogleDriveIntegrationController {

    @testVisible String access_token;                                       //variable to store access token
    @testVisible String refresh_token;                                      //variable to store refresh token
    @testVisible Google_Drive_Integration_Data__c gdi;                      //instance of custom object in org that will contain access token,refresh token and expiry time
    public GoogleDriveWrapper.File currentFolder{get;set;}                  //this varible will hold the folder that is currently open
    public List<GoogleDriveWrapper.File> currentFolderDirectories{get;set;} //holds folders contained by current folder
    public List<GoogleDriveWrapper.File> currentFolderFiles{get;set;}       //holds files contained by current folder
    public List<GoogleDriveWrapper.File> breadCrumbsList{get;set;}           //this list holds the list of breadcrumbs (folder ids)
    public boolean isAuthenticated{get;set;}                                //once autheticated then this variable's value will set to true
    public GoogleDriveWrapper.FileUpload fileToupload{get;set;}             //holds the parameters of file to uploded to google drive
    @testVisible String client_id;                                          // this will hold the client id;
    @testVisible String client_secret;                                      //this will hold the client secret
    @testVisible String redirect_uri = 'https://c.ap15.visual.force.com/apex/GoogleDriveIntegration?sfdc.tabName=01r2v000000iZF2';
    public DateTime expires_in{get;set;}

    //constructor
    public GoogleDriveIntegrationController () {
        isAuthenticated = false;
        breadCrumbsList = new List<GoogleDriveWrapper.File>();
        fileToupload = new GoogleDriveWrapper.FileUpload();
        // retrieval of client id and client secret from custom metadatatype
        Integration__mdt i = [SELECT client_id__c , client_secret__c FROM Integration__mdt WHERE developerName = 'GoogleDriveIntegration']; 
        client_id = i.client_id__c;
        client_secret = i.client_secret__c;
        currentFolder = new GoogleDriveWrapper.File('root','Home');
    }
    
    //1. method to create google drive data (access token, refresh token) record if there is not any
    public PageReference pageAction(){
        List<Google_Drive_Integration_Data__c> gdiLst = [SELECT access_Token__c , refresh_token__c , expires_in__c FROM Google_Drive_Integration_Data__c 
                                                        WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(!gdiLst.isEmpty()){
                gdi = gdiLst[0];
                refresh_token = gdi.refresh_token__c;
                expires_in = gdi.expires_in__c;
                if(gdi.expires_in__c > System.now()){
                    access_token = gdi.access_Token__c;
                    fetchData(currentFolder);
                    isAuthenticated = true;
                } else {
                    if(refresh_token != null){
                        getAccessTokenFromRefreshToken(); //called when access token is expired
                    }else{
                        Map<String,String> res = getAccessToken();
                        gdi.access_Token__c = res.get('access_token');
                        gdi.expires_in__c = System.now().addSeconds(Integer.valueOf(res.get('expires_in')));
                        update gdi;
                    }
                    return new PageReference(ApexPages.CurrentPage().getUrl());
                }
            } else {
                 String code = ApexPages.CurrentPage().getParameters().get('code');
                if(code != null && code != ''){ //if code is null of empty then no further processing will take place
                    Map<String,String> res = getAccessToken();
                    if(res == null){
                        return null;
                    }
                    gdi = new Google_Drive_Integration_Data__c();
                    gdi.user_email__c = UserInfo.getUserEmail();
                    gdi.SetupOwnerId = UserInfo.getUserId();
                    gdi.access_Token__c = res.get('access_token');
                    gdi.refresh_token__c = res.get('refresh_token');
                    gdi.expires_in__c = System.now().addSeconds(Integer.valueOf(res.get('expires_in')));
                    insert gdi;
                    return new PageReference(ApexPages.CurrentPage().getUrl());
                } else {
                    return new Pagereference(GoogleDriveService.authenticate(client_id, redirect_uri));
                }
            }
        return null;
    }

    //3. method to get access token from refresh token when access token is expired
    public void getAccessTokenFromRefreshToken(){
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://accounts.google.com/o/oauth2/token');
        String body = 'refresh_token='+refresh_token+
                      '&client_id='+client_id+
                      '&client_secret='+client_secret+
                      '&grant_type=refresh_token';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('setTimeout','60000');
        GoogleDriveWrapper.AccessDrive authResponse = (GoogleDriveWrapper.AccessDrive)GoogleDriveService.accessDrive(parameters);
        if(authResponse == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'null response received : '+'getAccessTokenFromRefreshToken'));
            return;
        }
        gdi.access_Token__c = authResponse.access_token;
        gdi.expires_in__c = System.now().addSeconds(authResponse.expires_in);
        update gdi;
    }

    //4. function to get access token first time when there is no refresh token available
    public Map<String,String> getAccessToken(){
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://accounts.google.com/o/oauth2/token');
        String body = 'code='+ApexPages.CurrentPage().getParameters().get('code')+
                      '&client_id='+client_id+
                      '&client_secret='+client_secret+
                      '&redirect_uri='+redirect_uri+
                      '&grant_type=authorization_code';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('setTimeout','60000');
        Map<String,String> res = new Map<String,String>();
        GoogleDriveWrapper.AccessDrive authResponse = (GoogleDriveWrapper.AccessDrive)GoogleDriveService.accessDrive(parameters);
        if(authResponse == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'null response received : '+'getAccessToken'));
            return null;
        }
        res.put('access_token',authResponse.access_token);
        res.put('refresh_token',authResponse.refresh_token);
        res.put('expires_in',String.valueOf(authResponse.expires_in));
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'Access token retrieved'));
        return res;
    }


    //7. function to fetch files and folders from google drive from curretly selected folder
    public void fetchData(GoogleDriveWrapper.File fldr){  
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','GET');
        parameters.put('setEndpoint','https://www.googleapis.com/drive/v2/files?q=%27'+fldr.id+'%27%20in%20parents'); //End point for List of Files in Folder
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('request_type','fetchData');
        parameters.put('setTimeout','60000');
        GoogleDriveWrapper fetchDataResponse = (GoogleDriveWrapper)GoogleDriveService.accessDrive(parameters);
        if(fetchDataResponse == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'null response received : '+'fetchData'));
            return;
        }
        currentFolderDirectories = new List<GoogleDriveWrapper.File>();
        currentFolderFiles = new List<GoogleDriveWrapper.File>();
        for(GoogleDriveWrapper.File f : fetchDataResponse.items){
            if(f.mimeType == 'application/vnd.google-apps.folder'){
                currentFolderDirectories.add(f);
            }else{
                currentFolderFiles.add(f);
            }
        }
        fldr.isLast = true;
        currentFolder = fldr;
        addToBreadCrumbsList(fldr);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'FetchData Successful : '+currentFolder.title));
    }

    //8. function to manage and add folders to breadCrumb list
    public void addToBreadCrumbsList(GoogleDriveWrapper.File fldr){
        for(GoogleDriveWrapper.File breadCrumb : breadCrumbsList){
            breadCrumb.isLast = false;
        }
        if(!breadCrumbsList.contains(fldr)){
            breadCrumbsList.add(fldr);
        }
    } 

    //9. function to goto selected folder on breadcrumb and update list in accordance with that action
    public void performBreadCrumbAction(){
        String folderId = ApexPages.CurrentPage().getParameters().get('folderId');
        String folderName = ApexPages.CurrentPage().getParameters().get('folderName');
        GoogleDriveWrapper.File fldr = new GoogleDriveWrapper.File(folderId, folderName);
        Integer selectedFolderIndex = breadCrumbsList.indexOf(fldr);    // for this statement we have to override equals method in wrapper class
        List<GoogleDriveWrapper.File> tempList = new List<GoogleDriveWrapper.File>();
        for(Integer i=0; i<selectedFolderIndex; i++){
            breadCrumbsList[i].isLast = false;
            tempList.add(breadCrumbsList[i]);
        }
        breadCrumbsList = tempList;
        fldr.isLast = true;
        fetchData(fldr);
    }

    //10. function to open selected directory on UI
    public void changeCurrentDirectory(){
        String folderId = ApexPages.CurrentPage().getParameters().get('selectedFolderId');
        String folderName = ApexPages.CurrentPage().getParameters().get('selectedFolderName');
        fetchData(new GoogleDriveWrapper.File(folderId, folderName));
    }

    //14. function to upload file to google drive
    public void UploadFile(){
        if(fileToupload != null && fileToupload.body != null && fileToupload.name != null){
            Map<String,String> Parameters = new Map<String,String>();
            String boundary = '----------9889464542212';
            String delimiter = '\r\n--' + boundary + '\r\n';
            String close_delim = '\r\n--' + boundary + '--';
            String bodyEncoded = EncodingUtil.base64Encode(fileToupload.body);
            String folder_id = currentFolder.id;
            String body = delimiter + 
                        'Content-Type: application/json\r\n\r\n' + 
                        '{ "name" : "' + fileToupload.name + '",' + 
                        ' "mimeType" : "' + fileToupload.type + '",'+
                        '"parents":["'+folder_Id+'"] }' + delimiter +      //this parents format is used for v3
                        'Content-Type: ' + fileToupload.type + '\r\n' + 
                        'Content-Transfer-Encoding: base64\r\n' + '\r\n' + 
                        bodyEncoded + 
                        close_delim;
            parameters.put('setMethod','POST');
            parameters.put('setBody',body);
            parameters.put('setEndpoint','https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart');
            parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
            parameters.put('content-type', 'multipart/mixed; boundary="' + boundary + '"');
            parameters.put('request_type','fileUpload');
            parameters.put('setTimeout','60000');
            GoogleDriveWrapper.File fur = (GoogleDriveWrapper.File)GoogleDriveService.accessDrive(parameters); //upload wrapper is same as to download wrapper
            if(fur == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'null response received : '+'UploadFile'));
                return;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'File uploaded successfully : '+fur.name));
            currentFolderFiles.add(new GoogleDriveWrapper.File(fur.id,fur.name));
            fileToupload = new GoogleDriveWrapper.FileUpload();
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'No file uploaded'));
        }
    }
}