public class ContactUploadController {
    public String csvFileString{get;set;}
    public blob csvFileBody{get;set;}
    public List<Contact> uploadedConList{get;set;}
    public String[] fieldsToDisplay{get;set;}
    @testVisible String objType;
    Map<String, Schema.SObjectType> schemaMap;
    Schema.SObjectType contactSchema;
    Map<String, Schema.SObjectField> fieldMap;
    public Integer created{get;set;}
    public Integer updated{get;set;}
    public Integer succeed{get;set;}
    public Integer failed{get;set;}
    public Integer invalidData{get;set;}
    public Integer totalRecords{get;set;}
    public String[] errorMsg{get;set;}
    
    public ContactUploadController(){
        init();
    }
    
    public void init(){
        uploadedConList = new List<Contact>();
        fieldsToDisplay = new List<String>();
        objType = 'Contact';
        schemaMap = Schema.getGlobalDescribe();
        contactSchema = schemaMap.get(objType);
        fieldMap = contactSchema.getDescribe().fields.getMap();
        created=0;
        updated=0;
        succeed=0;
        failed=0;
        invalidData=0;
        totalRecords=0;
        errorMsg=new List<String>();
    }
 
    public void importCSVFile(){
        try{
            init();
            String[] csvColumns= new List<String>();
            String[] csvRows = new List<String>();
            csvFileString = csvFileBody.toString();
            csvFileBody=null;
            if(csvFileString.contains('\r')){
                csvFileString = csvFileString.remove('\r');
            }
            
            csvRows = csvFileString.split('\n');
            
            csvColumns = csvRows[0].split(',');
           
            for(String field : csvColumns){
                field = field.contains('\"') ? field.remove('\"') : field;
                fieldsToDisplay.add(field);
            }
            totalRecords = csvRows.size()-1;
            for(Integer i=1 ; i<csvRows.size(); i++){
                try{
                    Contact con = new Contact();
                    
                    //split when fieldValues doesnot contain "" (only text area will contain "")
                    String[] fieldValues = new List<String>();
                    boolean flag = false;
                    String cellvalue='';
                    for(Integer x=0 ; x<csvRows[i].length(); x++){
                        if(csvRows[i].charAt(x) == 34 && flag==false){
                            flag=true;
                        }else if(csvRows[i].charAt(x) == 34 && flag==true){
                            flag=false;
                        }else if(csvRows[i].charAt(x) == 44 && flag==false){
                            fieldValues.add(cellValue);
                            cellValue='';
                        }else if(csvRows[i].charAt(x) == 44 && flag==true){
                            cellValue +=',';
                        }else{
                            cellvalue += String.fromCharArray( new List<integer> { csvRows[i].charAt(x) } );
                        }
                    }
                    fieldValues.add(cellValue);
                    
                    for(Integer j=0; j<csvColumns.size(); j++){
                        
                        String fieldName = csvColumns[j].contains('\"') ? csvColumns[j].remove('\"') : csvColumns[j] ; 
                        String fieldValue = fieldValues[j].contains('\"') ? fieldValues[j].remove('\"') : fieldValues[j];
                        
                        Schema.DisplayType fielddataType;
                        if(fieldMap.containsKey(fieldName)){
                            fielddataType = fieldMap.get(fieldName).getDescribe().getType();
                        }
                        
                        if(fielddataType!=null && fielddataType == Schema.DisplayType.Date){
                            con.put(fieldName, String.isBlank(fieldValue) ? null : Date.valueOf(fieldValue));
                        }else if(fielddataType!=null && fielddataType == Schema.DisplayType.DateTime){
                            con.put(fieldName, String.isBlank(fieldValue) ? null : DateTime.valueOf(fieldValue));
                        }else if(fielddataType!=null && fielddataType == Schema.DisplayType.Boolean){
                            con.put(fieldName, String.isBlank(fieldValue) ? false : Boolean.valueOf(fieldValue)); // in case of sobject null is not accepted hence false is used
                        }else{
                            con.put(fieldName, String.isBlank(fieldValue) ? null : fieldValue);
                        }
                    }
                    uploadedConList.add(con);
                }catch(exception e){
                    errorMsg.add('Record:'+i+' Error:'+e.getMessage()+' Line: '+e.getLineNumber()); 
                    invalidData++;
                }
            }
            Database.UpsertResult[]	dur = Database.upsert(uploadedConList,false);
            for(Database.UpsertResult d : dur){
                if(d.isSuccess()){
                    succeed++;
                    if(d.isCreated()){
                        created++;
                    }else{
                        updated++;
                    }
                }else{
                    failed++;
                    Database.Error[] errs = d.getErrors();
                    String errmsg='';
                    for(Database.Error err : errs){
                        errmsg+=err.getMessage()+' '+err.getFields();
                    }
                    errorMsg.add(errmsg);
                }
            }
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'CSV not found'));
        }
    }
}