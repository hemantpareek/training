public class CustomLookupController {

    //RETRIEVES NAME FOR RECORDS FOR WHICH ID IS PROVIDED
    @AuraEnabled
    public static string handleDoInit(String sobjName, String recordId){
        String nameField = 'name';
        if(sobjName == 'case'){
            nameField = 'caseNumber';
        }
        return Database.query('SELECT ' + nameField + ' FROM ' + sobjName + ' WHERE Id = \'' + recordId + '\'')[0].get(nameField).toString();
    }

    //RETRIVES RECORDS ON SEARCH RECORD BASIS
    @AuraEnabled
    public static string handleQueryRecords(String sobjName, String searchKeyword){
        String nameField = 'name';
        if(sobjName == 'case'){
            nameField = 'caseNumber';
        }
        String query = 'SELECT Id,' + nameField + ' FROM ' + sobjName + ' WHERE ' + nameField + ' LIKE \'' + searchKeyword + '%\' ORDER BY ' + nameField + ' LIMIT 5';
        List<Sobject> sobjLst = Database.query(query);
        return JSON.serialize(sobjLst);
    }
}