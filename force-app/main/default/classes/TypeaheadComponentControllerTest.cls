@isTest
class TypeaheadComponentControllerTest{
    @testSetup 
    static void setupRecordsForTesting(){
        List<Account> accLst = new List<Account>();
        for(Integer i=1; i<=300; i++){
            Account a = new Account();
            a.Name='Account-'+i;
            if(i>=1 && i<=100){
                a.Industry='Energy';
                a.rating='Hot';
            }else if(i>100 && i<=200){
                a.Industry='Electronics';
                a.rating='Warm';
            }else{
                a.Industry='Banking';
                a.rating='Cold';
            }
            accLst.add(a);
        }
        insert accLst;
    }  
    
    @isTest
    static void populateSobjectByName_test(){
        TypeaheadComponentController t = new TypeaheadComponentController();
        system.assert(t.recordId == null);
        system.assert(TypeaheadComponentController.sobjectNameList == null);
        List<String> rcdLst = TypeaheadComponentController.populateSobjectByName('Account', 'Account', 'id,Name');
        system.assert(rcdLst.size() == 300);
        rcdLst = TypeaheadComponentController.populateSobjectByName('gene', 'Account', 'id,Name');
        system.assert(rcdLst.size() == 0);
        rcdLst = TypeaheadComponentController.populateSobjectByName('gene', 'xxxxxxx', 'id,Name');
        system.assert(rcdLst == null);
    }
    
    @isTest
    static void displayDetail_test(){
        TypeaheadComponentController t = new TypeaheadComponentController();
        ApexPages.currentPage().getParameters().put('recordName','GenePoint');
        ApexPages.currentPage().getParameters().put('passedSobject','Account');
        ApexPages.currentPage().getParameters().put('passedFields','ID');
        String recordName = ApexPages.currentPage().getParameters().get('recordName');
        String passedSobject = ApexPages.currentPage().getParameters().get('passedSobject');
        String passedFields = ApexPages.currentPage().getParameters().get('passedFields');
        system.assert(t.recordId == null);
        system.assertEquals('GenePoint', recordName);
        system.assertEquals('Account', passedSobject);
        system.assertEquals('ID', passedFields);
        t.displayDetail();
        system.assertEquals(0 , [SELECT ID FROM Account WHERE NAME ='GenePoint'].size());
        system.assert(t.recordId == null);
        
        ApexPages.currentPage().getParameters().put('recordName','Account-1');
        ApexPages.currentPage().getParameters().put('passedSobject','Account');
        ApexPages.currentPage().getParameters().put('passedFields','ID');
        recordName = ApexPages.currentPage().getParameters().get('recordName');
        passedSobject = ApexPages.currentPage().getParameters().get('passedSobject');
        passedFields = ApexPages.currentPage().getParameters().get('passedFields');
        system.assert(t.recordId == null);
        system.assertEquals('Account-1', recordName);
        system.assertEquals('Account', passedSobject);
        system.assertEquals('ID', passedFields);
        t.displayDetail();
        system.assertEquals(t.recordId, [SELECT ID FROM Account WHERE Name=:recordName][0].id);
    }
}