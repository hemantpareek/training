public with sharing class NF_LbrandsUtility {
    @AuraEnabled
    public static String getEmail( String recordId ){
        String email = '';
        String objName = ( (Id)recordId ).getSObjectType().getDescribe().getName();
        System.debug( '@@@@ objName ==>> ' + objName );
        if( objName == 'Account' ){
           // email = [ SELECT PersonEmail FROM Account WHERE Id = :recordId ].PersonEmail;
        } else if( objName == 'Case' ){
           email = [ SELECT Contact.Email FROM Case WHERE Id = :recordId ].Contact.Email;
        } else if( objName == 'LiveChatTranscript' ){
           // email = [ SELECT Contact.Email FROM LiveChatTranscript WHERE Id = :recordId ].Contact.Email;
        }
        return email;
    }
}