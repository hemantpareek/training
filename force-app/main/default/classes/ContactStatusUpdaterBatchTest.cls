@isTest
class ContactStatusUpdaterBatchTest {
    
    @TestSetup
    static void makeData(){
        List<Contact> conToInsert = new List<Contact>();
        for(Integer i=1 ; i<=10; i++){
            conToInsert.add(new Contact(lastname='c'+i));
        }
        insert conToInsert;
        DateTime yesterday = system.today()-1;
        List<Contact> conToUpdate = new List<Contact>();
        for(Contact con : [SELECT createdDate, lastname from Contact]){
            Test.setCreatedDate(con.id,yesterday);
            conToUpdate.add(con);
        }
        update conToUpdate;
    }

    @isTest
    static void documentTest(){
        system.assertEquals(0,[SELECT name from Document where name like 'YesterdayCreatedRecords%'].size());
        Test.startTest();
        Database.executeBatch(new ContactStatusUpdaterBatch());
        Test.stopTest();
        system.assertEquals(1,[SELECT name from Document where name like 'YesterdayCreatedRecords%'].size());
    }
    
    @isTest
    static void sendEmailTest(){
        Folder f = [SELECT name FROM Folder where name='Public Docs' LIMIT 1];
        Datetime now = Datetime.now();
        Integer offset = UserInfo.getTimezone().getOffset(now);
        Datetime local = now.addSeconds(offset/1000);
        Document doc = new Document(name='YesterdayCreatedRecords_'+local, Body = Blob.valueOf('Hello Apex'), Type = 'csv', FolderId = f.id);
        insert doc;
        system.assertEquals(0, Limits.getEmailInvocations());
        new ContactStatusUpdaterBatch().sendEmail(doc.id);
        system.assertEquals(1, Limits.getEmailInvocations());
    }

    @isTest
    static void testContactsRecords(){
        boolean flag=true;
        for(Contact con : [SELECT status__c, createdDate, lastname from Contact]){
            if(con.status__c != 'Not Submitted'){
                flag=false;
                break;
            }
        }
        system.assertEquals(true,flag);
        Test.startTest();
        Database.executeBatch(new ContactStatusUpdaterBatch());
        Test.stopTest();
        for(Contact con : [SELECT status__c, createdDate, lastname from Contact]){
            if(con.status__c != 'Ready for approval'){
                flag=false;
                break;
            }
        }
        system.assertEquals(true,flag);
    }

    @isTest
    static void testContactsCreatedDate(){
        Test.startTest();
        boolean flag=true;
        DateTime yesterday = system.today()-1;
        List<Contact> conToTest = [SELECT createdDate, lastname from Contact];
        for(Contact con : conToTest){
            if(con.createdDate != yesterday){
                flag=false;
                break;
            }
        }
        system.assertEquals(true,flag);
        system.assertEquals(10,conToTest.size());
        Test.stopTest();
    }
}