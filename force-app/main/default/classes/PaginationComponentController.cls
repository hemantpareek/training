//************************* PAGINATION VISUALFORCE CUSTOM COMPONENT CONTROLLER *********************************
public class PaginationComponentController {
    
    //************************************** F I E L D S *******************************************************
    public String selectedobjectName{get; set;} // Name of the object in which objectToDisplay is passed from UI
    public String sortDir {get; private set;}   // holds the sortDir ASC / DESC
    public String sortField {get; set;}         // holds current field on which sorting is to be done
    @TestVisible String previousSortField;      // holds previous field on which sorting was done
    public String indexBy{get; set;}            // holds index variable (default ALL)
    public String csvString{get; private set;}  // variable to store generated String to be downloaded as CSV
    public List <Wrapper> wrapperList{get; set;}// wrapper list to be processed in pageblocktable
    public Boolean isSelectAll {get; set;}      // holds the value of isSelectAll checkBox for a page on UI
    @TestVisible String[] alphabets;                         // alphabet string to perform index based on "others" 
    public String[] fields{get; set;}           //fields the are processed as fieldsToDisplay via UI
    public String[] indexes{get; set;}          //holds the index values like A-Z, Other, All
    public ApexPages.StandardSetController setCtrl{get;set;}   //instance of StandardSetController
    public Boolean hasNext{get;set;}            //tells if setCtrl has a next pageset of records
    public Boolean hasPrevious{get;set;}        //tells if setCtrl has a previous pageset of records
    public String keyPrefix{get;set;}           // holds keyPrefix for sobject
    public String nameField{get;set;}           // holds the name field of sobject  
    public String selectedRecordIds{get;set;}   // recordId selected on UI
    
    //********************************** C O N S T R U C T O R ***************************************************
    public PaginationComponentController (){  
        fields=new List<String>();
        indexes = new String[]{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y',
            'Z','Other','All'};
                alphabets = new String[]{'A%','B%','C%','D%','E%','F%','G%','H%','I%','J%','K%','L%','M%','N%','O%','P%','Q%','R%',
                    'S%','T%','U%','V%','W%','X%','Y%','Z%'};
                        isSelectAll=false;
        setCtrl = new ApexPages.StandardSetController(new List<Account>());
    }  
    //************************************* W R A P P E R  C L A S S *********************************************
    //WRAPPER CLASS TO WRAP A CHECKBOX WITH A SOBJECT
    public class Wrapper{
        public sobject sobj {get; set;}
        public Boolean isSelected {get; set;}
        
        public Wrapper(Sobject sobj, Boolean isSelected){
            this.sobj = sobj;
            this.isSelected = isSelected;
        }
    }
    
    //************************************* M E T H O D S ********************************************************
    //1. ENTRY POINT FOR COMPONENT CONTROLLER 
    public void init(){
        sortDir='ASC';
        previousSortField = nameField;
        sortField=previousSortField;
        indexBy='All';
        initSetCtrl();
        setCtrl.setPageSize(10);
        initWrapperList();
        selectedRecordIds='';
    }
    
    //2. INITIALIZES WRAPPER LIST
    public void initWrapperList() {
        wrapperList = new List<Wrapper>();
        for (Sobject sobj : setCtrl.getRecords()) {
            wrapperList.add(new Wrapper(sobj, false));
        }
        hasNext = setCtrl.getHasNext();
        hasPrevious = setCtrl.getHasPrevious();    
    }
    
    //3. RETURNS THE FIRST PAGE OF THE PAGE SET
    public void first() {
        setCtrl.first();
        initWrapperList();
    }
    
    //4. RETURNS THE LAST PAGE OF THE PAGE SET
    public void last() {
        setCtrl.last();
        initWrapperList();
    }
    
    //5. RETURNS THE PREVIOUS PAGE OF THE PAGE SET
    public void previous() {
        setCtrl.previous();
        initWrapperList();
    }
    
    //6. RETURNS THE NEXT PAGE OF THE PAGE SET
    public void next() {
        setCtrl.next();
        initWrapperList();
    }
    
    //7. INITIALISES SETCTRL W.R.T. GENERATED QUERY
    public void initSetCtrl(){
        try{
            setCtrl = new ApexPages.StandardSetController(Database.Query(generateQuery()));
        }catch(Exception e){
            setCtrl = new ApexPages.StandardSetController(new List<Account>());
        }
        setCtrl.setPageSize(10);
    }
    
    //8. FUNCTION TO GENERATE QUERY FOR GIVEN OBJECT AND FIELDS WITH SORTING AND INDEXING
    public String generateQuery(){
        String query='SELECT ';
        for(Integer i = 0; i<fields.size(); i++){
            query += fields[i];
            if(i<fields.size()-1)
                query+=', ';
        }
        query+=' FROM '+ selectedobjectName +' ';
        if(indexBy.equals('All')){
            query+='';
        }
        else if(indexBy.equals('Other')){
            query+='Where Not '+ nameField +' Like :alphabets ';
        }
        else{
            query+='WHERE '+ nameField +' Like \''+indexBy+'%\'';
        }
        query+=' ORDER BY '+sortField+' '+sortDir;
        return query;
    }
    
    //9. FUNCTION TO PERFORM SORT
    public void sort(){
        sortDir = sortField==previousSortField  ? (sortDir=='ASC' ? 'DESC' : 'ASC') : 'ASC' ;
        previousSortField=sortField;       
        maintainPageState();
    }
    
    //10. FUNCTION FOR INDEXING
    public void index(){
        Integer existingPageSize = setCtrl.getPageSize();
        initSetCtrl();
        setCtrl.setPageSize(existingPageSize);
        setCtrl.setPageNumber(1);
        initWrapperList();
    }
    
    //11. MAINTAINS PAGE STATE LIKE PAGE SIZE AND PAGE NUMBER AFTER UPDATING SETCTRL INSTANCE    
    public void maintainPageState(){
        Integer prevPageSize = setCtrl.getPageSize();
        Integer prevPageNumber = setCtrl.getPageNumber();
        initSetCtrl();
        setctrl.setPageNumber(prevPageNumber);
        setCtrl.setPageSize(prevPageSize);
        initWrapperList();
    }
    
    //12. FUNCTION TO DELETE SELECTED RECORDS (IF ANY) 
    public void deleteSelected(){
        List<ID> rcdToDel = new List<String>(selectedRecordIds.split(','));
        selectedRecordIds = '';
        Database.DeleteResult[] delres = database.delete(rcdToDel,false);
        isSelectAll=false;
        maintainPageState();
        for(Database.deleteResult dr : delres){
            for(Database.Error err : dr.getErrors()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,err.getMessage()));   
            }
        }
    }
    
    //13. DELETES SINGLE RECORD
    public void deleteSingle(){
        Database.DeleteResult delres = Database.delete(Apexpages.currentPage().getParameters().get('recordIdToDel'),false);
        maintainPageState();
        for(Database.Error err : delres.getErrors()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,err.getMessage()));   
        }
    }
    
    //14. UPDATES PAGESIZE
    public void updatePageSize(){
        setCtrl.setPageNumber(1);
        initWrapperList();
    }
    
    //15. METHOD TO GENERATE STRING TO BE DOWNLOADED AS CSV FORMAT
    public void generateCsvString(){
        List<ID> recordIdsToDownload = new List<String>(selectedRecordIds.split(','));
        csvString='';
        String generatedCSVFile ='';
        String colums = '';
        for(String field : fields){
            colums+='\"'+field+'\"'+',';
        }
        colums=colums.removeEnd(',');
        colums+='\\n';
        generatedCSVFile+=colums;
        List<sobject> selectedRecords;
        String query = 'SELECT ';
        for(Integer i=0; i<fields.size(); i++){
            query+=fields[i];
            if(i<fields.size()-1)
                query+=', ';
        }
        query+=' FROM '+ selectedobjectName;
        query+=' WHERE ID IN :recordIdsToDownload';
        selectedRecords = database.query(query);
        for(Sobject s: selectedRecords){
            String row = '';
            for(String field : fields){
                row += '\"';
                row += s.get(field)!=null ? s.get(field) : '' ;
                row += '\"'+',';
            }
            row=row.removeEnd(',');
            row+='\\n';
            generatedCSVFile+=row;
        }
        csvString=generatedCSVFile;
    }
    //********************************************  E N D ********************************************************
}