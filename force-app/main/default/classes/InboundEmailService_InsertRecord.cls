//-------------------------------------------- INBOUND EMAIL HANDLER CLASS --------------------------------------------------------
global class InboundEmailService_InsertRecord implements Messaging.InboundEmailHandler {
    String errorMsg = '';
    
    //----------------------------------------------- METHODS -------------------------------------------------------------------
    //1. EMAIL HANDLER METHOD
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        //assumed that proper format is parsed  & any sobject field doesnot contains # character.
        system.debug(email.plainTextBody);
        processRecords(email.plainTextBody);
        if(errorMsg.length()!=0){
            system.debug(errorMsg);
        }
        return result;
    }
    
    //2. METHOD TO PROCESS RECORDS 
    public void processRecords(String emailBody){
        Map<String,Map<String,String>> records = getRecordsFromEmail(emailBody);
        List<sobject> sobjList = new List<sobject>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe(); 
        String errorMsg = '';
        for(String sobjInMap : records.keySet()){
            try{
                Sobject sobj = schemaMap.get(sobjInMap).newSObject();
                for(String fldInMap : records.get(sobjInMap).keySet()){
                    sobj.put(fldInMap,records.get(sobjInMap).get(fldInMap));
                }
                sobjList.add(sobj);
            }catch(Exception e){
                errorMsg += e.getMessage() +'\n';
            }
        }
        Database.SaveResult[] dsrs = Database.insert(sobjList,false);
        for(Database.SaveResult dsr : dsrs){
            for(Database.Error err : dsr.getErrors()){
                errorMsg += err.getMessage() +'\n';
            }
        }
    }
    
    //3. METHOD TO GENERATE A MAP TO BIND SOBJECT WITH FILEDS AND FIELDVALUES
    Map<String,Map<String,String>> getRecordsFromEmail(String emailbody){
        Map<String,Map<String,String>> recordsMap = new Map<String,Map<String,String>>();
        emailbody = emailbody.toLowerCase();
        List<String> records = emailBody.split('##');
        for(Integer i=0; i<records.size(); i++){
            try{ 														// field w/o not value will not be included
                List<String> record = records[i].split('#');            
                for(Integer j=1 ; j<record.size(); j+=2){
                    if(!recordsMap.containsKey(record[0])){
                        recordsMap.put(record[0],new Map<String,String>{record[j] => record[j+1]});
                    } else if(!recordsMap.get(record[0]).containsKey(record[j])){
                        recordsMap.get(record[0]).put(record[j],record[j+1]);
                    }
                }
            } catch(Exception e){
                errorMsg += e.getMessage() +'\n';
            }
        }
        return recordsMap;
    }
}