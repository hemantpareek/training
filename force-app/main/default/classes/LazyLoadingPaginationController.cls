public class LazyLoadingPaginationController {
    
    @AuraEnabled
    public static List<sobject> doInit(String objectName, List<String> fields, List<String> existingRecordIds){
        try{
            String query = 'SELECT ' + String.join(fields,',') + ' FROM ' + objectName + ' WHERE Id NOT IN : existingRecordIds ORDER By Name LIMIT 20';
            return Database.query(query);
        }catch(Exception e){
            return null;
        }
    }
}