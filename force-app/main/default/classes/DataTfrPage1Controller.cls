public class DataTfrPage1Controller {
    public string csv{get;set;}
    public DataTfrPage1Controller(){
        system.debug('Page 1 Constructor');
        csv = 'hello';
    }
    
    public pagereference downloadCsv(){
        PageReference pgref = new PageReference('/apex/DataTfrPage2');
        pgref.setRedirect(false);
        return pgref;
    }
}