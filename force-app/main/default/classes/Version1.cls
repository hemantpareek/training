@RestResource(urlMapping = '/v1/accounts/*')
global with sharing class Version1{
    
    @HttpGet
    global static Account doGet(){
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String accountNo = req.requestURI.subString(req.requestURI.lastIndexOf('/')+1);
        List<Account> a = [Select name, AccountNumber, industry, billingaddress, website, site from account where AccountNumber like :(accountNo+'%')];
        return a[0];
        
    }
    
}