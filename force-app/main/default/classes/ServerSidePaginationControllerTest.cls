@isTest
private class ServerSidePaginationControllerTest {

    //1. SETS UP THE DATA FOR TESTING
    @TestSetup
    static void makeData(){
        Test.startTest();
        List<Account> accList = new List<Account>();
        for(Integer i=1 ; i<=5; i++){
            accList.add(new account(name = 'a'+i));
        }
        insert accList;
        Test.stopTest();
    }

    //2. TEST FOR OBJECT INITIALIZATION
    @isTest
    private static void doInitSObjectList(){
        Test.startTest();
        String objList = ServerSidePaginationController.doInitSObjectList();
        system.assert(objList.contains('{"label":"Account","value":"account","keyPrefix":"001"}'));
        system.assert(objList.contains('{"label":"Contact","value":"contact","keyPrefix":"003"}'));
        Test.stopTest();
    }

    //3. TEST FOR FIELD LIST INITILIZATION
    @isTest
    private static void doInitFieldListTest(){
        Test.startTest();
        String fldList = ServerSidePaginationController.doInitFieldList('account');
        system.assert(fldList.contains('"nameField":"name"'));
        system.assert(fldList.contains('industry'));
        system.assert(fldList.contains('rating'));
        fldList = ServerSidePaginationController.doInitFieldList('case');
        system.assert(fldList.contains('"nameField":"casenumber"'));
        Test.stopTest();
    }

    //4. TEST FOR QUERY FOR RECORDS IF NOT REVERSED LIST
    @isTest
    private static void doQueryForRecordsTest_1(){
        Test.startTest();
        String query = 'SELECT name FROM account LIMIT 2';
        String expected = '[{"name":"a1"},{"name":"a2"}]';
        system.assertEquals(expected, ServerSidePaginationController.doQueryForRecords(query, false));
        Test.stopTest();
    }

    //5. TEST FOR QUERY FOR RECORDS IF REVERSED LIST
    @isTest
    private static void doQueryForRecordsTest_2(){
        Test.startTest();
        String query = 'SELECT name FROM account LIMIT 2';
        String expected = '[{"name":"a2"},{"name":"a1"}]';
        system.assertEquals(expected, ServerSidePaginationController.doQueryForRecords(query, true));
        Test.stopTest();
    }
}