@isTest
class ContactUploadControllerTest {
    
    @testSetup
    static void makedata(){
        Test.startTest();
        Contact con = new Contact(LastName='Sample Contact');
        Database.insert(con,false);
        Test.stopTest();
    }
    
    List<String> fields(){
        return new List<String>{'id','lastName','Sample_Boolean__c','Sample_Date__c','Sample_Date_Time__c','Sample_Text_Area__c'};
            }
    
    List<List<String>> prepareContacts(){
        List<String> c1 = new List<String>{'','c1','true','2019-01-01','2019-01-01 01:01:01','Hello, world, how are you'};  //ok
        List<String> c2 = new List<String>{'','c2','false','','',''};                                                       //ok// last three fields are blank
        List<String> c3 = new List<String>{'','c3','','','',''};                                                            //ok// last four fields are blank
        List<String> c4 = new List<String>{'','c4','xyz','2019-01-01','2019-01-01 01:01:01','Hello, world, how are you'};  //ok//invalid boolean string but boolean.valueOf('xyz') is false.
        List<String> c5 = new List<String>{'','','','','',''};                                                          //error//missing name field
        List<String> c6 = new List<String>{'','c6','','01-01-2019','',''};                                                 //error// invalid date format 
        List<String> c7 = new List<String>{'','c7','','2019-01-01 07:07:07','',''};                                      // valid date format 
        List<String> c8 = new List<String>{'','c8','','','01-01-2019 00:00:00',''};                                      //error// invalid datetime format
        List<String> c9 = new List<String>{'','c9','','','2019-01-01 11:22:33','Hello world how are you'};              //ok// valid datetime format, text area w/o comma
        List<String> c10 = new List<String>{'','c10','','','','Hello, world, how are you'};                             //ok// textarea with comma
        
        Id conId = [SELECT Id FROM Contact WHERE LastName='Sample Contact' Limit 1].id;
        List<String> c11 = new List<String>{conId,'c11','','','','Hello, world, how are you'};                       //ok// id already present
        return new List<List<String>>{c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11};
    }
    
    String generateNormalCSV(){
        String generatedCSVFile ='';
        String colums = '';
        
        for(String field : fields()){
            colums+='\"'+field+'\"'+',';
        }
        colums=colums.removeEnd(',');
        colums+='\n';
        generatedCSVFile+=colums;
        
        List<List<String>> contactRecords = prepareContacts();
        
        for(List<String> rcd: contactRecords){
            String row= '';
            for(String fieldValue : rcd){
                row += '\"' + fieldValue + '\"' +',';
            }
            row=row.removeEnd(',');
            row += '\n';
            generatedCSVFile+=row;
        }   
        return generatedCSVFile;
    }
    
    String generateExcelEditedCSV(){
        String generatedCSVFile ='';
        String colums = '';
        
        for(String field : fields()){
            colums+=field+',';
        }
        colums=colums.removeEnd(',');
        colums+='\r\n';
        generatedCSVFile+=colums;
        
        List<List<String>> contactRecords = prepareContacts();
        
        for(List<String> rcd: contactRecords){
            String row= '';
            for(Integer i=0; i<rcd.size()-1; i++){
                row += rcd[i] + ',';
            }
            row += '\"' + rcd[rcd.size()-1] + '\"';
            row += '\r\n';
            generatedCSVFile+=row;
        }   
        return generatedCSVFile;
    }

    @isTest
    static void ordinary_csv_test(){
        Test.startTest();
        
        ContactUploadController cuctrl = new ContactUploadController();
        
        system.assertEquals(0, cuctrl.uploadedConList.size());
        system.assertEquals(0, cuctrl.fieldsToDisplay.size());
        system.assertEquals('Contact',cuctrl.objType);
        system.assertEquals(0,cuctrl.errorMsg.size());
        system.assertEquals(0,cuctrl.succeed);
        system.assertEquals(0,cuctrl.failed);
        system.assertEquals(0,cuctrl.created);
        system.assertEquals(0,cuctrl.updated);
        system.assertEquals(0,cuctrl.invalidData);
        system.assertEquals(0,cuctrl.totalRecords);
        system.assertEquals(null,cuctrl.csvFileBody);
        system.assertEquals(null,cuctrl.csvFileString);
        
        cuctrl.importCSVFile();
        
        cuctrl.csvFileBody = Blob.valueOf(new ContactUploadControllerTest ().generateNormalCSV());
        String csvFileString = cuctrl.csvFileBody.toString();
        system.assert(cuctrl.csvFileBody != null);
        system.assert([select id from contact].size() == 1);
        
        cuctrl.importCSVFile();
        system.assert(cuctrl.csvFileBody == null);
        system.assertEquals(csvFileString,cuctrl.csvFileString);
        system.assertEquals(6, cuctrl.fieldsToDisplay.size());
        system.assertEquals(8,[select id from contact].size());
   
        system.assertEquals(3,cuctrl.errorMsg.size());
        system.assertEquals(8,cuctrl.succeed);
        system.assertEquals(2,cuctrl.failed);
        system.assertEquals(7,cuctrl.created);
        system.assertEquals(1,cuctrl.updated);
        system.assertEquals(1,cuctrl.invalidData);
        system.assertEquals(11,cuctrl.totalRecords);
        system.assert(![select sample_text_area__c from contact where name = 'c9'][0].sample_text_area__c.contains(','));
        system.assert([select sample_text_area__c from contact where name = 'c10'][0].sample_text_area__c.contains(','));
        
        Test.stopTest();
    }
    
    @isTest
    static void excel_csv_test(){
        Test.startTest();
        system.debug(new ContactUploadControllerTest ().generateExcelEditedCSV());
        
        ContactUploadController cuctrl = new ContactUploadController();
        
        system.assertEquals(0, cuctrl.uploadedConList.size());
        system.assertEquals(0, cuctrl.fieldsToDisplay.size());
        system.assertEquals('Contact',cuctrl.objType);
        system.assertEquals(0,cuctrl.errorMsg.size());
        system.assertEquals(0,cuctrl.succeed);
        system.assertEquals(0,cuctrl.failed);
        system.assertEquals(0,cuctrl.created);
        system.assertEquals(0,cuctrl.updated);
        system.assertEquals(0,cuctrl.invalidData);
        system.assertEquals(0,cuctrl.totalRecords);
        system.assertEquals(null,cuctrl.csvFileBody);
        system.assertEquals(null,cuctrl.csvFileString);
        
        cuctrl.importCSVFile();
        
        cuctrl.csvFileBody = Blob.valueOf(new ContactUploadControllerTest ().generateExcelEditedCSV());
        String csvFileString = cuctrl.csvFileBody.toString();
        system.assert(cuctrl.csvFileBody != null);
        system.assert([select id from contact].size() == 1);
        
        cuctrl.importCSVFile();
        system.assert(cuctrl.csvFileBody == null);
        
        system.assertEquals(6, cuctrl.fieldsToDisplay.size());
        system.assertEquals(8,[select id from contact].size());
   
        system.assertEquals(3,cuctrl.errorMsg.size());
        system.assertEquals(8,cuctrl.succeed);
        system.assertEquals(2,cuctrl.failed);
        system.assertEquals(7,cuctrl.created);
        system.assertEquals(1,cuctrl.updated);
        system.assertEquals(1,cuctrl.invalidData);
        system.assertEquals(11,cuctrl.totalRecords);
        system.assert(![select sample_text_area__c from contact where name = 'c9'][0].sample_text_area__c.contains(','));
        system.assert([select sample_text_area__c from contact where name = 'c10'][0].sample_text_area__c.contains(','));
        
        Test.stopTest();
    }
}