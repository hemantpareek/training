@isTest
class PaginationComponentControllerTest {
    
    @testSetup 
    static void setupRecordsForTesting(){
        Test.startTest();
        List<Account> accLst = new List<Account>();
        for(Integer i=1; i<=300; i++){
            Account a = new Account();
            a.Name='Account-'+i;
            if(i>=1 && i<=100){
                a.Industry='Energy';
                a.rating='Hot';
            }else if(i>100 && i<=200){
                a.Industry='Electronics';
                a.rating='Warm';
            }else{
                a.Industry='Banking';
                a.rating='Cold';
            }
            accLst.add(a);
        }
        insert accLst;
        Test.stopTest();
    }
    
    @isTest
    static void constructor_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        system.assertEquals(0, p.fields.size());
        system.assertEquals(new String[]{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V',
            'W','X','Y','Z','Other','All'},p.indexes);
        system.assertEquals(new String[]{'A%','B%','C%','D%','E%','F%','G%','H%','I%','J%','K%','L%','M%','N%','O%','P%','Q%','R%',
                    'S%','T%','U%','V%','W%','X%','Y%','Z%'}, p.alphabets);
        system.assertEquals(false,p.isSelectAll);
        system.assertEquals(0,p.setCtrl.getResultSize());
        Test.stopTest();
    }
    
    @isTest
    static void init_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        system.assertEquals(null, p.sortDir);
        system.assertEquals(null, p.previousSortField);
        system.assertEquals(null, p.sortField);
        system.assertEquals(null, p.indexBy);
        system.assertEquals(0, p.setCtrl.getResultSize());
        system.assertEquals(null, p.wrapperList);
        system.assertEquals(null, p.selectedRecordIds);
        p.init();
        system.assertEquals('', p.selectedRecordIds);
        system.assertEquals('ASC', p.sortDir);
        system.assertEquals(null, p.previousSortField);
        system.assertEquals(null, p.sortField);
        system.assertEquals('All', p.indexBy);
        system.assertEquals(0, p.setCtrl.getResultSize());
        system.assertEquals(0, p.wrapperList.size());
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals('ASC', p.sortDir);
        system.assertEquals('name', p.previousSortField);
        system.assertEquals('name', p.sortField);
        system.assertEquals('All', p.indexBy);
        system.assertEquals(300, p.setCtrl.getResultSize());
        system.assertEquals(10, p.wrapperList.size());
        Test.stopTest();
    }
    
     @isTest
    static void initWrapperList_Test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals(10,p.wrapperList.size());
        system.assertEquals('Account-1',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-107',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.wrapperList[0].isSelected=true;
        p.next();
        system.assertEquals('Account-108',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-116',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.first();
        system.assertEquals('Account-1',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-107',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        Test.stopTest();
    }
    
    @isTest
    static void first_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        p.next();
        p.next();
        p.next();
        system.assertEquals('Account-126',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-134',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.first();
        system.assertEquals('Account-1',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-107',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        Test.stopTest();
    }
    
    @isTest
    static void last_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        p.next();
        p.next();
        p.next();
        system.assertEquals('Account-126',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-134',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.last();
        system.assertEquals('Account-90',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-99',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        Test.stopTest();
    }
    
    @isTest
    static void previous_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        p.next();
        p.next();
        p.next();
        system.assertEquals('Account-126',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-134',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.previous();
        system.assertEquals('Account-117',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-125',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        Test.stopTest();
    }
    
    @isTest
    static void next_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        p.next();
        p.next();
        p.next();
        system.assertEquals('Account-126',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-134',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        Test.stopTest();
    } 
    
    @isTest
    static void initSetCtrl_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        system.assertEquals(0, p.setCtrl.getResultSize());
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals(300, p.setCtrl.getResultSize());
        system.assertEquals(10, p.setCtrl.getpageSize());
        system.assertEquals(1, p.setCtrl.getPageNumber());    
        p.setCtrl.setPageSize(50);
        system.assertEquals(50, p.setCtrl.getpageSize());
        //p.setCtrl.setPageNumber(10);
        //system.assertEquals(10, p.setCtrl.getPageNumber());
        p.setCtrl.setPageNumber(-1);
        system.assertEquals(1, p.setCtrl.getPageNumber());
        p.setCtrl.setPageNumber(0);
        system.assertEquals(1, p.setCtrl.getPageNumber());
        p.setCtrl.setPageNumber(100);
        system.assertEquals(1, p.setCtrl.getPageNumber());
        p.setCtrl.setpageSize(100);
        system.assertEquals(100, p.setCtrl.getRecords().size());
        Test.stopTest();
    }

    @isTest
    static void generateQuery_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.init();
        system.assertEquals('SELECT  FROM null  ORDER BY null ASC', p.generateQuery());
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals('SELECT id, name FROM Account  ORDER BY name ASC', p.generateQuery());
        p.indexBy = 'Other';
        p.index();
        system.assertEquals('SELECT id, name FROM Account Where Not name Like :alphabets  ORDER BY name ASC', p.generateQuery());
        p.indexBy = 'A';
        p.index();
        system.assertEquals('SELECT id, name FROM Account WHERE name Like \'A%\' ORDER BY name ASC', p.generateQuery());
        Test.stopTest();
    }
    
     @isTest
    static void sort_Test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals('ASC', p.sortDir);
        system.assertEquals('name', p.sortField);
        system.assertEquals('name', p.previousSortField);
        system.assertEquals('Account-1',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-107',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.sort();
        system.assertEquals('DESC', p.sortDir);
        system.assertEquals('name', p.sortField);
        system.assertEquals('name', p.previousSortField);
        system.assertEquals('Account-99',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-90',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.next();
        system.assertEquals('DESC', p.sortDir);
        system.assertEquals('name', p.sortField);
        system.assertEquals('name', p.previousSortField);
        system.assertEquals('Account-9',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-81',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.sortField = 'id';
        system.assertEquals('name', p.previousSortField);
        p.sort();
        system.assertEquals('ASC', p.sortDir);
        system.assertEquals('id', p.sortField);
        system.assertEquals('id', p.previousSortField);
        system.assertEquals('Account-11',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-20',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.sort();
        system.assertEquals('DESC', p.sortDir);
        system.assertEquals('id', p.sortField);
        system.assertEquals('id', p.previousSortField);
        system.assertEquals('Account-290',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-281',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        Test.stopTest();
    }

    @isTest 
    static void index_Test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals('All',p.indexBy);
        p.index();
        system.assertEquals('All',p.indexBy);
        p.indexBy = 'A';
        p.index();
        system.assertEquals('A',p.indexBy);
        system.assertEquals('Account-1',p.wrapperList[0].sobj.get('Name'));
        system.assertEquals('Account-107',p.wrapperList[p.setCtrl.getPageSize()-1].sobj.get('Name'));
        p.indexBy = 'Other';
        p.index();
        system.assertEquals(0,p.setCtrl.getRecords().size());
        Test.stopTest();
    }
       
    @isTest
    static void maintainPageState(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        p.setCtrl.setPageNumber(10);
        p.setCtrl.setPageSize(20);
        system.assertEquals(20, p.setCtrl.getPageSize());
        p.initSetCtrl();
        system.assertEquals(10, p.setCtrl.getPageSize());
        p.setCtrl.setPageSize(20);
        system.assertEquals(20, p.setCtrl.getPageSize());
        //system.assertEquals(10, p.setCtrl.getPageNumber());
        p.maintainPageState();
        system.assertEquals(20, p.setCtrl.getPageSize());
        //system.assertEquals(10, p.setCtrl.getPageNumber());
        Test.stopTest();
    }
    
     @isTest
    static void deleteSelected_Test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals(300, p.setCtrl.getResultSize());
        system.assertEquals('',p.selectedRecordIds);
        p.selectedRecordIds += p.WrapperList[0].sobj.get('id').toString()+',';
        p.next();
        p.selectedRecordIds += p.WrapperList[5].sobj.get('id').toString()+',';
        p.last();
        p.selectedRecordIds += p.WrapperList[4].sobj.get('id').toString()+',';
        p.first();
        p.selectedRecordIds += p.WrapperList[8].sobj.get('id').toString();
        p.deleteSelected();
        system.assertEquals('', p.selectedRecordIds);
        system.assertEquals(296, p.setCtrl.getResultSize());
        
        //testting for dependent records
        Account a = [select id from account where name='Account-93' Limit 1];
        Account b = [select id from account where name='Account-96' Limit 1];
        case c1 = new case(AccountId=a.id);
        case c2 = new case(AccountId=b.id);
        insert(new List<Case>{c1,c2});
        p.selectedRecordIds = a.id+','+b.id;
        p.deleteSelected();
        system.assertEquals('', p.selectedRecordIds);
        system.assertEquals(296, p.setCtrl.getResultSize());
        Test.stopTest();
    }
    
     @isTest
    static void deleteSingle_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals(1,[Select Id From Account Where Name='Account-1'].size());
        Account a = [Select Id From Account Where Name='Account-1' Limit 1];
        ApexPages.currentPage().getParameters().put('recordIdToDel',a.id);
        p.deleteSingle();
        system.assertEquals(0,[Select Id From Account Where Name='Account-1'].size());
        //testting for dependent records
        a = [select id from account where name='Account-93' Limit 1];
        case c1 = new case(AccountId=a.id);
        insert c1;
        ApexPages.currentPage().getParameters().put('recordIdToDel',a.id);
        p.deleteSingle();
        system.assertEquals(1,[Select Id From Account Where Name='Account-93'].size());
        Test.stopTest();
    }
    
    @isTest
    static void updatePageSize_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        p.setCtrl.setPageSize(50);
        p.updatePageSize();
        system.assertEquals(50, p.wrapperList.size());
        Test.stopTest();
    }
    
    @isTest
    static void generateCSV_test(){
        Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'name','industry'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals(null, p.csvString);
        p.generateCsvString();
        system.assertEquals('\"name\",\"industry\"\\n', p.csvString);
        p.selectedRecordIds += p.WrapperList[0].sobj.get('id').toString()+',';
        p.next();
        p.selectedRecordIds += p.WrapperList[5].sobj.get('id').toString()+',';
        p.last();
        p.selectedRecordIds += p.WrapperList[4].sobj.get('id').toString()+',';
        p.first();
        p.selectedRecordIds += p.WrapperList[8].sobj.get('id').toString();
        p.generateCsvString();   
        String csv = '\"name\",\"industry\"\\n';
        csv += '\"Account-1\",\"Energy\"\\n';
        csv += '\"Account-94\",\"Energy\"\\n';
        csv += '\"Account-106\",\"Electronics\"\\n';
        csv += '\"Account-112\",\"Electronics\"\\n';
        system.assertEquals(csv, p.csvString);  
        Test.stopTest();
    }
    
    
    @isTest
    static void keyPrefix_nameField_Testing(){
       Test.startTest();
        PaginationComponentController p = new PaginationComponentController();
        system.assertEquals(null, p.keyPrefix);
        system.assertEquals(null, p.nameField);
        p.selectedobjectName = 'Account';
        p.fields = new List<String>{'id','name'};
        p.keyPrefix = '001';
        p.nameField = 'name';
        p.init();
        system.assertEquals('001', p.keyPrefix);
        system.assertEquals('name', p.nameField);
        Test.stopTest();
    }
}