//-------------------------------------------------------- GOOGLE DRIVE LIGHTNING COMPONENT APEX CONTROLLER -----------------------------------------------
public class IC_GoogleDriveController {
    //--------------------------------------------------------------- CONTROLLER VARIABLES ----------------------------------------------------------------
    @testVisible static String access_token; 
    @testVisible static String refresh_token;
    @testVisible static DateTime expires_in;                                          
    @testVisible static String redirect_uri = 'https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/google-drive';
    // @testVisible static String redirect_uri = 'https://hemantpareek-dev-ed.lightning.force.com/lightning/n/TestLightningComponent';
    @testVisible static String clientID;    
    @testVisible static String clientSecret;
    @testVisible static Google_Drive_Integration_Data__c gdi;
    
    //---------------------------------------------------------------------------METHODS -------------------------------------------------------------------

    //1. RETRIEVES ACCESS TOKEN, REFRESH TOKEN, EXPIRES IN FROM CUSTOM SETTING IF AVAILABLE
    @testVisible static Boolean retrieveDataFromCustomSetting(){ //returns true when list is not empty
        List<Google_Drive_Integration_Data__c> gdiLst = [SELECT access_Token__c , refresh_token__c , expires_in__c FROM Google_Drive_Integration_Data__c 
                                                        WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(gdiLst.isEmpty()){
            return false;
        }
        gdi = gdiLst[0];
        refresh_token = gdi.refresh_token__c;
        expires_in = gdi.expires_in__c;
        if(gdi.expires_in__c > System.now()){
            access_token = gdi.access_Token__c;
        }else{
            access_token = null;
        }
        return true;
    }

    //2. RETRIEVES CLIENT ID AND CLIENT SECRET FROM CUSTOM METADATA TYPES IF AVAILABLE
    @testVisible static Boolean retrieveDataFromCustomMetadata(){
        List<Google_Drive_Integration_Credentials__c> integration = [SELECT client_id__c , client_secret__c FROM Google_Drive_Integration_Credentials__c];
        if(!integration.isEmpty()){
            clientID = integration[0].client_id__c;
            clientSecret = integration[0].client_secret__c;
            return true;
        }
        return false;
    }

    //3. PROVIDES AUTH URI TO GET ACCESS TOKEN VIA CODE
    @testVisible static String doGetAuthURI(){
        if(retrieveDataFromCustomMetadata()){
            String authURI = GoogleDriveService.authenticate(clientId,redirect_uri);
            return '{"status":"authenticateWithCode","authURI":"'+ authURI +'"}';
        }else{
            return '{"status":"CUSTOM_METADATA_CREDENTIAL_NOT_FOUND"}';
        }
    }

    //4. FETCHES DATA FROM GOOGLE DRIVE BASED ON PARENT FOLDER'S ID 
    public static String fetchData(String parentFolderID){  
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','GET');
        parameters.put('setEndpoint','https://www.googleapis.com/drive/v2/files?q=%27'+parentFolderID+'%27%20in%20parents'); //End point for List of Files in Folder
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('request_type','fetchData');
        parameters.put('setTimeout','60000');
        GoogleDriveWrapper fetchDataResponse = (GoogleDriveWrapper)GoogleDriveService.accessDrive(parameters);
        if(fetchDataResponse == null){
            return null;
        }
        String fetchDataResponseJSON  = '[';
        for(GoogleDriveWrapper.File f : fetchDataResponse.items){
            fetchDataResponseJSON += '{';
            fetchDataResponseJSON += '"id":"' + f.id + '",';
            fetchDataResponseJSON += '"name":"' + f.title + '",';
            fetchDataResponseJSON += '"type":"' + f.mimeType + '",';
            fetchDataResponseJSON += '"shared":' + f.shared + ',';
            fetchDataResponseJSON += '"webContentLink":"' + f.webContentLink + '",';
            fetchDataResponseJSON += '"isLast":"false"';
            fetchDataResponseJSON += '},';
        }
        return fetchDataResponseJSON.removeEnd(',') + ']';
    }

//------------------------------------------------------- @AURAENABLED METHODS ----------------------------------------------------------------------
    //1. PERFORMS INITIAL PROCESSING WHEN COMPONENT LOADS FIRST TIME
    @AuraEnabled
    public static String doInitApex(){
        if(retrieveDataFromCustomSetting()){
            if(gdi.expires_in__c > System.now()){
                access_token = gdi.access_Token__c;
                return '{"status":"readyToLoadData"}'; // perform fetch data first time action
            } else {
                if(refresh_token != null){
                    return '{"status":"getAccessTokenFromRefreshToken"}'; //called when access token is expired
                }else{
                    return doGetAuthURI(); // here refresh token is null and we have to get access token with authCode
                }
            }
        } else {
            return doGetAuthURI();
        }
    }
    
    //2. PROVIDES ACCESS TOKEN FROM REFRESH TOKEN WHEN ACCESS TOKEN IS EXPIRED AND REFRESH TOKEN IS AVAILABLE IN DATABASE
    @AuraEnabled
    public static String doGetAccessTokenFromRefreshToken(){
        if(!retrieveDataFromCustomSetting() & !retrieveDataFromCustomMetadata()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://accounts.google.com/o/oauth2/token');
        String body = 'refresh_token='+refresh_token+
                    '&client_id='+clientId+
                    '&client_secret='+clientSecret+
                    '&grant_type=refresh_token';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('setTimeout','60000');
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        GoogleDriveWrapper.accessDrive authResponse = (GoogleDriveWrapper.accessDrive)GoogleDriveService.accessDrive(parameters);
        if(authResponse == null){
            return null;
        }
        gdi.access_Token__c = authResponse.access_token;
        gdi.expires_in__c = System.now().addSeconds(authResponse.expires_in);
        update gdi;
        return 'SUCCESS';
    }

    //3. PROVIDES ACCESS TOKEN FIRST TIME WHEN NO DATA IS PRESENT IN DATABASE OR IF REFRESH TOKEN IS NULL AFTER FIRST TIME
    @AuraEnabled
    public static String doGetAccessToken(String code){
        if(!retrieveDataFromCustomMetadata()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://accounts.google.com/o/oauth2/token');
        String body = 'code='+code+
                    '&client_id='+clientId+
                    '&client_secret='+clientSecret+
                    '&redirect_uri='+redirect_uri+
                    '&grant_type=authorization_code';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('setTimeout','60000');
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        GoogleDriveWrapper.accessDrive authResponse = (GoogleDriveWrapper.accessDrive)GoogleDriveService.accessDrive(parameters);
        if(authResponse == null){
            return null;
        }
        List<Google_Drive_Integration_Data__c> gdiLst = [SELECT access_Token__c, refresh_token__c, expires_in__c FROM Google_Drive_Integration_Data__c
                                                        WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(!gdiLst.isEmpty()){
        gdi = gdiLst[0]; 
        }else{
            gdi = new Google_Drive_Integration_Data__c();
        }
        gdi.user_email__c = UserInfo.getUserEmail();
        gdi.SetupOwnerId = UserInfo.getUserId();
        gdi.access_Token__c = authResponse.access_token;
        gdi.refresh_token__c = authResponse.refresh_token;
        gdi.expires_in__c = System.now().addSeconds(authResponse.expires_in);
        upsert gdi;
        return 'SUCCESS';
    }

    //4. FUNCTION TO FETCH ROOT FOLDER DATA WHEN COMPONENT IS LOADED FIRST TIME
    @AuraEnabled    
    public static String doFetchDataFirstTime(){
        retrieveDataFromCustomSetting();
        return fetchData('root');
    }

    //5. FUNCTION TO PERFORM CHANGE DIRECTORY ACTION
    @AuraEnabled
    public static string doChangeDirectory(String folderId){
        retrieveDataFromCustomSetting();
        return fetchData(folderId);
    }

    //7. FUNCTION TO DELETE SELECTED FILE
    @AuraEnabled
    public static string deleteSelectedFile(String fileId){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','DELETE');
        parameters.put('setEndpoint','https://www.googleapis.com/drive/v2/files/'+fileId);
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('setTimeout','60000');
        parameters.put('request_type','deleteFile');
        String deleteResponse = (String)GoogleDriveService.accessDrive(parameters);
        if(deleteResponse == null){
            return null;
        }
        return deleteResponse;
    }

    //8. FUNCTION TO UPLOAD A FILE
    @AuraEnabled
    public static string doUploadFile(String fileName, String base64Data, String contentType, String parentFolderID){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        String boundary = '----------9889464542212';
        String delimiter = '\r\n--' + boundary + '\r\n';
        String close_delim = '\r\n--' + boundary + '--';
        //here base64Data is already encoded hence no need to encode this here no need of // String bodyEncoded = EncodingUtil.base64Encode(blobBody); 
        String folder_id = parentFolderID;
        String body = delimiter + 
                    'Content-Type: application/json\r\n\r\n' + 
                    '{ "name" : "' + fileName + '",' +
                    ' "mimeType" : "' + contentType + '",'+ 
                    '"parents":["'+folder_Id+'"] }' + delimiter +      //this parents format is used for v3
                    'Content-Type: ' + contentType + '\r\n' + 
                    'Content-Transfer-Encoding: base64\r\n' + '\r\n' + 
                    base64Data + 
                    close_delim;
        parameters.put('setMethod','POST');
        parameters.put('setBody',body);
        parameters.put('setEndpoint','https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart');
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('content-type', 'multipart/mixed; boundary="' + boundary + '"');
        parameters.put('request_type','fileUpload');
        GoogleDriveWrapper.File fileUploadResponse = (GoogleDriveWrapper.File)GoogleDriveService.accessDrive(parameters);
        if(fileUploadResponse == null){
            return null;
        }
        return '{"name":"'+fileUploadResponse.name+'","id":"'+fileUploadResponse.id+'","type":"'+fileUploadResponse.mimeType+'"}';
    }
}