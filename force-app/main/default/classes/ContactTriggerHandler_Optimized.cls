public class ContactTriggerHandler_Optimized {
    static Map<Double,Contact> conMap;
    static{
        conMap = new Map<Double,Contact>();
    }

    //INSERT HANDLER
    public static void insertRecordHandler(List<Contact> newList){
        //collect parent account ids 
        Set<ID> accountIds = new Set<ID>();
        for(Contact con : newList){
            accountIds.add(con.accountId);
        }

        // this list will contain all related contacts other than in trigger.new
        List<Contact> contacts = [SELECT lastName, Sequence_Number__c , accountId FROM Contact WHERE accountId IN :accountIds ORDER BY Sequence_Number__c];

        // this map will contain previous state of all related contacts other than in trigger.new
        // any change in contacts list will not reflect in this map (don't fill this map via contacts reference it will make shallow copy)
        // change in list will reflect in map
        Map<Id,Contact> oldStateMap = new Map<Id,Contact>([SELECT lastName, Sequence_Number__c , accountId FROM Contact WHERE accountId IN :accountIds]);

        //now add all trigger.new contacts to this contacts list
        contacts.addAll(newList);

        // this map will contain max_range value of each parent 
        Map<ID,Integer> maxRangeMap = new Map<Id,Integer>();
        for(Account acc : [Select Id,(Select id From contacts) From Account WHERE ID IN :accountIds]){
            Integer count = 0; //count will holds related records in trigger.new
            for(Contact con : newList){
                if(con.accountId == acc.id){
                    count++;
                }
            }
            maxRangeMap.put(acc.id, acc.contacts.size() + count);
        } 

        List<Contact> conToUpdate = new List<Contact>();
        for(Id accountId : accountIds){
            List<Contact> remainingContactList = new List<Contact>();
            List<Contact> updatedContactList = new List<Contact>();
            List<Contact> addToLastContactList = new List<Contact>();
            for(Contact con : contacts){
                if(con.accountId == accountId){
                    if(!oldStateMap.containsKey(con.id)){
                    //i.e. contacts are in trigger.new
                        if(con.Sequence_Number__c == null ||
                            con.Sequence_Number__c <= 0 ||
                            con.Sequence_Number__c > maxRangeMap.get(accountId) ||
                            Math.ceil(con.Sequence_Number__c) != Math.floor(con.Sequence_Number__c)){
                            addToLastContactList.add(con);
                        }else{
                            updatedContactList.add(con);
                        }
                    }else{
                        //i.e. previously related contacts
                        remainingContactList.add(con);
                    }
                }
            }
            addToSequenceMap( 'updatedContactList' , updatedContactList , maxRangeMap.get(accountId) , true);
            addToSequenceMap( 'remainingContactList' , remainingContactList , maxRangeMap.get(accountId) , false);
            addToSequenceMap( 'addToLastContactList' , addToLastContactList , maxRangeMap.get(accountId) , false);
            conMap.clear();
            for(Contact con : newList){

            }

            for(Contact con : contacts){
                if(oldStateMap.containsKey(con.id) && con.Sequence_Number__c != oldStateMap.get(con.id).Sequence_Number__c){
                    conToUpdate.add(con);
                }
            }
        }
        if(!conToUpdate.isEmpty())
            CheckRecursive.run = false;
            system.debug('conToUpdate.size() ==>> '+conToUpdate.size());
            system.debug('trigger.new.size() ==>> '+newList.size());
            update(conToUpdate);
            CheckRecursive.run = true;
    }

    public static void addToSequenceMap(String listname, List<Contact> contactList, Integer max_Range , Boolean maintainOriginalSequence){  //TRUE FOR MAINTAINS SEQ
        for(Contact con : contactList){
            if(maintainOriginalSequence == true){
                if(!conMap.containsKey(con.Sequence_Number__c)){
                    conMap.put(con.Sequence_Number__c, con);
                } else {
                    maintainOriginalSequence = insertToMapWithShifting(con, max_Range);
                }
            }else{
                addToMapWithoutMaintainSequence(con);
            }
        }
    }

    
    public static void addToMapWithoutMaintainSequence(Contact con){
        //FILL WITHIN EXISTING SEQUENCE
        Integer sequence = 0;
        while(conMap.containsKey(++sequence));
        con.Sequence_Number__c = sequence;
        conMap.put(sequence, con);
    }

    public static boolean insertToMapWithShifting(Contact con , Integer max_Range){
        //WRITE SHIFTING LOGIC HERE
        Contact prevContact = conMap.get(con.Sequence_Number__c);
        prevContact.Sequence_Number__c = con.Sequence_Number__c + 1;
        if(prevContact.Sequence_Number__c > max_Range){
            addToMapWithoutMaintainSequence(prevContact);
            return false; // false because no need to maintain seq now
        }
        if(!conMap.containsKey(prevContact.Sequence_Number__c)){
            conMap.put(prevContact.Sequence_Number__c, prevContact);
            conMap.put(con.Sequence_Number__c, con);
        }else{
            insertToMapWithShifting(prevContact , max_Range);
            conMap.put(con.Sequence_Number__c, con);
        }
        return true;
    }
}