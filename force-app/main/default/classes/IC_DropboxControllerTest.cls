@isTest
public class IC_DropboxControllerTest {
    @TestSetup
    private static void makeData(){
        Test.startTest();
        DropBox_Integration_Data__c dbi = new DropBox_Integration_Data__c();
        dbi.access_Token__c = 'SAMPLE_ACCESS_TOKEN';
        dbi.user_email__c = UserInfo.getUserEmail();
        database.insert(dbi);
        Dropbox_Integration_Credentials__c dbic = new Dropbox_Integration_Credentials__c();
        dbic.client_id__c = 'SAMPLE_CLIENT_ID';
        dbic.client_secret__c = 'SAMPLE_CLIENT_SECRET';
        database.insert(dbic);
        Test.stopTest();
    }

    //1. TEST IF THERE EXISTS VALID DATA IN CUSTOM SETTING OR NOT
    @isTest
    private static void retrieveDataFromCustomSettingTest(){
        Test.startTest();
        //POSITIVE TEST
        system.assertEquals(null,IC_DropboxController.access_token);
        String redirect_uri = 'https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/dropbox';
        system.assertEquals(redirect_uri,IC_DropboxController.redirect_uri);
        system.assertEquals(null,IC_DropboxController.dbi);
        system.assertEquals(true,IC_DropboxController.retrieveDataFromCustomSetting());
        system.assertEquals('SAMPLE_ACCESS_TOKEN',IC_DropboxController.access_token);
        system.assert(IC_DropboxController.dbi != null);
        //NO RECORD TEST
        database.delete(IC_DropboxController.dbi);
        system.assertEquals(false,IC_DropboxController.retrieveDataFromCustomSetting());
        Test.stopTest();
    }

    //2. TEST IF THERE EXISTS VALID DATA IN CUSTOM METADATA TYPES OR NOT 
    @isTest
    private static void retrieveDataFromCustomMetadataTest(){
        Test.startTest();
        system.assertEquals(null,IC_DropboxController.clientID);
        system.assertEquals(null,IC_DropboxController.clientSecret);
        system.assertEquals(true,IC_DropboxController.retrieveDataFromCustomMetadata());
        system.assert(IC_DropboxController.clientID != null); // this assertion will fail if you have no data in custom settings
        system.assert(IC_DropboxController.clientSecret != null); // this assertion will fail if you have no data in custom settings
        //IF NO RECORDS
        database.delete([SELECT ID FROM Dropbox_Integration_Credentials__c]);
        system.assertEquals(false,IC_DropboxController.retrieveDataFromCustomMetadata());
        Test.stopTest();
    }

    //3. TEST FOR AUTH URI RETRIVAL
    @isTest
    private static void doGetAuthURITest(){
        Test.startTest();
        String expectedOutput = '{"status":"authenticateWithCode",'+
                                '"authURI":"https://www.dropbox.com/oauth2/authorize?'+
                                'response_type=code&client_id=SAMPLE_CLIENT_ID&'+
                                'redirect_uri=https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/dropbox"'+
                                '}';
        system.assertEquals(expectedOutput, IC_DropboxController.doGetAuthURI());
        //WHEN CREDENTIALS DOES NOT EXISTS
        database.delete([SELECT ID FROM Dropbox_Integration_Credentials__c]);
        expectedOutput = '{"status":"CUSTOM_METADATA_CREDENTIAL_NOT_FOUND"}';
        system.assertEquals(expectedOutput, IC_DropboxController.doGetAuthURI());
        Test.stopTest();
    }

    //4. TEST FOR PROCESS WHEN COMPONENT LOADS FIRST TIME
    @isTest
    private static void doInitApexTest(){
        Test.startTest();
        //IF CUSTOM SETTING CREDENTIALS EXISTS
        String expectedOutput = '{"status":"readyToLoadData"}';
        system.assertEquals(expectedOutput, IC_DropboxController.doInitApex());
        system.assertEquals('SAMPLE_ACCESS_TOKEN', IC_DropboxController.access_token);
        //IF CUSTOM SETTING CREDENTIALS NOT EXISTS
        delete(IC_DropboxController.dbi);
        expectedOutput = '{"status":"authenticateWithCode",'+
                         '"authURI":"https://www.dropbox.com/oauth2/authorize?'+
                         'response_type=code&client_id=SAMPLE_CLIENT_ID&'+
                         'redirect_uri=https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/dropbox"'+
                         '}';
        system.assertEquals(expectedOutput, IC_DropboxController.doInitApex());
        Test.stopTest();
    }

    //5. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN NO CLIENT ID AND SECRET ARE PRESENT
    @isTest
    private static void doGetAccessTokenTest_1(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        delete([SELECT Id FROM Dropbox_Integration_Credentials__c]);
        Test.startTest();
        system.assertEquals(null, IC_DropboxController.doGetAccessToken('SAMPLE_CODE'));
        Test.stopTest();
    }

    //6. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN CLIENT ID AND SECRET ARE PRESENT
    @isTest
    private static void doGetAccessTokenTest_2(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        system.assertEquals('SUCCESS', IC_DropboxController.doGetAccessToken('SAMPLE_CODE'));
        system.assertEquals('NEW_ACCESS_TOKEN', IC_DropboxController.dbi.access_Token__c);
        Test.stopTest();
    }

    //7. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN THERE IS NO EXISTING RECORD OF DRIVE INTEGRATION DATA
    @isTest
    private static void doGetAccessTokenTest_3(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        delete([SELECT Id FROM DropBox_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals('SUCCESS', IC_DropboxController.doGetAccessToken('SAMPLE_CODE'));
        system.assert(IC_DropboxController.dbi != null);
        system.assertEquals('NEW_ACCESS_TOKEN', IC_DropboxController.dbi.access_Token__c);
        Test.stopTest();
    }

    //8. TEST FOR GETTING ACCESS TOKEN VIA AUTH CODE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doGetAccessTokenTest_4(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        DropBoxMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_DropboxController.doGetAccessToken('SAMPLE_CODE'));
        Test.stopTest();
    }

    //9. TEST FOR FETCH DATA FIRST TIME
    @isTest
    private static void doFetchDataFirstTimeTest(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        //FETCH DATA TEST FOR ROOT FOLDER
        String expectedOutput = '[{'+
                                '"id":"SAMPLE_ID",'+
                                '"name":"SAMPLE_NAME",'+
                                '"type":"SAMPLE_TYPE",'+
                                '"path_display":"",'+
                                '"isLast":"false"'+
                                '}]';
        system.assertEquals(expectedOutput, IC_DropboxController.doFetchDataFirstTime());
        //FETCH DATA TEST FOR NULL RESPONSE
        DropBoxMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_DropboxController.doFetchDataFirstTime());
        Test.stopTest();
    }

    //10. TEST FOR CHANGING DIRECTORY
    @isTest
    private static void doChangeDirectoryTest(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        String expectedOutput = '[{'+
                                '"id":"SAMPLE_ID",'+
                                '"name":"SAMPLE_NAME",'+
                                '"type":"SAMPLE_TYPE",'+
                                '"path_display":"",'+
                                '"isLast":"false"'+
                                '}]';
        system.assertEquals(expectedOutput, IC_DropboxController.doChangeDirectory('SAMPLE_PARENT_ID'));
        Test.stopTest();
    }

    //11. TEST FOR DELETING SELECTED FILE WHEN ALL INTEGRATION DATA RECORDS DOESNOT EXISTS
    @isTest
    private static void deleteSelectedFileTest_1(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        delete([SELECT Id FROM DropBox_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals(null, IC_DropboxController.deleteSelectedFile('SAMPLE_FILE_PATH'));
        Test.stopTest();
    }

    //12. TEST FOR DELETING SELECTED FILE WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void deleteSelectedFileTest_2(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        system.assertEquals('DELETE_SUCCESSFUL', IC_DropboxController.deleteSelectedFile('SAMPLE_FILE_PATH'));
        Test.stopTest();
    }

    //13. TEST FOR DELETING SELECTED FILE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void deleteSelectedFileTest_3(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        DropBoxMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_DropboxController.deleteSelectedFile('SAMPLE_FILE_PATH'));
        Test.stopTest();
    }

    //14. TEST FOR UPLOAD FILE WHEN ALL INTEGRATION DATA RECORDS DOESNOT EXISTS
    @isTest
    private static void doUploadFileTest_1(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        delete([SELECT Id FROM DropBox_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals(null, IC_DropboxController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLEBASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PATH_DISPLAY'));
        Test.stopTest();
    }

    //15. TEST FOR UPLOAD FILE WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void doUploadFileTest_2(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        String expectedOutput = '{"name":"SAMPLE_FILE_NAME","path_display":"SAMPLE_PATH_DISPLAY"}';
        system.assertEquals(expectedOutput, IC_DropboxController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLEBASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PATH_DISPLAY'));
        Test.stopTest();
    }   

    //16. TEST FOR UPLOAD FILE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void doUploadFileTest_3(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        DropBoxMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_DropboxController.doUploadFile('SAMPLE_FILE_NAME', 'SAMPLEBASE64DATA', 'SAMPLE_CONTENT_TYPE', 'SAMPLE_PATH_DISPLAY'));
        Test.stopTest();
    }

    //17. TEST FOR FILE DOWNLOAD WHEN ALL INTEGRATION DATA RECORDS DOESNOT EXISTS
    @isTest
    private static void doDownloadFileTest_1(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        delete([SELECT Id FROM DropBox_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals(null, IC_DropboxController.doDownloadFile('SAMPLE_FILE_PATH'));
        Test.stopTest();
    }

    //18. TEST FOR DOWNLOAD FILE WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void doDownloadFileTest_2(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        String expectedOutput = 'SAMPLE_FILEDOWNLOAD_LINK';
        system.assertEquals(expectedOutput, IC_DropboxController.doDownloadFile('SAMPLE_FILE_PATH'));
        Test.stopTest();
    }

    //19. TEST FOR DOWNLOAD FILE WHEN NULL RESPONSE IS RECEIVED
    @isTest
    private static void doDownloadFileTest_3(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        DropBoxMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_DropboxController.doDownloadFile('SAMPLE_FILE_PATH'));
        Test.stopTest();
    }

    //20. TEST FOR CREATE FOLDER WHEN ALL INTEGRATION DATA RECORDS DOESNOT EXISTS
    @isTest
    private static void doCreateFolderTest_1(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        delete([SELECT Id FROM DropBox_Integration_Data__c]); //THIS IS TAKEN OUT BCZ DML AND CALLOUT CAN'T BE DONE IN SINGLE TXN
        Test.startTest();
        system.assertEquals(null, IC_DropboxController.doCreateFolder('SAMPLE_FOLDER_PATH'));
        Test.stopTest();
    }

    //21. TEST FOR CREATE FOLDER WHEN ALL INTEGRATION DATA RECORDS EXISTS
    @isTest
    private static void doCreateFolderTest_2(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        String expectedOutput = 'SAMPLE_CREATED_FOLDER_PATH';
        system.assertEquals(expectedOutput, IC_DropboxController.doCreateFolder('SAMPLE_FOLDER_PATH'));
        Test.stopTest();
    }

    //22. TEST FOR CREATE FOLDER WHEN NULL RESPONSE IS RECEIVED
    @isTest
    private static void doCreateFolderTest_3(){
        Test.setMock(HttpCalloutMock.class, new DropBoxMockCallOut());
        Test.startTest();
        DropBoxMockCallOut.responseCode = 400;
        system.assertEquals(null, IC_DropboxController.doCreateFolder('SAMPLE_FOLDER_PATH'));
        Test.stopTest();
    }
}