public with sharing class LightningFileUploadController {
    
    @AuraEnabled
    public static Result createAttchment(String parentId, String attachmentName, String attachmentId, String base64Data, String fileType){
        try{
            ContentVersion attachment;
           if(attachmentId == null){
                attachment = new ContentVersion();
                attachment.VersionData = EncodingUtil.base64Decode(base64Data);
                attachment.PathOnClient = attachmentName;
                attachment.IsMajorVersion = false;
                attachment.FirstPublishLocationId = parentId; 
                insert attachment;
            } else {
                attachment = [SELECT Id, VersionData FROM ContentVersion WHERE Id = :attachmentId];
                String existingFileBody = EncodingUtil.base64Encode(attachment.VersionData);
                String newFileBody = base64Data;
                attachment.versionData = EncodingUtil.base64Decode( existingFileBody + newFileBody );
                update attachment;
            }
            return new Result(true, attachment.Id, null);
        }
        catch(Exception e){
            return new Result(false, null, e.getMessage());
        }
    }

    public class Result{
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public String attachmentId;
        @AuraEnabled public String errorMessage;

        public Result(Boolean isSuccess, String attachmentId, String errorMessage){
            this.isSuccess = isSuccess;
            this.attachmentId = attachmentId;
            this.errorMessage = errorMessage;
        }
    }
}