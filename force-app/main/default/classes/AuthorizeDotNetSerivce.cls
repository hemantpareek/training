global class AuthorizeDotNetSerivce {
    public static Object accessService(Map<String, String> parameters){
        Httprequest req = new Httprequest();
        if(parameters.get('setMethod') != null){
            req.setMethod(parameters.get('setMethod'));
        }
        if(parameters.get('setEndpoint') != null){
            req.setEndpoint(parameters.get('setEndpoint'));
        }
        if(parameters.get('content-type') != null){
            req.setHeader('content-type',parameters.get('content-type'));
        }
        if(parameters.get('setBody') != null){
            req.setBody(parameters.get('setBody'));
        }
        if(parameters.get('content-length') != null){
            req.setHeader('content-length',parameters.get('content-length'));
        }
        if(parameters.get('setTimeout') != null){
            req.setTimeout(Integer.valueOf(parameters.get('setTimeout')));
        }

        // system.debug('method  ==>> '+req.getMethod());
        // system.debug('endpoint  ==>> '+req.getEndpoint());
        // system.debug('content type ==>> '+req.getHeader('content-type'));
        // system.debug('authorization ==>> '+req.getHeader('Authorization'));
        // system.debug('contant-length ==>> '+req.getHeader('content-length'));
        // system.debug('body ==>> '+req.getBody());

        Http h = new Http();
        httpResponse res = h.send(req);
        if(res.getStatusCode() == 200){
            system.debug(res.getBody());
            return parseAuthResponse(res.getBody().remove(String.fromCharArray(new List<Integer>{65279})));
        }else{
            return null;
        }
    }

    public static Object parseAuthResponse(String json) {
        return System.JSON.deserialize(json, AuthorizeDotNetWrapper.class);
	}
}