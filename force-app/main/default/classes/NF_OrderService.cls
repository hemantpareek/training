public class NF_OrderService {
    public static Map<Integer,String> monthNameMap=new Map<Integer, String>{1 =>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May',
    6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep',10=>'Oct',
    11=>'Nov', 12=>'Dec'};

    public class OrderResponse{
        public OrderDate orderDate;
        public String orderNumber;
        public String orderDateTime;
        public String formattedDate;
        public OrderSummary orderSummary;
        Public String orderType;
        public String orderStatus;
        public StatusDate statusDate;
        public Boolean isLegacyOrder;
        public  List<Payment> payments;
        public List<Shipment> shipments;
        public String status;
        public String getFormattedOrderDate() {
            return NF_OrderService.monthNameMap.get(integer.valueOf(orderDate.month)) + ' ' + orderDate.day;
            // return orderDate.month + '/' + orderDate.day + '/' + orderDate.year;
        }
    }
    public class Method{
      public String code;
      public String title;
      public String estimatedDeliveryDate;
    }
    public class Shipment{
        public List<LineItem> lineItems;
        public Method method;
        public List<Payment> payments;
        public String status;
        public StatusDate statusDate;
        public ShipmentSummary shipmentSummary;
        public Address address;
        public String carrier;
        public String trackingNumber;
        public String shipmentTrackingURL;
        public String shipmentType;
    }
    public class Price{
        Public UnitPrice unitPrice;
        public TotalPrice totalPrice;
        public String priceType;
    }
    public class TotalPrice{
        public String originalPrice;
    }
    public class UnitPrice{
        public String originalPrice;
    }
    public class Quantity{
        public integer selected;
    }
    public class LineItem{
        public String family;
        public String variantId;
        public Boolean isAvailable;
        public String status;
        public StatusDate statusDate;
        public Quantity quantity;
        public String productImage;
        public Price price;
        //public List<Attribute> attributes;
        public Attribute attributes;
    }   
    public class Attribute{
        public Choice choice;
        public Size1 size1;
        public Size2 size2;
        public String to;
        public String fromAt;
        public String amount;
        public String email;
        public String message;
        public SendDate  sendDate;
    }
    public class SendDate{
        public String month;
        public String day;
        public String year;
    }
    public class Size2{
    }
    public class Size1{
        public string label;
        public String value;
    }
    public class Choice{
        public String value;
    }
    public class Address{
        public String streetAddress1;
        public String addressId;
        public String firstName;
        public String lastName;
    }
    public class ShipmentSummary{
        public String merchandiseSubTotal;
        public String orderTotal;
        public String salesTax;
        public String shippingHandling;
        public Boolean isRefundTotal;
    }
    public class StatusDate{
        public String month;
        public String day;
        public String year;
    }
    public class Payment{
        Public string paymenttype;
        public String paymentnumber;
        public String amount;       
    }   
    public class OrderSummary{
        public List<AppliedOffer> appliedOffers;
        public String merchandiseSubTotal;
        public String orderTotal;
        public String salesTax;
        public String shippingHandling;
        public Boolean isRefundTotal;
    }
    public class AppliedOffer{
        public String code;
        public String amount;
    }
    public class OrderDate{
        public String month;
        public String day;
        public String year;
    }
    public class OrderRequest{
        public String email;
        public String orderId;
        public String activeCountry;
    } 
    
   
}