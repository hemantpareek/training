public with sharing class LightningDataTableController {
    @AuraEnabled
    public static String initHandlerApex(String obj, List<String> fields){
        List<Column> columns = generateColumns(obj, fields);
        List<Row> rows = generateRows(obj, columns);
        return JSON.serialize(new SobjectWrapper(columns, rows));
    }

    private static List<Column> generateColumns(String obj, List<String> fields){
        Map<String, Schema.sObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.DescribeSObjectResult sObjectResult = schemaMap.get(obj).getDescribe();
        Map <String, Schema.SObjectField> fieldMap = sObjectResult.fields.getMap();
        Schema.DescribeFieldResult dfr;
        List<Column> columns = new List<Column>();
        for(String fld : fields){
            if(fieldMap.containsKey(fld)){
                dfr = fieldMap.get(fld).getDescribe();
                columns.add(new Column( dfr.getLabel(), dfr.getName(), dfr.getType(), dfr.getRelationshipName(), dfr.isNameField() ));
            }
        }
        return columns;
    }

    private static List<Row> generateRows(String obj, List<column> columns){
        List<Row> rows = new List<Row>();
        String commaSeparatedFields = '';
        for(column col : columns){
            commaSeparatedFields += col.apiName + ',';
        }
        commaSeparatedFields = commaSeparatedFields.removeEnd(',');
        String query = 'Select ' + commaSeparatedFields + ' FROM ' + obj;
        List<SObject> records = Database.query(query);
        Map<String,String> lookupMap = getLookupNames( getLookupObjects(columns), records );
        for(sobject rcd : records){
            List<Cell> cells = new List<Cell>();
            for(column col : columns){
                String fieldValue = '' + rcd.get(col.apiName);
                if(fieldValue != 'null'){
                    if(col.type == 'REFERENCE' && lookupMap.containsKey(fieldValue)){
                        cells.add(new cell( lookupMap.get(fieldValue) , fieldValue , col.isNameField, col.type));
                    } else {
                        cells.add(new cell( fieldValue , fieldValue , col.isNameField, col.type));
                    }
                } else {
                    cells.add(new cell( ' ' , ' ', col.isNameField, col.type));
                }
            }
            rows.add(new row(cells, rcd.id));
        }
        return rows;
    }

    private static List<lookup> getLookupObjects(List<column> columns){
        List<lookup> lookups = new List<lookup>();
        for(Column col : columns){
            if(col.type == 'REFERENCE'){
                if(col.relationshipName == 'Owner'){
                    col.relationshipName = 'User';
                }
                lookups.add(new lookup(col.apiName, col.relationshipName));
            }
        }
        return lookups;
    }

    private static Map<String,String> getLookupNames(List<lookup> lookups, List<sobject> records){
        Map<String,Set<String>> lookupMap = new Map<String, Set<String>>();
        for(Sobject rcd : records){
            for(lookup lkp : lookups){
                if(!lookupMap.containsKey(lkp.lookupObject)){
                    lookupMap.put(lkp.lookupObject, new Set<String>());
                }else{
                    lookupMap.get(lkp.lookupObject).add(String.valueOf(rcd.get(lkp.fieldName)));
                }
            }
        }
        Map<String,String> result = new Map<String,String>();
        for(String objName : lookupMap.keySet()){
            set<String> ids = lookupMap.get(objName);
            try{
                List<sobject> lkpRcds = Database.query('SELECT Name FROM ' + objName + ' WHERE ID IN :ids');
                for(SObject sobj : lkpRcds){
                    if(!result.containsKey(sobj.id)){
                        result.put(sobj.id, String.valueOf(sobj.get('Name')));
                    }
                }
            } catch (Exception e){
              
            }
        }
        return result;
    }

    public class SobjectWrapper{
        public List<column> columns;
        public List<row> rows;
        public SobjectWrapper(List<column> columns, List<row> rows){
            this.columns = columns;
            this.rows = rows;
        }
    }

    public class Column{
        public String label;
        public String apiName;
        public String type;
        public String relationshipName;
        public Boolean isNameField;
        public Column(String label, String apiName, Schema.DisplayType type, String relationshipName, Boolean isNameField){
            this.label = label;
            this.apiName = apiName;
            this.type = '' + type;
            this.relationshipName = relationshipName;
            this.isNameField = isNameField;
        }
    }

    public class Row{
        public string id;
        public List<Cell> cells;
        public Row(List<Cell> cells, String id){
            this.cells = cells;
            this.id = id;
        }
    }

     public class Cell{
        public String label;
        public String value;
        public String type;
        public Boolean isNameField;
        public Cell(String label, String value, Boolean isNameField, string type){
            this.label = label;
            this.value = value;
            this.isNameField = isNameField;
            this.type = '' + type;
        }
    }

    public class Lookup{
        public string fieldName;
        public string lookupObject;
        public Lookup(String fieldName, String lookupObject){
            this.fieldName = fieldName;
            this.lookupObject = lookupObject;
        }        
    }
}