public class DropBoxIntegrationController {
    @testVisible String access_token;                                       //variable to store access token
    @testVisible DropBox_Integration_Data__c dbi;                           //instance of custom object in org that will contain access token,refresh token and expiry time
    public DropBoxWrapper.File currentFolder{get;set;}                  //this varible will hold the folder that is currently open
    public List<DropBoxWrapper.File> currentFolderDirectories{get;set;} //holds folders contained by current folder
    public List<DropBoxWrapper.File> currentFolderFiles{get;set;}       //holds files contained by current folder
    public Map<String,DropBoxWrapper.File> fileMap{get;set;}            //filemap stores folders in <id,File> formatted map
    public List<String> breadCrumbsList{get;set;}                           //this list holds the list of breadcrumbs (folder ids)
    public boolean isAuthenticated{get;set;}                                //once autheticated then this variable's value will set to true
    public DropBoxWrapper.FileUpload fileToupload{get;set;}             //holds the parameters of file to uploded to google drive
    public String fileDownloadLink{get;set;}                                //this will hold the download link for downloadable files after clicking on file name
    @testVisible String client_id;                                          // this will hold the client id;
    @testVisible String client_secret; 
    @testVisible String redirect_uri = 'https://c.ap15.visual.force.com/apex/DropBoxIntegration?sfdc.tabName=01r2v000000ifu4';

    public DropBoxIntegrationController() {
        isAuthenticated = false;
        breadCrumbsList = new List<String>();
        fileToupload = new DropBoxWrapper.FileUpload();
        // retrieval of client id and client secret from custom metadatatype
        List<Dropbox_Integration_Credentials__c> i = [SELECT client_id__c , client_secret__c FROM Dropbox_Integration_Credentials__c LIMIT 1]; 
        if(i.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Credentials not found'));
            return;    
        }
        client_id = i[0].client_id__c;
        client_secret = i[0].client_secret__c;
        fileMap = new Map<String,DropBoxWrapper.File>();
        DropBoxWrapper.File f = new DropBoxWrapper.File();
        f.id = '';
        f.path_display = '';
        f.name = 'Home';
        f.type = 'folder';
        fileMap.put(f.id,f);
    }

    //1. method to create google drive data (access token, refresh token) record if there is not any
    public PageReference pageAction(){
        List<DropBox_Integration_Data__c> dbiLst = [SELECT access_Token__c FROM DropBox_Integration_Data__c	
                                                    WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(!dbiLst.isEmpty()){
            dbi = dbiLst[0];
            access_token = dbi.access_Token__c;
            init();
        } else {
            String code = ApexPages.CurrentPage().getParameters().get('code');
            if(code != null && code != ''){ //if code is null of empty then no further processing will take place
                Map<String,String> res = getAccessToken();
                dbi = new DropBox_Integration_Data__c();
                dbi.user_email__c = UserInfo.getUserEmail();
                dbi.access_Token__c = res.get('access_token');
                dbi.SetupOwnerId = UserInfo.getUserId();
                insert dbi;
                return new PageReference(ApexPages.CurrentPage().getUrl());
                
            } else {
                return new PageReference(DropBoxService.authenticate(client_id, redirect_uri));
            }
        }
        return null;
    }

    public Map<String,String> getAccessToken(){
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropbox.com/1/oauth2/token');
        String body = 'code='+ApexPages.CurrentPage().getParameters().get('code')+
                      '&client_id='+client_id+
                      '&client_secret='+client_secret+
                      '&redirect_uri='+redirect_uri+
                      '&grant_type=authorization_code';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('Content-length',String.valueOf(body.length()));
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        Map<String,String> res = new Map<String,String>();
        DropBoxWrapper.accessDrive authResponse = (DropBoxWrapper.accessDrive)DropBoxService.accessDrive(parameters,null);
        res.put('access_token',authResponse.access_token);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'Access token retrieved'));
        return res;
    }

    //5. this will perform initial processing (run only once after authetication)
    public void init(){
        DropBoxWrapper.File f = new DropBoxWrapper.File();
        f.id = '';
        f.name = 'Home';
        f.path_display = '';
        f.type = 'folder';
        currentFolder = f;
        updateFileMap(f.path_display);
        fetchData(f);
        isAuthenticated = true;
    }

    //6. updates fileMap including lastest updated records in google drive
    public void updateFileMap(String folderPath){
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropboxapi.com/2/files/list_folder');
        parameters.put('content-type','application/json');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        String body = '{'+
                       ' "path": "'+folderPath+'",'+   //path is empty bcz we want all folders/files
                       ' "recursive": false,'+
                       ' "include_media_info": false,'+
                       ' "include_deleted": false,'+
                       ' "include_has_explicit_shared_members": false,'+
                       ' "include_mounted_folders": true,'+
                       ' "include_non_downloadable_files": true'+
                       ' }';
        parameters.put('setBody',body);
        parameters.put('request_type','fetchData');
        DropBoxWrapper fetchDataResponse = (DropBoxWrapper)DropBoxService.accessDrive(parameters,null);
        if(fetchDataResponse == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Null response received / Access token expired.'));    
            return;
        }
        for(DropBoxWrapper.File file : fetchDataResponse.entries){
            fileMap.put(file.id,file);
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'FileMap update Successful'));
        // for(String key : fileMap.keySet()){
        //     ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,''+filemap.get(key)));
        // }
    }

    //7. function to fetch files and folders from google drive from curretly selected folder
    public void fetchData(DropBoxWrapper.File currentFolder){  
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropboxapi.com/2/files/list_folder');
        parameters.put('content-type','application/json');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        String body = '{'+
                       ' "path": "'+currentFolder.path_display+'",'+   //path is empty bcz we want all folders/files
                       ' "recursive": false,'+
                       ' "include_media_info": false,'+
                       ' "include_deleted": false,'+
                       ' "include_has_explicit_shared_members": false,'+
                       ' "include_mounted_folders": true,'+
                       ' "include_non_downloadable_files": true'+
                       ' }';
        parameters.put('setBody',body);
        parameters.put('request_type','fetchData');
        DropBoxWrapper fetchDataResponse = (DropBoxWrapper)DropBoxService.accessDrive(parameters,null);
        if(fetchDataResponse == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Null response received / Access token expired.'));    
            return;
        }
        currentFolderDirectories = new List<DropBoxWrapper.File>();
        currentFolderFiles = new List<DropBoxWrapper.File>();
        for(DropBoxWrapper.File f : fetchDataResponse.entries){
            if(f.type == 'folder'){
                currentFolderDirectories.add(f);
            }else{
                currentFolderFiles.add(f);
            }
        }
        addToBreadCrumbsList(currentFolder);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'FetchData Successful : '+currentFolder.name));
    }

    //8. function to manage and add folders to breadCrumb list
    public void addToBreadCrumbsList(DropBoxWrapper.File fldr){
        for(String folderId : breadCrumbsList){
            fileMap.get(folderId).isLast = false;
        }
        fileMap.get(fldr.id).isLast = true;
        if(!breadCrumbsList.contains(fldr.id)){
            breadCrumbsList.add(fldr.id);
        }
    }

     //9. function to goto selected folder on breadcrumb and update list in accordance with that action
    public void performBreadCrumbAction(){
        String breadCrumbSelectedFolderId = ApexPages.CurrentPage().getParameters().get('folderId');
        Integer selectedFolderIndex = breadCrumbsList.indexOf(breadCrumbSelectedFolderId);
        List<String> tempList = new List<String>();
        for(Integer i=0; i<selectedFolderIndex; i++){
            fileMap.get(breadCrumbsList[i]).isLast = false;
            tempList.add(breadCrumbsList[i]);
        }
        breadCrumbsList = tempList;
        DropBoxWrapper.File f = new DropBoxWrapper.File();
        f.id = breadCrumbSelectedFolderId;
        f.name = fileMap.get(f.id).name;
        f.path_display = fileMap.get(f.id).path_display;
        f.isLast = true;
        currentFolder = f;
        fetchData(f);
    }

    //10. function to open selected directory on UI
    public void changeCurrentDirectory(){
        String folderId = ApexPages.CurrentPage().getParameters().get('selectedFolderId');
         updateFileMap(fileMap.get(folderId).path_display);
        currentFolder = fileMap.get(folderId);
        fetchData(currentFolder);
    }

    //12. function to refresh current page contents
    public void refreshPage(){
        updateFileMap(currentFolder.path_display);
        fetchData(currentFolder);
    }
   
    //13. function to download clicked file from ui
    public void downloadFile(){
        String fileId = ApexPages.CurrentPage().getParameters().get('fileId');   
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropboxapi.com/2/files/get_temporary_link');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        parameters.put('content-type','application/json');
        parameters.put('setBody','{"path":"'+fileMap.get(fileId).path_display+'"}');
        parameters.put('request_type','downloadFile');
        DropBoxWrapper.FileDownload fd  = (DropBoxWrapper.FileDownload)DropBoxService.accessDrive(parameters,null);
        if(fd == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Null response received / Access token expired.'));    
            return;
        }
        fileDownloadLink = fd.link; 
    }

    //14. function to upload file to google drive
    public void uploadFile(){
        if(fileToupload != null && fileToupload.body != null && fileToupload.name != null){
            System.debug('body ---------------------------------: '+EncodingUtil.base64Encode(fileToupload.body));
            Map<String,String> parameters = new Map<String,String>();
            parameters.put('setMethod','POST');
            parameters.put('setEndpoint','https://content.dropboxapi.com/2/files/upload');
            parameters.put('Authorization','Bearer'+' '+ access_token);
            String parameter = '{'+
                                   ' "path": "'+currentFolder.path_display+'/'+fileToupload.name+'",'+
                                   ' "mode": "add",'+
                                   ' "autorename": true,'+
                                   ' "mute": false,'+
                                   ' "strict_conflict": false'+
                                '}';
            parameters.put('Dropbox-API-Arg',parameter);
            parameters.put('content-type','application/octet-stream');
            parameters.put('request_type','uploadFile');
            DropBoxWrapper.File FileUploadResponse = (DropBoxWrapper.File)DropBoxService.accessDrive(parameters,fileToupload.body);    
            if(FileUploadResponse == null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'null response received'));    
                    return;
                }
                // ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'File uploaded successfully : '+FileUploadResponse.entries[0].name));
                refreshPage();
                fileToupload = new DropBoxWrapper.FileUpload();
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'No file uploaded'));
        }
   }    

}