public class ContactStatusUpdaterBatchScheduler implements Schedulable{
   public void execute(SchedulableContext sc){
       System.debug('fired scheduler');
       database.executeBatch(new ContactStatusUpdaterBatch());
   }
}