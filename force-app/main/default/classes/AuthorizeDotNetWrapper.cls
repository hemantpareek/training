public class AuthorizeDotNetWrapper {
    //------------------------------------------------------------WRAPPER TO PARSE AUTH RESPONSE--------------------------------------------------------------------------
    public messages messages;
    public class messages{
        public String resultCode;
        public List<message> message;
    }

    public class message{
        public string code;
        public string text;
    }

    //--------------------------------------------------------------WRAPPER TO PARSE CART ITEMS----------------------------------------------------------------------------
    public List<CartItem> cartItems;
    public class CartItem{
        public String name;
        public string id;
        public string description;
        public integer quantity;
        public Decimal total;
        public Decimal unitPrice;
    }
}