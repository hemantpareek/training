public class BoxDotComIntegrationController {
    @testVisible String access_token;                                       //variable to store access token
    @testVisible String refresh_token;                                      //variable to store refresh token
    @testVisible Box_com_Integration_Data__c bdci;                           //instance of custom object in org that will contain access token,refresh token and expiry time
    public BoxDotComIntegrationWrapper.File currentFolder{get;set;}                  //this varible will hold the folder that is currently open
    public List<BoxDotComIntegrationWrapper.File> currentFolderDirectories{get;set;} //holds folders contained by current folder
    public List<BoxDotComIntegrationWrapper.File> currentFolderFiles{get;set;}       //holds files contained by current folder
    public Map<String,BoxDotComIntegrationWrapper.File> fileMap{get;set;}            //filemap stores folders in <id,File> formatted map
    public List<String> breadCrumbsList{get;set;}                           //this list holds the list of breadcrumbs (folder ids)
    public boolean isAuthenticated{get;set;}                                //once autheticated then this variable's value will set to true
    public BoxDotComIntegrationWrapper.FileUpload fileToupload{get;set;}             //holds the parameters of file to uploded to google drive
    public String fileDownloadLink{get;set;}                                //this will hold the download link for downloadable files after clicking on file name
    @testVisible String client_id;                                          // this will hold the client id;
    @testVisible String client_secret; 
    @testVisible String redirect_uri;
    public DateTime expires_in{get;set;}

    public BoxDotComIntegrationController() {
        isAuthenticated = false;
        breadCrumbsList = new List<String>();
        fileToupload = new BoxDotComIntegrationWrapper.FileUpload();
        // retrieval of client id and client secret from custom metadatatype
        List<Box_Com_Integration_Credentials__c> i = [SELECT client_id__c , client_secret__c, redirect_uri__c FROM Box_Com_Integration_Credentials__c LIMIT 1]; 
         if(i.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Credentials not found'));
            return;    
        }
        client_id = i[0].client_id__c;
        client_secret = i[0].client_secret__c;
        redirect_uri = i[0].redirect_uri__c;
        fileMap = new Map<String,BoxDotComIntegrationWrapper.File>();
        BoxDotComIntegrationWrapper.File f = new BoxDotComIntegrationWrapper.File();
        f.id = '0';
        f.name = 'Home';
        f.type = 'folder';
        fileMap.put(f.id,f);
    }

    //1. method to create google drive data (access token, refresh token) record if there is not any
    public PageReference pageAction(){
        List<Box_com_Integration_Data__c> bdciLst = [SELECT access_Token__c , refresh_token__c , expires_in__c FROM Box_com_Integration_Data__c
                                                    WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(!bdciLst.isEmpty()){
            bdci = bdciLst[0];
            refresh_token = bdci.refresh_token__c;
            expires_in = bdci.expires_in__c;
            if(bdci.expires_in__c > System.now()){
                access_token = bdci.access_Token__c;
                init();
            } else {
                if(refresh_token != null){
                    getAccessTokenFromRefreshToken(); //called when access token is expired
                }else{
                    Map<String,String> res = getAccessToken();
                    bdci.access_Token__c = res.get('access_token');
                    bdci.expires_in__c = System.now().addSeconds(Integer.valueOf(res.get('expires_in')));
                    update bdci;
                }
                    return new PageReference(ApexPages.CurrentPage().getUrl());
            }
        } else {
            String code = ApexPages.CurrentPage().getParameters().get('code');
            if(code != null && code != ''){ //if code is null of empty then no further processing will take place
                bdci = new Box_com_Integration_Data__c();
                Map<String,String> res = getAccessToken();
                bdci.user_email__c = UserInfo.getUserEmail();
                bdci.access_Token__c = res.get('access_token');
                bdci.refresh_token__c = res.get('refresh_token');
                bdci.expires_in__c = System.now().addSeconds(Integer.valueOf(res.get('expires_in')));
                bdci.SetupOwnerId = UserInfo.getUserId();
                insert bdci;
                return new PageReference(ApexPages.CurrentPage().getUrl());
            } else {
                Map<String,String> Parameters = new Map<String,String>();
                Parameters.put('client_id',client_id);
                Parameters.put('redirect_uri',redirect_uri);
                return BoxDotComIntegrationService.authenticate(Parameters);
            }
        }
        return null;
    }

    public Map<String,String> getAccessToken(){
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://app.box.com/api/oauth2/token');
        String body = 'code='+ApexPages.CurrentPage().getParameters().get('code')+
                      '&client_id='+client_id+
                      '&client_secret='+client_secret+
                      '&redirect_uri='+redirect_uri+
                      '&grant_type=authorization_code';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('Content-length',String.valueOf(body.length()));
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        Map<String,String> res = new Map<String,String>();
        BoxDotComIntegrationWrapper.AccessBoxDotCom authResponse = (BoxDotComIntegrationWrapper.AccessBoxDotCom)BoxDotComIntegrationService.accessBoxDotComDrive(parameters,null);
        res.put('access_token',authResponse.access_token);
        res.put('refresh_token',authResponse.refresh_token);
        res.put('expires_in',String.valueOf(authResponse.expires_in));
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'Access token retrieved'));
        return res;
    }


     //3. method to get access token from refresh token when access token is expired
    public void getAccessTokenFromRefreshToken(){
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://app.box.com/api/oauth2/token');
        String body = 'refresh_token='+refresh_token+
                      '&client_id='+client_id+
                      '&client_secret='+client_secret+
                      '&grant_type=refresh_token';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('Content-length',String.valueOf(body.length()));
        BoxDotComIntegrationWrapper.AccessBoxDotCom authResponse = (BoxDotComIntegrationWrapper.AccessBoxDotCom)BoxDotComIntegrationService.accessBoxDotComDrive(parameters,null);
        if(authResponse == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Null response received / refresh token expired.'));    
            return;
        }
        bdci.access_Token__c = authResponse.access_token;
        bdci.refresh_Token__c = authResponse.refresh_token;
        bdci.expires_in__c = System.now().addSeconds(authResponse.expires_in);
        update bdci;
    }


    //5. this will perform initial processing (run only once after authetication)
    public void init(){
        BoxDotComIntegrationWrapper.File f = new BoxDotComIntegrationWrapper.File();
        f.id = '0';
        f.name = 'Home';
        f.type = 'folder';
        currentFolder = f;
        updateFileMap(f.id);
        fetchData(f);
        isAuthenticated = true;
    }

    //6. updates fileMap including lastest updated records in google drive
    public void updateFileMap(String folderId){
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','GET');
        parameters.put('setEndpoint','https://api.box.com/2.0/folders/'+folderId+'/items');
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        parameters.put('Accept','application/json');
        parameters.put('request_type','fetchData');
        BoxDotComIntegrationWrapper fetchDataResponse = (BoxDotComIntegrationWrapper)BoxDotComIntegrationService.accessBoxDotComDrive(parameters,null);
        if(fetchDataResponse == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Null response received / Access token expired.'));    
            return;
        }
        for(BoxDotComIntegrationWrapper.File file : fetchDataResponse.entries){
            fileMap.put(file.id,file);
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'FileMap update Successful'));
        // for(String key : fileMap.keySet()){
        //     ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,''+filemap.get(key).name));
        // }
    }

    //7. function to fetch files and folders from google drive from curretly selected folder
    public void fetchData(BoxDotComIntegrationWrapper.File currentFolder){  
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','GET');
        parameters.put('setEndpoint','https://api.box.com/2.0/folders/'+currentFolder.id+'/items'); //End point for List of Files in Folder
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('Accept','application/json');
        parameters.put('request_type','fetchData');
        BoxDotComIntegrationWrapper fetchDataResponse = (BoxDotComIntegrationWrapper)BoxDotComIntegrationService.accessBoxDotComDrive(parameters,null);
        if(fetchDataResponse == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Null response received / Access token expired.'));    
            return;
        }
        currentFolderDirectories = new List<BoxDotComIntegrationWrapper.File>();
        currentFolderFiles = new List<BoxDotComIntegrationWrapper.File>();
        for(BoxDotComIntegrationWrapper.File f : fetchDataResponse.entries){
            if(f.type == 'folder'){
                currentFolderDirectories.add(f);
            }else{
                currentFolderFiles.add(f);
            }
        }
        addToBreadCrumbsList(currentFolder);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'FetchData Successful : '+currentFolder.name));
    }

    //8. function to manage and add folders to breadCrumb list
    public void addToBreadCrumbsList(BoxDotComIntegrationWrapper.File fldr){
        for(String folderId : breadCrumbsList){
            fileMap.get(folderId).isLast = false;
        }
        fileMap.get(fldr.id).isLast = true;
        if(!breadCrumbsList.contains(fldr.id)){
            breadCrumbsList.add(fldr.id);
        }
    }

     //9. function to goto selected folder on breadcrumb and update list in accordance with that action
    public void performBreadCrumbAction(){
        String breadCrumbSelectedFolderId = ApexPages.CurrentPage().getParameters().get('folderId');
        Integer selectedFolderIndex = breadCrumbsList.indexOf(breadCrumbSelectedFolderId);
        List<String> tempList = new List<String>();
        for(Integer i=0; i<selectedFolderIndex; i++){
            fileMap.get(breadCrumbsList[i]).isLast = false;
            tempList.add(breadCrumbsList[i]);
        }
        breadCrumbsList = tempList;
        BoxDotComIntegrationWrapper.File f = new BoxDotComIntegrationWrapper.File();
        f.id = breadCrumbSelectedFolderId;
        f.name = fileMap.get(f.id).name;
        f.isLast = true;
        currentFolder = f;
        fetchData(f);
    }

    //10. function to open selected directory on UI
    public void changeCurrentDirectory(){
        String folderId = ApexPages.CurrentPage().getParameters().get('selectedFolderId');
         updateFileMap(folderId);
        currentFolder = fileMap.get(folderId);
        fetchData(currentFolder);
    }

    //11. function to goto home directory
    public void home(){
        breadCrumbsList.clear();
        BoxDotComIntegrationWrapper.File f = new BoxDotComIntegrationWrapper.File();
        f.Name = 'Home';
        f.id = '0';
        currentFolder = f;
        refreshPage();
    }

    //12. function to refresh current page contents
    public void refreshPage(){
        updateFileMap(currentFolder.id);
        fetchData(currentFolder);
    }
   
    //13. function to download clicked file from ui
    public void downloadFile(){
        String fileId = ApexPages.CurrentPage().getParameters().get('fileId');
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','GET');
        parameters.put('setEndpoint','https://api.box.com/2.0/files/'+fileId+'/content');
        parameters.put('content-type','application/json');
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('Accept','application/json');
        parameters.put('request_type','fileDownload');
        Object fd = BoxDotComIntegrationService.accessBoxDotComDrive(parameters,null);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,''+fd));    
        if(fd == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Null response received / Access token expired.'));    
            return;
        }
        fileDownloadLink = fd.toString(); 
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'File downloaded successfully :'+fileMap.get(fileId).name));
    }

    //14. function to upload file to google drive
    public void uploadFile(){
        if(fileToupload != null && fileToupload.body != null && fileToupload.name != null){
            blob base64EncodeFile=base64EncodeFileContent(fileToupload.body,fileToupload.name);
            String boundary = '----------------------------741e90d31eff';
    
            Map<String,String> parameters = new Map<String,String>();
            parameters.put('setMethod','POST');
            parameters.put('setEndpoint','https://upload.box.com/api/2.0/files/content?parent_id='+currentFolder.id);
            parameters.put('Authorization', 'Bearer ' + access_Token);
            parameters.put('content-length',String.valueof(base64EncodeFile.size()));
            parameters.put('content-type','multipart/form-data; boundary='+boundary);
            BoxDotComIntegrationWrapper FileUploadResponse = (BoxDotComIntegrationWrapper)BoxDotComIntegrationService.accessBoxDotComDrive(parameters,base64EncodeFile);    
            if(FileUploadResponse == null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'null response received'));    
                    return;
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,'File uploaded successfully : '+FileUploadResponse.entries[0].name));
                refreshPage();
                fileToupload = new BoxDotComIntegrationWrapper.FileUpload();
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'No file uploaded'));
        }
   }    
   
   //reference :http://blog.enree.co/2013/01/salesforce-apex-post-mutipartform-data.html
    public blob base64EncodeFileContent(Blob file_body, String file_name){
        String boundary = '----------------------------741e90d31eff';
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+file_name+'";\nContent-Type: application/octet-stream';
        String footer = '--'+boundary+'--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('='))
        {
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }
        String bodyEncoded = EncodingUtil.base64Encode(file_body);
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
 
        if(last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        } else if(last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0,3) + 'N';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            footer = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
        } else {
            footer = '\r\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
        }
 
        return bodyBlob;
    }
}