public with sharing class ContactTriggerHandler_V3 {

    private static Map<Id, AccountWrapper> accountWrapperByAccountId = new Map<Id,AccountWrapper>();
    public static Boolean fireTrigger = true;
    
    public static void handleUpdate(List<Contact> oldList, List<Contact> newList, Map<Id,Contact> oldMap, Map<Id,Contact> newMap){
        fetchLagacyRecords(newList);
        fetchLagacyRecords(oldList); // To handle reparented records
        List<Contact> contactsToUpdate = new List<Contact>();
        for(String accountId : accountWrapperByAccountId.keySet()){
            AccountWrapper aw = accountWrapperByAccountId.get(accountId);
            for(Contact con : newList){
                if(con.AccountId == accountId){
                    if( con.Sequence_Number__c == null || con.Sequence_Number__c <= 0 || Math.ceil(con.Sequence_Number__c) != Math.floor(con.Sequence_Number__c)){
                        //invalid sequence to be at second priority
                        aw.invalidSequenceContacts.add(con);
                    } else if(con.AccountId != oldMap.get(con.Id).AccountId ){
                        //reparented will be at third priority
                        aw.reparentedContacts.add(con);
                    } else if(con.Sequence_Number__c != oldMap.get(con.Id).Sequence_Number__c){
                        //actual updated will be at first priority
                        aw.validSequenceContacts.add(con);
                    }
                }
            }
            aw.updateContacts();
            contactsToUpdate.addAll(aw.getContactsToUpdate(newMap));
        }                
        System.debug('@@@ contactsToUpdate.size() : ' + contactsToUpdate.size());
        if(!contactsToUpdate.isEmpty()){
            fireTrigger = false;
            update contactsToUpdate;
            fireTrigger = true;
        }
    }

    public static void fetchLagacyRecords(List<Contact> newList){
        System.debug('@@@ fetch lagacy records');
        Set<Id> accountIds = new Set<Id>();
        for(Contact con : newList){
            accountIds.add(con.accountId);
        }
        //fill lagacy records
        List<Contact> lagacyRecords = [SELECT LastName, Sequence_Number__c, AccountId FROM Contact
                                      WHERE AccountId IN :accountIds AND AccountId NOT IN :accountWrapperByAccountId.keySet() ORDER BY Sequence_Number__c NULLS LAST];
        if(!lagacyRecords.isEmpty()){
            for(Id accountId : accountIds){
                if(!accountWrapperByAccountId.containsKey(accountId)){
                    List<Contact> contactsRelatedToAccount = new List<Contact>();
                    Map<Id, Integer> prevSeqByConIdMap = new Map<Id, Integer>();
                    for(Contact con : lagacyRecords){
                        if(accountId == con.AccountId){
                            contactsRelatedToAccount.add(con);
                            prevSeqByConIdMap.put(con.Id, Integer.valueOf(con.Sequence_Number__c));
                        }
                    }
                    accountWrapperByAccountId.put(accountId, new AccountWrapper(accountId, contactsRelatedToAccount, prevSeqByConIdMap));
                }       
            }
        }
    }

    public class AccountWrapper{
        
        List<Contact> relatedContacts;
        String accountId;
        Map<Id, Integer> prevSeqByConIdMap;
        List<Contact> reparentedContacts = new List<Contact>();
        List<Contact> validSequenceContacts = new List<Contact>();
        List<Contact> invalidSequenceContacts = new List<Contact>();
        Map<Integer, Contact> conBySeqMap;
        Map<Integer, Contact> seqPriorityMap; //contacts will be moved to this map from valid list and after processing, will be moved back to this list
        
        Set<Id> actuallyUpdatedContactIds = new Set<Id>();
        
        public AccountWrapper(Id accountId, List<Contact> contactsRelatedToAccount, Map<Id, Integer> prevSeqByConIdMap){
            this.relatedContacts = contactsRelatedToAccount;            
            this.accountId = accountId;
            this.prevSeqByConIdMap = prevSeqByConIdMap;
        }

        public void updateContacts(){
            fillSeqPriorityMap();
            // removeToBeUpdatedFromMainList();
            // moveUpdatedToMainList();
        }

        public void fillSeqPriorityMap(){
            System.debug('@@@ fillSeqPriorityMap');

            seqPriorityMap = new Map<Integer, Contact>(); 
            Map<Integer, Integer> seqByOccuranceMap = new Map<Integer, Integer>(); //Map<Sequence, How much times the sequence occurs>
            Integer priorityOrder = 1;
            for(Contact con : validSequenceContacts){

                Integer sequenceNumber = (Integer)con.Sequence_Number__c;

                if(!seqByOccuranceMap.containsKey(sequenceNumber)){
                    System.debug('@@@ inside if');
                    //Sequence not present in map
                    seqByOccuranceMap.put(sequenceNumber, 1);
                    seqPriorityMap.put(priorityOrder++, con);
                } else {
                    System.debug('@@@ inside else');
                    //Sequence present in the map
                    seqByOccuranceMap.put(sequenceNumber, seqByOccuranceMap.get(sequenceNumber) + 1); //increse count by one
                    
                    //get last sequence on which comoponent is present
                    // Integer existingPriorityOrder = getPriority

                    //add new component after that 
                    
                }
            }
        }

        public void removeUpdatedFromMainList(){
            System.debug('@@@ removeUpdatedFromMainList');

            //nullify the contacts in main list that are actually updated
            for(Contact con : relatedContacts){
                if(actuallyUpdatedContactIds.contains(con.Id)){
                    con = null;
                }
            }

            //remove null element indexes from the list and update contact sequence in that list
            conBySeqMap = new Map<Integer, Contact>();
            Integer sequence = 1;
            for(Contact con : relatedContacts){
                if(con != null){
                    con.Sequence_Number__c = sequence++;
                    conBySeqMap.put(sequence, con);
                }    
            }
            relatedContacts.clear();
        }

        public void moveFromActuallyupdatedListToMainList(){
            System.debug('@@@ moveFromActuallyupdatedListToMainList');

            //move with shifting of existing elements
            for(Contact con : validSequenceContacts){
                addToListWithShifting(con);
            }
        }

        public void addToListWithShifting(Contact con){
            System.debug('@@@ addToListWithShifting');
            Integer sequence = Integer.valueOf(con.Sequence_Number__c);
            Contact previousCon = conBySeqMap.get(sequence);
            if(previousCon != null){
                //needs shifting
                previousCon.Sequence_Number__c = sequence + 1;                    
                addToListWithShifting(previousCon);
            }
            conBySeqMap.put(sequence, con);
        }

        public void correctOverflowSequences(){
            System.debug('@@@ correctOverflowSequences');
            Integer sequence = 1;
            Map<Integer, Contact> tempMap = new Map<Integer, Contact>();
            for(Integer key : conBySeqMap.keySet()){
                Contact con = conBySeqMap.get(key);
                con.Sequence_Number__c = sequence;
                tempMap.put(sequence, con);
            }
            conBySeqMap = tempMap;
        }

        public void processInvalidSequences(){
            System.debug('@@@ processInvalidSequences');
            Integer sequence = conBySeqMap.size() + 1;
            for(Contact con : invalidSequenceContacts){
                con.Sequence_Number__c = sequence;
                conBySeqMap.put(sequence, con);
            }
        }

        public void processReparentedSequences(){
            System.debug('@@@ processReparentedSequences');
            Integer sequence = conBySeqMap.size() + 1;
            for(Contact con : reparentedContacts){
                con.Sequence_Number__c = sequence;
                conBySeqMap.put(sequence, con);
            }
        }

        public List<Contact> getContactsToUpdate(Map<Id,Contact> newMap){
            System.debug('@@@ getContactsToUpdate');
            List<Contact> consQualifiedForUpdate = new List<Contact>();
            for(Contact con : relatedContacts){
                //if(!newMap.containsKey(con.Id) && con.Sequence_Number__c != prevSeqByConIdMap.get(con.Id)){
                if(!newMap.containsKey(con.Id)){
                    consQualifiedForUpdate.add(con);
                }
            }
            display(consQualifiedForUpdate, 'consQualifiedForUpdate');
            return consQualifiedForUpdate;
        }

        public void display(List<Contact> conList, String listName){
            System.debug('################################## START ' + listName + ' #####################################');
            for(Contact con : conList){
                System.debug(con.lastName + ' <==> ' + con.sequence_number__c);
            }
            System.debug('################################## End ' + listName + ' #####################################');
        }
    }
}