public class LDT_PaginationController {
    //1. GENERATE QUERY BASED ON SELECTED FIELDS
    @AuraEnabled
    public static String doQueryForRecords(String objectName, List<String> fields){
        System.debug(objectName);
        System.debug(fields);
        List<Column> columns = prepareColumnsForTable(objectName, fields);
        List<sobject> data = prepareDataForTable(objectName, fields);
        return JSON.serialize(new SobjectWrapper(columns, data));
    }

    //THIS SECTION PREPARES COLUMNS TO BE USED IN LIGHTNING DATA TABLE  
    private static List<Column> prepareColumnsForTable(String objectName, List<String> fields){
        Map<String, Schema.sObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        List<Column> columns = new List<Column>();
        for(String fieldName : fields){
            if(fieldMap.containsKey(fieldName)){
                Schema.DescribeFieldResult dfr = fieldMap.get(fieldName).getDescribe();
                columns.add(new Column(dfr.getLabel(), dfr.getName(), dfr.isSortable()));
            }
        }
        return columns;
    }

    private static List<sobject> prepareDataForTable(String objectName, List<String> fields){
        String commaSeparatedFields = '';
        for(String fld : fields){
            commaSeparatedFields += fld + ',';
        }
        commaSeparatedFields = commaSeparatedFields.removeEnd(',');
        List<Sobject> records = database.query('SELECT ' + commaSeparatedFields + ' FROM ' + objectName + ' ORDER BY Id LIMIT 50000');
        return records;
    }

    //Sobject Wrapper
    public class SobjectWrapper{
        @AuraEnabled public List<column> columns;
        @AuraEnabled public List<sobject> data;
        public SobjectWrapper(List<column> columns, List<sobject> data){
            this.columns = columns;
            this.data = data;
        }
    }

    // COLUMN WRAPPER
    public class Column{
        @AuraEnabled public String label;
        @AuraEnabled public String fieldName;
        public Boolean sortable;
        public Column(String label, String fieldName, boolean sortable){
            this.label = label;
            this.fieldName = fieldName;
            this.sortable = sortable;
        }
    }
}