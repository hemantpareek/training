@isTest
class ContactStatusUpdaterBatchSchedulerTest {
    
        @istest
        public static void  testscheduler(){
        Contact con = new Contact(lastname='TestContact');
        insert con;
        Test.setCreatedDate(con.id,System.Today()-1);
        update con;
        Test.startTest();
        system.assertEquals('Not Submitted',[select status__c from contact where lastname='TestContact' Limit 1].status__c);
        // Schedule the test job

        List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
        System.assertEquals(0, jobsScheduled.size(), 'expecting no scheduled job');

        // check apex batch is in the job list
        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(0, jobsApexBatch.size(), 'expecting no apex batch job');


        String cronExpr = '0 0 0 * * ? 2022';
        String jobId = System.schedule('ContactUpdateBatchSchedulerTest',cronExpr, new ContactStatusUpdaterBatchScheduler());

        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

        // Verify the expressions are the same
        System.assertEquals(cronExpr, ct.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2022-01-01 00:00:00', String.valueOf(ct.NextFireTime));
        System.assertEquals('Not Submitted',[select status__c from contact where lastname='TestContact' Limit 1].status__c);

        Test.stopTest();
        // There will now be two things in AsyncApexJob - the Schedulable itself
        // and also the Batch Apex job. This code looks for both of them

        // Check schedulable is in the job list
        jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
        System.assertEquals('ContactStatusUpdaterBatchScheduler', jobsScheduled[0].ApexClass.Name, 'expecting specific scheduled job');

        // check apex batch is in the job list
        jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('ContactStatusUpdaterBatch', jobsApexBatch[0].ApexClass.Name, 'expecting specific batch job');
    }    
}