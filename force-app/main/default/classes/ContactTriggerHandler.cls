public class ContactTriggerHandler{
    private static Map<Double,Contact> conMap = new Map<Double,Contact>();
    private static List<Contact> runtimeOverflowedContacts = new List<Contact>();
    private static Map<Id,Integer> maxRangeByAccountId = new Map<Id,Integer>();
    
    //APPLICABLE TO ONLY RECORDS WHO HAVE PARENT
    
    //1. INSERT
    public static void insertRecordHandler(Map<id,Contact> newMap){
        Set<ID> parentIDs = new Set<ID>();
        for(Contact c : newMap.values()){
            parentIDs.add(c.AccountId);
        }
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Contact> relatedContacts = [SELECT LastName, Sequence_Number__c, AccountID FROM Contact 
                                        WHERE AccountID IN:parentIDs AND ID NOT IN :newMap.keySet() 
                                        ORDER BY Sequence_number__c];                                       //previousContactList
        relatedContacts.addAll(         [SELECT LastName, Sequence_Number__c, AccountID FROM Contact
                                        WHERE AccountID IN:parentIDs AND ID IN:newMap.keySet()
                                        ORDER BY ID]);                                                       //new Contact list
        fillMaxRangeMap(parentIDs);
        for(Id AccountID : parentIDs){
            List<Contact> oldSequenceList     = new List<Contact>();
            List<Contact> updatedSequenceList = new List<Contact>();
            Integer max_range = maxRangeByAccountId.get(AccountId);
            for(Contact con : relatedContacts){
                if(con.AccountId == AccountID){
                    if(!newMap.containsKey(con.id)){
                        //for previous list
                         con.sequence_number__c = null;
                         oldSequenceList.add(con);
                    }else{
                        //for new list
                        if(con.sequence_number__c == null || con.sequence_number__c <=0 || con.sequence_number__c > max_range){
                            con.sequence_number__c = null;
                            oldSequenceList.add(con);
                        }else{
                             updatedSequenceList.add(con);
                        }
                    }
                }
            }
            addToSequenceMap('updatedSequenceList'   ,updatedSequenceList   ,true);   // maintainsequence = true
            addToSequenceMap('oldSequenceList'       ,oldSequenceList      ,false);   // maintainsequence = false
            contactsToUpdate.addAll(getContactListFromMap());
        }
        CheckRecursive.run = false; 
        update(contactsToUpdate);
        CheckRecursive.run = true;
    }
    
    //2. DELETE
    public static void deleteRecordHandler(Map<id,Contact> oldMap){
        Set<ID> parentIDs = new Set<ID>();
        for(Contact c : oldMap.values()){
            parentIDs.add(c.AccountId);
        }
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Contact> relatedContacts = [SELECT Sequence_Number__c, AccountId FROM Contact WHERE AccountId IN :parentIDs ORDER BY sequence_number__c];
        for(ID Accountid : parentIDs){
            Integer sequence = 0;
            for(Contact con : relatedContacts){
                if(con.AccountId == Accountid){
                    con.sequence_number__c = ++sequence;
                    contactsToUpdate.add(con);
                }
            }
        }
        CheckRecursive.run = false;
        update(contactsToUpdate);
        CheckRecursive.run = true;
    }
    
    //3. UNDELETE
    public static void undeleteRecordHandler(Map<Id,Contact> newMap){
        Set<ID> parentIDs = new Set<ID>();
        for(Contact c : newMap.values()){
            parentIDs.add(c.AccountId);
        } 
        List<Contact> contactsToUpdate =new List<Contact>();
        List<Contact> relatedContacts = [SELECT Sequence_Number__c, accountId FROM Contact WHERE AccountId IN :parentIDs ORDER BY ID];
        for(ID accountId : parentIDs){
            List<Contact> oldContacts = new List<Contact>();
            List<Contact> newContacts = new List<Contact>();
            for(Contact con : relatedContacts){
                if(con.AccountId == accountId){
                    if(!newMap.containsKey(con.id)){
                        oldContacts.add(con);
                    }else{
                        newContacts.add(con);
                    }
                }
            }
            addToSequenceMap('oldContacts',oldContacts,true);  // TRUE BCZ OLD CONTACT SEQUENCE WILL BE MAINTAINED
            addToSequenceMap('newContacts',newContacts,false);  // FASLE BCZ NEW CONTACT WILL BE RESEQUENCED
            contactsToUpdate.addAll(getContactListFromMap());
        }
        CheckRecursive.run = false;
        update(contactsToUpdate);
        CheckRecursive.run = true;
    }
    
    //4. UPDATE
    public static void updateRecordHandler(Map<id,Contact> oldMap , Map<id,Contact> newMap){
        Set<ID> parentIDs = new Set<ID>();
        for(Contact c : newMap.values()){
            parentIDs.add(c.AccountId);
        }
        for(Contact c : oldMap.values()){
            parentIDs.add(c.AccountId);
        }
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Contact> relatedContacts = [SELECT LastName, Sequence_Number__c, AccountID FROM Contact 
                                        WHERE AccountID IN:parentIDs AND ID IN:newMap.keySet() 
                                        ORDER BY ID];
        relatedContacts.addAll(         [SELECT LastName, Sequence_Number__c, AccountID FROM Contact
                                        WHERE AccountID IN:parentIDs AND ID NOT IN:newMap.keySet()
                                        ORDER BY Sequence_number__c]);
        fillMaxRangeMap(parentIDs);
        for(ID AccountId : parentIDs){
            List<Contact> changedSequenceList    = new List<Contact>();
            List<Contact> remiainingSequenceList = new List<Contact>();
            List<Contact> nullSequenceList       = new List<Contact>();
            List<Contact> outOfRangeSequenceList = new List<Contact>();
            List<Contact> reparentedSequenceList = new List<Contact>();
            List<Contact> invalidSequenceList    = new List<Contact>();
            Integer max_range = maxRangeByAccountId.get(AccountId);
            // FILL DATA TO COLLECTIONS
            for(Contact con : relatedContacts){
                if(con.AccountID == AccountId){
                    if(oldMap.containsKey(con.id) && con.AccountID != oldMap.get(con.id).AccountID){
                        con.Sequence_Number__c = null;
                        reparentedSequenceList.add(con);
                    }else if(con.sequence_number__c <= 0 || ( con.sequence_number__c!=null && Math.ceil(con.sequence_number__c)!=Math.floor(con.sequence_number__c))){
                        con.Sequence_Number__c = oldMap.get(con.id).sequence_number__c;
                        invalidSequenceList.add(con);    
                    }else if(con.sequence_number__c > max_range){
                        con.Sequence_Number__c = null;
                        outOfRangeSequenceList.add(con);
                    }else if(con.sequence_number__c == null){
                        nullSequenceList.add(con);
                    }else if(newMap.containsKey(con.id) && newMap.get(con.id).Sequence_Number__c != oldMap.get(con.id).Sequence_Number__c){
                        changedSequenceList.add(con);
                    }else{
                        remiainingSequenceList.add(con);
                    }
                }
            }
            // MANAGE DATA IN COLLECTION
            // ADD REMAINING SEQUENCE LIST TO MAP
            // ADD ADDTOLAST LIST TO MAP
            //UPDATE MAP CONTAINED CONTACT SEQUENCE WITH MAP KEY
            
            addToSequenceMap('invalidSequenceList'   ,invalidSequenceList   ,true);   // maintainsequence = true
            addToSequenceMap('changedSequenceList'   ,changedSequenceList   ,true);    // maintainsequence = true
            addToSequenceMap('remiainingSequenceList',remiainingSequenceList,false);   // maintainsequence = false
            addToSequenceMap('nullSequenceList'      ,nullSequenceList      ,false);   // maintainsequence = false
            addToSequenceMap('outOfRangeSequenceList',outOfRangeSequenceList,false);   // maintainsequence = false
            addToSequenceMap('reparentedSequenceList',reparentedSequenceList,false);   // maintainsequence = false
        
            //ADD MAP CONTACTS TO CONTACT UPDATE LIST
            contactsToUpdate.addAll(getContactListFromMap());
        }
        CheckRecursive.run = false;
        database.update(contactsToUpdate,false);
        CheckRecursive.run = true;
    }

    private static void fillMaxRangeMap(Set<Id> parentIDs){
        maxRangeByAccountId.clear();
        List<Account> parentAccounts = [SELECT ID, (SELECT id from contacts) FROM Account WHERE ID IN :parentIDs];
        for(Account acc : parentAccounts){
            maxRangeByAccountId.put(acc.id,acc.contacts.size());
        }
    }

    public static void addToSequenceMap(String listname, List<Contact> contactList,Boolean maintainOriginalSequence){  //TRUE FOR MAINTAINS SEQ
        system.debug('-----------------'+ listname +'---------------------');
        for(Contact c : contactList){
            system.debug(c);
        }
        system.debug('------------------- END -------------------');

        if(maintainOriginalSequence==true){
            addToMapWithMaintainSequence(contactList);  //MAINTAINS PREVIOUS SEQUENCE FOR CHANGED VALUES
        }else{      
            addToMapWithoutMaintainSequence(contactList); //FILLS IN REMAINING PLACES
        }
    }

    public static void addToMapWithMaintainSequence(List<Contact> contactList){
        //MAINTAIN EXISTING SEQUENCE

        for(Contact con : contactList){
            if(!conMap.containsKey(con.Sequence_Number__c)){
                    conMap.put(con.Sequence_Number__c, con);
            } else {
                    insertToMapWithShifting(con);
            }
        }
    }
    
    public static void addToMapWithoutMaintainSequence(List<Contact> contactList){
        //FILL WITHIN EXISTING SEQUENCE
        Integer sequence = 0;
        for(Contact con : contactList){
            while(conMap.containsKey(++sequence));
                con.Sequence_Number__c = sequence;
                conMap.put(sequence, con);
        }
    }

    public static void insertToMapWithShifting(Contact con){
        //WRITE SHIFTING LOGIC HERE
        Contact prevContact = conMap.get(con.Sequence_Number__c);
        prevContact.Sequence_Number__c = con.Sequence_Number__c + 1;
        if(prevContact.Sequence_Number__c <= maxRangeByAccountId.get(prevContact.AccountId)){
            if(!conMap.containsKey(prevContact.Sequence_Number__c)){
                conMap.put(prevContact.Sequence_Number__c, prevContact);
            }else{
                insertToMapWithShifting(prevContact);   
            }
        }else{
            prevContact.Sequence_Number__c = null;
            runtimeOverflowedContacts.add(prevContact);
        }
        conMap.put(con.Sequence_Number__c, con);
    }
    
    public static List<Contact> getContactListFromMap(){
        addToSequenceMap('runtimeOverflowedContacts'   ,runtimeOverflowedContacts   ,false);   // maintainsequence = false
        runtimeOverflowedContacts.clear();
        List<Contact> result = conMap.values();
        System.debug('--------------------final values-----------------------');
        for(Contact c : result){
            system.debug(c);
        }
        system.debug('----------------------- END ---------------------------');
        conMap.clear();
        return result;
    }
}