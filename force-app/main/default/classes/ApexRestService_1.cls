@RestResource(urlMapping='/ApexRestService/*') //endpoint => (SFDC base url)/services/apexrest/className
global class ApexRestService_1 {
    
    @HttpPost 
    global static jsonbody doPost(){
        system.debug('post request');
        return new jsonbody();
    }
   
    global class jsonbody{
        public boolean success = true;
    }
}