public class IC_AuthorizeDotNetController {
    //CLASS VARIABLES
    @testVisible static string apiLoginId;
    @testVisible static string transactionKey;
    @testVisible static string key;

    //1. METHOD TO FETCH INTEGRATION CREDENTIALS
    @testVisible static boolean fetchIntegrationCredentials(){
        List<Authorize_Net_Credentials__c> integrationCredentials = [SELECT API_Login_ID__c, Transaction_Key__c, Key__c	FROM Authorize_Net_Credentials__c];
        if(integrationCredentials.isEmpty()){
            return false;
        }else if(integrationCredentials[0].API_Login_ID__c == null || integrationCredentials[0].Transaction_Key__c == null){
            return false;
        }
        apiLoginId = integrationCredentials[0].API_Login_ID__c;
        transactionKey = integrationCredentials[0].Transaction_Key__c;
        key = integrationCredentials[0].Key__c;
        return true;
    }

    //2. INIT HANDLER METHOD
    @AuraEnabled
    public static string doInitApex(){
        List<Product2> products = [SELECT Name, ProductCode, description FROM Product2 WHERE Family = 'Solar_Panels' ORDER By Name];
        List<Id> productIds = new List<Id>();
        for(Product2 p : products){
            productIds.add(p.Id);
        }
        Map<Id, Decimal> pricebookEntries = new Map<Id, Decimal>();
        for(PricebookEntry pbe : [SELECT Product2Id, UnitPrice FROM PricebookEntry WHERE Product2Id IN :productIds]){
            pricebookEntries.put(pbe.Product2Id, pbe.UnitPrice);
        }
        String initResponse = '[';
        for(Product2 p : products){
            if(pricebookEntries.containsKey(p.id)){
                initResponse += '{"id":"' + p.id + '","name":"' + p.name + '","price":"' + pricebookEntries.get(p.id) + '","description":"' + p.description + '"},';
            }
        }
        return initResponse.removeEnd(',')+']';
    }

    //3. METHOD TO PROCESS PAYMENT FROM CREDIT CARD
    @AuraEnabled
    public static string processPaymentFromCreditCard(Integer totalAmount, Long cardNumber, Integer expiryYear, Integer expiryMonth, Integer cvv, String cartItemsJson){
        system.debug('cartJson' + cartItemsJson);
        if(!fetchIntegrationCredentials()){
            return null;
        }
        AuthorizeDotNetWrapper cartItemsResponse = (AuthorizeDotNetWrapper)System.JSON.deserialize('{"cartItems":' + cartItemsJson + '}', AuthorizeDotNetWrapper.class);
        String lineItem = '[';
        for(AuthorizeDotNetWrapper.CartItem item : cartItemsResponse.cartItems){
            String currentItem = '{'+
                                 '"itemId":"' + item.id + '",'+
                                 '"name":"' + item.name + '",'+  
                                 '"description":"' + item.description + '",'+  
                                 '"quantity":"' + item.quantity + '",'+  
                                 '"unitPrice":"' + item.unitPrice + '"'+
                                 '},';
            lineItem += currentItem;
        }
        lineItem = lineItem.removeEnd(',') + ']';
        system.debug('lineItem : '+lineItem);
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://apitest.authorize.net/xml/v1/request.api');
        parameters.put('content-type','application/json');
        String body = '{'+
                        '"createTransactionRequest": {'+
                            '"merchantAuthentication": {'+
                                '"name": "' + apiLoginId + '",'+
                                '"transactionKey": "' + transactionKey + '"'+
                            '},'+
                            '"refId": "123456",'+
                            '"transactionRequest": {'+
                                '"transactionType": "authOnlyTransaction",'+
                                '"amount": "' + totalAmount + '",'+
                                '"payment": {'+
                                    '"creditCard": {'+
                                        '"cardNumber": "' + cardNumber + '",'+
                                        '"expirationDate": "' + expiryYear + '-' + expiryMonth + '",'+
                                        '"cardCode": "' + cvv + '"'+
                                    '}'+
                                '},'+
                                '"lineItems": {"lineItem":'+ lineItem +
                                '},'+
                                '"tax": {'+
                                    '"amount": "4.26",'+
                                    '"name": "level2 tax name",'+
                                    '"description": "level2 tax"'+
                                '},'+
                                '"duty": {'+
                                    '"amount": "8.55",'+
                                    '"name": "duty name",'+
                                    '"description": "duty description"'+
                                '},'+
                                '"shipping": {'+
                                    '"amount": "4.26",'+
                                    '"name": "level2 tax name",'+
                                    '"description": "level2 tax"'+
                                '},'+
                                '"poNumber": "456654",'+
                                '"customer": {'+
                                    '"id": "99999456654"'+
                                '},'+
                                '"billTo": {'+
                                    '"firstName": "Ellen",'+
                                    '"lastName": "Johnson",'+
                                    '"company": "Souveniropolis",'+
                                    '"address": "14 Main Street",'+
                                    '"city": "Pecan Springs",'+
                                    '"state": "TX",'+
                                    '"zip": "44628",'+
                                    '"country": "USA"'+
                                '},'+
                                '"shipTo": {'+
                                    '"firstName": "China",'+
                                    '"lastName": "Bayles",'+
                                    '"company": "Thyme for Tea",'+
                                    '"address": "12 Main Street",'+
                                    '"city": "Pecan Springs",'+
                                    '"state": "TX",'+
                                    '"zip": "44628",'+
                                    '"country": "USA"'+
                                '},'+
                                '"customerIP": "192.168.1.1",'+
                                '"userFields": {'+
                                    '"userField": ['+
                                        '{'+
                                            '"name": "MerchantDef+inedFieldName1",'+
                                            '"value": "MerchantDefinedFieldValue1"'+
                                        '},'+
                                        '{'+
                                            '"name": "favorite_color",'+
                                            '"value": "blue"'+
                                        '}'+
                                    ']'+
                                '}'+
                            '}'+
                        '}'+
                    '}';    
        parameters.put('setBody',body);
        parameters.put('content-length',String.valueOf(body.length()));
        parameters.put('setTimeout','60000');
        parameters.put('request-type','authorizeCreditCard');
        AuthorizeDotNetWrapper authResponse = (AuthorizeDotNetWrapper)AuthorizeDotNetSerivce.accessService(parameters);
        if(authResponse == null){
            return null;
        }
        return '{"text":"' + authResponse.messages.message[0].text + '","code":"' + authResponse.messages.message[0].code + '"}';
    }

    //4. METHOD TO PROCESS PAYMENT FROM eCHECK
    @AuraEnabled
    public static string processPaymentFromEcheck(Integer totalAmount, Long routingNumber, Long accountNumber, String nameOnAccount, String cartItemsJson){
        if(!fetchIntegrationCredentials()){
            return null;
        }
        AuthorizeDotNetWrapper cartItemsResponse = (AuthorizeDotNetWrapper)System.JSON.deserialize('{"cartItems":' + cartItemsJson + '}', AuthorizeDotNetWrapper.class);
        String lineItem = '[';
        for(AuthorizeDotNetWrapper.CartItem item : cartItemsResponse.cartItems){
            String currentItem = '{'+
                                 '"itemId":"' + item.id + '",'+
                                 '"name":"' + item.name + '",'+  
                                 '"description":"' + item.description + '",'+  
                                 '"quantity":"' + item.quantity + '",'+  
                                 '"unitPrice":"' + item.unitPrice + '"'+
                                 '},';
            lineItem += currentItem;
        }
        lineItem = lineItem.removeEnd(',') + ']';
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://apitest.authorize.net/xml/v1/request.api');
        parameters.put('content-type','application/json');
        String body = '{'+
                        '"createTransactionRequest": {'+
                            '"merchantAuthentication": {'+
                                '"name": "' + apiLoginId + '",'+
                                '"transactionKey": "' + transactionKey + '"'+
                            '},'+
                            '"refId": "123456",'+
                            '"transactionRequest": {'+
                                '"transactionType": "authCaptureTransaction",'+
                                '"amount": "' + totalAmount + '",'+
                                '"payment": {'+
                                    '"bankAccount": {'+
                                        '"accountType": "checking",'+
                                        '"routingNumber": "' + routingNumber + '",'+
                                        '"accountNumber": "' + accountNumber + '",'+
                                        '"nameOnAccount": "' + nameOnAccount + '"'+
                                    '}'+
                                '},'+
                                '"lineItems": {'+
                                    '"lineItem": '+ lineItem +
                                '},'+
                                '"tax": {'+
                                    '"amount": "4.26",'+
                                    '"name": "level2 tax name",'+
                                    '"description": "level2 tax"'+
                                '},'+
                                '"duty": {'+
                                    '"amount": "8.55",'+
                                    '"name": "duty name",'+
                                    '"description": "duty description"'+
                                '},'+
                                '"shipping": {'+
                                    '"amount": "4.26",'+
                                    '"name": "level2 tax name",'+
                                    '"description": "level2 tax"'+
                                '},'+
                                '"poNumber": "456654",'+
                                '"billTo": {'+
                                    '"firstName": "Ellen",'+
                                    '"lastName": "Johnson",'+
                                    '"company": "Souveniropolis",'+
                                    '"address": "14 Main Street",'+
                                    '"city": "Pecan Springs",'+
                                    '"state": "TX",'+
                                    '"zip": "44628",'+
                                    '"country": "USA"'+
                                '},'+
                                '"shipTo": {'+
                                    '"firstName": "China",'+
                                    '"lastName": "Bayles",'+
                                    '"company": "Thyme for Tea",'+
                                    '"address": "12 Main Street",'+
                                    '"city": "Pecan Springs",'+
                                    '"state": "TX",'+
                                    '"zip": "44628",'+
                                    '"country": "USA"'+
                                '},'+
                                '"customerIP": "192.168.1.1"'+
                            '}'+
                        '}'+
                    '}';
        system.debug(body);
        parameters.put('setBody',body);
        parameters.put('content-length',String.valueOf(body.length()));
        parameters.put('setTimeout','60000');
        parameters.put('request-type','authorizeCreditCard');
        AuthorizeDotNetWrapper authResponse = (AuthorizeDotNetWrapper)AuthorizeDotNetSerivce.accessService(parameters);
        if(authResponse == null){
            return null;
        }
        return '{"text":"' + authResponse.messages.message[0].text + '","code":"' + authResponse.messages.message[0].code + '"}';
    }
}