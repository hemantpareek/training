public class GoogleDriveWrapper {
    //--------------------------------------------------WRAPPER CLASS TO PARSE AUTH JSON RESPONSE-------------------------------------------------------
    public class AccessDrive{
        public String access_token;
        public Integer expires_in;
        public String scope;
        public String token_type;
        public String refresh_token;
    }
    
    //----------------------------------------------- WRAPPER CLASS TO PARSE FTECH DATA JSON RESPONSE --------------------------------------------------
    public List<File> items;

	public class File{
		public String id{get;set;}
        public String title{get;set;}
        public String name{get;set;}
		public String mimeType;
        public boolean isLast{get;set;}
        public String webContentLink{get;set;}
        public boolean shared{get;set;}
        public File(String id, String name){
            this.id = id;
            this.name = name;
            this.title = name;
        }
        public Boolean equals(Object obj) {
            File f = (File)obj;
            if(f.id == id){
                return true;
            }
            return false;
        }
	}

    //----------------------------------------------- WRAPPER CLASS TO PARSE FTECH DATA JSON RESPONSE --------------------------------------------------
    public class FileUpload{
        public blob body{get;set;}
        public string name{get;set;}
        public string type{get;set;}
        public integer size{get;set;}
    }
}