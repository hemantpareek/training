public class DetailController {

    public String c_subject{get;set;}
    public boolean c_inlineedit{get;set;}
    public boolean c_relatedList{get;set;}
    public String sobjectName;

    public String getSobjectName() {
        Id rcdId = c_subject;
        sobjectName = rcdId.getSObjectType().getDescribe().getName();
        System.debug(sobjectName);
        return sobjectName;
    }

    
}