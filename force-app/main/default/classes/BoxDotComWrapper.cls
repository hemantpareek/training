public class BoxDotComWrapper {
    //--------------------------------------------------WRAPPER CLASS TO PARSE AUTH JSON RESPONSE-------------------------------------------------------
    public class AccessDrive{
        public String access_token{get;private set;}
        public Integer expires_in{get;private set;}
        public String token_type;
        public String refresh_token{get;private set;}
    }
    
    //----------------------------------------------- WRAPPER CLASS TO PARSE FTECH DATA JSON RESPONSE --------------------------------------------------
    public Integer total_count;
    public List<File> entries{get;private set;}
    
	public class File{
        public String type;
        public String id{get; set;}
        public String Name{get; set;}
        public boolean isLast{get;set;}
	}
}