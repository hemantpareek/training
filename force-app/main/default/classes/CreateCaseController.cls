public with sharing class CreateCaseController {
    
    @AuraEnabled
    public static string getCaseRecordTypes(){
        try{
            List<RecordType> caseRecordTypes = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'case'];
            if(!caseRecordTypes.isEmpty()){
                List<RecordTypeWrapper> recordTypes = new List<RecordTypeWrapper>();
                for(RecordType rt : caseRecordTypes){
                    recordTypes.add(new RecordTypeWrapper(rt.Name, rt.Id));
                }
                return JSON.serialize(new Result(true, recordTypes, null));
            } else {
                return JSON.serialize(new Result(false, null, 'No record types found for case.'));
            }
        } catch(Exception e){
            return JSON.serialize(new Result(false, null, e.getMessage()));
        }   
    }

    @AuraEnabled
    public static string updateSocialPost(String queryCode){
        try{
            Case newCase = [SELECT Id, CaseNumber, Query_Code__c FROM Case WHERE Query_Code__c = :queryCode];
            String socialPostRecordId = queryCode.substring(0, 18);
            Social_Post__c sp = [SELECT Id, Name, Case__c FROM Social_Post__c WHERE Id = :socialPostRecordId];
            sp.Case__c = newCase.Id;
            update sp;
            newCase.Query_Code__c = null; //TO AVOID CONFLICT IN FUTURE RECORDS
            update newCase;
            return JSON.serialize(new Result(true, null, 'Social post updated with case : ' + newCase.CaseNumber));
        } catch(Exception e){
            return JSON.serialize(new Result(false, null, e.getMessage()));
        }   
    }

    public class Result{
        public boolean isSuccess;
        public Object data;
        public String msg;
        public Result(Boolean isSuccess, Object data, String msg){
            this.isSuccess = isSuccess;
            this.data = data;
            this.msg = msg;
        }
    }

    public class RecordTypeWrapper{
        public String label;
        public String value;
        public RecordTypeWrapper(String label, String value){
            this.label = label;
            this.value = value;
        }
    }
}