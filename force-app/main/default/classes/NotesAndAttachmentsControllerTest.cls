@isTest
class NotesAndAttachmentsControllerTest {
    @isTest
    static void createAttachment_test(){
        Test.startTest();
        Account parent = new Account(Name='Parent');
        insert parent;
        system.assertEquals(0,[SELECT id FROM attachment WHERE parentid=:parent.id].size());
        String body = 'Hello apex';
        NotesAndAttachmentsController.createAttachment(blob.valueOf(body), 'sample File', parent.id);
        system.assertEquals(1,[SELECT id FROM attachment WHERE parentid=:parent.id].size());
        Test.stopTest();
    }
}