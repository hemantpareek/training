public class CheckRecursive {
    public static boolean run =  true;
    public static boolean preventTriggerFire = false; // TO PREVENT TRIGGERS FROM FIRE IN SOME TESTINGS
    public static Integer insertcount = 0;  // SINGLE TRANSACTION INSERT TRIGGER FIRE COUNTS
    public static Integer deletecount = 0;  // SINGLE TRANSACTION DELETE TRIGGER FIRE COUNTS
    public static  Integer undeletecount = 0;// SINGLE TRANSACTION UNDELETE TRIGGER FIRE COUNTS
    public static Integer updatecount = 0;  // SINGLE TRANSACTION UPDATE TRIGGER FIRE COUNTS
}