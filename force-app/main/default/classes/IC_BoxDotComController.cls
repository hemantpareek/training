//------------------------------------------------ BOX.COM INTEGRATION COMPONENT CONTROLLER -------------------------------------------------
public class IC_BoxDotComController {
    //------------------------------------------------------ CONTROLLER VARIABLES -----------------------------------------------------------
    @testVisible static String access_token; 
    @testVisible static String refresh_token;
    @testVisible static DateTime expires_in;                                          
    @testVisible static String redirect_uri = 'https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/box';
    @testVisible static String clientID;    
    @testVisible static String clientSecret;
    @testVisible static Box_com_Integration_Data__c bdci;
    
    //--------------------------------------------------------- GENERAL METHODS --------------------------------------------------------------

    //1. RETRIEVES ACCESS TOKEN, REFRESH TOKEN, EXPIRES IN FROM CUSTOM SETTING IF AVAILABLE
    @testVisible static Boolean retrieveDataFromCustomSetting(){ //returns true when list is not empty
        List<Box_com_Integration_Data__c> bdciLst = [SELECT access_Token__c , refresh_token__c , expires_in__c FROM Box_com_Integration_Data__c 
                                                        WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(bdciLst.isEmpty()){
            return false;
        }
        bdci = bdciLst[0];
        refresh_token = bdci.refresh_token__c;
        expires_in = bdci.expires_in__c;
        if(bdci.expires_in__c > System.now()){
            access_token = bdci.access_Token__c;
        }else{
            access_token = null;
        }
        return true;
    }

    //2. RETRIEVES CLIENT ID AND CLIENT SECRET FROM CUSTOM METADATA TYPES IF AVAILABLE
    @testVisible static Boolean retrieveDataFromCustomMetadata(){
        List<Box_Com_Integration_Credentials__c> integration = [SELECT client_id__c , client_secret__c, redirect_uri__c FROM Box_Com_Integration_Credentials__c LIMIT 1]; 
        if(!integration.isEmpty()){
            clientID = integration[0].client_id__c;
            clientSecret = integration[0].client_secret__c;
            return true;
        }
        return false;
    }

    //3. PROVIDES AUTH URI TO GET ACCESS TOKEN VIA CODE
    @testVisible static String doGetAuthURI(){
        if(retrieveDataFromCustomMetadata()){
            String authURI = BoxDotComService.authenticate(clientId,redirect_uri);
            return '{"status":"authenticateWithCode","authURI":"'+ authURI +'"}';
        }else{
            return '{"status":"CUSTOM_METADATA_CREDENTIAL_NOT_FOUND"}';
        }
    }

    //4. FETCHES DATA FROM BOX.COM DRIVE BASED ON PARENT FOLDER'S ID 
    public static String fetchData(String parentFolderID){  
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','GET');
        parameters.put('setEndpoint','https://api.box.com/2.0/folders/'+parentFolderID+'/items'); //End point for List of Files in Folder
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('Accept','application/json');
        parameters.put('request_type','fetchData');
        parameters.put('setTimeout','60000');
        BoxDotComWrapper fetchDataResponse = (BoxDotComWrapper)BoxDotComService.accessDrive(parameters);
        if(fetchDataResponse == null){
            return null;
        }
        String fetchDataResponseJSON  = '[';
        for(BoxDotComWrapper.File f : fetchDataResponse.entries){
            fetchDataResponseJSON += '{';
            fetchDataResponseJSON += '"id":"' + f.id + '",';
            fetchDataResponseJSON += '"name":"' + f.Name + '",';
            fetchDataResponseJSON += '"type":"' + f.type + '",';
            fetchDataResponseJSON += '"isLast":"false"';
            fetchDataResponseJSON += '},';
        }
        return fetchDataResponseJSON.removeEnd(',') + ']';
    }

//------------------------------------------------------- @AURAENABLED METHODS ----------------------------------------------------------------------
    //1. PERFORMS INITIAL PROCESSING WHEN COMPONENT LOADS FIRST TIME
    @AuraEnabled
    public static String doInitApex(){
        if(retrieveDataFromCustomSetting()){
            if(bdci.expires_in__c > System.now()){
                access_token = bdci.access_Token__c;
                return '{"status":"readyToLoadData"}'; // perform fetch data first time action
            } else {
                if(refresh_token != null){
                    return '{"status":"getAccessTokenFromRefreshToken"}'; //called when access token is expired
                }else{
                    return doGetAuthURI(); // here refresh token is null and we have to get access token with authCode
                }
            }
        } else {
            return doGetAuthURI();
        }
    }
    
    //2. PROVIDES ACCESS TOKEN FROM REFRESH TOKEN WHEN ACCESS TOKEN IS EXPIRED AND REFRESH TOKEN IS AVAILABLE IN DATABASE
    @AuraEnabled
    public static String doGetAccessTokenFromRefreshToken(){
        if(!retrieveDataFromCustomSetting() & !retrieveDataFromCustomMetadata()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://app.box.com/api/oauth2/token');
        String body = 'refresh_token='+refresh_token+
                      '&client_id='+clientId+
                      '&client_secret='+clientSecret+
                      '&grant_type=refresh_token';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('Content-length',String.valueOf(body.length()));
        parameters.put('setTimeout','60000');
        BoxDotComWrapper.AccessDrive authResponse = (BoxDotComWrapper.AccessDrive)BoxDotComService.accessDrive(parameters);
        if(authResponse == null){
            return null;
        }
        bdci.access_Token__c = authResponse.access_token;
        bdci.refresh_Token__c = authResponse.refresh_token;
        bdci.expires_in__c = System.now().addSeconds(authResponse.expires_in);
        update bdci;
        return 'SUCCESS';
    }

    //3. PROVIDES ACCESS TOKEN FIRST TIME WHEN NO DATA IS PRESENT IN DATABASE OR IF REFRESH TOKEN IS NULL AFTER FIRST TIME
    @AuraEnabled
    public static String doGetAccessToken(String code){
        if(!retrieveDataFromCustomMetadata()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://app.box.com/api/oauth2/token');
        String body = 'code='+code+
                      '&client_id='+clientId+
                      '&client_secret='+clientSecret+
                      '&redirect_uri='+redirect_uri+
                      '&grant_type=authorization_code';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('content-length',String.valueOf(body.length()));
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        parameters.put('setTimeout','60000');
        BoxDotComWrapper.AccessDrive authResponse = (BoxDotComWrapper.AccessDrive)BoxDotComService.accessDrive(parameters);
        if(authResponse == null){
            return null;
        }
        List<Box_com_Integration_Data__c> bdciLst = [SELECT access_Token__c, refresh_token__c, expires_in__c FROM Box_com_Integration_Data__c
                                                    WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(!bdciLst.isEmpty()){
        bdci = bdciLst[0]; 
        }else{
            bdci = new Box_com_Integration_Data__c();
        }
        bdci.user_email__c = UserInfo.getUserEmail();
        bdci.SetupOwnerId = UserInfo.getUserId();
        bdci.access_Token__c = authResponse.access_token;
        bdci.refresh_token__c = authResponse.refresh_token;
        bdci.expires_in__c = System.now().addSeconds(authResponse.expires_in);
        upsert bdci;
        return 'SUCCESS';
    }

    //4. FUNCTION TO FETCH ROOT FOLDER DATA WHEN COMPONENT IS LOADED FIRST TIME
    @AuraEnabled    
    public static String doFetchDataFirstTime(){
        retrieveDataFromCustomSetting();
        return fetchData('0');
    }

    //5. FUNCTION TO PERFORM CHANGE DIRECTORY ACTION
    @AuraEnabled
    public static string doChangeDirectory(String folderId){
        retrieveDataFromCustomSetting();
        return fetchData(folderId);
    }

    //7. FUNCTION TO DELETE SELECTED FILE
    @AuraEnabled
    public static string deleteSelectedFile(String type, String id){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','DELETE');
        String queryParam = type == 'folder' ? '?recursive=true' : '';
        parameters.put('setEndpoint','https://api.box.com/2.0/' + type + 's/' + id + queryParam);
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('setTimeout','60000');
        parameters.put('request_type','deleteFile');    
        String dfr = (String)BoxDotComService.AccessDrive(parameters);
        if(dfr == null){
            return null;
        }
        return dfr;
    }
    
    //8. FUNCTION TO CREATE FOLDER
    @AuraEnabled
    public static string doCreateFolder(String folderName, String parentId){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.box.com/2.0/folders');
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('setBody','{"name":"' + folderName + '", "parent": {"id": "' + parentId + '"}}');
        parameters.put('setTimeout','60000');
        parameters.put('request_type','createFolder');    
        BoxDotComWrapper.File cfr = (BoxDotComWrapper.File)BoxDotComService.AccessDrive(parameters);
        if(cfr == null){
            return null;
        }
        return cfr.id;
    }

    //9. FUNCTION TO DOWNLOAD FILE
    @AuraEnabled
    public static string doFileDownload(String fileId){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','GET');
        parameters.put('setEndpoint','https://api.box.com/2.0/files/'+fileId+'/content');
        parameters.put('content-type','application/json');
        parameters.put('Authorization','Bearer'+' '+ access_token); //Specific Authorization Syntax
        parameters.put('Accept','application/json');
        parameters.put('request_type','fileDownload');
        String fdr = (String)BoxDotComService.accessDrive(parameters);
        if(fdr == null){
            return null;
        }
        return fdr;
    }

    //10. FUNCTION TO UPLOAD A FILE
    @AuraEnabled
    public static string doUploadFile(String fileName, String base64Data, String contentType, String parentFolderID){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        blob base64EncodeFile = base64EncodeFileContent(EncodingUtil.base64Decode(base64Data),fileName);
        
        String boundary = '----------------------------741e90d31eff';

        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://upload.box.com/api/2.0/files/content?parent_id='+parentFolderID);
        parameters.put('Authorization', 'Bearer ' + access_Token);
        parameters.put('content-length',String.valueof(base64EncodeFile.size()));
        parameters.put('content-type','multipart/form-data; boundary='+boundary);
        parameters.put('request_type','fileUpload');
        BoxDotComWrapper fur = (BoxDotComWrapper)BoxDotComService.accessDrive(parameters,base64EncodeFile);    
        system.debug('fur : '+fur);
        if(fur == null){
            return null;
        }
        return '{"name":"' + fur.entries[0].name + '","id":"' + fur.entries[0].id + '","type":"' + fur.entries[0].type + '"}';
    }    

    //11. FUNCTION TO ENCODE BODY TO BASE64 FORMAT
    public static blob base64EncodeFileContent(Blob file_body, String file_name){
        String boundary = '----------------------------741e90d31eff';
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+file_name+'";\nContent-Type: application/octet-stream';
        String footer = '--'+boundary+'--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('='))
        {
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }
        String bodyEncoded = EncodingUtil.base64Encode(file_body);
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
 
        if(last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        } else if(last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0,3) + 'N';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            footer = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
        } else {
            footer = '\r\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
        }
 
        return bodyBlob;
    }
}