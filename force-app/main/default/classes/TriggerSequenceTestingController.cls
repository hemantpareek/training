public class TriggerSequenceTestingController {
    ID parent_A_ID;
    ID parent_B_ID;
    public string operation{get;set;}
    List<Contact> parent_A_Child{get;set;}
    List<Contact> parent_B_Child{get;set;}
    public Integer numberOfContacts{get;set;}
    public Integer startFrom{get;set;}
    public Integer insertSequence{get;set;}
    public List<ContactWrapper> wrapperList_A{get;set;}
    public List<ContactWrapper> wrapperList_B{get;set;}
    public Boolean isSelectAll{get;set;}
    public Boolean isEditMode{get;set;}
    Map<ID,Contact> conMap;
    public String selectedParent{get;set;}
    ID selected_parent_ID;
    public Boolean preventTriggerFire{get;set;}
    
    public TriggerSequenceTestingController(){
 		selectedParent = 'Parent A';
        numberOfContacts = 10;
        startFrom = 1;
        isEditMode = false;
        isSelectAll = false;
        preventTriggerFire = false;
    }
    
    public class ContactWrapper{
        public contact con {get;set;}
        public Boolean isSelected {get;set;}
        public ContactWrapper(Contact c){
            con = c;
            isSelected = false;
        }
    }
    
    public void init(){
        List<Account> parent_A = [SELECT ID From Account Where name='Parent A' LIMIT 1];
        if(parent_A.size()==0){
            Account a = new Account(name='Parent A');
            insert a;
            parent_A_ID = a.id;
        }else{
            parent_A_ID = parent_A[0].id;
        }

        List<Account> parent_B = [SELECT ID From Account Where name='Parent B' LIMIT 1];
        if(parent_B.size()==0){
            Account b = new Account(name='Parent B');
            insert b;
            parent_B_ID = b.id;
        }else{
            parent_B_ID = parent_B[0].id;
        }

        retrieveContacts();
    }

    public void generateParentIds(){
        selected_parent_ID = selectedParent == 'Parent A' ? parent_A_ID : parent_B_ID ;
    }

    public void toggleEditMode(){
        if(isEditMode == true){
            isEditMode=false;
        }else{
            isEditMode = true;
        }
    }
    
    public void setTriggerFire(){
        CheckRecursive.preventTriggerFire = preventTriggerFire;
    }

    public void prepareWrapperList(){
        wrapperList_A = new List<ContactWrapper>();
        wrapperList_B = new List<ContactWrapper>();
        if(parent_A_Child.size()!=0){
            for(Contact con : parent_A_Child){
                wrapperList_A.add(new ContactWrapper(con));
            }
        }
        if(parent_B_Child.size()!=0){
            for(Contact con : parent_B_Child){
                wrapperList_B.add(new ContactWrapper(con));
            }
        }
    }
    
    public void resetAll(){
        try{
            List<Contact> conListToDel = [select id from contact WHERE AccountId=:parent_A_ID OR AccountId=:parent_B_ID];
            Database.deleteResult[] ddr = Database.delete(conListToDel,false);
            List<Id> IdsToDel = new List<Id>();
            for(Database.deleteResult  d : ddr){
                if(d.isSuccess()){
                    IdsToDel.add(d.getId());
                }
            }
            if(IdsToDel.isEmpty()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Nothing to reset'));
                return;    
            }
            Database.emptyRecycleBin(IdsToDel);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Reseted Successfully'));
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Nothing to reset'));
        }
        retrieveContacts();
        isSelectAll = false;
        startFrom = 1;
    }

    public void deletePermanently(){
        setTriggerFire();
        List<Contact> conToDel = new List<Contact>();
        for(ContactWrapper cw : wrapperList_A){
            if(cw.isSelected){
                conToDel.add(cw.con);
            }
        }
        for(ContactWrapper cw : wrapperList_B){
            if(cw.isSelected){
                conToDel.add(cw.con);
            }
        }
       try{
           if(conToDel.size()==0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No records selected to delete'));
                return;
            }
            Database.deleteResult[] ddr = Database.delete(conToDel,false);
            List<Id> IdsToDel = new List<Id>();
            for(Database.deleteResult  d : ddr){
                if(d.isSuccess()){
                    IdsToDel.add(d.getId());
                }
            }
            Database.emptyRecycleBin(IdsToDel);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Records successfully deleted'));
       }catch(Exception e){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
       }
        retrieveContacts();
        isSelectAll = false;
    }

    public void save(){
        setTriggerFire();
        List<Contact> conToUpdate = new List<Contact>();
        for(ContactWrapper cw : wrapperList_A){
                conToUpdate.add(cw.con);
        }
        for(ContactWrapper cw : wrapperList_B){
                conToUpdate.add(cw.con);
        }
        update conToUpdate;
        isEditMode = false;
        retrieveContacts();
    }
    
    public void deleteSeleted(){
        setTriggerFire();
        List<Contact> conToDel = new List<Contact>();
        for(ContactWrapper cw : wrapperList_A){
            if(cw.isSelected){
                conToDel.add(cw.con);
            }
        }
        for(ContactWrapper cw : wrapperList_B){
            if(cw.isSelected){
                conToDel.add(cw.con);
            }
        }
       try{
           if(conToDel.size()==0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No records selected to delete'));
                return;
            }
            delete conToDel;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Records successfully deleted'));
       }catch(Exception e){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
       }
        retrieveContacts();
        isSelectAll = false;
    }
    
    public void undeleteAll(){
        setTriggerFire();
        generateParentIds();
        List<Contact> conToUndel = [SELECT lastName, sequence_number__c from contact
                                    WHERE (AccountId=:parent_A_id OR AccountId=:parent_B_id) AND isDeleted=true
                                    Order By Sequence_Number__c ALL ROWS];
        if(conToUndel.size()==0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No records to undelete'));
            return;
        }
      	try{
            database.undelete(conToUndel,false);
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Records undeleted successfully'));
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
        retrieveContacts();
    }
    
    public void deleteSingle(){
        setTriggerFire();
        Id idToDel = ApexPages.currentPage().getParameters().get('idToDel');
        Database.delete(idToDel);
        retrieveContacts();
    }
    
    public void bulkInsert(){
        try{
            setTriggerFire();
            generateParentIds();
            List<Contact> conToInsert = new List<Contact>();
            Integer i;
            for(i=startFrom; i<startFrom+numberOfContacts; i++){
                conToInsert.add(new Contact(LastName='c'+i, accountId=selected_parent_ID, sequence_number__c = insertSequence));
            }
            startFrom = i;
            insert conToInsert;
            retrieveContacts();
            insertSequence = null;
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
    }
    
    public void singleInsert(){
        setTriggerFire();
        generateParentIds();
        Contact con = new Contact();
        con.lastName = 'c'+String.valueOf(startFrom);
        con.sequence_number__c = insertSequence;
        con.accountID = selected_parent_ID;
        insert con;
        startFrom++;
        retrieveContacts();
        insertSequence = null;
    }
    
    public void emptyRecycleBin(){
        try{
            setTriggerFire();
            List<Contact> deletedContacts =[select id from contact WHERE (AccountId=:parent_A_ID OR AccountId=:parent_B_ID) AND isDeleted=true ALL ROWS];
            if(deletedContacts.size()==0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Recycle bin is already empty'));
                return;
            }
            Database.emptyRecycleBin(deletedContacts);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Recycle bin emptied successfully'));
            retrieveContacts();
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
    }
    
    
    public void retrieveContacts(){
        try{
            generateParentIds();
            parent_A_Child = [SELECT lastName, Sequence_number__c, accountid FROM Contact WHERE accountID = :parent_A_id order by sequence_number__c];
            parent_B_Child = [SELECT lastName, Sequence_number__c, accountid FROM Contact WHERE accountID = :parent_B_id order by sequence_number__c];           
            List<Contact> allCon = new List<Contact>();
            allCon.addAll(parent_A_Child);
            allCon.addAll(parent_B_Child);
            conMap = new Map<ID,contact>(allCon);
            prepareWrapperList();
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
    }
}