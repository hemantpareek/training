@isTest
global class AuthorizeDotNetMockCallout implements HttpCalloutMock {
    public static Integer responseCode = 200;
    global HTTPResponse respond(HTTPRequest req) {
        //TEST FOR CREDIT CARD PAYMENT
        if(req.getBody().indexOf('creditCard') != -1){
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"messages":{"message":[{"code":"SAMPLE_CODE","text":"CREDIT_CARD_PAYMENT"}]}}');
            res.setStatusCode(responseCode);
            return res;
        }
        else{
        //TEST FOR ECHECK PAYMENT
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"messages":{"message":[{"code":"SAMPLE_CODE","text":"ECHECK_PAYMENT"}]}}');
            res.setStatusCode(responseCode);
            return res;
        }
    }
}