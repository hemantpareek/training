public class NF_APIUtil {
    @AuraEnabled
    public static String getOrderDetail(String email, String orderId){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/orderhistory/v5');
        request.setMethod('GET');
        request.setBody('{"email": "'+email+'","orderId": "'+ orderId +'","activeCountry": "INDIA"}');
        NF_OrderMockAPI obj = new NF_OrderMockAPI();
        HttpResponse response = obj.respond(request);
        
        //parse here
        return response.getBody();
    }
    @AuraEnabled
    public static String orderByIdAndEmail(String email, String orderId){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/orderhistory/v6/orderByIdAndEmail');
        request.setMethod('GET');
        request.setBody('{"email": "'+email+'","orderId": "'+ orderId +'"}');
        NF_OrderMockAPI obj = new NF_OrderMockAPI();
        HttpResponse response = obj.respond(request);
        
        //parse here
        return response.getBody();
    }
    @AuraEnabled
    public static String searchOrder(String email){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/orderhistory/v6/search');
        request.setMethod('POST');
        request.setBody('{"email": "'+email+'"}');
        NF_OrderMockAPI obj = new NF_OrderMockAPI();
        HttpResponse response = obj.respond(request);
         
        //parse here
        return response.getBody();
    }
    @AuraEnabled
    public static String searchOrder(String email, String phone, String zip){
         Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/orderhistory/v6/search');
        request.setMethod('POST');
        request.setBody('{"email": "'+email+'","phone": "'+ phone +'","zip": "'+ zip +'"}');
        NF_OrderMockAPI obj = new NF_OrderMockAPI();
        HttpResponse response = obj.respond(request);
        
        //parse here
        return response.getBody();
    }
    @AuraEnabled
    public static String orders(String auth_token){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/orderhistory/v6/orders');
        request.setMethod('POST');
        request.setHeader('auth_token ', auth_token);
       
      // request.setBody('{"email": "'+email+'"}');
        NF_OrderMockAPI obj = new NF_OrderMockAPI();
        HttpResponse response = obj.respond(request);
        
        //parse here
        return response.getBody();
    }
    @AuraEnabled
    public static String getOrderDetailById(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/purchasehistory/armani/order/246154');
        request.setMethod('GET');
        //request.setTimeout(60000);  
        //HttpResponse response = http.send(request);
        //APIMock obj = new APIMock();
        //HttpResponse response = obj.respond(request);
        
        //parse here
        //return response.getBody();
        return null;
    }
    @AuraEnabled
    public static String getOrderDetailByOrderType(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/purchasehistory/armani/raworder/purchase/2456714562');
        request.setMethod('GET');
        //request.setTimeout(60000);  
        //HttpResponse response = http.send(request);
        //APIMock obj = new APIMock();
        //HttpResponse response = obj.respond(request);
        
        //parse here
        //return response.getBody();
        return null;
    }
    @AuraEnabled
    public static String getOrderDetailByBrand(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/purchasehistory/armani/orders');
        request.setMethod('GET');
        //request.setTimeout(60000);  
        //HttpResponse response = http.send(request);
        //APIMock obj = new APIMock();
        //HttpResponse response = obj.respond(request);
        
        //parse here
        //return response.getBody();
        return null;
    }
    @AuraEnabled
    public static NF_PurchasedHistoryService.purchaseHistory setOrder(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://api.victoriassecret.com/purchasehistory/orderset');
        request.setMethod('POST');
        NF_PurchasedHistoryService.OrderSetRequest orderDataWrapper = new NF_PurchasedHistoryService.OrderSetRequest();
        orderDataWrapper.email = 'abc@gmail.com';
        orderDataWrapper.firstName = 'aaron';
        orderDataWrapper.lastName = 'rood';
        orderDataWrapper.resultSize = '55';
        orderDataWrapper.sortedBy = 'someone';
        orderDataWrapper.zipCode = '12345';
        orderDataWrapper.variantToSku = true;
        request.setBody(JSON.serialize(orderDataWrapper));
        // request.setTimeout(60000);  
       //  HttpResponse response = http.send(request);
        // APIMock obj = new APIMock();
        // HttpResponse response = obj.respond(request);
        
        return null; // (NF_PurchasedHistoryService.purchaseHistory)JSON.deserialize(response.getBody(), NF_PurchasedHistoryService.purchaseHistory.class);
    }
}