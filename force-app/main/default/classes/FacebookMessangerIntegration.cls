public class FacebookMessangerIntegration {
    
    public static string access_token{get;set;}
    public static string psid{get;set;}
    public static List<selectoption> users{get;set;}
    public static string msg{get;set;}
    public static string page_access_token = 'EAAgHIECUAdcBAM7OO0T2euoI7pCJ4G6tZCNY2WZA8QhIzt820f0k7dCZBZAjeswcxPZCfvrh8LFJ5WZAo4uIhnq90nCshM4P0IumO7BiHgqgfWZCG42ATnJf5KwVHCrFH95MZCQNctH17LCHmv4OxlcDD3FfUHlMVZArv0TTJhUnEqE1IQEog21N6';
    
    public FacebookMessangerIntegration(apexPages.StandardController controller){
        
    }
    
    public FacebookMessangerIntegration(){
        msg = 'hello';
        users = new List<selectoption>();
        
        for(Facebook_Messanger_User__c fmu : [Select Name,PSID__c FROM Facebook_Messanger_User__c]){
            users.add(new SelectOption('hello', fmu.name)); //value,label
        }
    }
    
    public static void getAccessToken(){
        String tokenURI = 'https://graph.facebook.com/v5.0/oauth/access_token?';
        String messageBody = 'code='+ Apexpages.currentPage().getParameters().get('code') +
            '&client_id='+ '1453041268167411' +
            '&client_secret='+ '8914c02d9b424dc4d5ef6e19dade7778' +
            '&redirect_uri='+ 'https://hemantpareek-dev-ed--c.visualforce.com/apex/FacebookMessangerIntegration' ;
        
        HttpRequest request = new HttpRequest();
        request.setEndpoint(tokenURI);
        request.setMethod('GET');
        request.setHeader('content-type', 'application/x-www-form-urlencoded'); 
        request.setHeader('Content-length', String.valueOf(messageBody.length()));  
        request.setBody(messageBody); 
        request.setTimeout(60*1000);
        Http h = new Http();
        HttpResponse res = h.send(request);
        String response = res.getBody();
        System.debug('---------------------------------------'+response);
        parseJson(response);
        FacebookMessangerIntegrationData__c fmid = [SELECT Access_Token__c FROM FacebookMessangerIntegrationData__c LIMIT 1];
        if(fmid != null){
            fmid.Access_Token__c = access_token;
        }else{
            fmid = new FacebookMessangerIntegrationData__c();
            fmid.Access_Token__c = access_token;
        }
        upsert fmid;
    }
    
    public void getPageAccessToken(){
        
    }
    
    public pageReference authUri(){
        String authuri = 'https://www.facebook.com/dialog/oauth?'+
            'client_id='+'1453041268167411'+//application id or appid
            '&client_secret='+'8914c02d9b424dc4d5ef6e19dade7778'+
            '&redirect_uri='+'https://hemantpareek-dev-ed--c.visualforce.com/apex/FacebookMessangerIntegration'+//is  siteurl
            //'&state=churlu'+
            '&scope =  publish_actions,public_profile,user_friends,email,user_about_me,user_actions.books,user_photos,user_birthday,manage_pages,publish_pages,user_posts,read_mailbox,page_messaging';
        
        PageReference pgReference=new PageReference(authuri);
        system.debug(pgreference);
        return pgReference;
    }
    
    public void getDetails(){
        access_token = [SELECT Access_Token__c FROM FacebookMessangerIntegrationData__c LIMIT 1].Access_Token__c;
        //system.debug('access_token :  ' + access_token);
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://graph.facebook.com/me?access_token=' + access_token);
        req.setMethod('GET');
        HttpResponse res = h.send(req);
        String resBody = res.getBody();
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, '' + res.getBody()));
    } 
    
    public void getFriendList(){
        access_token = [SELECT Access_Token__c FROM FacebookMessangerIntegrationData__c LIMIT 1].Access_Token__c;
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('https://graph.facebook.com/v5.0/me/friends?limit=500&access_token='+access_token);
        
        Http h= new Http();
        HttpResponse res= h.send(req);
        System.debug('-----------------------------------------'+res.getBody());
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, '' + res.getBody()));
        
    }
   
    public void sendMessage(){
        Httprequest req = new httpRequest();
        req.setEndPoint('https://graph.facebook.com/v5.0/me/messages?access_token=EAAgHIECUAdcBAM7OO0T2euoI7pCJ4G6tZCNY2WZA8QhIzt820f0k7dCZBZAjeswcxPZCfvrh8LFJ5WZAo4uIhnq90nCshM4P0IumO7BiHgqgfWZCG42ATnJf5KwVHCrFH95MZCQNctH17LCHmv4OxlcDD3FfUHlMVZArv0TTJhUnEqE1IQEog21N6');
        String body = '{'+
            '"recipient": {'+
            '"id": "' + psid + '"'+
            '},'+
            '"message": {'+
            '"text": " '+ msg +' "' +
            '}'	+
            '}';
        req.setMethod('POST');
        req.setBody(body);
        req.setHeader('Content-Type','application/json');
        Http h = new Http();
        HttpResponse res = h.send(req);
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, '' + res.getBody()));
    }
    
    public static Object parseJson(String jsonString){
        return System.JSON.deserialize(jsonString, FacebookMessangerIntegration.class);
    }
}