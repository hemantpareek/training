global class ContactTriggerV2Handler {

    private static Map<Id,Parent> parentByIdMap = new Map<Id,Parent>();
    private static String previousTriggerEvent = '';
    public static Boolean fireTrigger = true;

    //Handles insert operations
    public static void handleInsert(List<Contact> Trigger_New){
        resetStaticFields('insert');
        
        Set<Id> parentIds = getParentIds(Trigger_New);

        List<Contact> lagacyRecords = [SELECT LastName, Sequence_Number__c, AccountId FROM Contact
                                       WHERE AccountId IN :parentIds AND AccountId NOT IN :parentByIdMap.keySet() ORDER BY Sequence_Number__c NULLS LAST];
        List<Contact> conToUpdate = new List<Contact>();

        for(Id parentId : parentIds){
            Integer currentLagacyCount = 0;
            //Create Parent Object and fill them with lagacy contacts
            for(Contact con : lagacyRecords){
                if(con.AccountId == parentId){
                    currentLagacyCount++;
                    if(!parentByIdMap.containsKey(con.AccountId)){
                        parentByIdMap.put(con.AccountId, new Parent(con.AccountId));
                    } 
                    Parent p = parentByIdMap.get(con.AccountId);
                    if(!p.isLagacyRecordsRetrived){
                        con.Sequence_Number__c = null;
                        parentByIdMap.get(con.AccountId).lagacyRecords.add(con);
                    }                  
                }
            }
            
            //Mark Current Parents lagacy-Retrived and set max range
            if(parentByIdMap.containsKey(parentId)){
                Parent p = parentByIdMap.get(parentId);
                p.isLagacyRecordsRetrived = true;
                if(!p.isFinalListInitialized){
                    p.maxRange = currentLagacyCount + Trigger_New.size();
                }else{
                    p.maxRange += Trigger_New.size();
                }
            }
            
            //Now fill parent will trigger.new records
            for(Contact con : Trigger_New){
                if(con.AccountId == parentId){
                    if(!parentByIdMap.containsKey(con.AccountId)){
                        parentByIdMap.put(con.AccountId, new Parent(con.AccountId, true, Trigger_New.size())); //At this state we assume that lagacy records are retrieved for this account
                    }
                    parentByIdMap.get(con.AccountId).filterRecords(con);
                }
            }

            //process records
            if(parentByIdMap.containsKey(parentId)){
                Parent p = parentByIdMap.get(parentId);
                p.initializeFinalList(); //with null
                p.addToListWithShifting(p.validSeqRecords);
                p.fillVacancies(p.lagacyRecords);
                p.fillVacancies(p.nullSeqRecords);
                p.fillVacancies(p.invalidSeqRecords);
                p.fillVacancies(p.outOfRangeSeqRecords);
                p.fillVacancies(p.runtimeOutOfRangeSeqRecords);
            }

            //Add lagacy records to update list
            if(parentByIdMap.containsKey(parentId)){
                for(Contact con : parentByIdMap.get(parentId).getFinalList()){
                    if(con!=null && con.id != null){ //id will be there for lagacy records
                        conToUpdate.add(con);
                    }
                }
            }
        }
        fireTrigger = false;
        update conToUpdate;
        fireTrigger = true;
    }

    //Handles update Operations
    public static void handleUpdate(Map<Id,Contact> Trigger_newMap, Map<Id,Contact> Trigger_oldMap){
        resetStaticFields('update');
    }

    //Handles update Operations
    public static void handleDelete(List<Contact> Trigger_old){
    }

    //Handles update Operations
    public static void handleUndelete(List<Contact> Trigger_new){
        resetStaticFields('undelete');
    }


    //Provides parentIds for the triggered contacts
    private static Set<Id> getParentIds(List<Contact> conLst){
        Set<Id> parentIds = new Set<Id>();
        for(Contact con : conLst){
            parentIds.add(con.AccountId);
        }
        return parentIds;
    }

    public static void resetStaticFields(String triggeredEvent){
        //Will be used when both update and insert will be performed in single txn
        System.debug('-----------------------------triggeredEvent : '+ triggeredEvent + '------------------------------------------');
        if(previousTriggerEvent != '' && previousTriggerEvent != triggeredEvent){
            parentByIdMap.clear();
        }
        previousTriggerEvent = triggeredEvent;
    }

    private class Parent{
        private Id recordId;
        private Boolean isLagacyRecordsRetrived;
        private Boolean isFinalListInitialized = false;
        private Integer currentFillSequence = 0;
        private Integer maxRange = 0;
        private Integer batchCount = 0;
        private List<Contact> finalRecords = new List<Contact>();
        private List<Contact> lagacyRecords = new List<Contact>();
        private List<Contact> validSeqRecords = new List<Contact>();
        private List<Contact> outOfRangeSeqRecords = new List<Contact>();
        private List<Contact> nullSeqRecords = new List<Contact>();
        private List<Contact> invalidSeqRecords = new List<Contact>();
        private List<Contact> runtimeOutOfRangeSeqRecords = new List<Contact>();
        private List<Contact> reparentedRecords = new List<Contact>();
        
        private Parent(String recordId, Boolean isLagacyRecordsRetrived, Integer maxRange){
            this.recordId = recordId;
            this.isLagacyRecordsRetrived = isLagacyRecordsRetrived;
            this.maxRange = maxRange;
        }
        
        private Parent(String recordId){
            this(recordId, false, 0);
        }

        private void filterRecords(Contact con){
            Double seq = con.Sequence_Number__c;
            if(seq == null){
                nullSeqRecords.add(con);
            }else if(seq <= 0){
                invalidSeqRecords.add(con);
            }else if(seq > maxRange){
                outOfRangeSeqRecords.add(con);
            }else if(Math.ceil(seq) != Math.floor(seq)){
                invalidSeqRecords.add(con);
            }else{
                validSeqRecords.add(con);
            }
        }

        private void initializeFinalList(){
            batchCount++;
            Integer startPosition;
            if(!isFinalListInitialized){
                isFinalListInitialized = true;
                startPosition = 0;
            }else{
                startPosition = finalRecords.size();
            }
            for(Integer i=startPosition; i<maxRange; i++){
                finalRecords.add(null);
            }
        }

        private void addToListWithShifting(List<Contact> conLst){
            for(Contact con : conLst){
                shiftRecords(con);
            }
        }

        private void shiftRecords(Contact con){
            Integer seqIndex = (Integer)con.sequence_Number__c - 1;
            if(finalRecords.get(seqIndex) != null){                 
                Contact conToShift = finalRecords.get(seqIndex);
                conToShift.Sequence_Number__c += 1;
                if(conToShift.Sequence_Number__c <= finalRecords.size()){
                    shiftRecords(conToShift);
                }else{
                    runtimeOutOfRangeSeqRecords.add(conToShift);
                }
            }
            finalRecords.set(seqIndex, con);
        }

        private void removeFromList(List<Contact> conToRemove, List<Contact> conToRemoveFrom){
            //Create Wrapper from Existing finalRecords List
            List<Child> conToRemoveFromWrprList = new List<Child>();
            for(Contact con : conToRemoveFrom){
                conToRemoveFromWrprList.add(new Child(con));
            } 
            for(Contact con : conToRemove){
                Integer conIndex = conToRemoveFromWrprList.indexOf(new Child(con));
                if(conIndex != -1){
                    conToRemoveFromWrprList.remove(conIndex);
                }
            }
            conToRemoveFrom.clear();
            for(Child c : conToRemoveFromWrprList){
                conToRemoveFrom.add(c.con);
            }
        }

        private void fillVacancies(List<Contact> conLst){
            for(Contact con : conLst){
                currentFillSequence = 0;
                for(Integer i=currentFillSequence; i<maxRange; i++){
                    currentFillSequence++;
                    if(finalRecords.get(i) == null){
                        con.Sequence_Number__c = currentFillSequence;
                        finalRecords.set(i, con);
                        break;
                    }
                }
            }
        }

        private void displayList(List<Contact> conLst, String lstName){
            if(conLst.isEmpty()){
                return;
            }
            System.debug('---------------------------------- ' + lstName + ' ---------------------Started---------------------- : ' + conLst.size() + ', maxRange : ' + maxRange + ', Batchcount: ' + batchCount);
            for(Contact con : conLst){
                if(con!=null){
                    System.debug(con.lastName + ' <=> ' + con.Sequence_number__c);
                }else{
                    System.debug(null);
                }
            }
            System.debug('---------------------------------- ' + lstName + ' ---------------------Ended----------------------');
        }

        private List<Contact> getFinalList(){
            //clear all temp list at this point bcz they will be used for next batch
            displayList(validSeqRecords, 'validSeqRecords');
            displayList(lagacyRecords, 'lagacyRecords');
            displayList(outOfRangeSeqRecords, 'outOfRangeSeqRecords');
            displayList(nullSeqRecords, 'nullSeqRecords');
            displayList(invalidSeqRecords, 'invalidSeqRecords');
            displayList(runtimeOutOfRangeSeqRecords, 'runtimeOutOfRangeSeqRecords');
            displayList(reparentedRecords, 'reparentedRecords');
            displayList(finalRecords, 'finalRecords');
            validSeqRecords.clear();
            lagacyRecords.clear();
            outOfRangeSeqRecords.clear();
            nullSeqRecords.clear();
            invalidSeqRecords.clear();
            runtimeOutOfRangeSeqRecords.clear();
            reparentedRecords.clear();
            return finalRecords;
        }
    }

    global class Child implements Comparable {
        
        private String recordId;
        private Contact con;

        public Child(Contact con){
            this.con = con;
            recordId = con != null ? con.id : null;
        }

        // Implement the compareTo() method
        global Integer compareTo(Object compareTo) {
            Child compareToChld = (Child)compareTo;
            if (recordId == compareToChld.recordId) return 0;
            if (recordId > compareToChld.recordId) return 1;
            return -1;        
        }
    }
}