public class ContactSequenceController {
    public static boolean flag = true;
    public static void add(List<Contact> contactList){
        
        //System.debug(contactList.size());
        Map<Id,List<Contact>> AccAssociatedWithContactMap = new Map<Id,List<Contact>>();
        Set<Id> AccountIdSet = new Set<Id>();
        for(Contact conObj : contactList){
            AccountIdSet.add(conObj.AccountId);
        }
        
        for(Account accList : [Select id,(Select accountId,Sequence_Number__c from contacts) from Account where Id =: AccountIdSet] ){
            List<Contact> toMap = New List<Contact>();
            for(Contact Con : accList.contacts){
                toMap.add(con);
            }
            AccAssociatedWithContactMap.put(accList.Id,toMap);
        }
        Integer Count = 0;
        List<Contact> conToUpdateList = new List<Contact>();
        List<Contact> toUpdateList = new List <Contact>();
        for(Id accId : AccAssociatedWithContactMap.keySet()){
            if((!AccAssociatedWithContactMap.get(accId).isEmpty()) && (AccAssociatedWithContactMap.get(accId)[0].Sequence_Number__c == 0 || AccAssociatedWithContactMap.get(accId)[0].Sequence_Number__c == null)){
                for(Contact conObj : AccAssociatedWithContactMap.get(accId)){
                    count++;
                    conObj.Sequence_Number__c = count;
                }
                conToUpdateList.addall(AccAssociatedWithContactMap.get(accId));
            }
            else {
                for(Contact conObj : contactList){
                    if(accId == conObj.AccountId){
                        if(conObj.Sequence_Number__c == null || conObj.Sequence_Number__c == 0){
                            conObj.Sequence_Number__c = ++count;
                        }else{
                            for(Contact conObjMap : AccAssociatedWithContactMap.get(accId)){
                                if(conObjMap.Sequence_Number__c == conObj.Sequence_Number__c ){
                                  //  System.debug(AccAssociatedWithContactMap.get(accId).size());
                                    conObjMap.Sequence_Number__c = AccAssociatedWithContactMap.get(accId).size() + 1;
                                    toUpdateList.add(conObjMap);
                                    flag = false;
                                }
                            }
                        }
                    } 
                }
            }
        }
        if(!toUpdateList.isEmpty()){
            Update toUpdateList;
        }
        if(!conToUpdateList.isEmpty()){
            update conToUpdateList;
        }
        flag = false;
    }
    public static void UpdateContact(List<Contact> contactList , List<Contact> oldContactList){
        //System.debug('Before Update');
        Map<Id,List<Contact>> AccAssociatedWithContactMap = new Map<Id,List<Contact>>();
        Set<Id> AccountIdSet = new Set<Id>();
        Set<Id> oldAccountIdSet = new Set<Id>();
        for(Contact conObj : oldContactList){
            oldAccountIdSet.add(conObj.AccountId);
        }
        for(Contact conObj : contactList){
            AccountIdSet.add(conObj.AccountId);
        }
        for(Account accList : [Select id,(Select LastName,accountId,Sequence_Number__c from contacts) from Account where Id =: AccountIdSet order by CreatedDate] ){
            List<Contact> toMap = New List<Contact>();
            for(Contact Con : accList.contacts){
                toMap.add(con);
            }
            AccAssociatedWithContactMap.put(accList.Id,toMap);
        }
        if(flag){
            if(AccountIdSet == oldAccountIdSet){
              //  System.debug('alliswell');
                List<Contact> toUpdate = new List<Contact>();
                for(List<Contact> conList : AccAssociatedWithContactMap.values()){
                    Integer ListSize = conList.Size();
                    for(Contact conObj : contactList){
                        if(conObj.AccountId == conList[0].AccountID && conObj.Sequence_Number__c > ListSize || conObj.Sequence_Number__c < 1){
                            conObj.addError('enter a valid number greater than zero and less than ' + ListSize);
                            flag = false;
                        }
                    }
                } 
            }
        }
    }
    public static void afterupdate(List<Contact> newContactList , List<Contact> oldContactList){
        System.debug('@@@ After update : outside if' + newContactList.size());
        if(flag){
            //new contact List
            System.debug('@@@ After update : inside if');
            flag = false;
            
            Set<Id> newAccountIdSet = new Set<Id>();
            for(Contact conObj : newContactList){
                newAccountIdSet.add(conObj.AccountId);
            }
            
            Set<Id> oldAccountIdSet = new Set<Id>();
            for(Contact conObj : oldContactList){
                oldAccountIdSet.add(conObj.AccountId);
            }
            if(newAccountIdSet == oldAccountIdSet){
                Map<Id,List<Contact>> newContactMap = new Map<Id,List<Contact>>();
                Map<Id,List<Contact>> oldContactMap = new Map<Id,List<Contact>>();
                for(Account accList : [Select id,(Select accountId,Sequence_Number__c,LastName from contacts) from Account where Id =: newAccountIdSet ] ){
                    List<Contact> toMap = New List<Contact>();
                    for(Contact Con : accList.contacts){
                        toMap.add(con);
                    }
                    newContactMap.put(accList.Id,toMap);
                }
                List<Contact> toUpdate = new List<Contact>();
                for(Id accId : newContactMap.keySet()){
                    for(Integer i = 0 ; i < newContactMap.get(accId).size() ; i++){
                        for(Integer k = 0 ; k < newContactMap.get(accId).size() ; k++){
                            if(newContactMap.get(accId)[i].Sequence_Number__c == newContactMap.get(accId)[k].Sequence_Number__c && newContactMap.get(accId)[i] != newContactMap.get(accId)[k]){
                                for(Contact conObj : oldContactList){
                                    if(newContactMap.get(accId)[i].id == conObj.id){
                                        newContactMap.get(accId)[k].Sequence_Number__c = conObj.Sequence_Number__c;
                                        toUpdate.add(newContactMap.get(accId)[k]);
                                        flag = false;
                                    }
                                }
                            }
                        }
                    }
                }
                Update toUpdate;
                flag = false;
            }else{
                flag = false;
                Map<Id,List<Contact>> newContactMap = new Map<Id,List<Contact>>();
                Map<Id,List<Contact>> oldContactMap = new Map<Id,List<Contact>>();
              //  System.debug('Reparenting');
                Integer count = 1;
                List<Contact> newToUpdate = new List<Contact>();
                for(Account accList : [Select id,(Select accountId,Sequence_Number__c,name from contacts order by LastModifiedDate) from Account where Id =: newAccountIdSet ] ){
                    List<Contact> toMap = New List<Contact>();
                    for(Contact Con : accList.contacts){
                        toMap.add(con);
                    }
                    newContactMap.put(accList.Id,toMap);
                }
                for(List<Contact> conList : newContactMap.values()){
                    count = 1;
                    for(Contact conObj : conList){
                        conObj.Sequence_Number__c = count;
                        count++;
                        newToUpdate.add(conObj);
                    }
                }
                Update newToUpdate;
                List<Contact> oldToUpdate = new List<Contact>();
                for(Account accList : [Select id,(Select accountId,Sequence_Number__c,name from contacts order by LastModifiedDate) from Account where Id =: oldAccountIdSet ] ){
                    List<Contact> toMap = New List<Contact>();
                    for(Contact Con : accList.contacts){
                        toMap.add(con);
                    }
                    oldContactMap.put(accList.Id,toMap);
                }
                for(List<Contact> conList : oldContactMap.values()){
                    count = 1;
                    for(Contact conObj : conList){
                        conObj.Sequence_Number__c = count;
                        count++;
                        oldToUpdate.add(conObj);
                    }
                }
                flag = false;
                Update oldToUpdate;
                flag = false;
            }
        }
        flag = false;
    }
    public static void afterDelete(List<Contact> contactList){
        if(flag){
           // System.debug('After Delete');
            flag = false;
            Map<Id,List<Contact>> AccAssociatedWithContactMap = new Map<Id,List<Contact>>();
            Set<Id> AccountIdSet = new Set<Id>();
            for(Contact conObj : contactList){
                AccountIdSet.add(conObj.AccountId);
            }
            for(Account accList : [Select id,(Select accountId,Sequence_Number__c from contacts ) from Account where Id =: AccountIdSet order by CreatedDate] ){
                List<Contact> toMap = New List<Contact>();
                for(Contact Con : accList.contacts){
                    toMap.add(con);
                }
                AccAssociatedWithContactMap.put(accList.Id,toMap);
            }
            Integer count = 1;
            List<Contact> newToUpdate = new List<Contact>();
            for(List<Contact> conList : AccAssociatedWithContactMap.values()){
                count = 1;
                for(Contact conObj : conList){
                    conObj.Sequence_Number__c = count;
                    count++;
                    newToUpdate.add(conObj);
                }
            }
            update newToUpdate;
        }
    }
    public static void afterUndelete(List<Contact> contactList){
        if(flag){
         //   System.debug(contactList[0].Sequence_Number__c);
         //   System.debug('After Undelete');
            flag = false;
            Map<Id,List<Contact>> AccAssociatedWithContactMap = new Map<Id,List<Contact>>();
            Set<Id> AccountIdSet = new Set<Id>();
            for(Contact conObj : contactList){
                AccountIdSet.add(conObj.AccountId);
            }
            for(Account accList : [Select id,(Select accountId,Sequence_Number__c from contacts ) from Account where Id =: AccountIdSet order by CreatedDate]){
                List<Contact> toMap = New List<Contact>();
                for(Contact Con : accList.contacts){
                    toMap.add(con);
                }
                AccAssociatedWithContactMap.put(accList.Id,toMap);
            }
            Integer count = 1;
            List<Contact> newToUpdate = new List<Contact>();
            for(List<Contact> conList : AccAssociatedWithContactMap.values()){
                count = 1;
                for(Contact conObj : conList){
                    conObj.Sequence_Number__c = count;
                    count++;
                    newToUpdate.add(conObj);
                }
            }
            update newToUpdate;
            flag = false;
        }
    }
}