public class ContactTriggerHandler_Peeyush {
	public static Boolean isCheck = true;
    public static Integer sequenceNumberNew = 1;
    public static Integer sequenceNumberOld = 1;
    public static Integer sequenceNumberTest = 1;
    public static Integer size = 0;
    public static list<Contact> listofcontactforcommanNumber = new list<Contact>();
	List<Contact> conList2 = new List<Contact>();
    
    // static Map<Integer ,Contact> mapOfContact = new Map<Integer,Contact>();
  
    
    public void OnBeforeInsert(List<Contact> newContactList){
        //system.debug('@@@@@@@@@@@@@'+newContactList);
        manageSequenceNumberBeforeInsert(newContactList, null);
    }
    
    public void OnBeforeUpdate(List<Contact> newContactList , List<Contact> oldContactList){
        //system.debug('@@@@@@@@@@@@@'+newContactList);
        manageSequenceNumberBeforeUpdate(newContactList, oldContactList);
    }
    
    
    public void OnBeforeDelete(List<Contact> oldContactList){
        //system.debug('@@@@@@@@@@@@@'+newContactList);
        manageSequenceNumberBeforeDelete(oldContactList, null);
    }
    
    public void OnAfterUndelete(List<Contact> newContactList){
        //system.debug('@@@@@@@@@@@@@'+newContactList);
        manageSequenceNumberAfterUnDelete(newContactList, null);
    }
    
 /*   
    public void OnAfterInsert(List<Contact> newContactList){
        //system.debug('@@@@@@@@@@@@@'+newContactList);
        // manageSequenceNumber(newContactList, null);
    }
    
    public  void OnAfterUpdate(List<Contact> newContactList){
        //system.debug('@@@@@@@@@@@@@'+newContactList);
        // manageSequenceNumber(newContactList, null);
    }
    
    
    
    public  void OnAfterDelete(List<Contact> newContactList){
        //system.debug('@@@@@@@@@@@@@'+newContactList);
        // manageSequenceNumber(newContactList, null);
    }
    */
    
    
    
    Public  void manageSequenceNumberBeforeInsert(List<Contact> newContactList, List<Contact> oldContactList){
        //System.debug('we are in insert method');
  
        Integer countsequencenumberforoldList = 1;
        Integer countsequencenumberfornewList = 1;
        Integer count = 0;
        List<Contact> conList = new List<Contact>();
        List<Contact> ConlistOld = new List<Contact>();
        set<Id> accontIds = new set<Id>();
        for(Contact con : newContactLIst) {        
            accontIds.add(con.AccountId);   
        }
        conList = [select Id , Name , Sequence_Number__c from contact where AccountId in :accontIds  order by Sequence_Number__c ASC NUllS LAST];
        
        if(conList.isEmpty()){
            countsequencenumberfornewList = 1; 
        }
        else {
            countsequencenumberfornewList = conList.Size() + 1;
            for(Contact con :  conList) {
                if(con.Sequence_Number__c == null) {
                    con.Sequence_Number__c = countsequencenumberforoldList;
                    ConlistOld.add(con);
                    countsequencenumberforoldList++;
                }
                else{
                    countsequencenumberforoldList++;
                }
            }
        }
        if(!ConlistOld.isEmpty()) {
            isCheck = false;
            update ConlistOld;
            isCheck = true;
        }
        
        for(Integer i =0 ;i<newContactLIst.Size()-1;i++){
            if(newContactLIst[i].Sequence_Number__c == newContactLIst[i+1].Sequence_Number__c  && newContactLIst[i].Sequence_Number__c != null ) {
                count ++;
            }
        }
        
        if(count == 0) {
            for(Contact con : newContactLIst) {     
                con.Sequence_Number__c = countsequencenumberfornewList;
                countsequencenumberfornewList++;
            }
        }
	else {
            System.debug('we are in else part and count is ' + count);
            for(contact con : newContactLIst ){
                System.debug('name before is ==>>>>' +con.Name + 'number before is ==>>>>' + con.Sequence_Number__c);
            }
            System.debug('we are in else part and size before is ' + size);
            size = size +  newContactLIst.size();
            System.debug('we are in else part and size is ' + size);
            for(Contact con : newContactLIst) {     
                con.Sequence_Number__c = size;
                System.debug('name is ==>>>>' +con.Name + 'number is ==>>>>' + con.Sequence_Number__c);
                size--;
            } 
           //  size = size +  newContactLIst.size();
        }
		
        // System.debug('size before is ' + size);
    }
    
    public void manageSequenceNumberBeforeUpdate(List<Contact> newContactList, List<Contact> OldContactList){
        
        
        
        List<Contact> conlistofnewAccount = new List<Contact>();
        List<Contact> conList = new List<Contact>();
        List<Contact> conlistofoldAccount = new List<Contact>();
        List<Contact> ConlistTest = new List<Contact>();
        set<Id> accontidNew = new set<Id>();
        set<Id> accontidOld = new set<Id>();
        
        for(Contact con : newContactLIst) {        
            accontidNew.add(con.AccountId);
        }
        
        conlistofnewAccount = [select Id , Name , Sequence_Number__c ,AccountID from contact where AccountId  IN :accontidNew and Id NOT IN:Trigger.oldMap.keySet()  order by Sequence_Number__c ASC ];
        for(Contact con : newContactLIst) { 
            
            accontidNew.add(con.AccountId);
            if(con.Sequence_Number__c < 1 ) {
                con.Sequence_Number__c = 1 ;
            } 
            else if(con.Sequence_Number__c > conlistofnewAccount.Size() + newContactLIst.Size() ) {
                con.Sequence_Number__c = conlistofnewAccount.Size() + 1; 
            }
            ConList.add(con);
          // System.debug('name for new in new contact list is>>>' + con.Name + 'Sequence number for new is==>>>' + con.Sequence_Number__c);
        }
        
        sorting(conList);
      //  for(Contact Con : conList){
           // sequenceNumberNew = (Integer)con.Sequence_Number__c;
          // System.debug('name for new is>>>' + con.Name + 'Sequence number for new is==>>>' + con.Sequence_Number__c);
       // }
			sequenceNumberNew = (Integer)conList[0].Sequence_Number__c;
        //sequenceNumberNew = (Integer)newContactLIst[0].Sequence_Number__c;
        conList.clear();
        for(Contact con : OldContactList ){
            conList.add(con);
            accontidOld.add(con.AccountId);
            // System.debug('name for new in old contact list is>>>' + con.Name + 'Sequence number for new is==>>>' + con.Sequence_Number__c);
        }
        sorting(conList);
       // for(Contact Con : conList){
           // sequenceNumberOld = (Integer)con.Sequence_Number__c;
           //System.debug('name for old is>>>' + con.Name + 'Sequence number for old is==>>>' + con.Sequence_Number__c);
       // }
        
        sequenceNumberOld = (Integer)conList[0].Sequence_Number__c;
        System.debug('sequence number new before==>>>>' + sequenceNumberNew );
        System.debug('sequence number old before==>>>>' + sequenceNumberOld );
        conlistofoldAccount = [select Id , Name , Sequence_Number__c ,AccountID from contact where AccountId  IN :accontidOld and Id NOT IN:Trigger.oldMap.keySet()  order by Sequence_Number__c ASC ];
        
        if(accontidNew.equals(accontidOld)) {
            //System.debug('we are in forst if');
            for(Contact con :  conlistofnewAccount) {
                if(sequenceNumberOld > sequenceNumberNew  ) {
                   // System.debug('we are in second if');
                    if(con.Sequence_Number__c >= sequenceNumberNew && con.Sequence_Number__c <= sequenceNumberOld ) {
                      //  System.debug('we are in if');
                        //if(con.Sequence_Number__c >= sequenceNumberNew ){
                            con.Sequence_Number__c =  con.Sequence_Number__c + newContactLIst.Size();
                            ConlistTest.add(con); 
                        //}
                    }
                }
                else{
                   
                    if(con.Sequence_Number__c <= sequenceNumberNew && con.Sequence_Number__c >= sequenceNumberOld) { 
                         System.debug('we are in else part');
                        con.Sequence_Number__c =  con.Sequence_Number__c - newContactLIst.Size();
                        ConlistTest.add(con); 
                    }
                }
            }
            for(Contact con : conlistTest){
                System.debug('name for test is ==>>>' + con.Name + 'sequence for test is==>>>' + con.Sequence_Number__c);
            }
            if(!ConlistTest.isEmpty()){
                isCheck = false;
                update ConlistTest; 
                isCheck = true;
            } 
        }
     
        else {
        // System.debug('we are in else part');   
            Integer i = conlistofnewAccount.Size();
            String checkforNull = null;
            for(Id idd : accontidNew){
                checkforNull = idd;
                break;
            }
            //System.debug('size of new account is set is ==>>>' + accontidNew.size());
            if(checkforNull == null) {
                //System.debug('we are in id nulll part');
                    for(Contact con : newContactLIst) {
                    con.Sequence_Number__c = null;
                }
            }
            else{
                // System.debug('we are not in id null part');
                for(Contact con : newContactLIst){
                    con.Sequence_Number__c = i + 1 ;
                    i++;
                }  
            }
            sequenceNumberTest = newContactLIst.Size(); 
            sequenceNumberOld = (Integer)OldContactList[0].Sequence_Number__c;
            
            for(Contact con : conlistofoldAccount){
                if(con.Sequence_Number__c >= sequenceNumberOld ) {
                    con.Sequence_Number__c = sequenceNumberOld;
                    sequenceNumberOld++;
                    conList2.add(con);
                }     
            }
            if(!conList2.isEmpty()){
                isCheck = false;
                update conList2;
                isCheck = true;
            }  
            
        }
        System.debug('sequence number new after==>>>>' + sequenceNumberNew );
        System.debug('sequence number old after==>>>>' + sequenceNumberOld );
    }   
    
    public void manageSequenceNumberBeforeDelete(List<Contact> oldContactList, List<Contact> newContactList){
        
        List<Contact> conList = new List<Contact>();
        List<Contact> ConlistOld = new List<Contact>();
        set<Id> accontIds = new set<Id>();
        for(Contact con : oldContactList) {        
            accontIds.add(con.AccountId);  
        }
        sequenceNumberOld = (Integer)oldContactList[0].Sequence_Number__c;
        conList = [select Id , Name , Sequence_Number__c from contact where AccountId  IN :accontIds and Id NOT IN:Trigger.oldMap.keySet() order by Sequence_Number__c ASC];    
        for(Contact con :  conList) {
            if(con.Sequence_Number__c > sequenceNumberOld) {
                con.Sequence_Number__c = sequenceNumberOld  ;
                sequenceNumberOld++ ;
                ConlistOld.add(con);
            }    
        }
        if(!ConlistOld.isEmpty()){
            isCheck=false;
            update ConlistOld; 
            isCheck = true;
        } 
    }   
    
    public void manageSequenceNumberAfterUnDelete( List<Contact> newContactList ,List<Contact> oldContactList){
        
        Integer i = 1;
        List<Contact> conList = new List<Contact>();
        List<Contact> conList1 = new List<Contact>();
        List<Contact> conlistNew = new List<Contact>(); 
        set<Id> accontIds = new set<Id>();
        for(Contact con : newContactLIst) {        
            accontIds.add(con.AccountId);   
        }
        conList = [select Id , Name , Sequence_Number__c from contact where AccountId in :accontIds AND Id IN :trigger.newmap.keyset()  order by Sequence_Number__c ASC];
        conList1 = [select Id , Name , Sequence_Number__c from contact where AccountId in :accontIds AND Id NOT IN :trigger.newmap.keyset()  order by Sequence_Number__c ASC ];
        if(conList1.isEmpty()){
            i=1;
        }
        else {
            i = conList1.Size() + 1;
        } 
        for(Contact con :  conList) {
            con.Sequence_Number__c = i;
            conlistNew.add(con);
            i++;
        }
        if(!conlistNew.isEmpty()) {
            isCheck=false;
            update conlistNew;
            isCheck = true;
        } 
    }  
    
    public void sorting(list<Contact> conList){
        
        for (Integer i = 0; i <ConList.size(); i++) {
            for (Integer j = i; j > 0; j--) {
                if (ConList[j-1].Sequence_Number__c > ConList[j].Sequence_Number__c) {
                    Integer temp = (Integer) ConList[j].Sequence_Number__c;
                    ConList[j].Sequence_Number__c = ConList[j-1].Sequence_Number__c;
                    ConList[j-1].Sequence_Number__c = temp;
                }
            }
        }   
    }
} 



//==>>>>>>>>>>>>>>>>>>>>>>>>u r in update mehod
//   
      /*  else if(accontidNew == null && accontidOld != null ) {
            
             System.debug('we are in else if part'); 
            Integer i = 1;
            for(Contact con : newContactLIst) {
                con.Sequence_Number__c = null;
            }
            sequenceNumberOld = (Integer)OldContactList[0].Sequence_Number__c; 
            i=sequenceNumberOld;
            
            for(Contact con : conlistofoldAccount){
                if(con.Sequence_Number__c >= sequenceNumberOld ) {
                    con.Sequence_Number__c = sequenceNumberOld;
                    sequenceNumberOld++;
                    conList2.add(con);
                }     
            }
            if(!conList2.isEmpty()){
                isCheck = false;
                update conList2;
                isCheck = true;
            }  
        }
        
      
        
        */
///* for(Contact con : conlistofoldAccount){
/*        if(con.Sequence_Number__c >= sequenceNumberOld ) {
con.Sequence_Number__c = i;
i++;
conList2.add(con);
}     
}
if(!conList2.isEmpty()){
isCheck = false;
update conList2;  
}
*/
//// System.debug('hello update Account id');
/*  if(!conList2.isEmpty()){
System.debug('conList 2 is not ewmpty');
sorting(conList2);
sequenceNumberOld = (Integer)conList2[0].Sequence_Number__c;
}
for(Contact con : conList2){
System.debug('Name in con list is==>>>' + con.Name + 'SEquene Number is ==>>' + con.Sequence_Number__c);
}
*/
//else if(accontidNew == Null) {
// Integer i = 1;
//List<Contact> conList2 = new List<Contact>();
/*      for(Contact con : newContactLIst) {
con.Sequence_Number__c = null;
}
// sequenceNumberOld = (Integer)OldContactList[0].Sequence_Number__c; 
/*  i=sequenceNumberOld;
for(Contact con : conlistofoldAccount){
if(con.Sequence_Number__c >= sequenceNumberOld ) {
con.Sequence_Number__c = i;
i++;
conList2.add(con);
}     
}
if(!conList2.isEmpty()){
isCheck = false;
update conList2;  
}*/
/*      if(!conList2.isEmpty()){
sorting(conList2);
sequenceNumberOld = (Integer)conList2[0].Sequence_Number__c;
}
Integer i = conlistofnewAccount.Size();
System.debug('VAlue of i is===>>>' + i);

sequenceNumberTest = newContactLIst.Size(); 
// System.debug('total size for old ==>>' + sequenceNumberTest  );
//  System.debug('SEQUENCE NUMBER OLD IS : ==>>' + sequenceNumberOld  );

for(Contact con : conlistofoldAccount){
if(con.Sequence_Number__c >= sequenceNumberOld ) {
con.Sequence_Number__c = con.Sequence_Number__c - sequenceNumberTest;
conList2.add(con);
}     
}
if(!conList2.isEmpty()){
isCheck = false;
update conList2;
isCheck = true;
}  

}
//   /*  for(Integer I  : mapOfContact.keySet() ){
/*  if(mapOfContact.containsKey(I)){
System.debug('Key is ==>>>' + I);
System.debug('value is ==>>>' + mapOfContact.get(I));
}
}

if(!mapOfContact.isEmpty()) {
isCheck = false;
update mapOfContact.values();
isCheck = true;

} 
*/          



//
// for(Contact con :  conlistofnewAccount) {
/*    if(sequenceNumberOld < sequenceNumberNew ) {
System.debug('WE ARE IN IF PART');
if(con.Sequence_Number__c <= sequenceNumberNew && con.Sequence_Number__c >= sequenceNumberOld ) {
con.Sequence_Number__c =  con.Sequence_Number__c - newContactLIst.Size();
mapOfContact.put((Integer)con.Sequence_Number__c ,con);
ConlistTest.add(con);
}
}
else{System.debug('WE ARE IN ELSE PART');
// sequenceNumberNew = (Integer)newContactLIst[0].Sequence_Number__c; 
// if(con.Sequence_Number__c >= sequenceNumberNew && con.Sequence_Number__c <= sequenceNumberOld ) { 
if(con.Sequence_Number__c >= sequenceNumberNew && con.Sequence_Number__c <= sequenceNumberOld ) {
SYstem.debug('We are in else if part');
con.Sequence_Number__c =  con.Sequence_Number__c + newContactLIst.Size();
mapOfContact.put((Integer)con.Sequence_Number__c ,con);
ConlistTest.add(con);
}

}
/*  if(newContactLIst.Size() > 1){
System.debug('WE ARE IN EQUAL SEQUENCE NUMBER');
Integer j = (Integer)newContactLIst[0].Sequence_Number__c;
System.debug('size is >>' +newContactLIst.Size() );
for(Integer i = 0 ; i < newContactLIst.Size() ; i++ ){
if( i == newContactLIst.Size() - 1){
newContactLIst[i].Sequence_Number__c = j;
break;
}
else{
if(newContactLIst[i].Sequence_Number__c == newContactLIst[i+1].Sequence_Number__c ){
newContactLIst[i].Sequence_Number__c = j;
j++;
}
}
// isCheck = false;
}
for(Contact con : newContactLIst ){
System.debug('NUmber is >>>>' + con.Sequence_Number__c + 'NAme is >>>>' + con.Name);
}
}
*/

/*   else if(accontidNew == Null) {
Integer i = 1;
List<Contact> conList2 = new List<Contact>();
for(Contact con : newContactLIst) {
con.Sequence_Number__c = null;
}
sequenceNumberOld = (Integer)OldContactList[0].Sequence_Number__c; 
i=sequenceNumberOld;
for(Contact con : conlistofoldAccount){
if(con.Sequence_Number__c >= sequenceNumberOld ) {
con.Sequence_Number__c = i;
i++;
conList2.add(con);
}     
}
if(!conList2.isEmpty()){
isCheck = false;
update conList2;  
}  

}
else{
System.debug('hello update Account id');
List<Contact> conList2 = new List<Contact>();
for(Contact con : conlistofnewAccount){
//System.debug('value of Name ===>>' + con.Name  + 'value of seqence Number is==>>>' +  con.Sequence_Number__c);
}
Integer i = conlistofnewAccount.Size();
System.debug('VAlue of i is===>>>' + i);
for(Contact con : newContactLIst){
con.Sequence_Number__c = i + 1 ;
System.debug('value of NEw Name ===>>' + con.Name  + 'value of seqence Number New is==>>>' +  con.Sequence_Number__c);
i++;
}         
sequenceNumberTest = newContactLIst.Size(); 
System.debug('total size for old ==>>' + sequenceNumberTest  );
// i=sequenceNumberOld;
for(Contact con : conlistofoldAccount){
if(con.Sequence_Number__c >= sequenceNumberOld ) {
con.Sequence_Number__c = con.Sequence_Number__c - sequenceNumberTest;
// i++;

conList2.add(con);
}     
}

for(Contact con : conList2){
System.debug('value of Old Name ===>>' + con.Name  + 'value of seqence Number Old is==>>>' +  con.Sequence_Number__c);
//System.debug('');
}

if(!conList2.isEmpty()){
isCheck = false;
update conList2;
isCheck = true;
}  
}*/

// uSe above code  ===>>>>>>>>>>>>>for update
// 
// 
// 
// 
// 
// 
// 
// 
/*public void manageSequenceNumberBeforeUpdate(List<Contact> newContactList, List<Contact> OldContactList){


//System.debug('WE ARE IN BEFORE UPDATE TRIGGER');
Integer sequenceNumberNew = 1;
Integer sequenceNumberOld = 1;
List<Contact> conList = new List<Contact>();
List<Contact> ConlistOld = new List<Contact>();
set<Id> accontIds = new set<Id>();
for(Contact con : newContactLIst) {        
accontIds.add(con.AccountId);
}
conList = [select Id , Name , Sequence_Number__c from contact where AccountId  IN :accontIds and Id NOT IN:Trigger.newMap.keySet() ];
for(Contact con : conList){
System.debug('name is>>>' + con.Name + 'Sequence number is >>>&&&&' + con.Sequence_Number__c);
}

for(Contact con : newContactLIst) {        
accontIds.add(con.AccountId);
if(con.Sequence_Number__c < 1 ) {
con.Sequence_Number__c = 1 ;
} 
else if(con.Sequence_Number__c > conList.Size() + 1) {
con.Sequence_Number__c = conList.Size() + 1; 
}
sequenceNumberNew = (Integer)con.Sequence_Number__c; 
}
for(Contact con : OldContactList) {        
sequenceNumberOld = (Integer)con.Sequence_Number__c;  
}
System.debug('new sequence number is >>>>' +sequenceNumberNew );
System.debug('old sequence number is >>>>' +sequenceNumberOld );

for(Contact con :  conList) {
if(sequenceNumberOld < sequenceNumberNew ) {
if(con.Sequence_Number__c <= sequenceNumberNew && con.Sequence_Number__c >= sequenceNumberOld) {
con.Sequence_Number__c =  con.Sequence_Number__c - 1;
ConlistOld.add(con);
}
}
else{
if(con.Sequence_Number__c >= sequenceNumberNew && con.Sequence_Number__c <= sequenceNumberOld ) {  
con.Sequence_Number__c = con.Sequence_Number__c + 1;
ConlistOld.add(con);
}
}
}
if(!ConlistOld.isEmpty()){
isCheck=false;
update ConlistOld;  
} 
}   

*/
/*  if(con.Sequence_Number__c > sequenceNumberOld) {
if(oldContactList.Size() > 1) {
//con.Sequence_Number__c =   con.Sequence_Number__c - sequenceNumberOld + 1 ;
con.Sequence_Number__c =  sequenceNumberOld  ;
sequenceNumberOld++ ;
ConlistOld.add(con);
// System.debug('name is>>>+cn.Name + 'Aequence number '+ con.Sequence_Number__c);
}
else {
con.Sequence_Number__c =  con.Sequence_Number__c - 1;
ConlistOld.add(con);
}
}
*/
// conList = [select Id , Name , Sequence_Number__c from contact where AccountId  IN :accontIds and Id NOT IN:Trigger.oldMap.keySet() ];.
//   if(! Trigger.newMap.containsKey(con.Id)){
//f(! Trigger.oldMap.containsKey(con.Id)){
// System.debug('we are in first for loop and in first if' + con.Id);

// accontIds.add(con.AccountId);
//Map<Id,List<Contact>> mapofAccount = new Map<Id,List<Contact>>();
/*   For(ID accID : accontIds) {
// conList = [select Id , Name , Sequence_Number__c from contact where AccountId in :accID];
String query = 'SELECT Id,Name,Sequence_Number__c FROM Contact WHERE AccountId = \'' + accId + '\'';
System.debug(query);
conList = Database.query(query);
mapofAccount.put(accId , conList);    
}

for(ID accID  : accontIds){
if(mapofAccount.containsKey(accID)){
if(!conList.isEmpty()) {
for(Contact con :  conList) {
System.debug('Name is>>' + con.Name);
con.Sequence_Number__c = i;
i++;
}
}
for(Contact con : newContactLIst) {     
con.Sequence_Number__c = i;
accontIds.add(con.AccountId);
i++;
}
}
}

*/ 

/* System.debug('conlist  size is>>>' + conList.Size());
if(!conList.isEmpty()){
for(Contact Con : conList) {
System.debug('contact is>>>' + con);
con.Sequence_Number__c = i;
i++;

} 
}
*/
//cnt.add(con); 
// accontIds.add(con.accountId);
// //list<Contact> cnt = new list<Contact>();
//set<Id> accontIds = new set<Id>();
// List<Contact> conList = [select Id , Name , Sequence_Number__c from contact where AccountId in :accontIds];
/*if(!cnt.isEmpty()){
System.debug('we are in new if');
update cnt;
}
*/
/* if(conList.isEmpty()){
i=1; 
System.debug('We are in if >>>' + i);
}
else {
i = conList.size(); 
System.debug('We are in else>>>' + i);
}
*/
// List<Contact> conList = new List<Contact>();
/* for(Id accId : accontIds){
System.debug('id is>>>' + accId);
String query = 'SELECT Id,Name,Sequence_Number__c FROM Contact WHERE AccountId = \'' + accId + '\'';
System.debug(query);
conList = Database.query(query);
System.debug('list is>>>'+ conList);
System.debug('conlist  size is>>>' + conList.Size());
*/