public class SalesOrderEntryController {

    @AuraEnabled
    public static string getCarrierPLValues(){
        Schema.DescribeFieldResult dfr = TC_Order__c.Carrier__c.getDescribe();
        List<Schema.PicklistEntry> carrierValues = dfr.getPicklistValues();
        return JSON.serialize(carrierValues);
    }
    
    @AuraEnabled
    public static Result handleCreateRecord(String recordJSON, String base64Data){
        try{
            SalesOrderEntry soe = (SalesOrderEntry) JSON.deserialize(recordJSON, SalesOrderEntry.Class);
            TC_Order__c so = new TC_Order__c();
            so.Order_Type__c = soe.Order_Type;
            so.Products_Correct__c = soe.is_Products_Correct;
            so.Dollars_Match_Quote__c = soe.is_Dollars_Match_Quote;
            so.Signed__c = soe.is_Signed;
            so.Ship_to_Address__c = soe.Ship_to_Address;
            so.Bill_to_Address__c = soe.Bill_to_Address;
            so.Carrier__c = soe.Carrier;
            so.Opportunity__c = soe.OpportunityId;
            if(so.Opportunity__c != null && String.valueOf(so.Opportunity__c).startsWith('006')){
                List<Opportunity> oppLst = [Select StageName FROM Opportunity WHERE Id = :so.Opportunity__c];
                if(!oppLst.isEmpty()){
                    oppLst[0].StageName = 'Closed Won';
                }
                update oppLst;
            }
            so.Account__c = soe.AccountId;
            so.Contact__c = soe.ContactId;
            so.Order_Discount__c = soe.Order_Discount;
            so.Quote_Number__c = soe.Quote_Number;
            so.Date_Needed__c = soe.Date_Needed;
            so.Estimated_Ship_Date__c = soe.Estimated_Ship_Date;
            so.ContactsEmails_Needed__c = soe.Contacts_Emails_Needed;
            so.Need_Credit_Forms_Sent__c = soe.Need_Credit_Forms_Sent;
            so.Need_Training__c = soe.Need_Training;
            so.Need_Configuration__c = soe.Need_Configuration;
            so.Special_Notes__c = soe.Special_Notes;
            so.Status__c = 'New';
            so.DateTime__c = System.now();
            insert so;
            Id orderId = so.id;
            //Create attachment
            if(soe.Need_Configuration){
                Attachment attachment = new Attachment();
                attachment.Body = soe.Upload_Drawing_Body;
                attachment.Name = String.valueOf(soe.Upload_Drawing_Name);
                attachment.ParentId = orderId; 
                insert attachment;
            }
            return new Result(true, new Toast('Success!', 'success', 'New Order has been created: ' + [SELECT Name FROM TC_Order__c WHERE Id=:orderId].Name));
        }catch(Exception e){
            return new Result(false, new Toast('Error!', 'error', e.getMessage()));
        }
    }

    public class SalesOrderEntry{
       public String Order_Type;
       public Boolean is_Products_Correct;
       public Boolean is_Dollars_Match_Quote;
       public Boolean is_Signed;
       public String Ship_to_Address;
       public String Bill_to_Address;
       public String Carrier;
       public String OpportunityId;
       public String AccountId;
       public String ContactId;
       public Double Order_Discount;
       public String Quote_Number;
       public Date Date_Needed;
       public Date Estimated_Ship_Date;
       public String Contacts_Emails_Needed;
       public Boolean Need_Credit_Forms_Sent;
       public Boolean Need_Training;
       public Boolean Need_Configuration;
       public String Upload_Drawing_Name;
       public Blob Upload_Drawing_Body;
       public String Special_Notes;
    }

    public class Result{
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public Toast toast;
        @AuraEnabled public Object data;
        
        public Result(Boolean isSuccess){
            this.isSuccess = isSuccess;
        }

        public Result(Boolean isSuccess, Toast toast){
            this.isSuccess = isSuccess;
            this.toast = toast;
        }

        public Result(Boolean isSuccess, Toast toast, Object data){
            this.isSuccess = isSuccess;
            this.toast = toast;
            this.data = data;
        }
    }

    public class Toast{
        @AuraEnabled public String title;
        @AuraEnabled public String type;
        @AuraEnabled public String message;
        
        public Toast(String title, String type, String message){
            this.title = title;
            this.type = type;
            this.message = message;
        }        
    }
}