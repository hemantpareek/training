public class BoxDotComService {

    //1. METHOD TO PROVIDE AUTH URI
    public static String authenticate(String clientId, String redirect_uri){
        return 'https://app.box.com/api/oauth2/authorize?'+
               'response_type=code'+
               '&client_id='+clientId+
               '&redirect_uri='+redirect_uri;
    }

    //2. METHOD TO ACCESS BOX.COM DRIVE FOR GIVEN PARAMETERS 
    public static Object accessDrive(Map<String,String> parameters){
        return accessDrive(parameters,null);
    }

    //3. OVERLOADED METHOD TO ACCESS BOX.COM DRIVE FOR GIVEN PARAMETERS (ALSO ACCEPTS BLOB BODY)
    public static Object accessDrive(Map<String,String> parameters, Blob setBodyAsBlob){
        HttpRequest req = new HttpRequest();//Getting access token from box.com
        if(parameters.get('setMethod') != null){
            req.setMethod(parameters.get('setMethod'));
        }
        if(parameters.get('setEndpoint') != null){
            req.setEndpoint(parameters.get('setEndpoint'));
        }
        if(parameters.get('content-type') != null){
            req.setHeader('content-type',parameters.get('content-type'));
        }
        if(parameters.get('Authorization') != null){
            req.setHeader('Authorization',parameters.get('Authorization'));
        }
        if(parameters.get('content-length') != null){
            req.setHeader('content-length',parameters.get('content-length'));
        }
        if(parameters.get('setBody') != null){
            req.setBody(parameters.get('setBody'));
        }
        if(parameters.get('setTimeout') != null){
            req.setTimeout(Integer.valueOf(parameters.get('setTimeout')));
        }
        if(setBodyAsBlob != null){
            req.setBodyAsBlob(setBodyAsBlob);
        }
        req.setTimeout(60*1000);


        system.debug('method  ==>> '+req.getMethod());
        system.debug('endpoint  ==>> '+req.getEndpoint());
        system.debug('content type ==>> '+req.getHeader('content-type'));
        system.debug('authorization ==>> '+req.getHeader('Authorization'));
        system.debug('contant-length ==>> '+req.getHeader('content-length'));
        system.debug('body ==>> '+req.getBody());

        Http h = new Http();
        HttpResponse res = h.send(req);

        system.debug('res : '+ res);
        system.debug('body : '+ res.getBody());


        Integer statusCode = res.getStatusCode();
        String request_type = parameters.get('request_type');
        if(statusCode == 200){
            if(request_type == 'getAccessToken'){
                return parseAuthResponse(res.getBody());
            }else if(request_type == 'fetchData'){
                return parseFetchDataResponse(res.getBody());
            }   
        }else if(statusCode == 302){
            return res.getHeader('Location'); //'fileDownload'
        } else if (statusCode == 201){
            if(request_type == 'createFolder'){
                return parseCreateFolderResponse(res.getBody()); //folder create 
            }else if(request_type == 'fileUpload'){
                return parseFetchDataResponse(res.getBody()); //file upload 
            }
        } else  if(statusCode == 204){
            return 'DELETE_SUCCESSFUL'; //file delete
        }
        return null;
    }

    //4. PARSES AUTH JSON RESPONSE
    public static Object parseAuthResponse(String json) {
        return System.JSON.deserialize(json, BoxDotComWrapper.AccessDrive.class);
	}

    //5. PARSES FETCH DATA RESPONSE
    public static Object parseFetchDataResponse(String json) {
        return System.JSON.deserialize(json, BoxDotComWrapper.class);
	}

    //6. CPARSES CREATE FOLDER/ FILE RESPONSE
    public static Object parseCreateFolderResponse(String json) {
        system.debug('json : '+json);
        return System.JSON.deserialize(json, BoxDotComWrapper.File.class);
	}
}