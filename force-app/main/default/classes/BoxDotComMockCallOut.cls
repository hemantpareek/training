@isTest
global class BoxDotComMockCallOut implements HttpCalloutMock {
    public static Integer responseCode = 200;
    public static String parentId = '0';
    global HTTPResponse respond(HTTPRequest req) {
        
        //1. FAKE RESPONSE FOR GET ACCESS TOKEN
        if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://app.box.com/api/oauth2/token'){            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"access_token":"NEW_ACCESS_TOKEN","refresh_token":"NEW_REFRESH_TOKEN","expires_in":3600}');
            res.setStatusCode(responseCode);
            return res;

        //2. FAKE RESPONSE FOR FETCH DATA
        } else if (req.getMethod() == 'GET' && req.getEndpoint() == 'https://api.box.com/2.0/folders/'+parentId+'/items'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String body = '{"entries":['+
                          '{"id":"SAMPLE_ID",'+
                          '"name":"SAMPLE_NAME",'+
                          '"type":"SAMPLE_TYPE"'+
                          '}]}';
            res.setBody(body);
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR UPLOAD FILE
        } else if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://upload.box.com/api/2.0/files/content?parent_id='+parentId){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            String body = '{"entries":[{"name":"SAMPLE_FILE_NAME","id":"SAMPLE_FILE_ID","type":"SAMPLE_FILE_TYPE"}]}';
            res.setBody(body);
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR DELETE FILE
        } else if (req.getMethod() == 'DELETE' && req.getEndpoint() == 'https://api.box.com/2.0/files/SAMPLE_FILE_ID'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(responseCode);
            return res;
        
        //FAKE RESPONSE FOR DELETE FOLDER
        } else if (req.getMethod() == 'DELETE' && req.getEndpoint() == 'https://api.box.com/2.0/folders/SAMPLE_FOLDER_ID?recursive=true'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR CREATE FOLDER
        } else if (req.getMethod() == 'POST' && req.getEndpoint() == 'https://api.box.com/2.0/folders'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"id":"SAMPLE_ID"}');
            res.setStatusCode(responseCode);
            return res;

        //FAKE RESPONSE FOR DOWNLOAD FILE
        } else if (req.getMethod() == 'GET' && req.getEndpoint() == 'https://api.box.com/2.0/files/SAMPLE_FILE_ID/content'){ 
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setHeader('Location','SAMPLE_DOWNLOAD_LINK');
            res.setStatusCode(responseCode);
            return res;
        }
        return null;
    }
}