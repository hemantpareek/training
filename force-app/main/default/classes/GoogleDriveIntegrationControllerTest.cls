@isTest
class GoogleDriveIntegrationControllerTest {

    //1. METHOD TO INSERT INTEGRATION DATA CREDENTIALS WITH DEFAULT BOOLEAN PARAMETER FALSE
    public static void insertCredentialData(){
        insertCredentialData(false, false);
    }

    //1. METHOD TO INSERT INTEGRATION DATA CREDENTIALS
    public static void insertCredentialData(boolean isexpired, boolean isRefreshTokenNull){
        Google_Drive_Integration_Data__c gdi = new Google_Drive_Integration_Data__c();
        gdi.access_Token__c = 'SAMPLE_ACCESS_TOKEN';
        gdi.refresh_token__c = isRefreshTokenNull ? null : 'SAMPLE_REFRESH_TOKEN';
        gdi.expires_in__c = isexpired ? system.now().addSeconds(-3600) : system.now().addSeconds(3600);
        gdi.user_email__c = UserInfo.getUserEmail();
        database.insert(gdi);
    }
    
    //1. CONTRUCTOR TEST
    @isTest
    private static void constructorTest(){
        Test.startTest();
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        system.assert(gdic.isAuthenticated == false);
        system.assert(gdic.breadCrumbsList.size() == 0);
        system.assert(gdic.fileToupload != null);
        system.assert(gdic.currentFolder != null);
        system.assert(gdic.client_id != null);
        system.assert(gdic.client_secret != null);
        Test.stopTest();
    }

    //2. PAGE ACTION TEST WHEN NO INTEGRATION CREDENTIALS FOUND AND NO CODE IN URL
    @isTest
    private static void pageActionTest_1(){
        Test.startTest();
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        String authURI = 'https://accounts.google.com/o/oauth2/auth?'+
                         'access_type=offline&'+
                         'client_id=819984311022-fb3mnl8abn3muta3kkecbpvrocaedlf4.apps.googleusercontent.com&'+
                         'login_hint=jsmith%40example.com&'+
                         'redirect_uri=https%3A%2F%2Fc.ap15.visual.force.com%2Fapex%2FGoogleDriveIntegration%3Fsfdc.tabName%3D01r2v000000iZF2&'+
                         'response_type=code&'+
                         'scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive&'+
                         'state=security_token%3D138r5719ru3e1%26url%3Dhttps%3A%2F%2Foa2cb.example.com%2FmyHome';
        system.assertEquals(authURI, gdic.pageAction().getUrl()); //HERE PAGE REFERENCE COMPARASION GIVES ERROR
        Test.stopTest();
    }

    //3. PAGE ACTION TEST WHEN NO INTEGRATION CREDENTIALS FOUND AND CODE PRESENT IN URL
    @isTest
    private static void pageActionTest_2(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        Test.setCurrentPage(new Pagereference('SAMPLE_URL'));
        ApexPages.currentPage().getParameters().put('code','SAMPLE_CODE');
        system.assert(gdic.gdi == null);
        gdic.pageAction();
        system.assert(gdic.gdi != null);
        system.assert(gdic.gdi.access_token__c == 'NEW_ACCESS_TOKEN');
        system.assert(gdic.gdi.refresh_token__c == 'NEW_REFRESH_TOKEN');
        system.assert(gdic.gdi.expires_in__c > System.now());
        Test.stopTest();
    }

    //4. PAGE ACTION TEST WHEN INTEGRATION CREDENTIALS ARE PRESENT AND EXPIRES IN > SYSTEM.NOW()
    @isTest
    private static void pageActionTest_3(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        insertCredentialData();
        Test.startTest();
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        system.assert(gdic.pageAction() == null);
        system.assert(gdic.gdi != null);
        system.assert(gdic.access_token == 'SAMPLE_ACCESS_TOKEN');
        system.assert(gdic.refresh_token == 'SAMPLE_REFRESH_TOKEN');
        system.assert(gdic.expires_in > System.now());
        system.assert(gdic.currentFolderDirectories != null);
        system.assert(gdic.currentFolderFiles != null);
        system.assert(gdic.isAuthenticated == true);
        Test.stopTest();
    }

    //5. PAGE ACTION TEST WHEN INTEGRATION CREDENTIALS ARE PRESENT AND EXPIRES IN < SYSTEM.NOW() & REFRESH TOKRN != NULL
    @isTest
    private static void pageActionTest_4(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        insertCredentialData(true, false);
        Test.startTest();
        Test.setCurrentPage(new Pagereference('SAMPLE_URL'));
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        gdic.pageAction();
        system.assert(gdic.gdi.access_token__c == 'NEW_ACCESS_TOKEN');
        system.assert(gdic.gdi.expires_in__c > system.now());
        Test.stopTest();
    }

    //6. PAGE ACTION TEST WHEN INTEGRATION CREDENTIALS ARE PRESENT AND EXPIRES IN < SYSTEM.NOW() & REFRESH TOKRN == NULL
    @isTest
    private static void pageActionTest_5(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        insertCredentialData(true, true);
        Test.startTest();
        Test.setCurrentPage(new Pagereference('SAMPLE_URL'));
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        gdic.pageAction();
        system.assert(gdic.gdi.access_token__c == 'NEW_ACCESS_TOKEN');
        system.assert(gdic.gdi.expires_in__c > system.now());
        Test.stopTest();
    }

    //7. TEST FOR GETTING ACCESS TOKEN FROM REFRESH TOKEN WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void getAccessTokenFromRefreshTokenTest(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        insertCredentialData(true, false);
        Test.startTest();
        Test.setCurrentPage(new Pagereference('SAMPLE_URL'));
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        GoogleDriveMockCallOut.responseCode = 400;
        gdic.pageAction();
        system.assert(gdic.gdi.expires_in__c < System.now());
        system.assert(gdic.gdi.access_token__c == 'SAMPLE_ACCESS_TOKEN');
        Test.stopTest();
    }

    //8. TEST FOR GETTING ACCESS TOKEN FROM CODE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void getAccessTokenTest(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        Test.setCurrentPage(new Pagereference('SAMPLE_URL'));
        ApexPages.currentPage().getParameters().put('code','SAMPLE_CODE');
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        GoogleDriveMockCallOut.responseCode = 400;
        gdic.pageAction();
        system.assertEquals(null, gdic.gdi);
        Test.stopTest();
    }

    //9. TEST FOR FETCH DATA FROM CODE WHEN NULL RESPONSE RECEIVED
    @isTest
    private static void fetchDataTest(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        insertCredentialData();
        Test.startTest();
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        GoogleDriveMockCallOut.responseCode = 400;
        gdic.pageAction();
        system.assert(gdic.currentFolderDirectories == null);
        system.assert(gdic.currentFolderFiles == null);
        Test.stopTest();
    }

    //10. TEST FOR BREADCRUMB LIST
    @isTest
    private static void addToBreadCrumbsListTest(){
        Test.startTest();
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        gdic.addToBreadCrumbsList(new GoogleDriveWrapper.File('FOLDER_A','A'));
        gdic.addToBreadCrumbsList(new GoogleDriveWrapper.File('FOLDER_B','B'));
        System.assert(gdic.breadCrumbsList.size() == 2);
        gdic.addToBreadCrumbsList(new GoogleDriveWrapper.File('FOLDER_B','B'));
        System.assert(gdic.breadCrumbsList.size() == 2);
        Test.stopTest();
    }

    //11. PERFORM BREADCRUMB ACTION TEST
    @isTest
    private static void performBreadCrumbActionTest(){
        Test.setMock(HttpCalloutMock.class, new GoogleDriveMockCallOut());
        Test.startTest();
        ApexPages.CurrentPage().getParameters().put('folderId','FOLDER_B');
        ApexPages.CurrentPage().getParameters().put('folderName','B');
        GoogleDriveIntegrationController gdic = new GoogleDriveIntegrationController();
        gdic.addToBreadCrumbsList(new GoogleDriveWrapper.File('FOLDER_A','A'));
        gdic.addToBreadCrumbsList(new GoogleDriveWrapper.File('FOLDER_B','B'));
        system.assert(gdic.breadCrumbsList.size() == 2);
        system.assert(gdic.currentFolder.name == 'Home');
        gdic.performBreadCrumbAction();
        system.assert(gdic.breadCrumbsList.size() == 2);
        system.assert(gdic.currentFolder.name == 'B');
        //AGAIN CHANGEING TO FOLDER A
        // ApexPages.CurrentPage().getParameters().put('folderId','FOLDER_A');
        // ApexPages.CurrentPage().getParameters().put('folderName','A');
        // gdic.performBreadCrumbAction();
        // system.assert(gdic.breadCrumbsList.size() == 1);
        // system.assert(gdic.currentFolder.name == 'A');
        Test.stopTest();
    }
}