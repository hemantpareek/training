public class ContactOverride {
    public ApexPages.StandardController stdCon;
    public contact con{get;set;}
    public ContactOverride(ApexPages.StandardController stdCon){
        this.stdCon = stdCon;
        con = new contact(lastName='sample');
        con.accountId = [Select id from account where name='master' limit 1].id;
    
    }
    
    public pagereference prrediect(Id recordId) {
    if(UserInfo.getUiThemeDisplayed() == 'Theme4d')
        return new pagereference('/one/one.app#/sObject/'+recordId+'?nooverride=1'); //you can change this URL

   return null;
}
}