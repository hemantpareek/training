@isTest 
public class SampleTest {
	
    static Account acc;
    static Contact con;
    static List<Opportunity> opps;
    
    static void makeData(String param){
        
        if(param.contains('account')){
        	//Create Account
            acc = new Account(Name='Test Account');
            insert acc;    
        }
        
        if(param.contains('contact')){
            //Create Contact
            con = new Contact(LastName='Test Contact', AccountId = acc.Id);
            insert con;
        }
    }
    
    @isTest
    static void validate_1(){
        Test.startTest();
        makeData('account,contact');
        system.debug(acc);
        system.debug(con);
        Test.stopTest();
    }
}