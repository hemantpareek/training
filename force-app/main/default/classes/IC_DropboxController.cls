//---------------------------------------------------------- DROPBOX LIGHTNING COMPONENT APEX CONTROLLER ----------------------------------------
public class IC_DropboxController {
    //----------------------------------------------------------- CONTROLLER VARIABLES -----------------------------------------------------------
    @testVisible static String access_token;                                          
    @testVisible static String redirect_uri = 'https://hemant-pareek-01-developer-edition.ap15.force.com/IntegrationCommunity/s/dropbox';
    @testVisible static String clientID;    
    @testVisible static String clientSecret;
    @testVisible static DropBox_Integration_Data__c dbi;
    
    //--------------------------------------------------------- -----------METHODS --------------------------------------------------------------

    //1. RETRIEVES ACCESS TOKEN, REFRESH TOKEN, EXPIRES IN FROM CUSTOM SETTING IF AVAILABLE
    @testVisible static Boolean retrieveDataFromCustomSetting(){ //returns true when list is not empty
        List<DropBox_Integration_Data__c> dbiLst = [SELECT access_Token__c FROM DropBox_Integration_Data__c 
                                                    WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(dbiLst.isEmpty()){
            return false;
        }
        dbi = dbiLst[0];
        access_token = dbi.access_Token__c;
        return true;
    }

    //2. RETRIEVES CLIENT ID AND CLIENT SECRET FROM CUSTOM METADATA TYPES IF AVAILABLE
    @testVisible static Boolean retrieveDataFromCustomMetadata(){
        List<Dropbox_Integration_Credentials__c> integration = [SELECT client_id__c , client_secret__c FROM Dropbox_Integration_Credentials__c];
        if(!integration.isEmpty()){
            clientID = integration[0].client_id__c;
            clientSecret = integration[0].client_secret__c;
            return true;
        }
        return false;
    }

    //3. PROVIDES AUTH URI TO GET ACCESS TOKEN VIA CODE
    @testVisible static String doGetAuthURI(){
        if(retrieveDataFromCustomMetadata()){
            String authURI = DropBoxService.authenticate(clientId,redirect_uri);
            return '{"status":"authenticateWithCode","authURI":"'+ authURI +'"}';
        }else{
            return '{"status":"CUSTOM_METADATA_CREDENTIAL_NOT_FOUND"}';
        }
    }

    //4. FETCHES DATA FROM GOOGLE DRIVE BASED ON PARENT FOLDER'S ID 
    public static String fetchData(String path_display){  
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropboxapi.com/2/files/list_folder'); //End point for List of Files in Folder
        parameters.put('content-type','application/json');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        String body = '{'+
                       ' "path": "'+path_display+'",'+   //path is empty bcz we want all folders/files
                       ' "recursive": false,'+
                       ' "include_media_info": false,'+
                       ' "include_deleted": false,'+
                       ' "include_has_explicit_shared_members": false,'+
                       ' "include_mounted_folders": true,'+
                       ' "include_non_downloadable_files": true'+
                       ' }';
        parameters.put('setBody',body);
        parameters.put('request_type','fetchData');
        parameters.put('setTimeout','60000');
        DropBoxWrapper fetchDataResponse = (DropBoxWrapper)DropBoxService.accessDrive(parameters);
        if(fetchDataResponse == null){
            return null;
        }
        String fetchDataResponseJSON  = '[';
        for(DropBoxWrapper.File f : fetchDataResponse.entries){
            fetchDataResponseJSON += '{';
            fetchDataResponseJSON += '"id":"' + f.id + '",';
            fetchDataResponseJSON += '"name":"' + f.name + '",';
            fetchDataResponseJSON += '"type":"' + f.type + '",';
            fetchDataResponseJSON += '"path_display":"' + f.path_display + '",';
            fetchDataResponseJSON += '"isLast":"false"';
            fetchDataResponseJSON += '},';
        }
        return fetchDataResponseJSON.removeEnd(',') + ']';
    }

//------------------------------------------------------- @AURAENABLED METHODS ----------------------------------------------------------------------
    //1. PERFORMS INITIAL PROCESSING WHEN COMPONENT LOADS FIRST TIME
    @AuraEnabled
    public static String doInitApex(){
        if(retrieveDataFromCustomSetting()){
            return '{"status":"readyToLoadData"}'; // perform fetch data first time action
        } else {
            return doGetAuthURI();
        }
    }

    //3. PROVIDES ACCESS TOKEN FIRST TIME WHEN NO DATA IS PRESENT IN DATABASE
    @AuraEnabled
    public static String doGetAccessToken(String code){
        if(!retrieveDataFromCustomMetadata()){
            return null;
        }
        Map<String,String> Parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropbox.com/1/oauth2/token');
        String body = 'code='+code+
                      '&client_id='+clientId+
                      '&client_secret='+clientSecret+
                      '&redirect_uri='+redirect_uri+
                      '&grant_type=authorization_code';//Message body for Authentication
        parameters.put('setBody',body);
        parameters.put('content-length',String.valueOf(body.length()));
        parameters.put('request_type','getAccessToken');
        parameters.put('content-type','application/x-www-form-urlencoded');
        DropBoxWrapper.AccessDrive authResponse = (DropBoxWrapper.AccessDrive)DropBoxService.accessDrive(parameters);
        if(authResponse == null){
            return null;
        }
        List<DropBox_Integration_Data__c> dbiLst = [SELECT access_Token__c FROM DropBox_Integration_Data__c
                                                    WHERE user_email__c = :UserInfo.getUserEmail() AND access_Token__c != null LIMIT 1];
        if(!dbiLst.isEmpty()){
        dbi = dbiLst[0]; 
        }else{
            dbi = new DropBox_Integration_Data__c();
        }
        dbi.user_email__c = UserInfo.getUserEmail();
        dbi.SetupOwnerId = UserInfo.getUserId();
        dbi.access_Token__c = authResponse.access_token;
        upsert dbi;
        return 'SUCCESS';
    }

    //4. FUNCTION TO FETCH ROOT FOLDER DATA WHEN COMPONENT IS LOADED FIRST TIME
    @AuraEnabled    
    public static String doFetchDataFirstTime(){
        retrieveDataFromCustomSetting();
        return fetchData('');
    }

    //5. FUNCTION TO PERFORM CHANGE DIRECTORY ACTION
    @AuraEnabled
    public static string doChangeDirectory(String path_display){
        retrieveDataFromCustomSetting();
        return fetchData(path_display);
    }

    //6. FUNCTION TO DOWNLOAD FILE
    @AuraEnabled
    public static string doDownloadFile(String path_display){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropboxapi.com/2/files/get_temporary_link');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        parameters.put('content-type','application/json');
        parameters.put('setBody','{"path":"'+path_display+'"}');
        parameters.put('request_type','downloadFile');
        DropBoxWrapper.FileDownload fd  = (DropBoxWrapper.FileDownload)DropBoxService.accessDrive(parameters);
        if(fd == null){
            return null;
        }
        return fd.link;
    }

    //7. FUNCTION TO CREATE FOLDER
    @AuraEnabled
    public static object doCreateFolder(String folderPath){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropboxapi.com/2/files/create_folder_v2');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        parameters.put('content-type','application/json');
        parameters.put('setBody','{"path":"'+folderPath+'"}');
        parameters.put('request_type','createFolder');
        DropBoxWrapper.CreateFolder cfr = (DropBoxWrapper.CreateFolder)DropBoxService.accessDrive(parameters);
        if(cfr == null){
            return null;
        }
        return cfr.metadata.path_display;
    }

    //8. FUNCTION TO DELETE SELECTED FILE
    @AuraEnabled
    public static string deleteSelectedFile(String filePath){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://api.dropboxapi.com/2/files/delete_v2');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        parameters.put('content-type','application/json');
        parameters.put('setBody','{"path":"'+filePath+'"}');
        parameters.put('request_type','deleteFile');
        String dfr = (String)DropBoxService.accessDrive(parameters);
        if(dfr == null){
            return null;
        }
        return dfr;
    }

    //9. FUNCTION TO UPLOAD A FILE
    @AuraEnabled
    public static string doUploadFile(String fileName, String base64Data, String contentType, String path_display){
        if(!retrieveDataFromCustomSetting()){
            return null;
        }
        Map<String,String> parameters = new Map<String,String>();
        parameters.put('setMethod','POST');
        parameters.put('setEndpoint','https://content.dropboxapi.com/2/files/upload');
        parameters.put('Authorization','Bearer'+' '+ access_token);
        String parameter = '{'+
                                ' "path": "'+path_display+'/'+fileName+'",'+
                                ' "mode": "add",'+
                                ' "autorename": true,'+
                                ' "mute": false,'+
                                ' "strict_conflict": false'+
                            '}';
        parameters.put('Dropbox-API-Arg',parameter);
        parameters.put('content-type','application/octet-stream');
        parameters.put('request_type','uploadFile');
        DropBoxWrapper.File fur = (DropBoxWrapper.File)DropBoxService.accessDrive(parameters,EncodingUtil.base64Decode(base64Data));    
        system.debug('fur : '+fur);
        if(fur == null){
            return null;
        }
        return '{"name":"' + fur.name + '","path_display":"' + fur.path_display + '"}';
    }
}