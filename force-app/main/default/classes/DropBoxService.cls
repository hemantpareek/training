public class DropBoxService {

    //1. FUNCTION TO PROVIDE AUTH URI
    public static String authenticate(String clientId, String redirect_uri){
        return 'https://www.dropbox.com/oauth2/authorize?'+
               'response_type=code'+
               '&client_id='+clientId+
               '&redirect_uri='+redirect_uri;
    }

    //2. METHOD TO GET ACCESS FOR DROPBOX DRIVE
    public static Object accessDrive(Map<String,String> parameters){
        return accessDrive(parameters, null);
    }

    //3. OVERLOADED METHOD TO GET ACCESS FOR DROPBOX DRIVE (ALSO TAKES BLOB BODY)
    public static Object accessDrive(Map<String,String> parameters, Blob setBodyAsBlob){
        HttpRequest req = new HttpRequest();//Getting access token from box.com
        if(parameters.get('setMethod') != null){
            req.setMethod(parameters.get('setMethod'));
        }
        if(parameters.get('setEndpoint') != null){
            req.setEndpoint(parameters.get('setEndpoint'));
        }
        if(parameters.get('content-type') != null){
            req.setHeader('content-type',parameters.get('content-type'));
        }
        if(parameters.get('Authorization') != null){
            req.setHeader('Authorization',parameters.get('Authorization'));
        }
        if(parameters.get('content-length') != null){
            req.setHeader('content-length',parameters.get('content-length'));
        }
        if(parameters.get('Dropbox-API-Arg') != null){
            req.setHeader('Dropbox-API-Arg',parameters.get('Dropbox-API-Arg'));
        }
        if(parameters.get('setBody') != null){
            req.setBody(parameters.get('setBody'));
        }
        if(setBodyAsBlob != null){
            req.setBodyAsBlob(setBodyAsBlob);
        }
        req.setTimeout(60*1000);

        // system.debug('getMethod ==>> '+req.getMethod());
        // system.debug('getEndPoint ==>> '+req.getEndPoint());
        // system.debug('content-type ==>> '+req.getHeader('content-type'));
        // system.debug('Authorization ==>> '+req.getHeader('Authorization'));
        // system.debug('content-length ==>> '+req.getHeader('content-length'));
        // system.debug('Dropbox-API-Arg ==>> '+req.getHeader('Dropbox-API-Arg'));
        // // system.debug('getBody ==>> '+req.getBody());
        // system.debug('getBodyAsBlob ==>> '+req.getBodyAsBlob());

        Http h = new Http();
        HttpResponse res = h.send(req);
        
        // system.debug(res);
        // system.debug(res.getBody());

        Integer statusCode = res.getStatusCode();
        if(statusCode == 200){
            String request_type = parameters.get('request_type');
            if(request_type == 'getAccessToken'){
                return parseAuthResponse(res.getBody());
            }else if(request_type == 'downloadFile'){
                return parseDownloadFileResponse(res.getBody());
            }else if(request_type == 'fetchData'){
                return parseFetchDataResponse(res.getBody().replaceAll('.tag','type'));
            }else if(request_type == 'deleteFile'){
                return 'DELETE_SUCCESSFUL';
            }else if(request_type == 'createFolder'){
                return parseCreateFolderResponse(res.getBody());
            }else if(request_type == 'uploadFile'){
                return parseFileResponse(res.getBody());
            }
        }
        return null;
    }

    //4. PARSES AUTH JSON RESPONSE
    public static Object parseAuthResponse(String json) {
        return System.JSON.deserialize(json, DropBoxWrapper.AccessDrive.class);
	}

    //5. PARSES FETCH DATA RESPONSE
    public static Object parseFetchDataResponse(String json) {
        return System.JSON.deserialize(json, DropBoxWrapper.class);
	}

    //6. PARSES DOWNLOAD FILE RESPONSE
    public static Object parseDownloadFileResponse(String json) {
        return System.JSON.deserialize(json, DropBoxWrapper.FileDownload.class);
	}

    //7. PARSES CREATE FOLDER RESPONSE
    public static Object parseCreateFolderResponse(String json) {
        return System.JSON.deserialize(json, DropBoxWrapper.CreateFolder.class);
	}
    
    //8. PARSES FILE RESPONSE
    public static Object parseFileResponse(String json) {
        return System.JSON.deserialize(json, DropBoxWrapper.File.class);
	}
}