public with sharing class lwcPaginationController {
    
    //--------------------------------------------------------------------- METHODS ---------------------------------------------------------------------------------
    //FETCHES SOBJECT LIST FROM OBJECT SCHEMA
    @AuraEnabled
    public static Object getObjects(List<String> filters){
        Map<String, Schema.sObjectType> schemaMap = Schema.getGlobalDescribe();
        List<SchemaWrapper> objList = new List<SchemaWrapper>(); //Map<value,label>
        for(String value : schemaMap.keySet()){
            DescribeSObjectResult dsr = schemaMap.get(value).getDescribe();
            if(filterObjects(dsr, filters)){
                SchemaWrapper sobjWrap = new SchemaWrapper();
                sobjWrap.label = dsr.getLabel();
                sobjWrap.value = dsr.getName();
                sobjWrap.keyPrefix = dsr.getKeyPrefix();
                objList.add(sobjWrap);
            }
        }
        objList.sort();
        return objList;
     }
 
     public static Boolean filterObjects(DescribeSObjectResult dsr, List<String> filters){
        if(filters.isEmpty()){
            return true;
        }
        Boolean result = true;
        result &= filters.contains('isAccessible') ? dsr.isAccessible() ? true : false : true ;
        result &= filters.contains('isCreateable') ? dsr.isCreateable() ? true : false : true ;
        result &= filters.contains('isCustom') ? dsr.isCustom() ? true : false : true ;
        result &= filters.contains('isCustomSetting') ? dsr.isCustomSetting() ? true : false : true ;
        result &= filters.contains('isDeletable') ? dsr.isDeletable() ? true : false : true ;
        result &= filters.contains('isDeprecatedAndHidden') ? dsr.isDeprecatedAndHidden() ? true : false : true ;
        result &= filters.contains('isFeedEnabled') ? dsr.isFeedEnabled() ? true : false : true ;
        result &= filters.contains('isMergeable') ? dsr.isMergeable() ? true : false : true ;
        result &= filters.contains('isMruEnabled') ? dsr.isMruEnabled() ? true : false : true ;
        result &= filters.contains('isQueryable') ? dsr.isQueryable() ? true : false : true ;
        result &= filters.contains('isSearchable') ? dsr.isSearchable() ? true : false : true ;
        result &= filters.contains('isUndeletable') ? dsr.isUndeletable() ? true : false : true ;
        result &= filters.contains('isUpdateable') ? dsr.isUpdateable() ? true : false : true ;
        return result;
    }

    //FETCHES SOBJECT FIELDS LIST FROM SOBJECT SCHEMA
    @AuraEnabled
    public static Object getFields(String objectName, List<String> filters){
        Map<String, Schema.sObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();        
        List<SchemaWrapper> fieldList = new List<SchemaWrapper>(); //Map<value,label>
        Map<String, String> nameFieldByObjectMap = getNameFieldByObjectNameMap(objectName, fieldMap);
        for(String value : fieldMap.keySet()){
            DescribeFieldResult dfr = fieldMap.get(value).getDescribe();
            if(filterFields(dfr, filters)){
                SchemaWrapper fldWrpr = new SchemaWrapper();
                fldWrpr.label = dfr.getLabel();
                fldWrpr.value = dfr.getName();
                fldWrpr.type = getLtngInputFieldType(dfr);
                fldWrpr.isNameField = dfr.isNameField() ? true : null;
                fldWrpr.isRequired = dfr.isCreateable() && !dfr.isNillable() && !dfr.isDefaultedOnCreate() ? true : null;
                fldWrpr.isStdInput = dfr.isUpdateable() && (dfr.getType() == DisplayType.PICKLIST || dfr.getType() == DisplayType.REFERENCE) ? true : null;
                fldWrpr.isReference = dfr.getType() == DisplayType.REFERENCE ? true : null;
                fldWrpr.isUrl = dfr.getType() == DisplayType.URL ? true : null;
                fldWrpr.isBoolean = dfr.getType() == DisplayType.BOOLEAN ? true : null;
                fldWrpr.relationshipName = dfr.getType() == DisplayType.REFERENCE && dfr.getRelationshipName() != null? dfr.getRelationshipName().endsWith('__c') ? dfr.getRelationshipName().removeEnd('__c') + '__r' : dfr.getRelationshipName() : null;
                fldWrpr.parentNameField = dfr.getType() == DisplayType.REFERENCE ? nameFieldByObjectMap.get(String.valueOf(dfr.getReferenceTo()[0])) : null;
                fieldList.add(fldWrpr);
            }
        }
        fieldList.sort();
        return fieldList;
    }

    public static Map<String,String> getNameFieldByObjectNameMap(String objName, Map<String, Schema.sObjectField> fieldMap){
        Map<String, String> result = new Map<String, String>();
        Set<String> relatedObjectNames = new Set<String>();
        for(String value : fieldMap.keySet()){
            DescribeFieldResult dfr = fieldMap.get(value).getDescribe();
            if(dfr.getType() == DisplayType.REFERENCE){
                for(SObjectType sobjt : dfr.getReferenceTo()){
                    relatedObjectNames.add(String.valueOf(sobjt));
                }
            }
        }
        for(FieldDefinition fd : [SELECT QualifiedApiName,EntityDefinition.QualifiedApiName FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName IN :relatedObjectNames AND IsNameField = TRUE]){
            result.put(fd.EntityDefinition.QualifiedApiName, fd.QualifiedApiName);
        }
        System.debug(result);
        return result;
    }

    public static String getLtngInputFieldType(DescribeFieldResult dfr){
        switch on dfr.getType() {
            when DATE { return 'date'; }
            when DATETIME { return 'datetime'; }
            when TIME { return 'time'; }
            when EMAIL { return 'email'; }
            when PHONE { return 'tel'; }
            when URL { return 'url'; }
            when DOUBLE, INTEGER, LONG, PERCENT, CURRENCY { return 'number'; }
            when BOOLEAN { return 'checkbox'; }
        }
        return 'text';
    }

    public static Boolean filterFields(DescribeFieldResult dfr, List<String> filters){
        if(filters.isEmpty()){
            return true;
        }
        Boolean result = true;
        result &= filters.contains('isAccessible') ? dfr.isAccessible() ? true : false : true ;
        result &= filters.contains('isAiPredictionField') ? dfr.isAiPredictionField() ? true : false : true ;
        result &= filters.contains('isAutoNumber') ? dfr.isAutoNumber() ? true : false : true ;
        result &= filters.contains('isCalculated') ? dfr.isCalculated() ? true : false : true ;
        result &= filters.contains('isCascadeDelete') ? dfr.isCascadeDelete() ? true : false : true ;
        result &= filters.contains('isCaseSensitive') ? dfr.isCaseSensitive() ? true : false : true ;
        result &= filters.contains('isCreateable') ? dfr.isCreateable() ? true : false : true ;
        result &= filters.contains('isCustom') ? dfr.isCustom() ? true : false : true ;
        result &= filters.contains('isDefaultedOnCreate') ? dfr.isDefaultedOnCreate() ? true : false : true ;
        result &= filters.contains('isDependentPicklist') ? dfr.isDependentPicklist() ? true : false : true ;
        result &= filters.contains('isDeprecatedAndHidden') ? dfr.isDeprecatedAndHidden() ? true : false : true ;
        result &= filters.contains('isExternalID') ? dfr.isExternalID() ? true : false : true ;
        result &= filters.contains('isFilterable') ? dfr.isFilterable() ? true : false : true ;
        result &= filters.contains('isFormulaTreatNullNumberAsZero') ? dfr.isFormulaTreatNullNumberAsZero() ? true : false : true ;
        result &= filters.contains('isGroupable') ? dfr.isGroupable() ? true : false : true ;
        result &= filters.contains('isHtmlFormatted') ? dfr.isHtmlFormatted() ? true : false : true ;
        result &= filters.contains('isIdLookup') ? dfr.isIdLookup() ? true : false : true ;
        result &= filters.contains('isNameField') ? dfr.isNameField() ? true : false : true ;
        result &= filters.contains('isNamePointing') ? dfr.isNamePointing() ? true : false : true ;
        result &= filters.contains('isNillable') ? dfr.isNillable() ? true : false : true ;
        result &= filters.contains('isPermissionable') ? dfr.isPermissionable() ? true : false : true ;
        result &= filters.contains('isRestrictedDelete') ? dfr.isRestrictedDelete() ? true : false : true ;
        result &= filters.contains('isRestrictedPicklist') ? dfr.isRestrictedPicklist() ? true : false : true ;
        result &= filters.contains('isSearchPrefilterable') ? dfr.isSearchPrefilterable() ? true : false : true ;
        result &= filters.contains('isSortable') ? dfr.isSortable() ? true : false : true ;
        result &= filters.contains('isUnique') ? dfr.isUnique() ? true : false : true ;
        result &= filters.contains('isUpdateable') ? dfr.isUpdateable() ? true : false : true ;
        result &= filters.contains('isWriteRequiresMasterRead') ? dfr.isWriteRequiresMasterRead() ? true : false : true ;
        return result;
    }

    //FETCHES REOCRDS 
    @AuraEnabled
    public static Object getRecords(String sobjectName, List<String> fields, String whereClause, String orderByClause, Integer lmt, Integer offset){
        String countQuery = 'SELECT count() FROM ' + sobjectName + ' ' + ( whereClause != '' ? 'WHERE ' + whereClause + ' ' : '');
        if(offset > 2000){
            System.debug('@@@ previous offset:' +  offset);
            Integer count = offset/2000;
            offset = Math.Mod(offset, count*2000);
            System.debug('@@@ count ' + count);
            System.debug('@@@ new offset:' +  offset);
            String offsetId = '';
            for(Integer i=1; i<=count; i++){
                //get last 2000th record id of current set
                String offsetQuery = 'SELECT Id FROM ' + sobjectName + (offsetId == '' ? '' : ' WHERE Id > \'' + offsetId + '\'') + ' LIMIT 1 OFFSET 1999';
                System.debug(offsetQuery);
                Sobject sobj = Database.query(offsetQuery);
                offsetId = String.valueOf(sobj.get('Id'));
            }
            whereClause = whereClause == '' ? 'Id > \'' + offsetId + '\'' : 'AND Id > \'' + offsetId + '\'';
        }
        String query = 'SELECT ';
        query += fields.contains('Id') ? String.join(fields, ',') + ' ' : 'Id,' + String.join(fields, ',') + ' ';
        query += 'FROM ' + sobjectName + ' ';
        query += whereClause != '' ? 'WHERE ' + whereClause + ' ' : '';
        query += orderByClause != '' ? 'ORDER BY ' + orderByClause + ' ' : '';
        query += 'LIMIT ' + lmt + ' ';
        query += 'OFFSET ' + offset;
        System.debug(query);
        System.debug(countquery);
        RecordsWrapper rw = new RecordsWrapper();
        rw.query = query;
        rw.data = Database.query(query);
        rw.totalSize = Database.countQuery(countQuery);
        return rw;
    }

    @AuraEnabled
    public static Object deleteRecords(List<String> recordIds){
        System.debug(recordIds);
        Integer successCount = 0;
        Integer failureCount = 0;
        List<String> succeedIds = new List<String>();
        for(Database.DeleteResult dr : Database.delete(recordIds, false)){
            if (dr.isSuccess()) {
                successCount++;
                succeedIds.add(dr.getId());
            }
            else {
                failureCount++;
            }
        }
        ResultWrapper drw = new ResultWrapper();
        drw.state = recordIds.size() == successCount ? 'success' : recordIds.size() == failureCount ? 'error' : 'warning'; 
        drw.message = 'Succeed : ' + successCount + ', Failed: ' + failureCount;
        drw.succeedIds = succeedIds;
        return drw;
    }

    @AuraEnabled
    public static Object createRecords(String objectName, List<String> recordsJsonArray){
        List<Sobject> rcdsToIns = new List<Sobject>();
        for(Integer i=0; i<recordsJsonArray.size(); i++){
            rcdsToIns.add( (Sobject)JSON.deserialize(recordsJsonArray[i], System.Type.forName(objectName)) );
        }
        Integer successCount = 0;
        Integer failureCount = 0;
        for(Database.SaveResult sr : Database.insert(rcdsToIns, false)){
            if (sr.isSuccess()) {
                successCount++;
            }
            else {
                failureCount++;
            }
        }
        ResultWrapper srw = new ResultWrapper();
        srw.state = rcdsToIns.size() == successCount ? 'success' : rcdsToIns.size() == failureCount ? 'error' : 'warning'; 
        srw.message = 'Succeed : ' + successCount + ', Failed: ' + failureCount;
        return srw;
    }

    // @AuraEnabled
    // public static Object getLookupFields(String objName, List<String> referenceFieldNames){
    //     Map<String, Schema.sObjectField> fieldMap = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
    //     Map<String, Set<String>> relationshipFieldByObjMap = new Map<String, Set<String>>();
    //     Map<String, String> nameFieldByObjMap = new Map<String, String>();
    //     Map<String, String> relationshipFieldByFieldMap = new Map<String, String>();
    //     for(String fld : referenceFieldNames){
    //         DescribeFieldResult dfr = fieldMap.get(fld).getDescribe();
    //         if(!relationshipFieldByObjMap.containsKey(dfr.getRelationshipName())){
    //             relationshipFieldByObjMap.put(dfr.getRelationshipName(), new Set<String>());    
    //         }
    //         relationshipFieldByObjMap.get(dfr.getRelationshipName()).add(dfr.getName());
    //     }
    //     List<FieldDefinition> fieldDefinationList = [SELECT QualifiedApiName,EntityDefinition.QualifiedApiName FROM FieldDefinition WHERE EntityDefinition.QualifiedApiName IN :relationshipFieldByObjMap.keySet() AND IsNameField = TRUE];
    //     for(String objApiName : relationshipFieldByObjMap.keySet()){
    //         //fill nameFieldByObjMap
    //         for(FieldDefinition fldef : fieldDefinationList){
    //             if(fldef.EntityDefinition.QualifiedApiName == objApiName){
    //                 String rectifiedObjName = objApiName.endsWith('__c') ? objApiName.removeEnd('__c') + '__r' : objApiName;
    //                 nameFieldByObjMap.put( objApiName, rectifiedObjName + '.' + fldef.QualifiedApiName);
    //             }
    //         }
    //         //fill relationshipFieldByFieldMap
    //         for(String field : relationshipFieldByObjMap.get(objApiName)){
    //             relationshipFieldByFieldMap.put(field, nameFieldByObjMap.get(objApiName));
    //         }
    //     }
    //     return relationshipFieldByFieldMap;
    // }

    //------------------------------------------------------WRAPPER TO HOLD OBJECT API NAME WITH ITS LABEL---------------------------------------------------------
    public class SchemaWrapper implements Comparable { //WORKS FOR BOTH SOBJECTS AND SOBJECT FIELDS
        @AuraEnabled public String value;
        @AuraEnabled public String label;
        @AuraEnabled public String keyPrefix; //USEFUL FOR ONLY SOBJECTS
        @AuraEnabled public Boolean isNameField; //USEFUL FOR ONLY SOBJECT FIELDS
        @AuraEnabled public Boolean isRequired; //USEFUL FOR ONLY SOBJECT FIELDS
        @AuraEnabled public Boolean isStdInput;
        @AuraEnabled public String type; //USEFUL FOR ONLY SOBJECT FIELDS
        @AuraEnabled public Boolean isReference;
        @AuraEnabled public Boolean isUrl;
        @AuraEnabled public Boolean isBoolean;
        @AuraEnabled public String relationshipName;
        @AuraEnabled public String parentNameField;

        public Integer compareTo(Object compareTo) {
            SchemaWrapper sow = (SchemaWrapper)compareTo;
            if(label > sow.label){
                return 1;
            }else if(label < sow.label){
                return -1;
            }else{
                return 0;
            }
        }
    }


    public class RecordsWrapper{
        @AuraEnabled public Integer totalSize;
        @AuraEnabled public String query;
        @AuraEnabled public List<Sobject> data;
    }

    public class ResultWrapper{
        @AuraEnabled public String state;
        @AuraEnabled public String message;
        @AuraEnabled public List<String> succeedIds;
    }
}