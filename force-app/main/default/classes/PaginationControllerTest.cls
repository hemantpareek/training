@isTest
class PaginationControllerTest {
    
    @isTest
    static void objectChangeAction_test(){
        Test.startTest();
        PaginationController p = new PaginationController();
        system.assert(p.renderComponent == null);
        system.assert(p.selectedObject=='-none-');
        system.assert(p.fieldList.size() == 0);
        p.selectedObject = 'Account';
        p.initFldLst();
        p.fieldsToAdd = new List<String>{'id','name'};
        p.addToSelectedFldLst();
        p.processRecords();
        system.assert(!p.fieldList.contains(new SelectOption('id','Account Id')));
        system.assert(!p.fieldList.contains(new SelectOption('name','Account Name')));
        system.assert(p.objectToDisplay == 'Account');
        system.assert(p.fieldsToDisplay.contains('id'));
        system.assert(p.fieldsToDisplay.contains('name'));
        system.assert(p.renderComponent == true);
        p.selectedObject = 'Contact';
        p.objectChangeAction();
        system.assert(p.selectedObject == 'Contact');
        system.debug(p.fieldList);
        system.assert(p.fieldList.contains(new SelectOption('lastname','Last Name')));
        system.assert(p.fieldList.contains(new SelectOption('accountid','Account ID')));
        system.assert(!p.fieldList.contains(new SelectOption('industry','Industry')));
        system.assert(p.renderComponent == false);
        Test.stopTest();
    }
    
    @isTest
    static void processRecords_test(){
        Test.startTest();
        PaginationController p = new PaginationController();
        system.assert(p.selectedObject == '-none-');
        system.assert(p.objectToDisplay == null);
        system.assert(p.fieldsToDisplay.size() == 0);
        p.selectedObject = 'Account';
        p.initFldLst();
        p.fieldsToAdd = new List<String>();
        for(SelectOption field : p.fieldList){
            p.fieldsToAdd.add(field.getValue());
        }
        p.addToSelectedFldLst();
        System.assert(p.renderComponent==null);
        p.processRecords();
        system.assert(p.objectToDisplay == 'Account');
        system.assert(p.fieldsToDisplay.contains('id'));
        system.assert(p.renderComponent == true);
        p.fieldsToRemove= new List<String>{'id'};
        p.removeFromSelectedFldLst();
        p.processRecords();
        system.assert(p.objectToDisplay == 'Account');
        system.assert(!p.fieldsToDisplay.contains('id'));
        system.assert(p.renderComponent == true);
        Test.stopTest();
    }
    
    @isTest
    static void removeFromSelectedFldLst_test(){
        Test.startTest();
        PaginationController p = new PaginationController();
        p.selectedObject = 'Account';
        p.initFldLst();
        p.fieldsToAdd = new List<String>();
        for(SelectOption field : p.fieldList){
            p.fieldsToAdd.add(field.getValue());
        }
        p.addToSelectedFldLst();
        system.assert(p.fieldsToRemove == null);
        p.fieldsToRemove = new List<String>{'id'};
        system.assert(p.SelectedfieldList.contains(new SelectOption('id','Account ID')));
        system.assert(!p.fieldList.contains(new SelectOption('id','Account ID')));
        p.removeFromSelectedFldLst();
        system.assert(!p.SelectedfieldList.contains(new SelectOption('id','Account ID')));
        system.assert(p.fieldList.contains(new SelectOption('id','Account ID')));
        system.assert(p.fieldsToRemove.size() == 0);
        Test.stopTest();
    }
    
    @isTest
    static void addToSelectedFldLst_test(){
        Test.startTest();
        PaginationController p = new PaginationController();
        p.selectedObject = 'Account';
        p.initFldLst();
        system.assert(p.fieldList.size()!=0);
        system.assert(p.SelectedfieldList.size()==0);
        system.assert(p.fieldsToAdd == null);
        p.fieldsToAdd = new List<String>();
        for(SelectOption field : p.fieldList){
            p.fieldsToAdd.add(field.getValue());
        }
        p.addToSelectedFldLst();
        system.assert(!p.fieldList.contains(new SelectOption('id','Account ID')));
        system.assert(!p.fieldList.contains(new SelectOption('name','Account Name')));
        system.assert(p.SelectedfieldList.size()==10);
        system.assert(p.fieldsToAdd.size() == 0);
        system.assert(p.selectedFieldList.contains(new SelectOption('id','Account ID')));
        system.assert(p.selectedFieldList.contains(new SelectOption('name','Account Name')));
        Test.stopTest();
    }
    
    @isTest
    static void initFldLst_test(){
        Test.startTest();
        PaginationController p = new PaginationController();
        system.assertEquals(0,p.fieldList.size());
        p.selectedObject = 'Account';
        system.assert(p.SelectedfieldList == null);
        p.initFldLst();
        system.assert(p.fieldList.size() != 0);
        system.assert(p.fieldList.contains(new SelectOption('id','Account ID')));
        system.assert(p.SelectedfieldList.size() == 0);
        Test.stopTest();
    }
    
    @isTest
    static void removeUnSupportedObjects_test(){
        Test.startTest();
        PaginationController p = new PaginationController();
        List<String> objectApiNames = p.removeUnSupportedObjects(new List<String>(Schema.getGlobalDescribe().keySet()));
        system.assert(!p.objectList.contains(new SelectOption('task','Task')));
        system.assert(p.objectList.contains(new SelectOption('account','Account')));
        // system.assert(p.objectList.contains(new SelectOption('a__c','A')));
        Test.stopTest();
    }
    
    @isTest
    static void initObjLst_test(){
        Test.startTest();
        PaginationController p = new PaginationController();
        system.assert(p.objectList.size()!=0);
        system.assert(p.objectList[0].getValue() == '-none-');
        system.assert(p.fieldList.size() == 0);
        system.assert(!p.objectList.contains(new SelectOption('task','Task')));
        system.assert(p.objectList.contains(new SelectOption('account','Account')));
        Test.stopTest();
    }
    
    @isTest
    static void basic_functionlity_test(){
        Test.startTest();
        PaginationController p = new PaginationController();
        system.assert(p.objectList!=null);
        system.assert(p.objectList.size()!=0);
        system.assertEquals('-none-',p.selectedObject);
        system.assert(p.objectList.contains(new SelectOption('account','Account')));
        system.assertEquals(0, p.fieldList.size());
        p.selectedObject='Account';
        p.objectChangeAction();
        system.assertEquals(false, p.renderComponent);
        system.assert(p.fieldList.size()!=0);
        system.assert(p.fieldsToAdd==null);
        p.fieldsToAdd = new List<String>();
        for(SelectOption field : p.fieldList){
            p.fieldsToAdd.add(field.getValue());
        }
        system.assert(p.fieldList.size() == p.fieldsToAdd.size());
        system.assert(p.SelectedfieldList.size() == 0);
        p.addToSelectedFldLst();
        system.assert(p.SelectedfieldList.size() == 10);
        system.assert(p.fieldsToAdd.size() == 0);
        system.assert(p.fieldsToRemove == null);
        system.assert(p.objectToDisplay == null);
        system.assert(p.fieldsToDisplay.size() == 0);
        p.processRecords();
        system.assert(p.renderComponent == true);
        system.assert(p.selectedObject == 'Account');
        system.assert(p.SelectedfieldList.size() == 10);
        p.fieldsToRemove = new List<String>();
        for(SelectOption field : p.SelectedFieldList){
            p.fieldsToRemove.add(field.getValue());
        }
        system.assertEquals(10, p.fieldsToRemove.size());
        p.removeFromSelectedFldLst();       
        system.assertEquals(0, p.fieldsToRemove.size());
        system.assertEquals(0, p.SelectedfieldList.size());
        p.processRecords();
        Test.stopTest();
    }
}