@isTest
private class IC_AuthorizeDotNetControllerTest {
    
    //1. SETS UP THE CREDENTIALS
    private static void setupCredentials(String loginId, String txnKey){
        Authorize_Net_Credentials__c adnc = new Authorize_Net_Credentials__c();
        adnc.API_Login_ID__c = loginId;
        adnc.Transaction_Key__c = txnKey;
        insert adnc;
    }

    //2. WHEN NO CREDENTIALS ARE PRESENT
    @isTest
    private static void fetchIntegrationCredentialsTest_1(){
        Test.startTest();
        system.assertEquals(false, IC_AuthorizeDotNetController.fetchIntegrationCredentials());
        Test.stopTest();
    }  

    //3. WHEN RECORD IS PRESENT BUT DATA IS NULL
    @isTest
    private static void fetchIntegrationCredentialsTest_2(){
        Test.startTest();
        setupCredentials(null,null);
        system.assertEquals(false, IC_AuthorizeDotNetController.fetchIntegrationCredentials());
        Test.stopTest();
    }

    //4. WHEN RECORD IS PRESENT BUT DATA IS VALID
    @isTest
    private static void fetchIntegrationCredentialsTest_3(){
        Test.startTest();
        setupCredentials('SAMPLE_LOGIN_ID','SAMPLE_TRANSACTION_KEY');
        system.assertEquals(true, IC_AuthorizeDotNetController.fetchIntegrationCredentials());
        Test.stopTest();
    }

    //5. METHOD TO SET UP PRODUCT, PRICE BOOK AND PRICE BOOK ENTRY
    private static List<String> setupProducts(){
        Product2 p = new Product2();
        p.name = 'SAMPLE_NAME';
        p.productCode = 'SAMPLE_PRODUCT_CODE';
        p.description = 'SAMPLE_DESCRIPTION';
        p.family = 'Solar_Panels';
        insert p;
        Pricebook2 pb2 = new Pricebook2();
        pb2.id = Test.getStandardPricebookId(); //IMPORTANT
        pb2.isActive = true;
        update pb2;
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = pb2.id;
        pbe.Product2Id = p.id;
        pbe.unitPrice = 100.00;
        pbe.isActive = true;
        insert pbe;
        return new List<String>{p.id};
    }

    //6. TEST FOR INIT HANDLER
    @isTest
    private static void doInitApexTest(){
        Test.startTest();
        List<String> ids = setupProducts();
        String expectedValue = '[{"id":"' + ids[0] + '","name":"SAMPLE_NAME","price":"100.00","description":"SAMPLE_DESCRIPTION"}]';
        system.assertEquals(expectedValue, IC_AuthorizeDotNetController.doInitApex());
        Test.stopTest();
    }

    //7. PAYMENT FROM CREDIT CARD TEST WHEN CREDENTIALS NOT PRESENT 
    @isTest
    private static void processPaymentFromCreditCardTest_1(){
        Test.startTest();
        system.assertEquals(null, IC_AuthorizeDotNetController.processPaymentFromCreditCard(100, 1111222233334444L, 2020, 12, 999, '[]'));
        Test.stopTest();
    }

    //8. WHEN CREDENTIALS ARE PRESENT
    @isTest
    private static void processPaymentFromCreditCardTest_2(){
        Test.setMock(HttpCalloutMock.class, new AuthorizeDotNetMockCallout());
        setupCredentials('SAMPLE_LOGIN_ID','SAMPLE_TRANSACTION_KEY');
        setupProducts();
        Test.startTest();
        String cartItemJson = '[{"id":"CART_ITEM_ID","name":"CART_ITEM_NAME","description":"CART_ITEM_DESCRIPTION","quantity":"1","unitPrice":"100"}]';
        String expectedValue = '{"text":"CREDIT_CARD_PAYMENT","code":"SAMPLE_CODE"}';
        system.assertEquals(expectedValue, IC_AuthorizeDotNetController.processPaymentFromCreditCard(100, 1111222233334444L, 2020, 12, 999, cartItemJson));
        Test.stopTest();
    }

    //9. WHEN CREDENTIALS ARE PRESENT AND NULL RESPONSE RETURNED
    @isTest
    private static void processPaymentFromCreditCardTest_3(){
        Test.setMock(HttpCalloutMock.class, new AuthorizeDotNetMockCallout());
        setupCredentials('SAMPLE_LOGIN_ID','SAMPLE_TRANSACTION_KEY');
        setupProducts();
        AuthorizeDotNetMockCallout.responseCode = 400;
        Test.startTest();
        String cartItemJson = '[{"id":"CART_ITEM_ID","name":"CART_ITEM_NAME","description":"CART_ITEM_DESCRIPTION","quantity":"1","unitPrice":"100"}]';
        String expectedValue = '{"text":"CREDIT_CARD_PAYMENT","code":"SAMPLE_CODE"}';
        system.assertEquals(null, IC_AuthorizeDotNetController.processPaymentFromCreditCard(100, 1111222233334444L, 2020, 12, 999, cartItemJson));
        Test.stopTest();
    }

    //10. PAYMENT FROM ECHECK TEST WHEN CREDENTIALS NOT PRESENT 
    @isTest
    private static void processPaymentFromEcheckTest_1(){
        Test.startTest();
        system.assertEquals(null, IC_AuthorizeDotNetController.processPaymentFromEcheck(100, 123456789, 123456789, 'SAMPLE_NAME', '[]'));
        Test.stopTest();
    }

    //11. PAYMENT FROM ECHECK TEST WHEN CREDENTIALS ARE PRESENT 
    @isTest
    private static void processPaymentFromEcheckTest_2(){
        Test.setMock(HttpCalloutMock.class, new AuthorizeDotNetMockCallout());
        setupCredentials('SAMPLE_LOGIN_ID','SAMPLE_TRANSACTION_KEY');
        setupProducts();
        Test.startTest();
        String cartItemJson = '[{"id":"CART_ITEM_ID","name":"CART_ITEM_NAME","description":"CART_ITEM_DESCRIPTION","quantity":"1","unitPrice":"100"}]';
        String expectedValue = '{"text":"ECHECK_PAYMENT","code":"SAMPLE_CODE"}';
        system.assertEquals(expectedValue, IC_AuthorizeDotNetController.processPaymentFromEcheck(100, 123456789, 123456789, 'SAMPLE_NAME', cartItemJson));
        Test.stopTest();
    }

    //12. PAYMENT FROM ECHECK TEST WHEN CREDENTIALS ARE PRESENT AND NULL RESPONSE RETURNED
    @isTest
    private static void processPaymentFromEcheckTest_3(){
        Test.setMock(HttpCalloutMock.class, new AuthorizeDotNetMockCallout());
        setupCredentials('SAMPLE_LOGIN_ID','SAMPLE_TRANSACTION_KEY');
        setupProducts();
        AuthorizeDotNetMockCallout.responseCode = 400;
        Test.startTest();
        String cartItemJson = '[{"id":"CART_ITEM_ID","name":"CART_ITEM_NAME","description":"CART_ITEM_DESCRIPTION","quantity":"1","unitPrice":"100"}]';
        String expectedValue = '{"text":"ECHECK_PAYMENT","code":"SAMPLE_CODE"}';
        system.assertEquals(null, IC_AuthorizeDotNetController.processPaymentFromEcheck(100, 123456789, 123456789, 'SAMPLE_NAME', cartItemJson));
        Test.stopTest();
    }
}