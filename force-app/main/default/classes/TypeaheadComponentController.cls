//****************************************** T Y P E A H E A D   C O M P O N E N T  C O N T R O L L E R  ****************************************************************
public class TypeaheadComponentController {
 //*************************************************************  F I E L D S  ******************************************************************************************
    public static List<String> sobjectNameList{get;set;}
    public String recordId{get;set;}
    
 //******************************************************************* M E T H O D S ************************************************************************************    
 //1. METHOD TO GENERATE SUGGESTIONS VIA JQUERY
   @RemoteAction
    public static List<Result> populateSobjectByName(String value, String passedSobject, String passedfields)
    {
        String nameField = 'Name';
        if(passedSobject.toLowerCase() == 'case'){
            nameField = 'CaseNumber';
        }

        try       
        { 
            sobjectNameList=new List<String>();   
            List<Result> resultList = new List<Result>();
            if(String.isNotBlank(value))
            {
                value= '%'+value+'%';   
                List<sobject> lstSobject= database.query('Select '+ passedfields +' from ' + passedSobject + ' WHERE ' + nameField + ' LIKE:value');
                for(Sobject s:lstSobject)
                {
                    Result res = new Result();
                    res.label = s.get(nameField).toString();
                    res.value = s.get(nameField).toString();
                    res.recordId = s.get('Id').toString();
                    // res.sobj = s;
                    resultList.add(res);
                }
            }
            return resultList;
        }
        catch(Exception e)   
        {
            System.debug('Message:='+e.getMessage()+'**Line Number='+e.getLineNumber());   
        }
        return null;
    }

    public class Result{
        public String label {get; set;}
        public String value {get; set;}
        public String recordId {get; set;}
        // public Sobject sobj {get; set;}
    }
}
//**********************************************************  E N D *********************************************************************************************************