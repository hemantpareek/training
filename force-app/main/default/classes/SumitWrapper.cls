public class SumitWrapper {
	
    public static Configuration parse(){
        String body = '{ \\"name\\": \\"Schedule 1\\", \\"objectName\\": \\"Contact\\", \\"batchQuery\\": \\"SELECT Contact.Id,Contact.Email, Contact.FirstName, Contact.LastName, Contact.Phone, Contact.Birthdate, Contact.OtherPostalCode FROM Contact\\", \\"userType\\": \\"loyalty\\", \\"frequency\\": \\"daily\\", \\"startDate\\": \\"2021-07-26\\", \\"startTime\\": \\"04:10 PM\\", \\"timezone\\":\\"America/Los_Angeles\\", \\"active\\": true, \\"fieldMapping\\": [{ \\"punchhAttr\\": \\"external_source_id\\", \\"salesforceAttr\\": \\"Id\\" }, { \\"punchhAttr\\": \\"email\\", \\"salesforceAttr\\": \\"Email\\" }, { \\"punchhAttr\\": \\"first_name\\", \\"salesforceAttr\\": \\"FirstName\\" }, { \\"punchhAttr\\": \\"last_name\\", \\"salesforceAttr\\": \\"LastName\\" }, { \\"punchhAttr\\": \\"phone\\", \\"salesforceAttr\\": \\"Phone\\" }, { \\"punchhAttr\\": \\"birthday\\", \\"salesforceAttr\\": \\"Birthdate\\" }, { \\"punchhAttr\\": \\"zipcode\\", \\"salesforceAttr\\": \\"OtherPostalCode\\" }] }';
       
        String config = '{ "apiEndpoint" : "https://goclient-sfservicecloud.punchh.io/api/v1", "accessToken" : "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJiaWQiOjksImNpZCI6IjVlZWEyMGI4ODJiODJkODY1MWM1ODZmMSIsImJfbmFtZSI6IlNGTUMgRGVtbyIsImFkbWluX2lkIjoiIiwiYnV1aWQiOiIiLCJ0ZW5hbnQiOiIiLCJzY28iOiIiLCJpc3MiOiJQdW5jaGgtc2FsZXNmb3JjZS1jbGllbnQifQ.kNFhUGSExqzqJwb-uajGUEGgad6Ez2VkMVCtESLsOUGYMi7zgDqIk9ugJW0A0QySPrrXVL6Cay2AY8-P0sZvjw", "body" : "'+ body +'" }'; 
        return (Configuration)(JSON.Deserialize(config, Configuration.class));
    }
    
    public abstract class Request {
        Boolean status;
        String action;
        String body;
        String sfOrgId;
    }

    public class Configuration extends Request {
        String adminKey;
        String clientId;
        String punchhUrl;
        String secretId;
        String id;
        String accessToken;
        String apiEndpoint;
    }
    
    
}