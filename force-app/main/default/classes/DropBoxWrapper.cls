public class DropBoxWrapper {
    //1. ACCESS DRIVE WRAPPER
    public class AccessDrive{
        public String access_token;
    }

    public List<File> entries; //VARIABLE TO HOLD FTECH DATA LIST OF FILES
    //2. FILE WRAPPER
    public class File{
        public String id{get;set;}
        public String name{get;set;}
        public String type{get;set;}
        public String path_display{get;set;}
        public boolean isLast{get;set;}
    }

    //3. FILE UPLOAD WRAPPER (NOT TO PARSE RESPONSE JUST TO CREATE UPLOAD FILE OBJECT FOR APEX:INPUTFILE)
    public class FileUpload{
        public String type{get;set;}
        public String name{get;set;}
        public blob body{get;set;}
        public Integer size{get;set;}
    }

    //4. FILE DOWNLOAD WRAPPER
    public class FileDownload{
        public String link{get;set;}
    }

    //5. CREATE FOLDER WRAPPER
    public class CreateFolder{
        public File metadata;
    }
}