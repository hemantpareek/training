import { LightningElement, api} from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import * as utils from 'c/nf_lbrandsUtils';

export default class Nf_itemDetail extends LightningElement {
    @api itemNumber; //Contains Item Number which is retrieved in url params
    @api stateless;  //Sets to true when component does not run in a record context
    @api item;       //Used to display data in stateless mode
    itemDetail;     //temp varible
    showSpinner;      //Decides wheather spinner will be shown or not
    
    /**
     * @description Fetches order for argumeted item number from server in stateful mode
     */
    connectedCallback(){
        console.log('OrderDetail > stateless', this.stateless);
        console.log('OrderDetail > orderNumber', this.itemNumber);
        console.log('OrderDetail > item', this.item);
        if(this.item){this.itemNumber = this.item.itemNumber;}
        let itemDeal= this.getDummyData();
        if(this.itemNumber){this.itemNumber=this.itemNumber}else{this.itemNumber='1000';}
        this.itemDetail  = itemDeal.filter(id=>{return id.itemNumber === this.itemNumber;})[0];
        console.log('hii item',this.itemDetail);
        if(this.stateless == false && this.orderNumber){
            this.showSpinner = true;
            
        }
    }

    /**
     * @name renderedCallback
     * @description Loads custom styles from static resources.
     */
    renderedCallback(){
        loadStyle(this, utils.LBRANDS_CSS_RESOURCE);
    }
    
    /**
     * @name back
     * @description Fires custom event to goback to record page.
     */
    back(){
        this.dispatchEvent(new CustomEvent(utils.BACK_EVENT));
    }

    getDummyData(){

        let families = ['bag', 'beauty', 'cardCases', 'sunglasses', 'keychains'];
        let thumbnailLinks = {
            bag : [
                'https://www.victoriassecret.com/p/760x1013/tif/ed/f6/edf6569ebfff4174b52e03b4c763630c/F20_BTY_071_e10497284.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/86/fa/86faede0ed5c41b798056a103ffa6ff3/F20_BTY_073_e10497288.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/5e/a1/5ea1177e489f4b7d9dea002ade2941f1/F20_BTY_072_e10497290.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/f2/6d/f26d663143d145a3aac75fca935ece1b/11175763001C_OM_B.jpg'
            ],
            beauty : [
                'https://www.victoriassecret.com/p/760x1013/tif/29/33/29333ffeee3e4b9c99576988728af61c/F19_BTY_203_e9243125.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/2a/96/2a96719a68cd4eb985653501c01370ac/S20_BTY_088_e9932512.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/45/6a/456a8d6eed2b409a841b466c05f9a1ec/S20_BTY_212_e10037214.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/5c/53/5c53e6a363a14488ae120e382ecdc416/S20_BTY_066_e9932399.jpg'
            ],
            cardCases : [
                'https://www.victoriassecret.com/p/760x1013/tif/51/02/5102dc7634e848edbcd5a384916abe10/11174930001C_OM_F.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/2e/5e/2e5e0b48176b4557adf164b736a1f038/11174930054R_OM_F.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/84/9f/849f7abaf41c42a2bd7c08258fb72ede/11174930020O_OM_F.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/8e/c0/8ec0e13991c042a2b71295a8e73045ce/11177403696P_OM_F.jpg'
            ],
            sunglasses : [
                'https://www.victoriassecret.com/p/760x1013/tif/80/f3/80f30e0103a841f39bdb8ac0fd7c721c/406722NEU_OM_B.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/8c/60/8c6033ffcd0f46b88dc244666079ff64/406729NEV_OM_B.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/60/aa/60aa1e7d35024c59849d408dac21bcca/406729BPZ_OM_B.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/25/4f/254fa55dee784bf393f3d8d4556df140/406729DRZ_OM_B.jpg'
            ],
            keychains : [
                'https://www.victoriassecret.com/p/760x1013/tif/22/3c/223c68f4a59f48f5bb27dc1753de1098/F20_BTY_124_e10501403.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/8e/ef/8eefcdea83f54d96bc56f1059ebdb70a/11175725001C_OM_F.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/3a/c4/3ac40b4a6f0942be840f99bb6e996b72/11175805001C_OM_F.jpg',
                'https://www.victoriassecret.com/p/760x1013/tif/4b/fe/4bfe33fc8fc2417eba31783701ba3058/11175805033R_OM_F.jpg'
                
            ]
        };
    
        let itemNames = ['V Aviator Sunglasses','The Victoria Expandable Card Case','The Victoria Wallet','The Victoria Small Wallet','Charm Keychain'];
    
        let items = [];
        for(let i=0; i<5; i++){
            let item = {};
            item.itemNumber = '' + (1000 + i);
            item.itemName = itemNames[i];
            item.family = families[i];
            item.size = 'Small';
            item.quantity = i+1,
            item.price = '$' + (12345 * i) + '.00';
            item.thumbnailLinks =   thumbnailLinks[families[i]];
            items.push(item);
        }
        return items;
    }
}