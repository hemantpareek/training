import { LightningElement, track, api} from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
// import getItemDetail from '@salesforce/apex/NF_APIUtil.getItemDetail';
// import getEmail from '@salesforce/apex/NF_ItemHistoryController.getEmail';
import * as utils from 'c/nf_lbrandsUtils';

export default class Nf_cart extends LightningElement {
    //Public Attributes
    @api stateless;                 //tells wheather the component is running in stateless/stateful mode
    @api statefulHeight;            //admin can set a value to this from app builder
    @api statefulColumns;           //Admin can set number of colunms for a stateful component
    @api recordId;                  //gets recordId from recordPage passed by parent aura component
    @api briefInfoFields;           //admin can set comma separated fields from app builder to be displayed as summary on cards (stateful only)

    //Private Attributes
    @track cartItems;               //holds the data retrived from APIs (orders)
    @track cartItemsBackup;         //keeps a copy of the cartItems list (Will be used to restore data during filter process)
    @track cartItemFields;          //item fields with its properties in object form

    showSpinner;                    //boolean variable to show/hide spinner
    emailId;                        //holds emailId entered in text box (stateless only)
    itemId;                         //holds itemId entered in text box (stateless only)
    filterText;                     //holds text entered in the filter search fox
    emailSelected = true;           //Sets to true when email is selected as orderParameter at UI
    itemIdSelected = false;         //Sets to true when itemId is selected as orderParameter at UI
    sortField;                      //holds the field which is currently sorted the data
    sortDirection;                  //holds the sort direction either asc or desc
    appliedFilterField;
    dynamicStyles;                  //holds dynamic styles for stateless/stateful views

    //Getters
    /**
     * @description Search bar will not display when one or less records retrieved
     */
    get displaySearchBar(){
        return this.cartItemsBackup && this.cartItemsBackup.length > 1;
    }

    /**
     * @description shows record count in stateful view
     */
    get recordCount(){
        return this.cartItems && this.cartItems.length > 0 ? this.cartItems.length : 0;
    }

    /**
     * @description used to show "No Items" message
     */
    get displayNoItems(){
        return this.stateless &&( !this.cartItems || this.cartItems.length === 0);
    }

    /**
     * @description Returns label for the filter options.
     */
    get filterLabel(){
        let appliedFilter = this.cartItemFields ? this.cartItemFields.filter( ele => ele.isFiltered ) : [];
        return appliedFilter && appliedFilter.length > 0 ? appliedFilter[0].label : '';
    }

    /**
     * @description Returns label for the sort options
     */
    get sortLabel(){
        let sortedField = this.cartItemFields ? this.cartItemFields.filter( fld => fld.isSorted ) : [];
        return sortedField && sortedField.length > 0 ? sortedField[0].label : '';
    }

    /**
     * @description Return sortIcon as per the sort direction.
     */
    get sortIcon(){
        let sortedField = this.cartItemFields ? this.cartItemFields.filter( fld => fld.isSorted ) : [];
        return (!sortedField || sortedField.length === 0) ? utils.SORT_ICON_DEFAULT : (
            (this.sortDirection == utils.SORT_DIR_ASC) ? utils.SORT_ICON_ARROW_UP : utils.SORT_ICON_ARROW_DOWN
        );
    }

    /**
     * @description When nothing is entered in text box then go button will not be active 
     */
    get disableGoButton(){
        return this.emailSelected ? !this.emailId : !this.itemId;
    }

    /**
     * @description Returns fields to show in sort section
     */
    get sortableFields(){
        let sortableFields = [];
        utils.CART_ITEM_SORTABLE_FIELDS.forEach( sfld => {
            let sortableField = this.cartItemFields.filter( fld => {return fld.value === sfld;});
            if(sortableField && sortableField.length == 1){
                sortableFields.push(sortableField[0]);
            }
        });
        return sortableFields;
    }

    /**
     * @description Returns fields to show in filter section
     */
    get filterOptions(){
        let filterOptions = [];
        utils.CART_ITEM_FILTERABLE_FIELDS.forEach( ffld => {
            let filterableField = this.cartItemFields.filter( fld => {return fld.value === ffld;});
            if(filterableField && filterableField.length == 1){
                filterOptions.push(filterableField[0]);
            }
        });
        return filterOptions;
    }

    /**
     * @description Tells if fields has been passed from design attributes or not.
     */
    get hasBriefInfoFields(){
        return (this.stateless === false && this.briefInfoFields && this.briefInfoFields.length > 0) || this.stateless;
    }

    /**
     * @name connectedCallback
     * @description Loads when component inserts to DOM and performs initialization of properties.
     *              Also fetches data from server with the email id of the associated record in stateful case. 
     */
    connectedCallback(){
        this.cartItemFields = utils.getCartItemFields();  
        this.dynamicStyles = utils.getCartItemDynamicStyles(this);
        this.appliedFilterField = utils.CART_ITEM_NAME_FIELD;
        // this.emailSelected = true;
        // this.sortDirection = utils.SORT_DIR_ASC;

        if( this.stateless == false && this.recordId ){
            // this.getEmailForStateful(this.recordId);
            this.cartItemsBackup = utils.getDummyData( this );
            this.cartItems = this.prepareItemsToDisplay( utils.getDummyData( this ) );
        }
    }

    /**
     * @name renderedCallback
     * @description Loads custom styles from static resources.
     */
    renderedCallback(){
        loadStyle( this, utils.LBRANDS_CSS_RESOURCE );
    }

    // /**
    //  * @name getEmailForStateful
    //  * @param {1} recordId 
    //  * @description Fetches email Id for the customer that is associated with the recordId (for stateful only).
    //  */
    // getEmailForStateful(recordId){
    //     console.log('ItemHistory > getEmailForStateful > recordId', this.recordId);
    //     getEmail({
    //         "recordId" : recordId
    //     })
    //     .then(result => {
    //         this.showSpinner = false;
    //         console.log('ItemHistory > getEmailForStateful > result', result);
    //         if(result){
    //             this.fetchItemsFromServer( result, null);
    //         } else {
    //             utils.showToast(utils.TOAST_CONFIG.messages.email_not_found, utils.TOAST_CONFIG.variant.info, this);
    //         }
    //     })
    //     .catch(err=>{
    //         console.log('ItemHistory > getEmailForStateful > err', err);
    //         utils.showToast(err, utils.TOAST_CONFIG.variant.error, this);
    //         this.showSpinner = false;
    //     })
    // }
    
    /**
    * @name searchItems
    * @description Calls apex method to search orders with appropriate inputs
    */
    searchItems(){
        if( this.emailSelected && this.emailId ){
            this.fetchItemsFromServer(this.emailId, null);
        } else if( this.itemIdSelected && this.itemId ){
            this.fetchItemsFromServer('', this.itemId);
        }
    }

    /**
    * @name applyFilter 
    * @param {1} event
    * @description Applies filter to the clientSide records when selected at UI.
    */
    applyFilter( event ){
        let value = event.target.value;
        this.cartItemFields.forEach( ele => ele.isFiltered = ( ele.value == value ) ? true : false );
        this.appliedFilterField = value;
        this.filterItems({
            target : { value : this.appliedFilterField ? this.filterText : '' }
        });
    }

    /**
    * @name filterItems
    * @param {1} event 
    * @description This method performs the filteration on the client side records with the applied filters
    */
    filterItems( event ){
       try{
            this.filterText = event.target.value;
            if( this.filterText ){
                let fld = this.filterOptions.filter( ele => ele.isFiltered )[0].value;
                let appliedFilteres = [ fld ];
                this.cartItems = this.prepareItemsToDisplay(utils.filterRecords(this.cartItemsBackup, this.filterText, appliedFilteres));
            } else {
                this.cartItems = this.prepareItemsToDisplay(this.cartItemsBackup);
            }
       }catch(err){
           console.log('# Error: ', err);
       }
    }

    /**
    * @name sort
    * @param {1} event
    * @description Performs sorting for client side records 
    */
    sort(event){
        let currentField = event.target.value;
        this.cartItems = this.prepareItemsToDisplay(utils.sortRecords(this.cartItems, currentField, this, this.cartItemFields));
    }

    /**
    * @name handleInputValue
    * @param {1} event
    * @description Updates emailId and ItemId when input is taken in the text boxes.
    */
    handleInputValue( event ){
        this[ event.target.dataset.input ] = event.target.value;
    }

    /**
    * @name fetchItemsFromServer
    * @param {1} emailId 
    * @param {2} itemId
    * @description Fetches corresponding orders from server with the emailId or ItemId accordingly. 
    */
    fetchItemsFromServer(emailId, itemId){
        this.cartItemsBackup = utils.getDummyData( this );
        this.cartItems       = this.prepareItemsToDisplay( utils.getDummyData( this ) );
        if( !emailId ){
            this.cartItems = this.cartItems.filter( ci => ci.itemNumber.toLowerCase() == itemId.toLowerCase() );
            this.cartItemsBackup = this.cartItems;
        }
        // this.showSpinner = true;
        // getItemDetail({"email" : emailId , "itemId" : itemId})
        // .then(result => {
        //     console.log('ItemHistory > fetchItemsFromServer > stateless', this.stateless);
        //     console.log('ItemHistory > fetchItemsFromServer > result', result);
        //     if(result){
        //         this.showSpinner = false;
        //         let data = JSON.parse(result);
        //         if(data && data.length > 0){
        //             utils.showToast(data.length + ' ' + (data.length == 1 ? utils.TOAST_CONFIG.messages.order_retrieved : utils.TOAST_CONFIG.messages.orders_retrieved), utils.TOAST_CONFIG.variant.success, this );
        //             let cartItems = [];
        //             data.forEach( oh => {
        //                 cartItems.push(getItem(oh));
        //             });
        //             this.cartItemsBackup = JSON.parse(JSON.stringify(cartItems)); //It will not contain brief data
        //             this.cartItems = this.prepareItemsToDisplay(cartItems); //it will contain brief data
        //         } else {
        //             utils.showToast( utils.TOAST_CONFIG.messages.no_orders_retrieved, utils.TOAST_CONFIG.variant.info, this );
        //         }
        //     }else {
        //         this.cartItems = [];
        //         this.cartItemsBackup = [];
        //         this.showSpinner = false;
        //         utils.showToast( utils.TOAST_CONFIG.messages.no_orders_retrieved, utils.TOAST_CONFIG.variant.info, this );
        //     }
        // })
        // .catch(err=>{
        //     console.log('ItemHistory > fetchItemsFromServer > err', err);
        //     this.showSpinner = false;
        //     utils.showToast( err, utils.TOAST_CONFIG.variant.error, this );
        // })
    }

    /**
    * @name prepareItemsToDisplay
    * @param {1} records 
    * @description Attachs briefInfo to actual item data to be used to displayed at UI.
    */
    prepareItemsToDisplay( records ){
        let fieldsToDisplay = this.stateless ? utils.CART_ITEM_SUMMARY_FIELDS_STATELESS_MODE : (this.briefInfoFields ? this.briefInfoFields.split(",") : []);
        return utils.prepareDataToDisplay(records, fieldsToDisplay, this.cartItemFields, utils.CART_ITEM_KEY_FIELD, this.filterText, this.appliedFilterField ? [this.appliedFilterField] : [], this.stateless);
    }

    /**
     * @name openItem
     * @param {1} event
     * @description Opens an item when user clicks on link in stateful mode 
     */
    openItem(event){
        try{
            event.preventDefault(); 
            let itemNumber = event.target.dataset.itemnumber;
            this.dispatchEvent(new CustomEvent(utils.OPEN_SUBTAB_EVENT, { detail: {
                "uniqueId" : itemNumber
            }}));
        }catch(err){
            console.log('# Error: ', err);
        }
    }

    /**
    * @name selectFilter
    * @param {1} event 
    * @description Selects parameter that should be proceed to fetch item according to radio selection.
    */
    selectFilter( event ){
        let value = event.target.value;
        if( value == utils.EMAIL_STRING ){
            this.emailSelected  = true;
            this.itemIdSelected = false;
        } else if( value == utils.CART_ITEM_ITEMID_STRING ){
            this.emailSelected  = false;
            this.itemIdSelected = true;
        }
    }
}