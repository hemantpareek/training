import { LightningElement } from 'lwc';

import m1 from '@salesforce/apex/AsyncJsController.m1';
import m2 from '@salesforce/apex/AsyncJsController.m2';
import m3 from '@salesforce/apex/AsyncJsController.m3';
import m4 from '@salesforce/apex/AsyncJsController.m4';
import m5 from '@salesforce/apex/AsyncJsController.m5';
import m6 from '@salesforce/apex/AsyncJsController.m6';
import m7 from '@salesforce/apex/AsyncJsController.m7';
import m8 from '@salesforce/apex/AsyncJsController.m8';
import m9 from '@salesforce/apex/AsyncJsController.m9';
import m10 from '@salesforce/apex/AsyncJsController.m10';

export default class AsyncJs extends LightningElement {
    
    constructor(){
        super();
        this.promiseAll();
    }

    promiseAll(){

        Promise.all([m1(),m2(),m3(),m4(),m5(),m6({'param' : 'n6'}),m7({'param' : 'n7'}),m8({'param' : 'n8'}),m9({'param' : 'n9'}),m10({'param' : 'n10'})])
        .then( result => {
            result.forEach(element => {
                console.log(element);
            });
        })
        .catch( error => {
            console.log('Error!', error.body.message || error.message, 'error');
        })
    }

    promiseCall(){
        m1()
        .then(result => {
            console.log(result);
        })
      

        .then(
            m2()
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            })    
        )

        .then(
            m3()
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            })    
        )

        .then(
            m4()
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
                throw error;
            }) 
        )

        .then(
            m5()
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            })
        )

        .then(
            m6({'param' : 'n6'})
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            })
        )

        .then(
            m7({'param' : 'n7'})
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            })
        )

        .then(
            m8({'param' : 'n8'})
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            })
        )

        .then(
            m9({'param' : 'n9'})
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            })
        )

        .then(
            m10({'param' : 'n10'})
            .then(result => {
                console.log(result);
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            })
        )

        .catch(error => {
            console.log('Error in outer!', error.body.message || error.message, 'error');
        })
    }

   

    directCall(){
        m1()
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m2()
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m3()
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m4()
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m5()
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m6({'param' : 'n6'})
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m7({'param' : 'n7'})
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m8({'param' : 'n8'})
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m9({'param' : 'n9'})
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });

        m10({'param' : 'n10'})
        .then(result => {
            console.log(result);
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });
    }

    nestedCall(){
        m1()
        .then(result => {
            console.log(result);
            m2()
            .then(result => {
                console.log(result);
                m3()
                .then(result => {
                    console.log(result);
                    m4()
                    .then(result => {
                        console.log(result);
                        m5()
                        .then(result => {
                            console.log(result);
                            m6({'param' : 'n6'})
                            .then(result => {
                                console.log(result);
                                m7({'param' : 'n7'})
                                .then(result => {
                                    console.log(result);
                                    m8({'param' : 'n8'})
                                    .then(result => {
                                        console.log(result);
                                        m9({'param' : 'n9'})
                                        .then(result => {
                                            console.log(result);
                                            m10({'param' : 'n10'})
                                            .then(result => {
                                                console.log(result);
                                            })
                                            .catch(error => {
                                                console.log('Error!', error.body.message || error.message, 'error');
                                            });
                                        })
                                        .catch(error => {
                                            console.log('Error!', error.body.message || error.message, 'error');
                                        });
                                    })
                                    .catch(error => {
                                        console.log('Error!', error.body.message || error.message, 'error');
                                    });
                                })
                                .catch(error => {
                                    console.log('Error!', error.body.message || error.message, 'error');
                                });
                            })
                            .catch(error => {
                                console.log('Error!', error.body.message || error.message, 'error');
                            });
                        })
                        .catch(error => {
                            console.log('Error!', error.body.message || error.message, 'error');
                        });
                    })
                    .catch(error => {
                        console.log('Error!', error.body.message || error.message, 'error');
                    });
                })
                .catch(error => {
                    console.log('Error!', error.body.message || error.message, 'error');
                });
            })
            .catch(error => {
                console.log('Error!', error.body.message || error.message, 'error');
            });
        })
        .catch(error => {
            console.log('Error!', error.body.message || error.message, 'error');
        });
    }
}