import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class LwcFilterRecords extends LightningElement {

    @api selectedObject;
    @api fieldList;
    @api filterRecordValue;
    selectedLogicalOperator;
    @track conditionList;
    customLogic;

    // constructor(){
    //     super();
        
    // }

    connectedCallback(){
        //console.log(JSON.stringify(this.filterRecordValue));
        this.conditionList = JSON.parse(JSON.stringify(this.filterRecordValue.conditionList));
        this.selectedLogicalOperator = JSON.parse(JSON.stringify(this.filterRecordValue.selectedLogicalOperator));
        this.customLogic = JSON.parse(JSON.stringify(this.filterRecordValue.customLogic));
    }

    get textOperators(){
        return [
            {label: 'Equals', value: '='},
            {label: 'Does not equal', value: '<>'},
            {label: 'Greater than', value: '>'},
            {label: 'Greater than or equal', value: '>='},
            {label: 'Less than', value: '<'},
            {label: 'Less than or equal', value: '<='},
            {label: 'Contains', value: ' LIKE '}
        ];
    }

    get booleanOperators(){
        return [
            {label: 'Equals', value: '='},
            {label: 'Does not equal', value: '<>'}
        ];
    }

    get booleanOptions(){
        return [
            {label: 'True', value: true},
            {label: 'False', value: false}
        ];
    }

    get conditions(){
        return [
            {label: 'All of the conditions are met (AND)', value: 'AND'},
            {label: 'Any of the conditions are met (OR)', value: 'OR'},
            {label: 'Customize the logic', value: 'custom'}
        ];
    }

    get helpText(){
        return 'Customize the logic for the filter conditions that you specified. For example, if you enter 1 AND (2 OR 3), this criteria evaluates to true when the first condition is true and either the second or third condition is true.';
    }

    get isCustomLogic(){
        return this.selectedLogicalOperator == 'custom';
    }

    handleConditionChange(event){
        this.selectedLogicalOperator = event.target.value;
    }

    addCondition(event){
        this.conditionList.push({serial: this.conditionList.length + 1, field:{}, notHasField : true, notHasOperator : true});
    }

    closeRecordsFilterModal(){
        this.dispatchEvent(new CustomEvent('cancel'));
    }

    removeCondition(event){
        //console.log('remove condition');
        var index = parseInt(event.target.dataset.index);
        this.conditionList = this.conditionList.filter( condition =>{
            return condition.serial != index + 1;
        });
        for(let i = index; i<this.conditionList.length; i++){
            this.conditionList[i].serial = i + 1;
        }
    }

    showToast(title, message, variant){
        const evt = new ShowToastEvent({
            'title': title,
            'message': message,
            'variant': variant,
            'mode': 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    handleFieldChange(event){
        //console.log('@@@ value => ' + event.target.value);
        //console.log('@@@ index => ' + event.target.dataset.index);
        var index = parseInt(event.target.dataset.index);
        var fieldName = event.target.value;
        var fieldObj = this.fieldList.filter( field => { return field.value == fieldName })[0];
        var condition = this.conditionList.filter( condition => { return condition.serial == index + 1})[0];
        if(fieldObj && condition){
            condition.field = {};
            condition.field.name = fieldName;
            condition.field.type = fieldObj.type;
            condition.field.isStdInput = fieldObj.isStdInput;
            condition.notHasField = false;
            condition.field.isBoolean = fieldObj.isBoolean;
        }
    }

    handleOperatorChange(event){
        //console.log('@@@ value => ' + event.target.value);
        //console.log('@@@ index => ' + event.target.dataset.index);
        var index = parseInt(event.target.dataset.index);
        var operator = event.target.value;
        var condition = this.conditionList.filter( condition => { return condition.serial == index + 1})[0];
        if(operator && condition){
            condition.operator = operator;
            condition.notHasOperator = false;
        }
    }

    handleValueChange(event){
        //console.log('@@@ value => ' + event.target.value);
        //console.log('@@@ index => ' + event.target.dataset.index);
        var index = parseInt(event.target.dataset.index);
        var fieldValue = event.target.value;
        var condition = this.conditionList.filter( condition => { return condition.serial == index + 1})[0];
        if(fieldValue && condition){
            if(condition.field.isBoolean){
                condition.field.value = JSON.parse(fieldValue);
            } else {
                condition.field.value = fieldValue;
            }
        }
    }

    handleCustomLogicChange(event){
        this.customLogic = event.target.value;
    }

    save(){
        //console.log('@@@@ save');
        if(this.isValid()){
            var self = this;
            this.dispatchEvent(new CustomEvent('save', {detail:
                {
                    filterValue : {
                        conditionList : self.conditionList,
                        selectedLogicalOperator : self.selectedLogicalOperator,
                        customLogic : self.customLogic
                    }
                }
            }));
        }
    }

    applyFilter(){
        if(this.isValid()){
            var whereClause = this.createWhereClause();
            //console.log(whereClause);
            var self = this;
            this.dispatchEvent(new CustomEvent('apply', {detail: 
                {
                    'whereClause' : whereClause,
                    'filterValue' : {
                        conditionList : self.conditionList,
                        selectedLogicalOperator : self.selectedLogicalOperator,
                        customLogic : self.customLogic
                    }
                }
            }));
        }
    }

    isValid(){
        var self = this;
        var flag = true;
        self.conditionList.forEach( condition => {
            if(!condition.field.value){
                this.showToast('','Required field missing','error');
                flag = false;
            }
        });
        return flag;
    }

    createWhereClause(){
        var extractedConditions = this.extractConditionsFromList();
        var logicOperator = this.selectedLogicalOperator;
        var whereClause = '';
        if(logicOperator == 'OR' || logicOperator == 'AND'){
            extractedConditions.forEach( ec => {
                whereClause += ec + logicOperator;
            });
            whereClause = whereClause.substring(0, whereClause.length - logicOperator.length);
        } else {
            //Convert custom logic to form '({1}OR{2}AND{3})
            var num = '';
            var numFlag = false;
            for(var i=0; i<this.customLogic.length; i++){

                if(this.customLogic.charAt(i) >= '0' && this.customLogic.charAt(i) <= '9'){
                    numFlag = true;
                    num += this.customLogic.charAt(i);
                } else {
                    if(numFlag){
                        numFlag = false;
                        whereClause += '#' + num + '#';
                        num = '';
                    }
                    whereClause += this.customLogic.charAt(i);
                }
            }
            if(numFlag){
                whereClause += '#' + num + '#';
            }
            //console.log(whereClause);
            // now map the expressions in where clause with conditions
            for(var i=1; i<=this.conditionList.length; i++){
                var regex = new RegExp("#" + i + "#", "g");
                whereClause = whereClause.replace(regex, extractedConditions[i-1]);
            }
        }
        return '(' + whereClause + ')';
    }

    extractConditionsFromList(){
        var extractedConditions = [];
        this.conditionList.forEach( condition => {
            var ec = '(';
            ec += condition.field.name;
            ec += condition.operator;
            if(condition.operator == ' LIKE '){
                ec += '\'%' + condition.field.value + '%\'' ;
            } else if (condition.field.type == 'text'){
                ec += '\'' + condition.field.value + '\'' ;
            } else {
                ec += condition.field.value;
            }
            ec += ')';
            extractedConditions.push(ec);
        });
        //console.log(extractedConditions);
        return extractedConditions;
    }

    removeFilter(){
        this.dispatchEvent(new CustomEvent('remove'));
   }
}