import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import LbrandsResources from '@salesforce/resourceUrl/LbrandsResources'
import * as Constants from './nf_lbrandsConstants';
export * from './nf_lbrandsConstants';

/******************************************************* ORDER DETAIL AND ORDER HISTORY DATA ******************************************************************/

/**
 * @description Schema Properties for order History
 */
export const SORTABLE_ORDER_FIELDS = ['orderNumber', 'orderDate', 'orderTotal', 'paymentType']
export const FILTERABLE_ORDER_FIELDS = ['orderNumber', 'orderStatus', 'trackingNumber', 'shippingToName'];
export const ORDER_SUMMARY_FIELDS_STATELESS_MODE = ['orderNumber','orderDate', 'orderTotal', 'paymentType', 'orderStatus', 'trackingNumber', 'shippingToName', 'shippingToAddress_1'];

/**
 * @name getOrderHistoryDynamicStyles
 * @param {1} self 
 * @description Provides dynamic styles for the order History markup on conditional basis (For both stateful and stateless)
 */
export const getOrderHistoryDynamicStyles = (self) => {
    return {
        ltngCardVariant  : self.stateless ? ''  : 'Narrow',
        ltngCardIconName : self.stateless ? ''  : 'standard:orders',
        statefulHeight   : self.stateless ? ''  : (self.statefulHeight && self.statefulHeight > 100 ? 'max-height: ' + self.statefulHeight + 'px' : ''),
        statefulViewClass: self.stateless ? ''  : 'slds-var-m-right_medium scroll-order-history',
        orderLinkClass   : self.stateless ? 'stateless-order-link' : '',
        briefLayoutSize  : self.stateless ? 6   :  (self.statefulColumns == '2' ? 6 : 12),
        briefLabelClass  : self.stateless ? 'slds-form-element__label slds-truncate slds-text-align_right slds-p-right_xx-large' : 'slds-form-element__label slds-truncate',
        briefValueSize   : self.stateless ? 5 : 7,
        cccTrackingClass : self.stateless ? 'slds-form-element__static slds-text-link slds-text-align_right slds-truncate slds-p-right_xx-large' : 'slds-form-element__static slds-text-link slds-truncate',
    };
}

/**
 * @name getOrderFields
 * @description Provides Order Fields 
 */
export const getOrderFields = () => {
    return [
        {label : "Order Number"   , value : "orderNumber"        , isSorted : false , isFiltered : true },
        {label : "Order Date"     , value : "orderDate"          , isSorted : false , isFiltered : false},
        {label : "Order Total"    , value : "orderTotal"         , isSorted : false , isFiltered : false},
        {label : "Payment Type"   , value : "paymentType"        , isSorted : false , isFiltered : false},
        {label : "Status"         , value : "orderStatus"        , isSorted : false , isFiltered : false},
        {label : "Tracking Number", value : "trackingNumber"     , isSorted : false , isFiltered : false},
        {label : "Ship To"        , value : "shippingToName"     , isSorted : false , isFiltered : false},
        {label : "Address"        , value : "shippingToAddress_1", isSorted : false , isFiltered : false},
    ];
}

/**
 * @name getOrder
 * @param {1} orderResponse
 * @description Fetches some selected fields from order response and creates an object from that 
 */
export const getOrder = (orderResponse) => {
    try{
        if(orderResponse){
            let order = {};
            order.orderNumber = orderResponse.orderNumber ? orderResponse.orderNumber : '';
            order.orderStatus = orderResponse.status;
            if(order.orderNumber){
                order.cccTracking = Constants.CCC_TRACKING_URL.replace('{orderNumber}', order.orderNumber);
            }
            if(orderResponse.orderDate){
                order.orderDate = '' + getMonth(parseInt(orderResponse.orderDate.month)) + ' ' + orderResponse.orderDate.day + ', ' + orderResponse.orderDate.year;
            }
            if(orderResponse.orderSummary){
                order.orderTotal = formatMoney(orderResponse.orderSummary.orderTotal);
                order.merchandise = formatMoney(orderResponse.orderSummary.merchandiseSubTotal);
                order.shippingHandling = formatMoney(orderResponse.orderSummary.shippingHandling);
                order.offerDiscount = formatMoney(orderResponse.orderSummary.appliedOffers[0].amount);
                order.salesTax = formatMoney(orderResponse.orderSummary.salesTax);
            }
            if(orderResponse.payments){
                order.paymentType = orderResponse.payments[0].paymenttype;
                order.isOnline = order.paymentType == 'Visa';
                order.paymentNumber = 'XXXX-XXXX-XXXX-' + orderResponse.payments[0].paymentnumber;
                order.paymentAmount = formatMoney(orderResponse.payments[0].amount);
            }
            if(orderResponse.shipments){
                order.shippingToName = orderResponse.shipments[0].address.firstName + ' ' + orderResponse.shipments[0].address.lastName;
                order.shippingToAddress_1 = orderResponse.shipments[0].address.streetAddress1;
                let trackingNumber = orderResponse.shipments[0].trackingNumber;
                order.trackingNumber = trackingNumber ? ('' + trackingNumber).replaceAll(',','') : '';
                order.carrier = orderResponse.shipments[0].carrier;
                order.shipmentType = orderResponse.shipments[0].shipmentType;
                order.family = orderResponse.shipments[0].lineItems[0].family;
                order.variantId = orderResponse.shipments[0].lineItems[0].variantId;
                order.size = orderResponse.shipments[0].lineItems[0].attributes.size1.value;
                order.qty = orderResponse.shipments[0].lineItems[0].quantity.selected;
                order.price = formatMoney(orderResponse.shipments[0].lineItems[0].price.totalPrice.originalPrice);
            }
            // console.log('# order: ', JSON.parse(JSON.stringify(order)));
            return order;
        } 
    }catch(err){
        console.log('# Error: ', err);
        return {};
    }
}

/**
 * @name getMonth
 * @param {1} num
 * @description Returns month name corresponding to the provided number 
 */
const getMonth = (num) => {
    switch(num) {
        case 1: return 'January';
        case 2: return 'February';
        case 3: return 'March';
        case 4: return 'April';
        case 5: return 'May';
        case 6: return 'June';
        case 7: return 'July';
        case 8: return 'August';
        case 9: return 'September';
        case 10: return 'October';
        case 11: return 'November';
        case 12: return 'December';
      }
}

/******************************************* CART AND ITEM DETAIL COMPONENT DATA *****************************************************/
/**
 * @description Schema Properties for Cart Item
 */
export const CART_ITEM_SUMMARY_FIELDS_STATELESS_MODE = ['itemNumber','itemName','family','size','quantity','price'];
export const CART_ITEM_SORTABLE_FIELDS = ['itemNumber','itemName','family','size','quantity','price'];
export const CART_ITEM_FILTERABLE_FIELDS = ['itemName', 'family'];

export const getCartItemFields = () => {
    return [
        {label : "Item Name"  , value : "itemName"    , isSorted : false , isFiltered : true},
        {label : "Item Number", value : "itemNumber"  , isSorted : false , isFiltered : false},
        {label : "Item Family", value : "family"      , isSorted : false , isFiltered : false},
        {label : "Size"       , value : "size"        , isSorted : false , isFiltered : false},
        {label : "Quantity"   , value : "quantity"    , isSorted : false , isFiltered : false},
        {label : "Price"      , value : "price"       , isSorted : false , isFiltered : false},
    ];
}



/**
 * @name getCartDynamicStyles
 * @param {1} self 
 * @description Provides dynamic styles for the Cart markup on conditional basis (For both stateful and stateless)
 */
export const getCartItemDynamicStyles = (self) => {
    return {
        ltngCardVariant  : self.stateless ? ''  : 'Narrow',
        ltngCardIconName : self.stateless ? ''  : 'standard:webcart',
        statefulHeight   : self.stateless ? ''  : (self.statefulHeight && self.statefulHeight > 100 ? 'max-height: ' + self.statefulHeight + 'px' : ''),
        statefulViewClass: self.stateless ? ''  : 'slds-var-m-right_medium scroll-order-history',
        orderLinkClass   : self.stateless ? 'stateless-order-link' : '',
        briefLayoutSize  : self.stateless ? 6   :  (self.statefulColumns == '2' ? 6 : 12),
        briefLabelClass  : self.stateless ? 'slds-form-element__label slds-truncate slds-text-align_right slds-p-right_xx-large' : 'slds-form-element__label slds-truncate',
    };
}

export const getDummyData = (self) => {

    let families = ['bag', 'beauty', 'cardCases', 'sunglasses', 'keychains'];
    let thumbnailLinks = {
        bag : [
            'https://www.victoriassecret.com/p/760x1013/tif/ed/f6/edf6569ebfff4174b52e03b4c763630c/F20_BTY_071_e10497284.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/86/fa/86faede0ed5c41b798056a103ffa6ff3/F20_BTY_073_e10497288.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/5e/a1/5ea1177e489f4b7d9dea002ade2941f1/F20_BTY_072_e10497290.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/f2/6d/f26d663143d145a3aac75fca935ece1b/11175763001C_OM_B.jpg',
            'https://www.victoriassecret.com/p/504x672/tif/73/3c/733cf946e36c4b6f8485ffa86c1829c1/F20_BTY_050_e10497240.jpg'
        ],
        beauty : [
            'https://www.victoriassecret.com/p/760x1013/tif/29/33/29333ffeee3e4b9c99576988728af61c/F19_BTY_203_e9243125.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/2a/96/2a96719a68cd4eb985653501c01370ac/S20_BTY_088_e9932512.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/45/6a/456a8d6eed2b409a841b466c05f9a1ec/S20_BTY_212_e10037214.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/5c/53/5c53e6a363a14488ae120e382ecdc416/S20_BTY_066_e9932399.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/fb/89/fb8926150d5349318c2452dceeec5376/S20_PKBEAUTY_053_1121.jpg'
        ],
        cardCases : [
            'https://www.victoriassecret.com/p/760x1013/tif/51/02/5102dc7634e848edbcd5a384916abe10/11174930001C_OM_F.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/2e/5e/2e5e0b48176b4557adf164b736a1f038/11174930054R_OM_F.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/84/9f/849f7abaf41c42a2bd7c08258fb72ede/11174930020O_OM_F.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/8e/c0/8ec0e13991c042a2b71295a8e73045ce/11177403696P_OM_F.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/1d/34/1d34fdcf8f394d9b9b31114c62e82612/11175802971W_OM_B.jpg'
        ],
        sunglasses : [
            'https://www.victoriassecret.com/p/760x1013/tif/80/f3/80f30e0103a841f39bdb8ac0fd7c721c/406722NEU_OM_B.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/8c/60/8c6033ffcd0f46b88dc244666079ff64/406729NEV_OM_B.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/60/aa/60aa1e7d35024c59849d408dac21bcca/406729BPZ_OM_B.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/25/4f/254fa55dee784bf393f3d8d4556df140/406729DRZ_OM_B.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/f8/7a/f87a761521bd479c881b79ea9f506523/4082114HR_OM_B.jpg'
        ],
        keychains : [
            'https://www.victoriassecret.com/p/760x1013/tif/22/3c/223c68f4a59f48f5bb27dc1753de1098/F20_BTY_124_e10501403.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/8e/ef/8eefcdea83f54d96bc56f1059ebdb70a/11175725001C_OM_F.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/3a/c4/3ac40b4a6f0942be840f99bb6e996b72/11175805001C_OM_F.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/4b/fe/4bfe33fc8fc2417eba31783701ba3058/11175805033R_OM_F.jpg',
            'https://www.victoriassecret.com/p/760x1013/tif/46/9d/469d8cd152ae4691bf10c7a2ce743fc2/11175804131S_OM_F.jpg'
        ]
    };

    let itemNames = ['V Aviator Bag','The Victoria Beauty','The Victoria Card case','The Victoria Sunglass','Charm Keychain'];

    let items = [];
    for(let i=0; i<5; i++){
        let item = {};
        item.itemNumber = '' + (1000 + i);
        item.itemName = itemNames[i];
        item.family = families[i];
        item.size = 'Small';
        item.quantity = i+1,
        item.price = '$' + (12345 * i) + '.00';
        item.thumbnailLinks = self.stateless ? thumbnailLinks[families[i]] : thumbnailLinks[families[i]][0];
        items.push(item);
    }
    return items;
}


/************************************************** GENERAL UTILITY ITEMS **********************************************************/

export const LBRANDS_CSS_RESOURCE = LbrandsResources + '/css/lbrandsStyles.css';

/**
 * @name showToast
 * @param {1} msg 
 * @param {2} type 
 * @param {3} self
 * @description Displays toast with the argumented type and message 
 */
export const showToast = (msg , type, self) => {
    const event = new ShowToastEvent({
        message: (type === 'error') ? getErrorMessage(msg) : msg,
        variant: type,
        mode: 'dismissable'
    });
    self.dispatchEvent(event);
}

/**
 * @name getErrorMessage
 * @param {1} err 
 * @description Returns error message for the argumented error as per error type
 */
const getErrorMessage = (err) => {
    var error;
    if (err instanceof TypeError) {
        error = err.message;
    }else if (err instanceof ReferenceError) {
        error = err.message;
    }else if (err instanceof RangeError) {
        error = err.message;
    }else if (err instanceof SyntaxError) {
        error = err.message;
    }else if (err instanceof URIError) {
        error = err.message;
    } else if (err instanceof EvalError) {
        error = err.message;
    } else if (err && err.body && err.body.message){ //Apex errors have this type of schema
        error = err.body.message;
    } else {
        // Handle all other types of exceptions
        error = err;
    }
    return error;
}

/**
 * @name formatMoney
 * @param {1} amount 
 * @param {2} decimalCount 
 * @param {3} decimal 
 * @param {4} thousands 
 * @description Converts unformatted currency value to 'XXX,XX.XX' format
 */
const formatMoney = (amount, decimalCount = 2, decimal = ".", thousands = ",") => {
    try {
        amount = amount ? ('' + amount).replaceAll(',','') : 0;
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
    
        const negativeSign = amount < 0 ? "-" : "";
    
        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;
    
        return negativeSign + '$' + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        console.log('# Error: ', e);
    }
}

/**
 * 
 * @param {1} records (Object[])
 * @param {2} filterText (String)
 * @param {3} appliedFilters (String[])
 */
export const filterRecords = (records, filterText, appliedFilters) => {
    try{
        return JSON.parse(JSON.stringify(records)).filter( r => {
            for( let i = 0; i < appliedFilters.length; i++ ){
                if( ( '' + r[ appliedFilters[i] ] ).toLowerCase().includes( filterText.toLowerCase() ) ){
                    return r;
                }
            }
        });
   }catch(err){
       console.log('# Error: ', err);
   }
}

/**
 * 
 * @param {1} records (Object[])
 * @param {2} sortField (String)
 * @param {3} self (component reference)
 * @param {4} sortableFields (Object[])
 */
export const sortRecords = (records, sortField, self, sortableFields) => {
    //There should be two class properties for sortDirection and sortField
    if(sortField){
        self.sortDirection = (self.sortField == sortField) ? (self.sortDirection == Constants.SORT_DIR_ASC ? Constants.SORT_DIR_DESC : Constants.SORT_DIR_ASC) : Constants.SORT_DIR_ASC;
        self.sortField = sortField;
        if(sortableFields && sortableFields.length > 0){
            sortableFields.forEach( fld => fld.isSorted = ( fld.value == sortField ) ? true : false ); //to set isSorted flag for only sorted field
        }
        return records.sort(compareValues(sortField, self.sortDirection));
    }
}

/**
 * @name compareValues
 * @param {1} key 
 * @param {2} order
 * @description Helper method for client side sort functionality 
 */
const compareValues = (key, order = Constants.SORT_DIR_ASC) => {
    return function innerSort(a, b) {
        if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            return 0;
        }

        const varA = (typeof a[key] === 'string')
            ? a[key].toUpperCase() : a[key];
        const varB = (typeof b[key] === 'string')
            ? b[key].toUpperCase() : b[key];

        let comparison = 0;
        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }
        return (
            (order === Constants.SORT_DIR_DESC) ? (comparison * -1) : comparison
        );
    };
}

/**
 * 
 * @param {1} records (Object[])
 * @param {2} fieldsToDisplay (String[])
 * @param {3} recordFields (Object[])
 * @param {4} keyField (String)
 * @param {5} filterText (String)
 * @param {6} appliedFilters (String[])
 */
export const prepareDataToDisplay = (records, fieldsToDisplay, recordFields, keyField, filterText, appliedFilters, stateless) => {
    try{
        let dataToDisplay = [];
        JSON.parse(JSON.stringify(records)).forEach( rcd => {
            let record = JSON.parse(JSON.stringify(rcd));
            record.briefInfo = {};
            record.briefInfo.fields = [];
            fieldsToDisplay.forEach( fld =>{
                let field = {};
                field.isRecordLink = (fld === keyField && stateless === false);
                field.label = recordFields.filter( f => {return f.value == fld})[0].label;
                field.fieldName = fld;
                if(appliedFilters && appliedFilters.length > 0 && filterText && filterText.trim() && appliedFilters.includes(fld)){
                    
                    //Now write logic to bold the selected text
                    let fieldValue = '' + rcd[fld];
                    let fieldValue_lowerCase = fieldValue.toLowerCase();
                    let filterText_lowerCase = filterText.toLowerCase();
                    let startIndex = fieldValue_lowerCase.indexOf(filterText_lowerCase);
                    let endIndex = startIndex + filterText_lowerCase.length;

                    // console.log('prepareDataToDisplay > StartIndex', startIndex);
                    // console.log('prepareDataToDisplay > endIndex', endIndex);
                    // console.log('prepareDataToDisplay > fieldValue.length', fieldValue.length);

                    if(startIndex != null && startIndex != undefined && startIndex != -1 && endIndex != null && endIndex != undefined && endIndex <= fieldValue.length){
                        field.value = '';
                        if(startIndex != 0){
                            field.value += fieldValue.substring(0, startIndex);
                        }
                        if(startIndex <= endIndex){
                            field.value += '<b style="font-size:16px;">' + fieldValue.substring(startIndex, endIndex)+ '</b>';
                        }
                        if(endIndex <= fieldValue.length-1){
                            field.value += fieldValue.substring(endIndex, fieldValue.length);
                        }
                        // console.log('prepareDataToDisplay > field.value', field.value);
                    } else {
                        field.value = rcd[fld];
                    }
                } else {
                    field.value = rcd[fld];
                }
                record.briefInfo.fields.push(field);
            });
            dataToDisplay.push(record);
        });
        return dataToDisplay;
    } catch(err) {
        console.log('# Error: ', err);
        return null;
    }
}