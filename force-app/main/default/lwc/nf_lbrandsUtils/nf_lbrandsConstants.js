/**
 * @description Contains all contants that are being used by components
 */
export const 

OPEN_SUBTAB_EVENT                       = 'opensubtab',
BACK_EVENT                              = 'back',
OPEN_SUBTAB_EVENT_PARAM                 = 'uniqueId',
EMAIL_STRING                            = 'Email',
CONSOLE_LOG_ERR                         = '# Error: ',

//OrderHistory & OrderDetail
ORDER_KEY_FIELD                         = 'orderNumber',
ORDER_FIELD_SHIP_TO_NAME                = 'shippingToName',
ORDER_FIELD_SHIP_TO_ADDRESS             = 'shippingToAddress_1',
ORDER_ORDERID_STRING                    = 'OrderId',
TEST_TRACK_LINK                         = 'https://victoriassecret.narvar.com/victoriassecret/tracking/DHL?dzip=92037-1936&order_number=W032322762&promise_date=2020-08-12T01%3A08%3A00-04%3A00&service=S&ship_date=2020-08-04T18%3A33%3A32-04%3A00&tracking_numbers=9274890249320867443889',
CCC_TRACKING_URL                        = 'http://webiq.cdn.strategiqcommerce.com/v2/WebIQ/0447-PROD-WEBIQ-44F6-B554-9F4A-E12C3/OrderNbr-{orderNumber}?Manifest=True',

//CART & CART ITEM
CART_ITEM_KEY_FIELD                     = 'itemNumber',
CART_ITEM_NAME_FIELD                    = 'itemName',
CART_ITEM_ITEMID_STRING                 = 'ItemId',

//TOAST
TOAST_VARIANT_INFO                      = 'info',
TOAST_VARIANT_ERROR                     = 'error',
TOAST_VARIANT_SUCCESS                   = 'success',
TOAST_VARIANT_WARNING                   = 'warning',

//MESSAGES
MSG_NO_ORDER_RETRIEVED                  = 'No orders retrieved',
MSG_EMAIL_NOT_FOUND                     = 'Email-Id not found for this record',
MSG_ORDER_RETRIEVED                     = 'Order retrieved',
MSG_ORDERS_RETRIEVED                    = 'Orders retrieved',
MSG_NO_ORDERS_RETRIEVED                 = 'No orders retrieved',
MSG_INVALID_DATA                        = 'Invalid Data',
MSG_ORDER_NOT_FOUND                     = 'Order not found',

//SORT
SORT_ICON_DEFAULT                       = 'utility:sort',
SORT_ICON_ARROW_UP                      = 'utility:arrowup',
SORT_ICON_ARROW_DOWN                    = 'utility:arrowdown',
SORT_DIR_ASC                            = 'asc',
SORT_DIR_DESC                           = 'desc';