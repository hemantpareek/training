import { LightningElement, api} from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import * as utils from 'c/nf_lbrandsUtils';
import getOrderDetail from '@salesforce/apex/NF_APIUtil.getOrderDetail';

export default class OrderDetail extends LightningElement {

    @api orderNumber; //Contains Order Number which is retrieved in url params
    @api stateless;   //Sets to true when component does not run in a record context
    @api order;       //Used to display data in stateless mode
    showSpinner;      //Decides wheather spinner will be shown or not 
    
    /**
     * @description Fetches order for argumeted order number from server in stateful mode
     */
    connectedCallback(){
        if(this.stateless == false && this.orderNumber){
            this.showSpinner = true;
            getOrderDetail({"email" : '' , "orderId" : this.orderNumber})
            .then(result => {
                console.log('# result: ', result);
                if(result){
                    this.showSpinner = false;
                    let data = JSON.parse(result);
                    if(data && data.length > 0){
                        this.order = utils.getOrder(data[0]);
                    } else {
                        console.log( utils.MSG_ORDER_NOT_FOUND );
                    }
                } else {
                    console.log( utils.MSG_ORDER_NOT_FOUND );
                    this.showSpinner = true;
                }
            })
            .catch(err => {
                utils.showToast(err, TOAST_CONFIG.variant.error, this);
                console.log('# Error: ', err);
                this.showSpinner = false;
            })
        }
    }

    /**
     * @name renderedCallback
     * @description Loads custom styles from static resources.
     */
    renderedCallback(){
        loadStyle(this, utils.LBRANDS_CSS_RESOURCE);
    }

    /**
     * @name back
     * @description Fires custom event to goback to record page.
     */
    back(){
        this.dispatchEvent(new CustomEvent(utils.BACK_EVENT));
    }

    /**
     * @name trackOrder
     * @description Redirects to order track link
     */
    trackOrder(){
        let a = document.createElement('a');
        a.href = utils.TEST_TRACK_LINK;
        a.target = '_blank';
        a.click();
    }
}