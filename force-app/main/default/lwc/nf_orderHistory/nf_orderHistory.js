import { LightningElement, track, api} from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import * as utils from 'c/nf_lbrandsUtils';
import getOrderDetail from '@salesforce/apex/NF_APIUtil.getOrderDetail';
import getEmail from '@salesforce/apex/NF_LbrandsUtility.getEmail';

export default class OrderHistory extends LightningElement {
    
    //Public Attributes
    @api stateless;                   //tells wheather the component is running in stateless/stateful mode
    @api statefulHeight;              //admin can set a value to this from app builder
    @api statefulColumns;             //Admin can set number of colunms for a stateful component
    @api recordId;                    //gets recordId from recordPage passed by parent aura component
    @api briefInfoFields;             //admin can set comma separated fields from app builder to be displayed as summary on cards (stateful only)

    //Private Attributes
    @track orderHistoryRecords;       //holds the data retrived from APIs (orders)
    @track orderHistoryRecordsBackup; //keeps a copy of the orderHistoryRecords list (Will be used to restore data during filter process)
    @track orderFields;               //Order fields with its properties in object form

    showSpinner;                      //boolean variable to show/hide spinner
    emailId;                          //holds emailId entered in text box (stateless only)
    orderId;                          //holds orderId entered in text box (stateless only)
    filterText;                       //holds text entered in the filter search fox
    emailSelected;                    //Sets to true when email is selected as orderParameter at UI
    orderIdSelected;                  //Sets to true when orderId is selected as orderParameter at UI
    sortField;                        //holds the field which is currently sorted the data
    appliedFilterField;               //contains the field which is currently applied as filter
    sortDirection;                    //holds the sort direction either asc or desc
    dynamicStyles;                    //holds dynamic styles for stateless/stateful views

    //Getters
    /**
     * @description Search bar will not display when one or less records retrieved
     */
    get displaySearchBar(){
        return this.orderHistoryRecordsBackup && this.orderHistoryRecordsBackup.length > 1;
    }

    /**
     * @description shows record count in stateful view
     */
    get recordCount(){
        return this.orderHistoryRecords && this.orderHistoryRecords.length > 0 ? this.orderHistoryRecords.length : 0;
    }

    /**
     * @description used to show "No records" message
     */
    get displayNoOrders(){
        return this.stateless &&( !this.orderHistoryRecords || this.orderHistoryRecords.length === 0)
    }

    /**
     * @description Returns label for the filter options.
     */
    get filterLabel(){
        let appliedFilter = this.orderFields ? this.orderFields.filter( ele => ele.isFiltered ) : [];
        return appliedFilter && appliedFilter.length > 0 ? appliedFilter[0].label : '';
    }

    /**
     * @description Returns label for the sort options
     */
    get sortLabel(){
        let sortedField = this.orderFields ? this.orderFields.filter( fld => fld.isSorted ) : [];
        return sortedField && sortedField.length > 0 ? sortedField[0].label : '';
    }

    /**
     * @description Return sortIcon as per the sort direction.
     */
    get sortIcon(){
        let sortedField = this.orderFields ? this.orderFields.filter( fld => fld.isSorted ) : [];
        return (!sortedField || sortedField.length === 0) ? utils.SORT_ICON_DEFAULT : (
            (this.sortDirection == utils.SORT_DIR_ASC) ? utils.SORT_ICON_ARROW_UP : utils.SORT_ICON_ARROW_DOWN
        );
    }

    /**
     * @description When nothing is entered in text box then go button will not be active 
     */
    get disableGoButton(){
        return this.emailSelected ? !this.emailId : !this.orderId;
    }

    /**
     * @description Returns fields to show in sort section
     */
    get sortableFields(){
        let sortableFields = [];
        utils.SORTABLE_ORDER_FIELDS.forEach( sfld => {
            let sortableField = this.orderFields.filter( fld => {return fld.value === sfld;});
            if(sortableField && sortableField.length == 1){
                sortableFields.push(sortableField[0]);
            }
        });
        return sortableFields;
    }

    /**
     * @description Returns fields to show in filter section
     */
    get filterOptions(){
        let filterOptions = [];
        utils.FILTERABLE_ORDER_FIELDS.forEach( ffld => {
            let filterableField = this.orderFields.filter( fld => {return fld.value === ffld;});
            if(filterableField && filterableField.length == 1){
                filterOptions.push(filterableField[0]);
            }
        });
        return filterOptions;
    }

    /**
     * @description Tells if fields has been passed from design attributes or not.
     */
    get hasBriefInfoFields(){
        return (this.stateless === false && this.briefInfoFields && this.briefInfoFields.length > 0) || this.stateless;
    }

    /**
     * @name connectedCallback
     * @description Loads when component inserts to DOM and performs initialization of properties.
     *              Also fetches data from server with the email id of the associated record in stateful case. 
     */
    connectedCallback(){
        this.orderFields = utils.getOrderFields();  
        this.dynamicStyles = utils.getOrderHistoryDynamicStyles(this);

        // this.filterSearchDisable = false;
        this.emailSelected = true;
        this.sortDirection = utils.SORT_DIR_ASC;
        this.appliedFilterField = utils.ORDER_KEY_FIELD;

        if(this.stateless == false && this.recordId){
            this.getEmailForStateful(this.recordId);
        }
    }

    /**
     * @name renderedCallback
     * @description Loads custom styles from static resources.
     */
    renderedCallback(){
        loadStyle(this, utils.LBRANDS_CSS_RESOURCE);
    }

    /**
     * @name getEmailForStateful
     * @param {1} recordId 
     * @description Fetches email Id for the customer that is associated with the recordId (for stateful only).
     */
    getEmailForStateful(recordId){
        getEmail({
            "recordId" : recordId
        })
        .then(result => {
            this.showSpinner = false;
            if(result){
                this.fetchOrdersFromServer( result, null);
            } else {
                console.log(utils.MSG_EMAIL_NOT_FOUND);
            }
        })
        .catch(err=>{
            console.log('# Error: ', err);
            this.showSpinner = false;
            utils.showToast(err, utils.TOAST_VARIANT_ERROR, this);
        })
    }
    
    /**
     * @name searchOrders
     * @description Calls apex method to search orders with appropriate inputs
     */
    searchOrders(){
        if( this.emailSelected && this.emailId ){
            this.fetchOrdersFromServer(this.emailId, null);
        } else if( this.orderIdSelected && this.orderId ){
            this.fetchOrdersFromServer('', this.orderId);
        }
    }

    /**
     * @name applyFilter 
     * @param {1} event
     * @description Applies filter to the clientSide records when selected at UI.
     */
    applyFilter( event ){
        let value = event.target.value;
        // Single Select
        this.orderFields.forEach( ele => ele.isFiltered = ( ele.value == value ) ? true : false );
        this.appliedFilterField = value;
        
        this.filterOrders({
            target : { value : this.appliedFilterField ? this.filterText : '' }
        });
    }

    /**
     * @name filterOrders
     * @param {1} event 
     * @description This method performs the filteration on the client side records with the applied filters
     */
    filterOrders( event ){
       try{
            this.filterText = event.target.value;
            if( this.filterText ){
                let fld = this.appliedFilterField;
                let appliedFilteres = ( fld == utils.ORDER_FIELD_SHIP_TO_NAME ) ? [  fld, utils.ORDER_FIELD_SHIP_TO_ADDRESS ] : [ fld ];
                this.orderHistoryRecords = this.prepareOrdersToDisplay(utils.filterRecords(this.orderHistoryRecordsBackup, this.filterText, appliedFilteres));
            } else {
                this.orderHistoryRecords = this.prepareOrdersToDisplay(this.orderHistoryRecordsBackup);
            }
       }catch(err){
           console.log('# Error: ', err);
       }
    }

    /**
     * @name sort
     * @param {1} event
     * @description Performs sorting for client side records 
     */
    sort(event){
        try{
            let currentField = event.target.value;
            this.orderHistoryRecords = this.prepareOrdersToDisplay(utils.sortRecords(this.orderHistoryRecords, currentField, this, this.orderFields));
        }catch(err){
            console.log('err', err);
        }
    }

    /**
     * @name handleInputValue
     * @param {1} event
     * @description Updates emailId and OrderId when input is taken in the text boxes.
     */
    handleInputValue(event){
        this[ event.target.dataset.input ] = event.target.value;
    }

    /**
     * @name fetchOrdersFromServer
     * @param {1} emailId 
     * @param {2} orderId
     * @description Fetches corresponding orders from server with the emailId or OrderId accordingly. 
     */
    fetchOrdersFromServer(emailId, orderId){
        this.showSpinner = true;
        getOrderDetail({"email" : emailId , "orderId" : orderId})
        .then(result => {
            console.log('# result: ', result);
            if(result){
                this.showSpinner = false;
                let data = JSON.parse(result);
                if(data && data.length > 0){
                    let orderHistoryRecords = [];
                    data.forEach( oh => {
                        orderHistoryRecords.push(utils.getOrder(oh));
                    });
                    this.orderHistoryRecordsBackup = JSON.parse(JSON.stringify(orderHistoryRecords)); //It will not contain brief data
                    this.orderHistoryRecords = this.prepareOrdersToDisplay(orderHistoryRecords); //it will contain brief data
                } else {
                    console.log( utils.MSG_NO_ORDERS_RETRIEVED );
                }
            }else {
                this.orderHistoryRecords = [];
                this.orderHistoryRecordsBackup = [];
                this.showSpinner = false;
                console.log( utils.MSG_NO_ORDERS_RETRIEVED );
            }
        })
        .catch(err=>{
            console.log('# Error: ', err);
            this.showSpinner = false;
            utils.showToast( err, utils.TOAST_VARIANT_ERROR, this );
        })
    }

    /**
     * @name prepareOrdersToDisplay
     * @param {1} records 
     * @description Attachs briefInfo to actual order data to be used to displayed at UI.
     */
    prepareOrdersToDisplay(records){
        let fieldsToDisplay = this.stateless ? utils.ORDER_SUMMARY_FIELDS_STATELESS_MODE : (this.briefInfoFields ? this.briefInfoFields.split(",") : []);
        let appliedFilters = this.appliedFilterField ? (this.appliedFilterField === utils.ORDER_FIELD_SHIP_TO_NAME ? [utils.ORDER_FIELD_SHIP_TO_NAME, utils.ORDER_FIELD_SHIP_TO_ADDRESS] : [this.appliedFilterField]) : [];
        return utils.prepareDataToDisplay(records, fieldsToDisplay, this.orderFields, utils.ORDER_KEY_FIELD, this.filterText, appliedFilters, this.stateless);
    }

    /**
     * @name openOrder
     * @param {1} event
     * @description Opens an order when user clicks on link in stateful mode 
     */
    openOrder(event){
        try{
            if(this.stateless == false){
                event.preventDefault(); 
                let orderNumber = event.target.dataset.ordernumber;
                this.dispatchEvent(new CustomEvent(utils.OPEN_SUBTAB_EVENT, { detail: {
                    "uniqueId" : orderNumber
                }}));
            }
        }catch(err){
            console.log('# Error: ', err);
        }
    }

    /**
     * @name selectFilter
     * @param {1} event 
     * @description Selects parameter that should be proceed to fetch order according to radio selection.
     */
    selectFilter( event ){
        let value = event.target.value;
        if( value == utils.EMAIL_STRING ){
            this.emailSelected   = true;
            this.orderIdSelected = false;
        } else if( value == utils.ORDER_ORDERID_STRING ){
            this.emailSelected   = false;
            this.orderIdSelected = true;
        }
    }
}