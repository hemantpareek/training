import { LightningElement, track, wire} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { deleteRecord } from 'lightning/uiRecordApi';
import getObjects from '@salesforce/apex/lwcPaginationController.getObjects';
import getFields from '@salesforce/apex/lwcPaginationController.getFields';
import getRecords from '@salesforce/apex/lwcPaginationController.getRecords';
import deleteRecords from '@salesforce/apex/lwcPaginationController.deleteRecords';
import createRecords from '@salesforce/apex/lwcPaginationController.createRecords';

export default class LwcPagination extends LightningElement {

    //ATTRIBUTES 
    defaultValueMap;
    objectList;
    fieldList;
    selectedFields;
    selectedObject;
    showSpinner;
    paginationMode;
    totalRecords;
    totalPages;
    pageNumber;
    showFieldsModal;
    showSchemaFilterModal;
    showRecordEditFormModal;
    showAttachFileModal;
    showRecordsFilterModal;
    @track columns;
    @track data; //@track bcz after sorting, chnages not reflecting 
    pageSize;
    sortField;
    sortDirection;
    @track records;
    firstRecordId;
    lastRecordSetLastRecordId;
    selectedRecordIds;
    recordIdOfCurrentRecord;
    editFormMode;
    objectFilterValue;
    fieldFilterValue;
    xDirectionShift;
    whereClause;
    filterRecordValue;

    //GETTERS
    get notHasPrevious(){
        return this.pageNumber == 1;
    }

    get notHasNext(){
        if(this.totalPages == 1 || this.pageNumber == this.totalPages){
            return true;
        }else{
            return false;
        }
    }
    
    get hasNoObject(){
        return this.selectedObject == '--None--';
    }

    get hasNoRecords(){
        return this.data.length == 0;
    }

    get hasNoColumns(){
        return this.columns.length == 0;  
    }

    get editFormLabel(){
        return this.editFormMode + ' ' + this.selectedObject;
    }

    get availablePageNumbers(){
        var availablePageNumbers = [];
        for(var i=1; i<=this.totalPages; i++){
            availablePageNumbers.push({label : i, value : i});
        }
        return availablePageNumbers;
    }

    get paginationModes(){
        return [
            {label: 'Server Side', value:'SERVER'},
            {label: 'Client Side', value:'CLIENT'},
            {label: 'Lazy Loading', value:'LAZYLOAD'}
        ];
    }

    get availablePageSizes(){
        return [
            {label:5, value:5},
            {label:10, value:10},
            {label:25, value:25},
            {label:100, value:100},
            {label:200, value:200},
        ];
    }

    get isAsc(){
        return this.sortDirection == 'ASC';
    }

    get isLazyLoad(){
        return this.paginationMode == 'LAZYLOAD';
    }

    get selectedCount(){
        return this.selectedRecordIds.length;
    }

    get lazyLoadRecordsSize(){
        return this.data.length;
    }

    get masterCheckBoxState(){  
        //If all current page data has its id in selectedRecordIds set
        if(this.selectedRecordIds.length == 0){
            return false;
        }
        var records = this.data;
        for(var i=0; i<records.length; i++){
            if(!this.selectedRecordIds.includes(records[i].recordId)){
                return false;
            }
        }
        return true;
    }

    get hasNoSelectedRecords(){
        return this.selectedRecordIds.length == 0;
    }

    get editFormFields(){
        return this.selectedFields;
    }

    get objectFilteroptions(){
        return [
            {label : 'isAccessible', value : 'isAccessible'},
            {label : 'isCreateable', value : 'isCreateable'},
            {label : 'isCustom', value : 'isCustom'},
            {label : 'isCustomSetting', value : 'isCustomSetting'},
            {label : 'isDeletable', value : 'isDeletable'},
            {label : 'isDeprecatedAndHidden', value : 'isDeprecatedAndHidden'},
            {label : 'isFeedEnabled', value : 'isFeedEnabled'},
            {label : 'isMergeable', value : 'isMergeable'},
            {label : 'isMruEnabled', value : 'isMruEnabled'},
            {label : 'isQueryable', value : 'isQueryable'},
            {label : 'isSearchable', value : 'isSearchable'},
            {label : 'isUndeletable', value : 'isUndeletable'},
            {label : 'isUpdateable', value : 'isUpdateable'}
        ];
    }

    get fieldFilteroptions(){
        return [
            {label : 'isAccessible', value : 'isAccessible'},
            {label : 'isAiPredictionField', value : 'isAiPredictionField'},
            {label : 'isAutoNumber', value : 'isAutoNumber'},
            {label : 'isCalculated', value : 'isCalculated'},
            {label : 'isCascadeDelete', value : 'isCascadeDelete'},
            {label : 'isCaseSensitive', value : 'isCaseSensitive'},
            {label : 'isCreateable', value : 'isCreateable'},
            {label : 'isCustom', value : 'isCustom'},
            {label : 'isDefaultedOnCreate', value : 'isDefaultedOnCreate'},
            {label : 'isDependentPicklist', value : 'isDependentPicklist'},
            {label : 'isDeprecatedAndHidden', value : 'isDeprecatedAndHidden'},
            {label : 'isExternalID', value : 'isExternalID'},
            {label : 'isFilterable', value : 'isFilterable'},
            {label : 'isFormulaTreatNullNumberAsZero', value : 'isFormulaTreatNullNumberAsZero'},
            {label : 'isGroupable', value : 'isGroupable'},
            {label : 'isHtmlFormatted', value : 'isHtmlFormatted'},
            {label : 'isIdLookup', value : 'isIdLookup'},
            {label : 'isNameField', value : 'isNameField'},
            {label : 'isNamePointing', value : 'isNamePointing'},
            {label : 'isNillable', value : 'isNillable'},
            {label : 'isPermissionable', value : 'isPermissionable'},
            {label : 'isRestrictedDelete', value : 'isRestrictedDelete'},
            {label : 'isRestrictedPicklist', value : 'isRestrictedPicklist'},
            {label : 'isSearchPrefilterable', value : 'isSearchPrefilterable'},
            {label : 'isSortable', value : 'isSortable'},
            {label : 'isUnique', value : 'isUnique'},
            {label : 'isUpdateable', value : 'isUpdateable'},
            {label : 'isWriteRequiresMasterRead', value : 'isWriteRequiresMasterRead'}
        ];
    }

    get listViewContainerStyle(){
        return 'overflow-x: hidden; height:' + 33 + 'rem;'; //The 33 will be changed with the dynamic height of window
    }

    get checkboxHeaderStyle(){
        return 'transform: translate3d(' + (32 - this.xDirectionShift) + 'px, 0px, 0px);';
    }

    get headerStyle(){
        return 'width: 20rem; transform: translate3d(' + (24 - this.xDirectionShift) + 'px, 0px, 0px);';
    }

    //CONSTRUCTOR
    constructor() {
        super();
        this.init();
    }

    //1. INITIALIZES THE COMPONENT
    init(){
        this.prepareDefaultValueMap();
        this.initializeAllAttributes();
        this.getObjectListFromServer(); 
    }
    
    //2. PREPARES MAP TO HOLD DEFAULT VALUES OF ALL ATTRIBUTES
    prepareDefaultValueMap(){
        this.defaultValueMap = new Map();
        this.defaultValueMap.set('objectList', JSON.stringify([{label:'--None--', value:'--None--'}]));
        this.defaultValueMap.set('fieldList', JSON.stringify([]));
        this.defaultValueMap.set('selectedFields', JSON.stringify([]));
        this.defaultValueMap.set('columns', JSON.stringify([]));
        this.defaultValueMap.set('data', JSON.stringify([]));
        this.defaultValueMap.set('records', JSON.stringify([]));
        this.defaultValueMap.set('selectedRecordIds', JSON.stringify([]));
        this.defaultValueMap.set('objectFilterValue', JSON.stringify(['isCreateable','isDeletable','isUpdateable']));
        this.defaultValueMap.set('fieldFilterValue', JSON.stringify(['isAccessible', 'isSortable']));
        this.defaultValueMap.set('selectedObject', JSON.stringify('--None--'));
        this.defaultValueMap.set('paginationMode', JSON.stringify('SERVER'));
        this.defaultValueMap.set('totalRecords', JSON.stringify(0));
        this.defaultValueMap.set('totalPages', JSON.stringify(1));
        this.defaultValueMap.set('pageNumber', JSON.stringify(1));
        this.defaultValueMap.set('pageSize', JSON.stringify(10));
        this.defaultValueMap.set('sortField', JSON.stringify(''));
        this.defaultValueMap.set('sortDirection', JSON.stringify('ASC'));
        this.defaultValueMap.set('lastRecordSetLastRecordId', JSON.stringify(null));
        this.defaultValueMap.set('recordIdOfCurrentRecord', JSON.stringify(null));
        this.defaultValueMap.set('editFormMode', JSON.stringify(null));
        this.defaultValueMap.set('showSpinner', JSON.stringify(false));
        this.defaultValueMap.set('showFieldsModal', JSON.stringify(false));
        this.defaultValueMap.set('showSchemaFilterModal', JSON.stringify(false));
        this.defaultValueMap.set('showRecordEditFormModal', JSON.stringify(false));
        this.defaultValueMap.set('showAttachFileModal', JSON.stringify(false));
        this.defaultValueMap.set('showRecordsFilterModal', JSON.stringify(false));
        this.defaultValueMap.set('xDirectionShift', JSON.stringify(0));
        this.defaultValueMap.set('whereClause', JSON.stringify(''));
        this.defaultValueMap.set('filterRecordValue', JSON.stringify({
            conditionList : [{serial: 1, isFirst: true, field:{}, notHasField : true, notHasOperator : true }],
            selectedLogicalOperator : 'AND',
            customLogic : ''
        }));
    }

    //3. INITIALIZES ALL ATTRIBUTES WITH THEIR DEFAULT VALUES
    initializeAllAttributes(){
        for (let attr of this.defaultValueMap.keys()) {
            this[attr] = JSON.parse(this.defaultValueMap.get(attr));
        }
    }

    //4. RESETS ARGUMENTED ATTRIBUTES TO THEIR DEFAULT VALUES
    resetAttributes(attributes){
        if(attributes){
            for (let attr of attributes.split(',')) {
                if(this.defaultValueMap.has(attr)){
                    this[attr] = JSON.parse(this.defaultValueMap.get(attr));
                }
            }
        }
    }

    //5. RESETS ATTRIBUTES OTHER THAN ARGUMENTED ATTRIBUTES TO THEIR DEFAULT VALUES
    resetAttributesExcept(attributes){
        if(attributes){
            var attrToExcept = attributes.split(',');
            for (let attr of this.defaultValueMap.keys()) {
                if(!attrToExcept.includes(attr) && this.defaultValueMap.has(attr)){
                    this[attr] = JSON.parse(this.defaultValueMap.get(attr));
                }
            }
        }
    }

    //6. FETCHES OBJECT LIST FROM SCHEMA CLASS
    getObjectListFromServer(){
        this.showSpinner = true;
        getObjects({'filters' : this.objectFilterValue})
            .then(result => {
                this.objectList = this.objectList.concat(result);
                this.showSpinner = false;
            })
            .catch(error => {
                this.showToast('Error!', error.body.message || error.message, 'error');
                this.showSpinner = false;
            });
    }

    //7. FETCHES FIELD LIST FROM SCHEMA CLASS
    getFieldsListFromServer(){
        this.showSpinner = true;
        getFields({objectName : this.selectedObject, filters : this.fieldFilterValue})
            .then(result => {
                this.fieldList = result;
                // console.log(result);
                var requiredFields = this.fieldList.filter( fld => { return fld.isRequired;});
                requiredFields.forEach( fld => {
                    this.selectedFields.push(fld.value);
                });
                this.prepareColumns();
                this.showSpinner = false;
            })
            .catch(error => {
                this.showToast('Error!', error.body.message || error.message, 'error');
                this.showSpinner = false;
            });
    }

    //8. PASSES QUERY PARAMETERS TO APEX CLASS AND FETCHES RECORDS
    getRecordsFromServer(whereClause, orderByClause, limit, offset){
        this.showSpinner = true;
        getRecords({
            'sobjectName' : this.selectedObject,
            'fields' : this.selectedFields.concat(this.getRelationshipFields()),
            'whereClause' : whereClause,
            'orderByClause' : orderByClause,
            'lmt' : limit,
            'offset' : offset
            })
            .then(result => {
                console.log(result);
                if(this.paginationMode == 'LAZYLOAD'){
                    this.totalRecords = this.data.length + result.totalSize;
                } else {
                    this.totalRecords = result.totalSize;
                }
                this.calculateTotalPages();
                if(result.data && result.data.length > 0){
                    if(this.paginationMode == 'SERVER'){
                        this.firstRecordId = result.data[0].Id;
                        this.lastRecordSetLastRecordId = result.data[result.data.length - 1].Id;
                        this.data = this.prepareData(result.data);
                        this.highLightCheckedRows();
                    }else if(this.paginationMode == 'CLIENT'){
                        this.records = result.data;
                        this.data = this.prepareData(this.records.slice(0, this.pageSize));
                    }else{ //LAZYLOAD
                        this.lastRecordSetLastRecordId = result.data[result.data.length - 1].Id;
                        this.data = this.data.concat(this.prepareData(result.data));
                        if(this.sortField){
                            this.sortDataWrapper();
                        }
                        this.highLightCheckedRows();
                    }
                } else {
                    this.showToast('', 'No Records!', 'info');
                }
                this.showSpinner = false;
            })
            .catch(error => {
                this.showToast('Error!', error.message || error.body.message , 'error');
                this.showSpinner = false;
            });
    }

    //9. HITS APEX TO GET FIELDS WHEN A OBJECT CHANGE IS PERFORMED
    handleObjectChange(event){
        this.resetAttributesExcept('objectList,paginationMode,objectFilterValue');
        this.selectedObject = event.target.value;
        if(this.selectedObject == '--None--'){
            return;
        }
        this.getFieldsListFromServer();
    }

    //10. PREPARES COLUMNS FROM THE SELECTED FIELDS AT CLIENT SIDE
    handleFieldsChange(event){
        this.resetAttributesExcept('objectList,paginationMode,fieldList,selectedObject,showFieldsModal,objectFilterValue,fieldFilterValue');
        this.selectedFields = event.target.value;
        this.prepareColumns();
    }

    //11. PREPARES COLUMNS FOR THE SELECTED FIELDS
    prepareColumns(){
        this.resetAttributes('columns,data');
        this.selectedFields.forEach( selFld => {
            var col = this.fieldList.filter( fld => { return selFld == fld.value})[0];
            this.columns.push(JSON.parse(JSON.stringify(col)));
        });
    }

    //12. RETRIVES THE INITAL RECORD SET 
    process(event){
        this.resetAttributes('data,records,selectedRecordIds');
        if (this.paginationMode == 'SERVER'){
            this.pageNumber = 1;
            this.refreshPage();
        } else if (this.paginationMode == 'CLIENT'){
            //Get All Records
            this.getRecordsFromServer(this.whereClause, 'Id ASC' , 49999, 0);
        } else { //Lazyload
            this.getRecordsFromServer(this.whereClause, 'Id ASC' , 20, 0);    
        }
    }

    //13. REFRESHES THE CURRENT PAGE RECORD SET
    refreshPage(){
        if(this.paginationMode == 'SERVER'){
            this.getRecordsFromServer(this.whereClause, this.sortField ? this.sortField + ' ' + this.sortDirection : '', this.pageSize, (this.pageNumber - 1) * this.pageSize); 
        } else if(this.paginationMode == 'CLIENT'){
            this.data = this.prepareData(this.records.slice( ((this.pageNumber - 1) * this.pageSize), ((this.pageNumber) * this.pageSize) ));
            this.highLightCheckedRows();
        } else {}//LAZYLOAD
    }
    
    //14. RESET ATTRIBUTES AND CALLS THE PROCESS() WITH NEW MODE
    handleModeChange(event){
        this.paginationMode = event.target.value;
        this.resetAttributes('totalRecords,totalPages,pageNumber,columns,pageSize,data,reords,sortField,sortDirection,selectedRecordIds');
        if(this.selectedFields.length > 0){
            this.prepareColumns();
            this.process();
        }
    }

    //15. PERFORMS NAVIGATIONS WITH SELECTED DIRECTION
    navigate(event){
        switch(event.target.name) {
            case 'first'   : this.pageNumber  = 1 ; break;
            case 'previous': this.pageNumber -= 1 ; break;
            case 'next'    : this.pageNumber += 1 ; break;
            case 'last'    : this.pageNumber = this.totalPages ; break;
        }
        this.refreshPage();
    }

    //16. IMPLEMENTS SORTING W.R.T. SELECTED MODE
    handleSort(event){
        this.sortDirection = this.sortField == event.target.name ? (this.sortDirection == 'ASC' ? 'DESC' : 'ASC') : 'ASC';
        this.sortField = event.target.name;
        //Set column as sorted
        this.columns.forEach( col => {
            if(col.value == this.sortField){
                col.isSorted = true;
            }else{
                col.isSorted = false;
            }
        });
        if(this.paginationMode == 'SERVER'){
            this.refreshPage();
        }else if(this.paginationMode == 'LAZYLOAD'){
            this.sortDataWrapper();
        }else{
            this.sortClientSideRecords();
            this.refreshPage();
        }
    }

    //17. SORTS RECORDS AT CLIENT SIDE
    sortClientSideRecords() {
        var key = this.sortField;
        var dir = this.sortDirection;
        if (key) {
          this.records.sort(function (a, b) {
            var x = a[key] ? a[key] : '';
            var y = b[key] ? b[key] : '';
            x = x.toLowerCase();
            y = y.toLowerCase();
            if((x<y && dir == 'ASC') || (x>y && dir == 'DESC')){
                return -1;
            }
            return 0;
          });
        }
    }

    //18. SORT DATA WRAPPER LIST
    sortDataWrapper(){
        var dir = this.sortDirection;
        var sortField = this.sortField;
        this.data.sort(function (row1, row2) {
            var x = row1.cells.filter( cell => {return cell.fieldName === sortField;})[0].fieldValue;
            var y = row2.cells.filter( cell => {return cell.fieldName === sortField;})[0].fieldValue;
            x = x ? x.toLowerCase() : '';
            y = y ? y.toLowerCase() : '';
            if((x<y && dir == 'ASC') || (x>y && dir == 'DESC')){
                return -1;
            }
            return 0;
        });
    }

    //19. CALCULATES TOTAL PAGES WITH TOTAL RECORDS AND PAGE SIZE
    calculateTotalPages(){
        this.totalPages = this.totalRecords / this.pageSize;
        if(Math.ceil(this.totalPages) != Math.floor(this.totalPages) || this.totalPages == 0){
            this.totalPages = parseInt(this.totalPages + 1);
        }
    }

    //20. PREPARES DATA WRPPAER W.R.T. SELECTED MODES
    prepareData(records){
        var data = [];
        records.forEach( record => {
            var row = {};
            row.cells = [];
            row.recordId = record.Id;
            row.isSelected = this.selectedRecordIds.includes(record.Id) ? true : false;
            this.columns.forEach( col => {
                var cell = {};
                cell.fieldName = col.value;
                cell.fieldValue = record[col.value];
                if(col.isNameField){
                    cell.isNameField = true;
                    cell.recordUrl = '/one/one.app?#/sObject/' + record.Id + '/view';
                } else if (cell.fieldValue && col.isReference && col.relationshipName && col.parentNameField && record[col.relationshipName] && record[col.relationshipName][col.parentNameField]){
                    cell.isReference = true;
                    cell.relationshipName = record[col.relationshipName][col.parentNameField];
                    cell.referenceValue = cell.fieldValue ? '/one/one.app?#/sObject/' + cell.fieldValue + '/view' : '#href';
                } else if(cell.fieldValue && col.isUrl){
                    cell.isUrl = true;
                }else{
                    cell.isText = true;
                }
                row.cells.push(cell);
            });
            data.push(row);
        });
        return data;
    }

    //23. FIRES TOAST MESSAGE WITH ARGUMENTED MESSAGE AND VARIANTS
    showToast(title, message, variant){
        const evt = new ShowToastEvent({
            'title': title,
            'message': message,
            'variant': variant,
            'mode': 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    //24. FETHES FIRST RECORD SET WITH NEW PAGE SIZE
    handlePageSizeChange(event){
        this.pageSize = parseInt(event.target.value);
        this.calculateTotalPages();
        this.pageNumber = 1;
        this.refreshPage();
    }

    //25. NAVIGATES TO TARGET PAGE
    handlePageNumberChange(event){
        this.pageNumber = parseInt(event.target.value);
        this.refreshPage();
    }

    //26. MOVES TABLE HEADER ACCORDING TO SCROLLBAR & LOADS MORE RECORDS FROM LAZYLOAD
    handleOnScroll(event){
        this.xDirectionShift = parseInt(event.target.scrollLeft);
        if(this.paginationMode == 'LAZYLOAD' && this.data.length != this.totalRecords){
            var scrollTop = event.target.scrollTop;
            var clientHeight = event.target.clientHeight;
            var scrollHeight = event.target.scrollHeight;
            if(clientHeight + (Math.round(scrollTop)) == scrollHeight){
                this.getRecordsFromServer(this.whereClause ? this.whereClause + ' AND Id > \'' + this.lastRecordSetLastRecordId + '\'' : 'Id > \'' + this.lastRecordSetLastRecordId + '\'', 'Id ASC', this.pageSize, 0);
            }
        }
    }

    //27. ADDS OR REMOVES SELECTED RECORDIDS FROM THE SELECTEDRECORDIDS LIST
    checkUncheckSingle(event){
        var checked = event.target.checked;
        var recordId = event.target.dataset.id;
        this.data.filter( rcd => {return rcd.recordId == recordId; })[0].isSelected = checked;
        if(checked && !this.selectedRecordIds.includes(recordId)){
            this.selectedRecordIds.push(recordId);
        }else if(this.selectedRecordIds.includes(recordId)){
            this.selectedRecordIds = this.selectedRecordIds.filter( rcdId => { return rcdId != recordId;});
        }
        this.highLightCheckedRows();
    }

    //28. ADDS OR REMOVES (ALL RECORDIDS ON CURRENT PAGE) FROM THE SELECTEDRECORDIDS LIST
    checkUncheckAll(event){
        var checked = event.target.checked;
        this.data.forEach( rcd => {
            rcd.isSelected = checked;
            if(checked){
                this.selectedRecordIds.push(rcd.recordId);
            }else{
                this.selectedRecordIds = this.selectedRecordIds.filter( rcdId => { return rcdId != rcd.recordId;});
            }
        });
        this.highLightCheckedRows();
    }

    uncheckAllFromCurrentPage(){
        this.data.forEach( rcd => {
            rcd.isSelected = false;
        });
        this.highLightCheckedRows();
    }

    highLightCheckedRows(){
        var self = this;
        setTimeout(function(){
                var allRows = self.template.querySelectorAll('tr');
                allRows.forEach(row => {
                    if (row.dataset.id && self.selectedRecordIds.includes(row.dataset.id)) {
                    row.style.backgroundColor = 'lightblue';
                    } else {
                    row.style.backgroundColor = 'unset';
                    }
                });
        },100);
    }

    //29. MOVES ALL AVAILABLE FIELDS TO SELECTED LIST
    addAllFields(event){
        this.resetAttributes('selectedFields');
        this.fieldList.forEach( fld =>{
            this.selectedFields.push(fld.value);
        });
        this.prepareColumns();
    }
    
    //30. MOVES ALL SELECTED FIELDS TO AVAILABLE LIST
    removeAllFields(event){
        this.selectedFields = [];
        this.prepareColumns();
    }

    //31. DELETES SINGLE RECORD
    deleteRecord(event){
        this.showSpinner = true;
        var recordId = event.target.dataset.id;
        deleteRecord(recordId)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Record deleted',
                        variant: 'success'
                    })
                );
                this.removeRecordsFromClientSide([recordId]);
                this.refreshPage();
                this.showSpinner = false;
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error deleting record',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
                this.showSpinner = false;
            });
    }

    //32. DELETES SELECTED RECORDS
    deleteSelected(event){
        this.showSpinner = true;
        deleteRecords({
            'recordIds' : this.selectedRecordIds
            })
            .then(result => {
                this.showToast('', result.message , result.state);
                this.resetAttributes('selectedRecordIds,showSpinner');
                this.uncheckAllFromCurrentPage();
                this.removeRecordsFromClientSide(result.succeedIds);
                this.refreshPage();
            })
            .catch(error => {
                this.showToast('Error!', error.message || error.body.message , 'error');
                this.showSpinner = false;
            });
    }

    //33. REMOVES DELETED RECORDS FROM CLIENT SIDE
    removeRecordsFromClientSide(deletedRecordIds){
        this.data = this.data.filter( rcd => {return !deletedRecordIds.includes(rcd.recordId);});
        if(this.paginationMode == 'CLIENT'){
            this.totalRecords -= 1;
            this.records = this.records.filter( rcd => {return !deletedRecordIds.includes(rcd.Id);});
        }
    }

    //34. PASSES SELECTED RECORD IDS TO APEX AND DOWNLOADS CSV OF RETRIEVED RECORDS
    downloadCsv(event){
        this.showSpinner = true;
        getRecords({
            'sobjectName' : this.selectedObject,
            'fields' : this.selectedFields,
            'whereClause' : 'Id IN (\'' + this.selectedRecordIds.join('\',\'') + '\')',
            'orderByClause' : this.sortField ? this.sortField + ' ' + this.sortDirection : '',
            'lmt' : '49999',
            'offset' : 0
            })
            .then(result => {
                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(this.prepareCsv(result.data));
                hiddenElement.download = 'ExportData.csv';  
                hiddenElement.click();
                this.resetAttributes('selectedRecordIds,showSpinner');
                this.uncheckAllFromCurrentPage();
            })
            .catch(error => {
                this.showToast('Error!', error.message || error.body.message , 'error');
                this.showSpinner = false;
            });
    }

    //35. PREPARES CSV STRING FROM THE SOBJECT RECORDS
    prepareCsv(records){
        var generatedCSVFile ='';
        var colums = '';
        this.selectedFields.forEach( field =>{
            colums += '\"'+field+'\"'+',';
        });
        colums = colums.slice(0, -1); //removes last char
        colums +='\n';
        generatedCSVFile += colums;
        records.forEach( rcd =>{
            var row = '';
            this.selectedFields.forEach( field =>{
                row += '\"';
                row += rcd[field] != null ? rcd[field] : '' ;
                row += '\"'+',';
            });
            row = row.slice(0, -1);
            row += '\n';
            generatedCSVFile += row;
        });
        return generatedCSVFile;
    }
    
    //36. READS CSV FILE AND PASSES IT TO CONVERT IT TO JSON
    uploadCsv(event) {
        var uploadedFile = event.target.files[0];
        this.template.querySelector('input').value = null; //TO WORK ONCHANGE EVENT PROPERLY
        var fileSize = '' + uploadedFile.size / 1024;
        fileSize = fileSize.substring(0, fileSize.indexOf('.') + 3);
        if (uploadedFile.size > 1 * 1024 * 1024) {
          this.showToast('', 'File Size is greater than 1 MB: ' + ' (' + fileSize + ' KB)', 'error');
          return;
        } 
        var self = this;
        var reader = new FileReader();
        reader.onloadend = function(e){
            self.createRecordsFromCsv(reader.result);
        };
        reader.readAsText(uploadedFile);
    }

    //37. CREATES RECORDS AT APEX SIDE WITH THE CSV'S CONVERTED JSON
    createRecordsFromCsv(csv){
        this.showToast('', 'Processing in background' , 'info');
        createRecords({
            'objectName' : this.selectedObject,
            'recordsJsonArray' : this.createJsonFromCsv(csv)
            })
            .then(result => {
                this.showToast('', result.message , result.state);
            })
            .catch(error => {
                this.showToast('Error!', error.message || error.body.message , 'error');
            });
    }

    //38. CREATES LIST<JSON OBJECTS> TO BE PARSED AT APEX SIDE
    createJsonFromCsv(csv){
        // console.log(csv);
        var rows = csv.split("\n").filter( row => { return row != '' && row.trim() != '' && row != '\n';});
        var fields = rows[0].split(",");
        var records = [];   
        for(var i=1 ; i<rows.length; i++){
            var cells = this.getCells(rows[i]);
            var record = {};
            for(var j=0; j<fields.length; j++){
                record[fields[j]] = cells[j];   
            }
            records.push(JSON.stringify(record).replace(/\\\"/g, "").replace(/\\r/g, ""));
        }
        // console.log(records);
        return records;
    }

    //39. CREATES CELLS FOR CSV ROWS
    getCells(row){
        var cells = [];
        var flag = false;
        var cell = '';
        for(var i=0 ; i<row.length ; i++){
            if(row[i] == '"'){
                flag = flag == true ? false : true ;
            }else if(row[i] == ','){
                if(flag == true){
                    cell += row[i];
                }else{
                    cells.push(cell);
                    cell = '';
                }
            }else{
                cell += row[i];
            }
        }
        cells.push(cell);
        return cells;
    }

    //40. THIS METHOD FIXES THE ISSUE OF LIGHTNING-MENU-ITEM ALLOWS CLICK EVEN AFTER DISABLING.
    updateLtngMenuItemStyle(){
        var menuItems = this.template.querySelectorAll('lightning-menu-item');
        menuItems.forEach( mi => {
            mi.style.userSelect = 'none';
            if(mi.disabled){
                mi.style.pointerEvents = 'none';
            }else{
                mi.style.pointerEvents = 'auto';
            }
        });
    }

    //41. OPENS SELECT FIELDS MODAL
    openSelectFieldsModal(event){
        this.showFieldsModal = true;
    }

    //42. CLOSES SELECT FIELDS MODAL
    closeSelectFieldsModal(event){
        this.showFieldsModal = false;
    }

    //43. OPENS RECORD EDIT MODAL
    openRecordEditModal(event){
        // console.log(event.target.value);
        switch(event.target.dataset.mode){
            case 'Edit' : this.editFormMode = 'Edit'; this.recordIdOfCurrentRecord = event.target.dataset.id; break;
            case 'Create' : this.editFormMode = 'Create'; this.recordIdOfCurrentRecord = ''; break;
        }
        this.showRecordEditFormModal = true;
    }

    //44. CLOSES RECORD EDIT MODAL
    closeRecordEditModal(event){
        this.showRecordEditFormModal = false;
    }

    //45. HANDLES SUCCESS EVENT FOR RECORD EDIT/CREATE OPERATION
    handleRecordEditSuccess(event){
        this.closeRecordEditModal();
        var message = this.editFormMode == 'Edit' ? 'Record Updated successfully' : 'Record Created successfully: ' + event.detail.id;
        this.showToast('', message ,'success');
        if(this.paginationMode == 'SERVER'){
            this.refreshPage();
        }
    }

    //46. OPENS UPLOAD CSV BOX
    openUploadCsvBox(event){
        this.template.querySelector('input').click();
    }

    //47. OPENS SCHEMA FILTER MODAL
    openSchemaFilterModal(event){
        this.showSchemaFilterModal = true;
    }

    //48. CLOSES SCHEMA FILTER MODAL
    closeSchemaFilterModal(event){
        this.showSchemaFilterModal = false;
    }

    //49. UPDATES OBJECT FILTER ATTRIBUTE WHEN A CHANGE OCCURS
    handleObjectFilterChange(event){
        this.objectFilterValue = event.detail.value;
    }

    //50. UPDATES FIELD FILTER ATTRIBUTE WHEN A CHANGE OCCURS
    handleFieldFilterChange(event){
        this.fieldFilterValue = event.detail.value;
    }

    //51. APPLY FITER TO OBJECT SCHEMA
    applyObjectFilter(event){
        this.resetAttributes('objectList,selectedObject,data,records,fieldList,selectedFields,columns');
        this.getObjectListFromServer();
        this.closeSchemaFilterModal();
    }

    //52. APPLY FILTER TO FIELD SCHEMA
    applyFieldFilter(event){
        this.resetAttributes('data,records,fieldList,selectedFields,columns');
        this.getFieldsListFromServer();
        this.closeSchemaFilterModal();
    }

    //53. OPENS ATTACH FILE MODAL
    openAttachFileModal(event){
        this.showAttachFileModal = true;
        this.recordIdOfCurrentRecord = event.target.dataset.id;
    }

    //54. CLOSES ATTACH FILE MODAL
    closeAttachFileModal(event){
        this.showAttachFileModal = false;
    }

    openRecordsFilterModal(event){
        this.showRecordsFilterModal = true;
    }

    closeRecordsFilterModal(event){
        this.showRecordsFilterModal = false;
    }

    saveRecordsFilter(event){
        // console.log('@@@ save filter');
        console.log(JSON.stringify(event.detail));
        this.filterRecordValue = event.detail.filterValue;
        this.closeRecordsFilterModal();
    }

    applyRecordsFilter(event){
        // console.log('@@@ apply filter');
        // console.log(event.detail);
        this.whereClause = event.detail.whereClause;
        this.filterRecordValue = event.detail.filterValue;
        this.process();
        this.closeRecordsFilterModal();
    }

    removeRecordsFilter(){
        this.resetAttributes('filterRecordValue,whereClause');
        this.process();
        this.closeRecordsFilterModal();
    }

    getRelationshipFields(){
        var relationshipFields = [];
        this.columns.forEach( col => {
            if(col.relationshipName && col.parentNameField){
                relationshipFields.push( col.relationshipName + '.' + col.parentNameField);
            }
        });
        // console.log(relationshipFields);
        return relationshipFields;
    }
}