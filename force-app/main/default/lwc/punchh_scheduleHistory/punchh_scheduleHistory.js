import { LightningElement, api, track } from 'lwc';

export default class Punchh_scheduleHistory extends LightningElement {
    
    @api scheduleId;
    @track historyRecords;

    connectedCallback(){
        this.createSampleData();
    }

    createSampleData(){
        this.historyRecords = [];
        this.historyRecords.push( {jobId: "123,456", jobType: "Bulk Import"          , recordsSynced: "145,563 of 1,116,221", status: "In Progress"} );
        this.historyRecords.push( {jobId: "123,456", jobType: "Bulk Import"          , recordsSynced: "1,053,115"           , status: "Done"       } );
        this.historyRecords.push( {jobId: "123,456", jobType: "Email Campaign"       , recordsSynced: "145,564 of 1,116,221", status: "In Progress"} );
        this.historyRecords.push( {jobId: "123,456", jobType: "Email Campaign"       , recordsSynced: "2,251,119"           , status: "Done"       } );
        this.historyRecords.push( {jobId: "123,456", jobType: "Notification Campaign", recordsSynced: "1,053,115"           , status: "Done"       } );
        this.historyRecords.push( {jobId: "123,456", jobType: "Notification Campaign", recordsSynced: "263 of 1,116,221"    , status: "Failed"     } );
        this.historyRecords.push( {jobId: "123,456", jobType: "Email Campaign"       , recordsSynced: "145,563"             , status: "Done"       } );
        this.historyRecords.push( {jobId: "123,456", jobType: "Email Campaign"       , recordsSynced: "145,563"             , status: "Done"       } );
        this.historyRecords.push( {jobId: "123,456", jobType: "Email Campaign"       , recordsSynced: "145,563"             , status: "Done"       } );
        this.historyRecords = this.historyRecords.concat(this.historyRecords);
        this.historyRecords = this.historyRecords.concat(this.historyRecords);
        this.historyRecords = this.historyRecords.concat(this.historyRecords);
        this.historyRecords = this.historyRecords.concat(this.historyRecords);
        console.log(this.historyRecords.length);
    }
}