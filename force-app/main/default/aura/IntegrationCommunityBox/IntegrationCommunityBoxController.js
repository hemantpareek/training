({
	//1. PERFORMS INITIAL PROCESSING WHEN COMPONENT LOADS
	doInit : function(component, event, helper) {
		var initAction = component.get("c.doInitApex");
        initAction.setCallback(this, function(initResponse) {
			var returnedValue = initResponse.getReturnValue();
			if(helper.checkForNull(component, returnedValue, 'doInit')){
				return;
			}
			var parsedValue = JSON.parse(returnedValue);
			if(parsedValue.status == 'authenticateWithCode'){
				var url = new URL(window.location.href);
				var code = url.searchParams.get("code");
				if(code != null && code != ''){
					helper.authenticateWithCode(component, code);
				}else{
					helper.getCodeForAuthentication(parsedValue.authURI);
				}
			}else if(parsedValue.status == 'getAccessTokenFromRefreshToken'){
				helper.getAccessTokenFromRefreshToken(component, event);
			}else if(parsedValue.status == 'readyToLoadData'){
				helper.fetchDataFirstTime(component, event);
			}
		});
        $A.enqueueAction(initAction);
	},

	//2. HANDLES CHANGE DIRECTORY ACTIONS
	changeDirectory : function(component, event, helper){
		helper.changeDirectory(component, event);
	},

	//3. DOWNLOADS FILE VIA DOWNLOAD LINK PROVIDED BY APEX CONTROLLER
	downloadFile : function(component, event, helper){
		helper.downloadFile(component, event);
	},

	//4. UPLOADS FILE 
	uploadFile : function(component, event, helper){
		helper.uploadFile(component, event);
	},

	//5. PERFORMS BREADCRUMB ACTION
	performBreadCrumbAction : function(component, event, helper){
		helper.performBreadCrumbAction(component, event);
	},

	//6. DELETED THE SELECTED FILE/FOLDER
	deleteFile : function(component, event, helper){
		helper.deleteFile(component, event);
	},

	//7. CREATE FOLDER FUNCTIONALITY
	createFolder : function(component, event, helper){
		helper.createFolder(component, event);
	}
})