({
	//1. HELPER FOR FIRST TIME DATA LOAD FUNCTIONALITY TO JS CONTROLLER
	fetchDataFirstTime : function(component, event) {
		this.showSpinner(component, event);
		var fetchDataFirstTimeAction = component.get("c.doFetchDataFirstTime");
		fetchDataFirstTimeAction.setCallback(this,function(fetchDataResponse){
			var returnedValue = fetchDataResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'fetchDataFirstTime')){
				return;
			}
			this.updateCurrentFolder(component, '', 'Home');
			this.updateCurrentFolderContents(component, event, returnedValue);
			this.addToBreadCrumbList(component, event, {'name':'Home','path_display':'','isLast':'true'});
			this.hideSpinner(component, event);
		});
		$A.enqueueAction(fetchDataFirstTimeAction);
	},

	//3. PROVIDES CODE TO GET ACCESS TOKEN
	getCodeForAuthentication : function(component, event, authURI){
		window.location.href = authURI;
	},

	//4. HELPER FOR GETTING ACCESS TOKEN VIA CODE WHEN REFRESH TOKEN IS NULL OR IT IS FIRST TIME AUTHENTICATION
	authenticateWithCode : function(component, event, code){
		var getAccessTokenAction = component.get("c.doGetAccessToken");
		getAccessTokenAction.setParams({ "code" : code });
		getAccessTokenAction.setCallback(this,function(accessTokenResponse){
			var returnedValue = accessTokenResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'authenticateWithCode')){
				return;
			}
			window.location.reload();
		});
		$A.enqueueAction(getAccessTokenAction);
	},

	//5. HELPER FUNCTION TO PERFORM CHANGE DIRECTORY ACTION
	changeDirectory : function(component, event){
		this.showSpinner(component, event);
		var changeDirectoryAction = component.get("c.doChangeDirectory");
		changeDirectoryAction.setParams({ "path_display" : event.getSource().get("v.name") }); //v.name holds folder id of selected folder
		changeDirectoryAction.setCallback(this,function(changeDirectoryResponse){
			var returnedValue = changeDirectoryResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'changeDirectory')){
				return;
			}
			var btnClicked = event.getSource();
			var folderName = btnClicked.get("v.label");
			var folderPath = btnClicked.get("v.name");
			this.updateCurrentFolder(component, folderPath, folderName);
			this.updateCurrentFolderContents(component, event, returnedValue);
			this.addToBreadCrumbList(component, event, {'name':folderName,'path_display':folderPath,'isLast':true});
			this.hideSpinner(component, event);
		});
		$A.enqueueAction(changeDirectoryAction);
	},
	
	//6. HELPER FUNCTION TO DOWNLOAD FILE
	downloadFile : function(component, event){
		this.showSpinner(component,event);
		var downloadFileAction = component.get("c.doDownloadFile");
		downloadFileAction.setParams({
			"path_display" : event.getSource().get("v.name")
		});
		downloadFileAction.setCallback(this,function(downloadFileResponse){
			var returnedValue = downloadFileResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'downloadFile')){
				return;
			}
			window.location.href = downloadFileResponse.getReturnValue();
			this.hideSpinner(component,event);
		});
		$A.enqueueAction(downloadFileAction);
	},

	//7. FUNCTION TO UPDATE CURRENT FOLDER 
	updateCurrentFolder: function(component, folderPath, folderName){
		var currentFolder = new Object();
		currentFolder.path_display = folderPath;
		currentFolder.name = folderName;
		component.set("v.currentFolder",currentFolder);
	},

	//8. FUNCTION TO UPDATE CURRENT FOLDER CONTENTS
	updateCurrentFolderContents : function(component, event, returnedValue){
		if(this.checkForNull(component, returnedValue, 'updateCurrentFolderContents')){
			return;
		}
		var files = JSON.parse(returnedValue);
		var currentFolderDirectories = [];
		var currentFolderFiles = [];
		files.forEach(function(file){
			if(file.type == 'folder'){
				currentFolderDirectories.push(file);
			}else{
				currentFolderFiles.push(file);
			}
		});
		component.set("v.currentFolderDirectories",currentFolderDirectories);
		component.set("v.currentFolderFiles",currentFolderFiles);
	},

	//9. FUNCTION TO ADD CURRENT FOLDER TO BREADCRUMB LIST
	addToBreadCrumbList : function(component, event, currentFolder){
		var breadCrumbsList = component.get("v.breadCrumbsList");
		if(breadCrumbsList == null){
			breadCrumbsList = [];
		}
		if(breadCrumbsList.findIndex(x => x.path_display === currentFolder.path_display) == -1){ //i.e. list does not contain element
			breadCrumbsList.forEach(function(breadCrumbFolder){
				breadCrumbFolder.isLast = false;
			});
			breadCrumbsList.push(currentFolder);
			component.set("v.breadCrumbsList",breadCrumbsList);
		}
	},

	//10. HELPER FUNCTION TO PERFORM BREADCRUMB ACTION
	performBreadCrumbAction : function(component, event){
		this.showSpinner(component, event);
		var btnClicked = event.getSource();
		var folderPath = btnClicked.get("v.name");
		var breadCrumbsList = component.get("v.breadCrumbsList");
		var selectedFolderIndex = breadCrumbsList.findIndex(x => x.path_display === folderPath); 
		if(selectedFolderIndex != -1){
			var tempList = [];
			for(var i = 0; i <= selectedFolderIndex ; i++){
				tempList.push(breadCrumbsList[i]);
			}
			tempList[tempList.length-1].isLast = true;
			component.set("v.breadCrumbsList",tempList);
		}
		//now fetch data from selected folder in breadcrumbs list
		this.changeDirectory(component, event);
	},

	//11. DELETED THE SELECTED FILE/FOLDER
	deleteFile : function(component, event){
		this.showSpinner(component, event);
		var deleteFileAction = component.get("c.deleteSelectedFile");
		var filePath = event.getSource().get("v.name");
		deleteFileAction.setParams({ "filePath" : filePath }); //v.name holds folder id of selected folder
		deleteFileAction.setCallback(this,function(deleteFileResponse){
			var returnedValue = deleteFileResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'deleteFile')){
				return;
			}
			if(event.getSource().get("v.label") == 'Delete Folder'){
				//remove file from folders list
				var currentFolderDirectories = component.get("v.currentFolderDirectories");
				var deletedElementIndex = currentFolderDirectories.findIndex(x => x.path_display === filePath);
				currentFolderDirectories.splice(deletedElementIndex, 1); //1 is used bcz we want to delete exact 1 element 
				component.set("v.currentFolderDirectories",currentFolderDirectories); 
				this.fireToast('Folder deleted successfully','success');
			}else{
				// remove file from files list
				var currentFolderFiles = component.get("v.currentFolderFiles");
				var deletedElementIndex = currentFolderFiles.findIndex(x => x.path_display === filePath);
				currentFolderFiles.splice(deletedElementIndex, 1);
				component.set("v.currentFolderFiles",currentFolderFiles);
				this.fireToast('file deleted successfully','success');
			}
			this.hideSpinner(component, event);
		});
		$A.enqueueAction(deleteFileAction);
	},

	//13. FUNCTION TO SHOW SPINNER
	showSpinner : function (component, event) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
	},
	
	//14. FUNCTION TO HIDE SPINNER
    hideSpinner : function (component, event) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
	},
	
	//15. FUNCTION TO UPLOAD FILE
	uploadFile : function(component, event){
		this.showSpinner(component, event);
		var files = component.get("v.fileToBeUploaded");
		var file = files[0];
		var reader = new FileReader();
		var self = this; //used bcz function inside function can't use this reference
		reader.onloadend = function(e){
			var dataURL = reader.result;
			var base64Data = dataURL.match(/,(.*)$/)[1]; //encode to base64Data
			self.uploadFileOnServer(component, event, file, base64Data);
		};
		reader.readAsDataURL(file);
	},

	//16. hit server to createfile
	uploadFileOnServer : function(component, event, file, base64Data){
		var fileUploadAction = component.get("c.doUploadFile");
		fileUploadAction.setParams({ 
			"fileName" : file.name,
			"base64Data" : base64Data,
			"contentType" : file.type,
			"path_display" : component.get("v.currentFolder").path_display
		}); //v.name holds folder id of selected folder
		fileUploadAction.setCallback(this,function(fileUploadResponse){
			//create file in directory without hitting api
			var returnedValue = fileUploadResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'uploadFileOnServer')){
				return;
			}
			var parsedValue = JSON.parse(returnedValue);
			var currentFolderFiles = component.get("v.currentFolderFiles");
			if(currentFolderFiles == null){
				currentFolderFiles = [];
			}
			var uploadedFile = {'name' : parsedValue.name, 'path_display' : parsedValue.path_display, 'type' : parsedValue.type};
			currentFolderFiles.push(uploadedFile);
			component.set("v.currentFolderFiles",currentFolderFiles);
			this.fireToast('File '+parsedValue.name+' uploaded successfully', 'success');
			this.hideSpinner(component, event);
		});
		$A.enqueueAction(fileUploadAction);
	},

	//17. fucntion to create folder
	createFolder : function(component, event){
		var folderName = component.find("createFolder").get("v.value");
		var folders = component.get("v.currentFolderDirectories");
		if(folderName == null || folderName == '' || folderName.trim().length == 0){
			this.fireToast("Please Enter a folder Name",'error');
			return;
		}else if(folders.findIndex(x => x.name.toLowerCase() == folderName.toLowerCase()) != -1){
			this.fireToast("This folder name already exists in current directory please enter a different one","error");
			return;
		}
		this.showSpinner(component, event);
		var createFolderAction = component.get("c.doCreateFolder");
		createFolderAction.setParams({
			"folderPath" : component.get("v.currentFolder").path_display + '/' + folderName
		});
		createFolderAction.setCallback(this,function(createFolderResponse){
			var returnedValue = createFolderResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'createFolder')){
				return;
			}
			var newFolder = {};
			newFolder.name = folderName;
			newFolder.path_display = returnedValue;
			folders.push(newFolder);
			component.set("v.currentFolderDirectories",folders);
			this.fireToast('Folder : '+ folderName +' created successfully!','success');
			this.hideSpinner(component, event);
		});
		$A.enqueueAction(createFolderAction);
	},

	//18. FIRE TOAST
	fireToast : function(message,type) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"type" : type,
			"message": message
		});
		toastEvent.fire();
	},
	
	//19. METHOD TO PERFORM NULL CHECK ON RETUNRNED VALUE
	checkForNull : function(component, returnedValue, location){
		if(returnedValue == null){
			this.fireToast('null response received ' + location ,'error');
			this.hideSpinner(component);
			return true;
		}else{
			return false;
		}
	}
})