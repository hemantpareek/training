({
    //this method retrives object list
    doInit : function(component, event, helper){
        var initAction = component.get("c.doGetObjectList");
        initAction.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                component.set("v.objectList", JSON.parse(response.getReturnValue()));
            }else{
                alert('Error in getting object list');
            }
            $A.util.addClass(component.find("mySpinner"), "slds-hide");
        });
        $A.enqueueAction(initAction);
    },

    //this method fetches fields and records
    getPickListFieldsWithValues : function(component, event, helper){
        component.set("v.showKanban", false);
        var objectName = component.get("v.objectName");
        component.set("v.picklistFieldName", "");
        if(objectName == 'none'){
            component.set("v.pickListFields", "");
            return;
        }
        $A.util.removeClass(component.find("mySpinner"), "slds-hide");
        var getFieldsAction = component.get("c.doGetPickListFieldsWithValues");
        getFieldsAction.setParams({
            "sObjectvalue" : objectName
        });
        getFieldsAction.setCallback(this, response => {
            if(response.getState() == 'SUCCESS'){
                component.set("v.pickListFields", JSON.parse(response.getReturnValue()));
            }else{
                alert('Error in getting pickListFields');
            }
            $A.util.addClass(component.find("mySpinner"), "slds-hide");
        });
        $A.enqueueAction(getFieldsAction);
    },

    getRecords : function(component, event, helper){
        $A.util.removeClass(component.find("mySpinner"), "slds-hide");
        var pickListFields = component.get("v.pickListFields");
        var pickListFieldName = component.get("v.picklistFieldName");
        var selectedPickList = pickListFields.filter( pl => {
            return pl.value == pickListFieldName;
        });
        var pickListValues = selectedPickList[0].pickListValues;
        pickListValues = pickListValues.split(',');
        component.set("v.pickListValues", pickListValues);
        console.log(selectedPickList[0]);
        component.set("v.isNillable", selectedPickList[0].isNillable);
        
        var getRecordsAction = component.get("c.doGetRecords");
        getRecordsAction.setParams({
            objectName : component.get("v.objectName"),
            picklistFieldName : component.get("v.picklistFieldName")
        });
        getRecordsAction.setCallback(this, response => {
            if(response.getState() == 'SUCCESS'){
                var records = JSON.parse(response.getReturnValue());
                component.set("v.records", records);
                helper.displayRecords(component, records);
            }else{
                alert('Error in getting records');
            }
            $A.util.addClass(component.find("mySpinner"), "slds-hide");
        });
        $A.enqueueAction(getRecordsAction);
    },

    handleDragstart : function(component, event, helper) {
        console.log('drag start');
        console.log(event.target.id);
        component.set("v.draggedRecordId", event.target.id);
    },

    handleDragover : function(component, event, helper) {
        // console.log('drag over');
        // console.log(event.target.id);
        event.preventDefault();
    },

    handleDrop : function(component, event, helper) {
        // event.preventDefault();
        helper.handleDrop(component, event, event.target.id);
    },

    handleInnerDrop : function(component, event, helper) {
        // event.preventDefault();
        helper.handleDrop(component, event, event.target.dataset.plv);
        event.preventDefault();
        event.stopPropagation();
        return false;
    },
})