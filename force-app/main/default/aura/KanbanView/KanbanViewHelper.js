({
    displayRecords : function(component, records){
        var pickListValues = component.get("v.pickListValues");
        var pickListFieldName = component.get("v.picklistFieldName");
        var recordsToDisplay = [];
        if(component.get("v.isNillable")){
            recordsToDisplay.push( { fieldValue : null , dataValue : records.filter( rcd => {return rcd[pickListFieldName] == null || rcd[pickListFieldName] == 'null' || rcd[pickListFieldName] == '' || rcd[pickListFieldName] == undefined}) })
        }
        pickListValues.forEach( plv => {
            recordsToDisplay.push( { fieldValue : plv , dataValue : records.filter( rcd => {return rcd[pickListFieldName] == plv;}) })
        });
        component.set("v.recordsToDisplay", recordsToDisplay);
        console.log(recordsToDisplay);
        component.set("v.showKanban", true);
        var height =  (screen.height * 0.7)+'px';
        console.log('height ::: --- '+height);
        component.set('v.kanbanDivHeight',height);
    },

    handleDrop : function(component, event, plv){
        var draggedRecordId = component.get("v.draggedRecordId");
        var pickList = component.get("v.picklistFieldName");
        if(plv == 'null' || plv == '' || plv == undefined || plv == null){
            plv = null;
        }
        if(this.isDroppedInSameDiv(component, draggedRecordId, pickList, plv)){
            return;
        }
        $A.util.removeClass(component.find("mySpinner"), "slds-hide");
        var dropAction = component.get("c.updateRecord");
        dropAction.setParams({
            objectName : component.get("v.objectName"),
            recordId : draggedRecordId,
            pickListfield : pickList,
            pickListFieldValve : plv
        });
        dropAction.setCallback(this, response => {
            if(response.getState() == 'SUCCESS' && response.getReturnValue() == 'true'){
                this.updateRecordClientSide(component, draggedRecordId, plv, pickList);
            }else{
                alert('Error in updating record');
            }
            $A.util.addClass(component.find("mySpinner"), "slds-hide");
        });
        $A.enqueueAction(dropAction);
    },

    updateRecordClientSide : function(component, recordId, plv, pickListfield){
        var records = component.get("v.records");
        var index = records.findIndex( r => r.id == recordId);
        if(plv == 'null' || plv == '' || plv == undefined || plv == null){
            plv = null;
        }
        records[index][pickListfield] = plv;
        this.displayRecords(component, records);
    },

    isDroppedInSameDiv : function(component, recordId, pickListField, plv){
        var records = component.get("v.records");
        var index = records.findIndex( r => r.id == recordId);
        if(plv == 'null' || plv == '' || plv == undefined || plv == null){
            plv = 'null';
        }
        if(records[index][pickListField] == plv){
            return true;
        }
        return false;
    }
})