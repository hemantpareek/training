({
    doInit : function(component, event, helper) {
        console.log('@@@ doinit');
        component.set('v.currentLocation', window.location.href);
        var initAction = component.get('c.getCaseRecordTypes');
        initAction.setCallback(this, response => {
            if(response.getState() == 'SUCCESS'){
                var returnValue = JSON.parse(response.getReturnValue());
                var isSuccess = returnValue.isSuccess;
                if(isSuccess){
                    console.log('@@@ retruendValue : ');
                    console.log(returnValue.data);
                    component.set('v.recordTypes', returnValue.data);
                } else {
                    console.log('@@@ error : ' + returnValue.msg);
                }
                
            } else {
                helper.showToast('error','Error in calling apex');
            }
        });
        $A.enqueueAction(initAction);
    },

    handleNext : function(component, event, helper){
        console.log('@@@ handleNext');
        console.log(component.get('v.selectedRecordTypeId'));
        console.log(component.get('v.recordId'));

        var queryCode = component.get('v.recordId') + Math.floor(Math.random() * 10000000000);
        component.set('v.queryCode', queryCode);

        var createCaseEvent = $A.get("e.force:createRecord");
        createCaseEvent.setParams({
            "entityApiName": "Case",
            "defaultFieldValues": {
                RecordTypeId  : component.get('v.selectedRecordTypeId'),
                Query_Code__c : queryCode
            },
            "navigationLocation": "LOOKUP",
            "panelOnDestroyCallback": function(event) {
                helper.changeCaseOfSocialPost(component, event);
            }
        });
        createCaseEvent.fire();
    },

    handleCancel : function(component, event, helper){
        console.log('@@@ handleCancel');
        $A.get("e.force:closeQuickAction").fire();
    },
})