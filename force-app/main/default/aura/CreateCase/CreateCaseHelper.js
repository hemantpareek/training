({
    showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    changeCaseOfSocialPost: function(component, event){
        console.log('@@@ changeCaseOfSocialPost');
        var updateRecordAction = component.get('c.updateSocialPost');
        updateRecordAction.setParams({
            queryCode : component.get('v.queryCode')
        });
        updateRecordAction.setCallback(this, response => {
            if(response.getState() == 'SUCCESS'){
                var returnValue = JSON.parse(response.getReturnValue());
                var isSuccess = returnValue.isSuccess;
                console.log('@@@ isSuccess: ' + isSuccess);
                if(isSuccess){
                    window.location.href = component.get('v.currentLocation');
                    console.log('success', returnValue.msg);
                } else {
                    console.log('error', returnValue.msg);
                }
            } else {
                this.showToast('error','Error in changeCaseOfSocialPost');
            }
            var cancelAction = component.get('c.handleCancel');
            $A.enqueueAction(cancelAction);
        });
        $A.enqueueAction(updateRecordAction);
    }
})