({
    handleInit : function(component, event, helper){
        helper.handleInit(component, event);
    },

    handleScroll : function(component, event, helper){
        helper.handleScroll(component, event);
    },
})