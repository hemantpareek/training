({
    handleInit : function(component, event) {
        $A.util.removeClass(component.find("mySpinner"), "slds-hide");
        var initAction = component.get("c.doInit");
        initAction.setParams({
            objectName : component.get("v.objectName"),
            fields : component.get("v.fields"),
            existingRecordIds : component.get("v.existingRecordIds")
        });
        initAction.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                // console.log(response.getReturnValue());
                this.prepareDataToShow(component, response.getReturnValue());
            }else{
                alert("Error in init handler");
            }
            $A.util.addClass(component.find("mySpinner"), "slds-hide");
        });
        $A.enqueueAction(initAction);
    },

    prepareDataToShow : function(component, records){
        if(records.length == 0){
            alert("No more records");
            return;
        }
        var existingRecordIds = component.get("v.existingRecordIds");
        var existingRecords = component.get("v.existingRecords");
        var fields = component.get("v.columns");
        records.forEach(record => {
            existingRecordIds.push(record.Id);
            var row = [];
            fields.forEach(field =>{
                row.push(record[field]);
            });
            existingRecords.push(row);
        });
        component.set("v.existingRecordIds", existingRecordIds);
        component.set("v.existingRecords", existingRecords);
        // console.clear();
        // console.log(existingRecordIds);
        // console.log(existingRecords);
    },

    handleScroll : function(component, event){
        
        // console.log(event.currentTarget.offsetHeight);
        
        // var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        // var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    
        var winScroll = event.currentTarget.scrollTop;
        var height = event.currentTarget.scrollHeight - event.currentTarget.clientHeight;
        var scrolled = (winScroll / height) * 100;
        // console.log(scrolled);
        
        if(scrolled == 100){
            this.handleInit(component, event);
        }
    },
})