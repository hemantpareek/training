({
	doInit : function(component, event){
		this.showSpinner(component);
		var fetchProductsAction = component.get("c.doInitApex");
		fetchProductsAction.setCallback(this, function(initResponse){
			if(initResponse.getState() == 'SUCCESS'){
				var products = JSON.parse(initResponse.getReturnValue());
				component.set("v.products",products);
				component.set("v.currentProduct",products[0]);
				component.set("v.unitPrice", products[0].price);
				component.set("v.selectedProductId", products[0].id);
				this.fillMonths(component);
			}else{
				this.fireToast("Error in doInit",'error');
			}
			this.hideSpinner(component);
		});
		$A.enqueueAction(fetchProductsAction);
	}, 

	fillMonths : function(component){
		var months = [];
		months.push({"name":"JAN", "value":"1"});
		months.push({"name":"FEB", "value":"2"});
		months.push({"name":"MAR", "value":"3"});
		months.push({"name":"APR", "value":"4"});
		months.push({"name":"MAY", "value":"5"});
		months.push({"name":"JUN", "value":"6"});
		months.push({"name":"JUL", "value":"7"});
		months.push({"name":"AUG", "value":"8"});
		months.push({"name":"SEP", "value":"9"});
		months.push({"name":"OCT", "value":"10"});
		months.push({"name":"NOV", "value":"11"});
		months.push({"name":"DEC", "value":"12"});
		component.set("v.months",months); 
	},

	payFromCreditCard : function(component, event){
		if(!this.validityCheck(component, event)){
			return;
		}
		this.showSpinner(component);
		var totalAmount = component.get("v.totalAmount");
		var cardNumber = component.get("v.creditCardNumber");
		var expiryYear = component.get("v.expiryYear");
		var expiryMonth = component.get("v.expiryMonth");
		var cvv = component.get("v.cvv");
		var cartItems = component.get("v.cartItems");
		var payFromCardAction = component.get("c.processPaymentFromCreditCard");
		payFromCardAction.setParams({
			"totalAmount" : totalAmount,
			"cardNumber" : cardNumber,
			"expiryYear" : expiryYear,
			"expiryMonth" : expiryMonth,
			"cvv" : cvv,
			"cartItemsJson" : JSON.stringify(cartItems)
		});
		payFromCardAction.setCallback(this, function(response){
			console.log('completed');
			if(response.getState() == 'SUCCESS'){
				var returnedValue = JSON.parse(response.getReturnValue());	
				this.fireToast(returnedValue.text + '  code : ' + returnedValue.code, returnedValue.code == 'I00001' ? 'success' : 'error');
				console.log(returnedValue.text + ' ' + returnedValue.code);
			}else{
				this.fireToast('Error occured in payFromCreditCard ','error');
			}
			this.hideSpinner(component);
		});
		$A.enqueueAction(payFromCardAction);
	},

	payFromEcheck : function(component, event){
		if(!this.validityCheck(component, event)){
			return;
		}
		this.showSpinner(component);
		var totalAmount = component.get("v.totalAmount");
		var routingNumber = component.get("v.routingNumber");
		var accountNumber = component.get("v.accountNumber");
		var nameOnAccount = component.get("v.nameOnAccount");
		var cartItems = component.get("v.cartItems");
		var payFromEcheckAction = component.get("c.processPaymentFromEcheck");
		payFromEcheckAction.setParams({
			"totalAmount" : totalAmount,
			"routingNumber" : routingNumber,
			"accountNumber" : accountNumber,
			"nameOnAccount" : nameOnAccount,
			"cartItemsJson" : JSON.stringify(cartItems)
		});
		payFromEcheckAction.setCallback(this, function(response){
			console.log('completed');
			if(response.getState() == 'SUCCESS'){
				var returnedValue = JSON.parse(response.getReturnValue());	
				this.fireToast(returnedValue.text + '  code : ' + returnedValue.code, returnedValue.code == 'I00001' ? 'success' : 'error');
				console.log(returnedValue.text + ' ' + returnedValue.code);
			}else{
				this.fireToast('Error occured in payFromCreditCard ','error');
			}
			this.hideSpinner(component);
		});
		$A.enqueueAction(payFromEcheckAction);
	},

	addToCart : function(component, event){
		var cartItems = component.get("v.cartItems");
		if(cartItems == null){
			cartItems = [];
		}
		var selectedProductId = component.get("v.selectedProductId");
		var products = component.get("v.products");
		var selectedProduct = products[products.findIndex(x => x.id === selectedProductId)];
		if(cartItems.findIndex(x => x.id === selectedProductId) == -1){ //i.e. product not exists in cart
			var newCartItem = {};
			newCartItem.name = selectedProduct.name;
			newCartItem.description = selectedProduct.description;
			newCartItem.id = selectedProduct.id;
			newCartItem.quantity = component.get("v.currentQuantity");
			newCartItem.unitPrice = selectedProduct.price;	
			newCartItem.total = newCartItem.unitPrice * newCartItem.quantity;
			cartItems.push(newCartItem);	
		}else{
			var cartItemIndex = cartItems.findIndex(x => x.id === selectedProductId);
			cartItems[cartItemIndex].quantity = cartItems[cartItemIndex].quantity * 1 + component.get("v.currentQuantity");
			cartItems[cartItemIndex].total = cartItems[cartItemIndex].unitPrice * cartItems[cartItemIndex].quantity;
		}
		component.set("v.currentQuantity", 1);
		component.set("v.cartItems", cartItems);
		this.calculateTotal(component, event);
		this.fireToast(selectedProduct.name + ' successfully added to cart','success');
	},

	removeFromCart : function(component, event){
		var cartItems = component.get("v.cartItems");
		var selectedCartItemId = component.get("v.selectedCartItemId"); 
		var selectedCartItemIndex = cartItems.findIndex(x => x.id === selectedCartItemId);
		cartItems.splice(selectedCartItemIndex, 1);
		component.set("v.cartItems", cartItems);
		this.calculateTotal(component, event);
		this.fireToast('Item successfully removed from cart','success');
	},

	calculateTotal : function(component, event){
		var cartItems = component.get("v.cartItems");
		var sum = 0;
		cartItems.forEach(function(item){
			sum += item.total;
		});
		component.set("v.totalAmount", sum);
	},

	updatePrice : function(component, event){
		var selectedProductId = component.get("v.selectedProductId");
		var products = component.get("v.products");
		var selectedProduct = products[products.findIndex(x => x.id === selectedProductId)];
		component.set("v.unitPrice", selectedProduct.price);
	},

	togglePaymentUI : function(component, event) {
		var isPayByCard = component.get("v.isPayByCard");
		isPayByCard = isPayByCard ? false : true;
		component.set("v.isPayByCard", isPayByCard);
	},

	showSpinner : function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
	},
	
    hideSpinner : function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
	},

	fireToast : function(message, type) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"type": type,
			"message": message
		});
		toastEvent.fire();
	},

	validateCardNumber : function(component, event){
		var cardNumber = event.getSource();
		var value = cardNumber.get("v.value");
		var length = value.length;
		if(length > 16){
			value = value/((length-16)*10);
			cardNumber.set("v.value",parseInt(value));
		}
	},

	validateCvv : function(component, event){
		var cvv = event.getSource();
		var value = cvv.get("v.value");
		var length = value.length;
		if(length > 3){
			value = value/((length-3)*10);
			cvv.set("v.value",parseInt(value));
		}
	},

	validityCheck : function(component, event){
		var isPayByCard = component.get("v.isPayByCard");
		if(isPayByCard){
			var cardNumber = component.get("v.creditCardNumber");
			var reg = /\d{16}/g;
			if(!reg.test(cardNumber)){
				this.fireToast('Invalid Card Number','error');
				return false;
			}
			var cvv = component.get("v.cvv");
			reg = /\d{3}/g;
			if(!reg.test(cvv)){
				this.fireToast('Invalid CVV','error');
				return false;
			}
		}else{
			var accountNumber = component.get("v.accountNumber");
			var reg = /\d{9}/g;
			if(!reg.test(accountNumber)){
				this.fireToast('Invalid Account Number','error');
				return false;
			}
			var routingNumber = component.get("v.routingNumber");
			reg = /\d{9}/g;
			if(!reg.test(routingNumber)){
				this.fireToast('Invalid Routing Number','error');
				return false;
			}
			// var nameOnAccount = component.get("v.nameOnAccount");
			// reg = /[a-zA-Z]{10}/g;
			// if(!reg.test(nameOnAccount)){
			// 	this.fireToast('Invalid Name','error');
			// 	return false;
			// }
		}
		return true;
	}
})