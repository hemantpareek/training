({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	togglePaymentUI : function(component, event, helper){
		helper.togglePaymentUI(component, event);
	},

	payFromCreditCard : function(component, event, helper){
		helper.payFromCreditCard(component, event);
	},

	payFromEcheck : function(component, event, helper){
		helper.payFromEcheck(component, event);
	},

	addToCart : function(component, event, helper){
		helper.addToCart(component, event);
	},

	removeFromCart : function(component, event, helper){
		helper.removeFromCart(component, event);
	},

	updatePrice : function(component, event, helper){
		helper.updatePrice(component, event);
	},

	validateCardNumber : function(component, event, helper){
		helper.validateCardNumber(component, event);
	},

	validateCvv : function(component, event, helper){
		helper.validateCvv(component, event);
	},

	validateAccountNumber : function(component, event, helper){
		helper.validateAccountNumber(component, event);
	},

	validateNameOnAccount : function(component, event, helper){
		helper.validateNameOnAccount(component, event);
	}
})