({
    openSubtab : function( component, uniqueId) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openSubtab({
            pageReference: {
                "type": "standard__component",
                "attributes": { "componentName": component.get('v.targetComponent') },
                "state":{ 
                        c__uniqueId : uniqueId,
                        c__returnURL : component.get('v.returnURL'),
                        c__mode : component.get('v.mode')
                    }
            },
            focus: true
        }).then( function( subtabId ) {
            workspaceAPI.setTabLabel({
                tabId: subtabId,
                label: uniqueId
            });
            workspaceAPI.setTabIcon({
                tabId: subtabId,
                icon: component.get('v.subTabIconName'),
                iconAlt: component.get('v.targetTabAlt')
            });
        }).catch(function(error) {
            console.log('error',  error);
        });
    },

    focusTab : function(component, resp, uniqueId) {
        let subtabs = resp.subtabs;
        let workspaceAPI = component.find("workspace");
        let isTabFocused = false;
        for(let j=0; j<subtabs.length; j++){
            if(subtabs[j].pageReference.state.c__uniqueId === uniqueId){
                console.log('# uniqueIdMatched: ',uniqueId);
                let tabId = subtabs[j].tabId;
                console.log('# tabId: ',tabId);
                workspaceAPI.focusTab({tabId});
                isTabFocused = true;
                break;
            }
        }
        if(!isTabFocused){
            this.openSubtab(component, uniqueId);
        }
    }  
})