({

    doInit : function(component, event, handler){
        var pageReference = component.get( "v.pageReference" );
        if(pageReference){
            console.log('mode: ', pageReference.state.c__mode);
            component.set('v.mode', pageReference.state.c__mode);
            component.set('v.stateless', true);
        }

        let mode = component.get('v.mode');
        if(mode === 'OrderHistory'){
            component.set('v.subTabIconName' , 'standard:orders');
            component.set('v.targetTabAlt'   , 'Order Detail');

        } else if(mode === 'Cart'){
            component.set('v.subTabIconName' , 'standard:webcart');
            component.set('v.targetTabAlt'   , 'Item Detail');
        }
    },

    handleOpenTab : function(component, event, helper) {
        let uniqueId = event.getParam('uniqueId');
        console.log('uniqueId: ' , uniqueId);
        //Check wheater tab is already open or not
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            console.log('response: ', JSON.parse(JSON.stringify(response)));
            component.set('v.returnURL', response.url);
            if(response.isSubtab && response.parentTabId){
                try{
                    workspaceAPI.getAllTabInfo().then(function(resp) {
                        console.log('resp: ', JSON.parse(JSON.stringify(resp)));
                        if(resp && resp.length > 0){
                            for(let i=0; i<resp.length; i++){
                                if(resp[i].tabId === response.parentTabId){
                                    console.log('# parentIdMatched: ',resp[i].tabId);
                                    helper.focusTab(component, resp[i], uniqueId);
                                    break;
                                }
                            }
                        }
                    });
                }catch(err){
                    console.log('err: ', err);
                }
            } else {
                helper.focusTab(component, response, uniqueId);
            }
        })
        .catch(function(error) {
            console.log('error: ', JSON.parse(JSON.stringify(error)));
        });
    }
})