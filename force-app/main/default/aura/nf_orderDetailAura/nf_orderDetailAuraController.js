({

    doInit : function(component, event, helper) {
        let libCmp = component.find('utils');
        try{
            var pageReference = component.get( "v.pageReference" );

            libCmp.createLog('OrderDetailAura > doInit > recordId',  pageReference.state.c__recordId);
            libCmp.createLog('OrderNumber > OrderDetailAura > doInit', pageReference.state.c__orderNumber);

            component.set('v.recordId', pageReference.state.c__recordId);
            component.set('v.orderNumber', pageReference.state.c__orderNumber);
        } catch (err){
            libCmp.createLog('OrderDetailAura > doInit > err', err);
        }
    },

    back : function(component, event) {
        let libCmp = component.find('utils');
        libCmp.createLog('OrderDetailAura > back > recordId', component.get('v.recordId'));
        if(component.get('v.recordId')){
            var workspaceAPI = component.find("workspace");
            workspaceAPI.openTab({
                url: '/lightning/r/Sobject/' + component.get('v.recordId') + '/view',
                focus: true
            });
        } else {
            libCmp.createLog('OrderDetailAura > back', 'Record Id not found');
        }
    }
})