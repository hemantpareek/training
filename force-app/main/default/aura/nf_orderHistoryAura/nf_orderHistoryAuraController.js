({
    getFocusedTabInfo : function(component, event, helper) {
        let libCmp = component.find('utils');

        let orderNumber = event.getParam('orderNumber');
        libCmp.createLog('OrderHistoryAura > getFocusedTabInfo > orderNumber',  orderNumber);
        
        //Check wheater tab is already open or not
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            libCmp.createLog('OrderHistoryAura > getFocusedTabInfo > response',  response);
            let tabId;
            if(response){
                try{
                    let subtabs = response.subtabs;
                    if(subtabs && subtabs.length>0){   
                        for(let j=0; j<subtabs.length; j++){
                            if(subtabs[j].pageReference.state.c__orderNumber == orderNumber){
                                tabId = subtabs[j].tabId;   
                                break;
                            }
                        }
                    }
                }catch(err){
                    libCmp.createLog('OrderHistoryAura > getFocusedTabInfo > err',  err);
                }
            }
            //Open new tab or focus to existing one accordingly
            libCmp.createLog('OrderHistoryAura > getFocusedTabInfo > tabId',  tabId);
            if(tabId){
                workspaceAPI.focusTab({tabId});
            } else {
                helper.openSubtab(component, orderNumber);
            }
        })
        .catch(function(error) {
            libCmp.createLog('OrderHistoryAura > getFocusedTabInfo > error',  error);
        });
    }
})