({
    openSubtab : function( component, orderNumber) {
        let libCmp = component.find('utils');
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openSubtab({
            pageReference: {
                "type": "standard__component",
                "attributes": { "componentName": 'c__nf_orderDetailAura' },
                "state": { 
                    c__orderNumber: orderNumber, 
                    c__recordId : component.get('v.recordId')
                }
            },
            focus: true
        }).then( function( subtabId ) {
            workspaceAPI.setTabLabel({
                tabId: subtabId,
                label: orderNumber
            });
            workspaceAPI.setTabIcon({
                tabId: subtabId,
                icon: "standard:orders",
                iconAlt: "Order History"
            });
        }).catch(function(error) {
            libCmp.createLog('OrderHistoryAura > openSubtab > error',  error);
        });
    }
})