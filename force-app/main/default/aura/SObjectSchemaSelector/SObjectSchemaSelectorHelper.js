({
	//1. HELPER FUNCTION FOR INIT HANDLER
	doInit : function(component, event) {
		var doInitAction = component.get("c.doInitSObjectList");
		doInitAction.setCallback(this, function(initResponse){
			if(initResponse.getState() == 'SUCCESS'){
				console.log(JSON.parse(initResponse.getReturnValue()));
				component.set("v.SObjects", JSON.parse(initResponse.getReturnValue()));
				component.set("v.selectedSObject", 'none');
			}else{
				this.fireToast('Error in doInit','error');
            }
            $A.util.addClass(component.find("mySpinner"),"slds-hide");
		});
		$A.enqueueAction(doInitAction);
	},

	//2. HELPER FUNCTION TO RETRIVE FIELDS LIST
	getFields : function(component, event){
        component.set("v.fields","");
		var selectedFields = component.get("v.selectedFields");
		selectedFields.splice(0,selectedFields.length);
		this.fireEvent(component,event);
        $A.util.removeClass(component.find("mySpinner"),"slds-hide");
		var selectedSObject = event.getParam("value");
		if(selectedSObject == 'none'){
			$A.util.addClass(component.find("mySpinner"),"slds-hide");
			return;
		}
		var objectChangeAction = component.get("c.doInitFieldList");
		objectChangeAction.setParams({
			"sObjectApiName" : selectedSObject
		});
		objectChangeAction.setCallback(this, function(response){
			if(response.getState() == 'SUCCESS'){
				var returnedValue = JSON.parse(response.getReturnValue());
				component.set("v.fields", returnedValue.fields);
				component.set("v.nameField", returnedValue.nameField);
			}else{
				this.fireToast('Error in getFields','error');
            }
            $A.util.addClass(component.find("mySpinner"),"slds-hide");
		});
		$A.enqueueAction(objectChangeAction);
    },

    fireEvent : function(component, event){
        var selectedSObject = component.get("v.selectedSObject");
        // var fields = component.get("v.selectedFields").join(",");
        // if(!fields){
        //     return;
        // }
        // var query = "SELECT ";
        // query += fields;
        // query += " FROM ";
        // query += selectedSObject;
        // query += " LIMIT 50000";
		// console.log(query);
		var SchemaSelection = $A.get("e.c:SchemaSelection");
        SchemaSelection.setParams({
			"objectName" : selectedSObject,
			"fields" : component.get("v.selectedFields")
        });
        SchemaSelection.fire();
    }
})