({
	//1. FUNCTION FOR INIT HANDLER
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	//2. FUNCTION TO GET FIELDS LIST 
	getFields : function(component, event, helper){
		helper.getFields(component, event);
	},

	fireEvent : function(component, event, helper){
		helper.fireEvent(component, event);
	}
})