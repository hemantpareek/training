({  
    handleSubmit : function(component, event, helper){
        var isRememberMe = component.get('v.isRememberMe');
        if(isRememberMe){
            helper.handleSubmit(component, event);
        } else {
            console.log('credentials not saved');
        }
    },

    doInit : function(component, event, helper){
        helper.handleInit(component);
    }
})