({
    handleSubmit : function(component, event, helper){
        this.toMem(component);
    },

    handleInit : function(component){
        component.set('v.userName', this.readCookie('userName'));
        component.set('v.password', this.readCookie('password'));
        console.log('cookies retrieved');
        console.log('username : ' +  component.get('v.userName'));
        console.log('password : ' + component.get('v.password'));
    },

    newCookie : function(name,value,days) {
        var days = 1;   // the number at the left reflects the number of days for the cookie to last
                        // modify it according to your needs
        if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString(); }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/"; 
    },
   
    readCookie : function(name) {
      var nameSG = name + "=";
      var nuller = '';
     if (document.cookie.indexOf(nameSG) == -1)
       return nuller;
   
      var ca = document.cookie.split(';');
     for(var i=0; i<ca.length; i++) {
       var c = ca[i];
       while (c.charAt(0)==' ') c = c.substring(1,c.length);
     if (c.indexOf(nameSG) == 0) return c.substring(nameSG.length,c.length); }
       return null; 
    },
   
    eraseCookie : function (name) {
     newCookie(name,"",1); 
    },
   
    toMem : function(component) {
        console.log('saved to mem');
        console.log('username :' +  component.get('v.userName'));
        console.log('password : ' + component.get('v.password'));
       this.newCookie('userName', component.get('v.userName'));     // add a new cookie as shown at left for every
       this.newCookie('password', component.get('v.password'));   // field you wish to have the script remember
       console.log('credentials saved');
    },
   
    delMem : function (component){
     eraseCookie('userName');   // make sure to add the eraseCookie function for every field
     eraseCookie('password');
   
     component.set('v.userName', '');   // add a line for every field
     component.set('v.password', ''); 
    }
})