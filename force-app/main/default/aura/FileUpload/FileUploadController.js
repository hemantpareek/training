({
    handleFilesChange : function(component, event, helper){
        component.set('v.showSpinner', true);
        var uploadedFile = event.getParam('files')[0];
        component.set('v.fileSize', uploadedFile.size/1024);
        console.log(uploadedFile);
        var reader = new FileReader();
		reader.onloadend = function(e){
			var dataURL = reader.result;
            var base64Data = dataURL.match(/,(.*)$/)[1]; //encode to base64Data
            component.set('v.fileName', uploadedFile.name);
            component.set('v.base64Data', base64Data);
            component.set('v.fileType', uploadedFile.type);
            console.log('base64Data length : ' + base64Data.length);
            console.log('File uploaded : ' + uploadedFile.name + ' (' + uploadedFile.size/1024 + ') KB');
            component.set('v.showSpinner', false);
		};
		reader.readAsDataURL(uploadedFile);
    },

    handleFileUpload : function(component, event, helper){
        component.set('v.uploadButtonLabel', 'Uploading');
        component.set('v.successMessage', '');
        component.set('v.showSpinner', true);
        if(component.get('v.fileSize') <= component.get('v.chunkSize')){
            //upload file immediately
            var chunks = [];
            chunks.push(component.get('v.base64Data'));
            component.set('v.chunks', chunks);
        } else {
            //split and upload chunks
            helper.splitFile(component);
        }
        helper.processChunks(component);
    },
})