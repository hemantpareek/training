({
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type" : type,
            "message": message
        });
        toastEvent.fire();
    },

    splitFile : function(component){
        //Write code to split files and create chunks
        var fileSize = component.get('v.fileSize');
        var chunkSize = component.get('v.chunkSize');
        var totalChunks = Math.ceil(fileSize / chunkSize);
        component.set('v.totalChunks', totalChunks);
        console.log('totalChunks : ' + totalChunks);
        //str.substring(2, 4); from index 2 to index 4 (index 4 excluded)
        var base64Data = component.get('v.base64Data');
        var chunkLength = Math.floor(base64Data.length/totalChunks);
        console.log(chunkLength);
        var firstIndex = 0;
        var lastIndex = chunkLength;
        var chunks = [];
        for(var i=1 ; i<= totalChunks; i++){
            console.log('firstIndex : ' + firstIndex + ' , lastIndex : ' + lastIndex);
            var chunk = base64Data.substring(firstIndex, lastIndex); //note last index is not included
            chunks.push(chunk);
            firstIndex = lastIndex;
            if( i == totalChunks - 1 ){
                lastIndex = base64Data.length;
            } else {
                lastIndex += chunkLength;
            }
        }
        component.set('v.chunks', chunks);
    },

    processChunks : function(component){
        var chunkIndex = component.get('v.currentChunkIndex');
        console.log('processing chunks : ' + (chunkIndex + 1));
        var uploadAction = component.get('c.createAttchment');
        uploadAction.setParams({
            parentId : component.get('v.parentId'),
            attachmentName : component.get('v.fileName'),
            attachmentId : component.get('v.attachmentId'),
            base64Data : component.get('v.chunks')[chunkIndex],
            fileType : component.get('v.fileType')
        });
        uploadAction.setCallback(this, response => {
            if(response && response.getState() == 'SUCCESS'){
                var result = response.getReturnValue();
                if(result.isSuccess){
                    console.log('Success : ' + result.attachmentId + ' => ' + (chunkIndex + 1) );
                    component.set('v.status', 'Processed chunks : ' + (chunkIndex + 1) + '/' + component.get('v.totalChunks'));
                    if(chunkIndex == 0){
                        component.set('v.attachmentId', result.attachmentId);
                    }
                    chunkIndex = chunkIndex + 1; 
                    component.set('v.currentChunkIndex', chunkIndex );
                    if(chunkIndex == component.get('v.totalChunks')){
                        console.log('All chunks processed');
                        component.set('v.status', 'All chunks processed');
                        this.showToast('Success!', 'success', 'Attachment Id: ' + result.attachmentId); 
                        this.resetAttributes(component);
                        return;
                    }else{
                        this.processChunks(component);
                    }   
                }else{
                    console.log('Error : ' + result.errorMessage);   
                    this.showToast('Error!', 'error', result.errorMessage);
                    this.resetAttributes(component); 
                }
            } else {
                console.log('error in uploading file');
                this.showToast('Error!', 'error', 'Error in uploading file');
                this.resetAttributes(component);
            }
        });
        $A.enqueueAction(uploadAction);
    },

    resetAttributes : function(component){
        component.set('v.fileName', null);
        component.set('v.base64Data', null);
        component.set('v.fileType', null);
        component.set('v.chunks', null);
        component.set('v.attachmentId', null);
        component.set('v.uploadButtonLabel', 'Upload');
        component.set('v.totalChunks', 1);
        component.set('v.chunkSize', 500);
        component.set('v.fileSize', 0);
        component.set('v.currentChunkIndex', 0);
        component.set('v.showSpinner', false);
        component.set('v.status', '');
    }
})