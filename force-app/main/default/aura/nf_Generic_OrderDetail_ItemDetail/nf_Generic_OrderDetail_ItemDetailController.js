({

    doInit : function(component, event, helper) {
        try{
            var pageReference = component.get( "v.pageReference" );

            console.log('returnURL: ',  pageReference.state.c__returnURL);
            console.log('mode: ', pageReference.state.c__mode);
            console.log('uniqueId: ', pageReference.state.c__uniqueId);

            component.set('v.returnURL', pageReference.state.c__returnURL);
            component.set('v.mode', pageReference.state.c__mode);
            component.set('v.uniqueId', pageReference.state.c__uniqueId);

        } catch (err){
            console.log('err', err);
        }
    },

    back : function(component, event) {
        console.log('returnURL: ', component.get('v.returnURL'));
        if(component.get('v.returnURL')){
            var workspaceAPI = component.find("workspace");
            workspaceAPI.openTab({
                url: component.get('v.returnURL'),
                focus: true
            });
        } else {
            console.log('back', 'returnURL not found');
        }
    }
})