({
    editQuickAction : function(component, event, helper) {
        console.log('@@@ editQuickAction');
        var actionAPI = cmp.find("quickActionAPI");
        var args = {actionName: "Social_Post__c.Edit"};
        actionAPI.selectAction(args).then(function(result){
            console.log(result);
            //Action selected; show data and set field values
        }).catch(function(e){
            console.log(e);
            if(e.errors){
                console.log('@@@ err');
                //If the specified action isn't found on the page, show an error message in the my component
            }
        });
    },

    createCaseQuickAction : function(component, event, helper) {
        var actionAPI = cmp.find("quickActionAPI");
        var args = {actionName: "Social_Post__c.Create_Case"};
        actionAPI.selectAction(args).then(function(result){
            //Action selected; show data and set field values
        }).catch(function(e){
            if(e.errors){
                //If the specified action isn't found on the page, show an error message in the my component
            }
        });
    },

    openModal : function(component, event, helper){
        component.set('v.showModal', true);
    },

    closeModal : function(component, event, helper){
        component.set('v.showModal', false);
    },
})