({
	//1. HELPER FOR FIRST TIME DATA LOAD FUNCTIONALITY TO JS CONTROLLER
	fetchDataFirstTime : function(component, event) {
		this.showSpinner(component);
		var fetchDataFirstTimeAction = component.get("c.doFetchDataFirstTime");
		fetchDataFirstTimeAction.setCallback(this,function(fetchDataResponse){
			var returnedValue = fetchDataResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'fetchDataFirstTime')){
				return;
			}
			this.updateCurrentFolder(component, 'root', 'Home');
			this.updateCurrentFolderContents(component, returnedValue);
			this.addToBreadCrumbList(component, event, {'name':'Home','id':'root','isLast':'true'});
			this.hideSpinner(component);
		});
		$A.enqueueAction(fetchDataFirstTimeAction);
	},

	//2. HELPER FOR GETTING ACCESS TOKEN FROM REFRESH TOKEN WHEN ACCESS TOKEN EXPIRES
	getAccessTokenFromRefreshToken : function(component, event){
		this.showSpinner(component);
		var getAccessTokenFromRefreshTokenAction = component.get("c.doGetAccessTokenFromRefreshToken");
		getAccessTokenFromRefreshTokenAction.setCallback(this,function(getAccessTokenFromRefreshTokenResponse){
			var returnedValue = getAccessTokenFromRefreshTokenResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'getAccessTokenFromRefreshToken')){
				return;
			}
			window.location.reload();
			this.hideSpinner(component);
		});
		$A.enqueueAction(getAccessTokenFromRefreshTokenAction);
	},

	//3. PROVIDES CODE TO GET ACCESS TOKEN
	getCodeForAuthentication : function(authURI){
		window.location.href = authURI;
	},

	//4. HELPER FOR GETTING ACCESS TOKEN VIA CODE WHEN REFRESH TOKEN IS NULL OR IT IS FIRST TIME AUTHENTICATION
	authenticateWithCode : function(component, code){
		var getAccessTokenAction = component.get("c.doGetAccessToken");
		getAccessTokenAction.setParams({ "code" : code });
		getAccessTokenAction.setCallback(this,function(accessTokenResponse){
			var returnedValue = accessTokenResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'authenticateWithCode')){
				return;
			}
			window.location.reload();
		});
		$A.enqueueAction(getAccessTokenAction);
	},

	//5. HELPER FUNCTION TO PERFORM CHANGE DIRECTORY ACTION
	changeDirectory : function(component, event){
		this.showSpinner(component);
		var changeDirectoryAction = component.get("c.doChangeDirectory");
		changeDirectoryAction.setParams({ "folderId" : event.getSource().get("v.name") }); //v.name holds folder id of selected folder
		changeDirectoryAction.setCallback(this,function(changeDirectoryResponse){
			var returnedValue = changeDirectoryResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'changeDirectory')){
				return;
			}
			var btnClicked = event.getSource();
			var folderName = btnClicked.get("v.label");
			var folderId = btnClicked.get("v.name");
			this.updateCurrentFolder(component, folderId, folderName);
			this.updateCurrentFolderContents(component, returnedValue);
			this.addToBreadCrumbList(component, event, {'name':folderName,'id':folderId,'isLast':true});
			this.hideSpinner(component, event);
		});
		$A.enqueueAction(changeDirectoryAction);
	},
	
	//6. HELPER FUNCTION TO DOWNLOAD FILE
	downloadFile : function(event){
		var shared = event.getSource().get("v.title");
		if(shared == true){ //string is used bcz we have true in string format
			window.location.href = event.getSource().get("v.name");
		}else{
			this.fireToast('File is not available for download','error');
		}
	},

	//7. FUNCTION TO UPDATE CURRENT FOLDER 
	updateCurrentFolder: function(component, folderId, folderName){
		var currentFolder = new Object();
		currentFolder.id = folderId;
		currentFolder.name = folderName;
		component.set("v.currentFolder",currentFolder);
	},

	//8. FUNCTION TO UPDATE CURRENT FOLDER CONTENTS
	updateCurrentFolderContents : function(component, returnedValue){
		if(this.checkForNull(component, returnedValue, 'updateCurrentFolderContents')){
			return;
		}
		var files = JSON.parse(returnedValue);
		var currentFolderDirectories = [];
		var currentFolderFiles = [];
		files.forEach(function(file){
			if(file.type == 'application/vnd.google-apps.folder'){
				currentFolderDirectories.push(file);
			}else{
				currentFolderFiles.push(file);
			}
		});
		component.set("v.currentFolderDirectories",currentFolderDirectories);
		component.set("v.currentFolderFiles",currentFolderFiles);
	},

	//9. FUNCTION TO ADD CURRENT FOLDER TO BREADCRUMB LIST
	addToBreadCrumbList : function(component, event, currentFolder){
		var breadCrumbsList = component.get("v.breadCrumbsList");
		if(breadCrumbsList == null){
			breadCrumbsList = [];
		}
		if(breadCrumbsList.findIndex(x => x.id === currentFolder.id) == -1){ //i.e. list does not contain element
			breadCrumbsList.forEach(function(breadCrumbFolder){
				breadCrumbFolder.isLast = false;
			});
			breadCrumbsList.push(currentFolder);
			component.set("v.breadCrumbsList",breadCrumbsList);
		}
	},

	//10. HELPER FUNCTION TO PERFORM BREADCRUMB ACTION
	performBreadCrumbAction : function(component, event){
		this.showSpinner(component);
		var btnClicked = event.getSource();
		var folderId = btnClicked.get("v.name");
		var breadCrumbsList = component.get("v.breadCrumbsList");
		var selectedFolderIndex = breadCrumbsList.findIndex(x => x.id === folderId); 
		if(selectedFolderIndex != -1){
			var tempList = [];
			for(var i = 0; i <= selectedFolderIndex ; i++){
				tempList.push(breadCrumbsList[i]);
			}
			tempList[tempList.length-1].isLast = true;
			component.set("v.breadCrumbsList",tempList);
		}
		//now fetch data from selected folder in breadcrumbs list
		this.changeDirectory(component, event);
	},

	//11. DELETED THE SELECTED FILE/FOLDER
	deleteFile : function(component, event){
		this.showSpinner(component);
		var deleteFileAction = component.get("c.deleteSelectedFile");
		var fileId = event.getSource().get("v.name");
		deleteFileAction.setParams({ "fileId" : fileId }); //v.name holds folder id of selected folder
		deleteFileAction.setCallback(this,function(deleteFileResponse){
			var returnedValue = deleteFileResponse.getReturnValue();
			if(this.checkForNull(component, returnedValue, 'deleteFile')){
				return;
			}
			if(event.getSource().get("v.label") == 'Delete Folder'){
				//remove file from folders list
				var currentFolderDirectories = component.get("v.currentFolderDirectories");
				var deletedElementIndex = currentFolderDirectories.findIndex(x => x.id === fileId);
				currentFolderDirectories.splice(deletedElementIndex, 1); //1 is used bcz we want to delete exact 1 element 
				component.set("v.currentFolderDirectories",currentFolderDirectories); 
				this.fireToast('Folder deleted successfully','success');
			}else{
				// remove file from files list
				var currentFolderFiles = component.get("v.currentFolderFiles");
				var deletedElementIndex = currentFolderFiles.findIndex(x => x.id === fileId);
				currentFolderFiles.splice(deletedElementIndex, 1);
				component.set("v.currentFolderFiles",currentFolderFiles);
				this.fireToast('file deleted successfully','success');
			}
			this.hideSpinner(component, event);
		});
		$A.enqueueAction(deleteFileAction);
	},

	//13. FUNCTION TO SHOW SPINNER
	showSpinner : function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
	},
	
	//14. FUNCTION TO HIDE SPINNER
    hideSpinner : function (component) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
	},
	
	//15. FUNCTION TO UPLOAD FILE
	uploadFile : function(component, event){
		this.showSpinner(component);
		var files = component.get("v.fileToBeUploaded");
		var file = files[0];
		var reader = new FileReader();
		var self = this; //used bcz function inside function can't use this reference
		reader.onloadend = function(e){
			var dataURL = reader.result;
			var base64Data = dataURL.match(/,(.*)$/)[1]; //encode to base64Data
			self.uploadFileOnServer(component, event, file, base64Data);
		};
		reader.readAsDataURL(file);
	},

	//16. HIT SERVER TO UPLOAD FILE
	uploadFileOnServer : function(component, event, file, base64Data){
		var fileUploadAction = component.get("c.doUploadFile");
		fileUploadAction.setParams({ 
			"fileName" : file.name,
			"base64Data" : base64Data,
			"contentType" : file.type,
			"parentFolderID" : component.get("v.currentFolder").id
		}); //v.name holds folder id of selected folder
		fileUploadAction.setCallback(this,function(fileUploadResponse){
			//create file in directory without hitting api
			var returnedValue = JSON.parse(fileUploadResponse.getReturnValue());
			if(this.checkForNull(component, returnedValue, 'uploadFileOnServer')){
				return;
			}
			if(returnedValue.type == 'application/vnd.google-apps.folder'){
				var currentFolderDirectories = component.get("v.currentFolderDirectories");
				if(currentFolderDirectories == null){
					currentFolderDirectories = [];
				}
				var createdFolder = {'name':returnedValue.name, 'id':returnedValue.id, 'type':returnedValue.type};
				currentFolderDirectories.push(createdFolder);
				component.set("v.currentFolderDirectories",currentFolderDirectories);
				component.find("createFolder").set("v.value","");
				this.fireToast('Folder ' + createdFolder.name + ' created successfully', 'success');
			}else{
				var currentFolderFiles = component.get("v.currentFolderFiles");
				if(currentFolderFiles == null){
					currentFolderFiles = [];
				}
				var uploadedFile = {'name':returnedValue.name, 'id':returnedValue.id, 'type':returnedValue.type};
				currentFolderFiles.push(uploadedFile);
				component.set("v.currentFolderFiles",currentFolderFiles);
				this.fireToast('File ' + returnedValue.name + ' uploaded successfully', 'success');
			}
			this.hideSpinner(component);
		});
		$A.enqueueAction(fileUploadAction);
	},

	//17. FUNCTION TO CREATE FOLDER
	createFolder : function(component, event){
		this.showSpinner(component);
		var folder = {};
		folder.name = component.find("createFolder").get("v.value");
		folder.type = 'application/vnd.google-apps.folder';
		this.uploadFileOnServer(component, event, folder, window.btoa('')); //window.btoa('') is just creating a empty base64Data. 
	},

	//18. FIRE TOAST
	fireToast : function(message, type) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			"type" : type,
			"message": message
		});
		toastEvent.fire();
	},

	//19. RETURNED VALUE NULL CHECK
	checkForNull : function(component, returnedValue, location){
		if(returnedValue == null){
			this.fireToast('null response received at ' + location,'error');
			this.hideSpinner(component);
			return true;
		}else{
			return false;
		}
	}
})