({
	doInit : function(component, event, helper) {
		var url = new URL(window.location.href);
		var id = url.searchParams.get("id");
        console.log('id : ' + id);
        component.set('v.id',id);
        var action = component.get('c.getAcc');
        action.setParams({
            'accId' : id
        });
        action.setCallback(this,function(response){
            console.log(response.getReturnValue());
            component.set('v.accname',response.getReturnValue());
        });
        $A.enqueueAction(action);
	}
})