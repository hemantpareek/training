({
	handleSuccess : function(component, event, helper) {
		console.log('record has been created successfully');
        component.set('v.showModal', false);
        helper.showToast(component, event);
        helper.clearAttributes(component, event);
	},
  	
    openModal : function(component, event, helper){
        component.set('v.showModal', true);
    },
    
    closeModel : function(component, event, helper){
        component.set('v.showModal', false);
    },
    
    onsubmit : function(component, event, helper){
        component.set('v.showModal', false);
    },
    
    next : function(component, event, helper){
        var currentScreen = component.get('v.screenNumber');
        if(currentScreen == 20){
            return;
        }
        component.set('v.screenNumber', currentScreen + 1);
    },

    previous : function(component, event, helper){
        var currentScreen = component.get('v.screenNumber');
        if(currentScreen == 1){
            return;
        }
        component.set('v.screenNumber', currentScreen - 1);
    },
    
    submit : function(component, event, helper){
      component.set('v.showModal', false);
      helper.clearAttributes(component, event);
    },
    
})