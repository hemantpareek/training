({
	showToast : function(component, event) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Success!",
        "type" : "success",
        "message": "The record has been updated successfully."
    });
    toastEvent.fire();
    },
    
    clearAttributes : function(component, event){
        component.set('v.screenNumber', 1);
    }
})