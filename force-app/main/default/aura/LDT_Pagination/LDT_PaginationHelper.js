({
    doInit : function(component, event) {
        var initAction = component.get("c.doQueryForRecords");
        initAction.setParams({
            "objectName" :  component.get("v.objectName"),
            "fields" : component.get("v.fields")
        });
        initAction.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var res = JSON.parse(response.getReturnValue());
                console.log(res);
                component.set("v.columns", res.columns);
                component.set("v.allRecords", JSON.stringify(res.data));
                component.set("v.totalRecords", res.data.length);
                this.changePageSize(component, event);
            }else{
                alert("Error Occured");
            }
            $A.util.addClass(component.find("mySpinner"),"slds-hide");
        });
        $A.enqueueAction(initAction);
    },

    changePageSize : function(component, event){
        var pageSize = component.get("v.pageSize");
        var totalRecords = component.get("v.totalRecords");
        var totalPages = 1;
        if(pageSize < totalRecords){
            totalPages = totalRecords/pageSize;
            if(Math.ceil(totalPages) != Math.floor(totalPages)){
                totalPages = Math.ceil(totalPages);
            }
        }
        component.set("v.totalPages", totalPages);
        component.set("v.pageNumber", 1);
        component.set("v.recordsOffset", 0);
        this.updateHasNextHasPrevious(component);
        this.prepareDataToShow(component, event);
    },

    prepareDataToShow : function(component, event){
        var pageSize = component.get("v.pageSize");
        var allRecords = JSON.parse(component.get("v.allRecords"));
        var recordsOffset = component.get("v.recordsOffset");
        var totalRecords = component.get("v.totalRecords");
        component.set("v.data", allRecords.splice(recordsOffset, pageSize));
        // console.clear();
        // console.log("local all records size : " + allRecords.length);
        // console.log("pageSize : " + pageSize);
        // console.log("allRecords : " + JSON.parse(component.get("v.allRecords")).length);
        // console.log("recordsOffset : " + recordsOffset);
        // console.log("pageNumber : " + component.get("v.pageNumber"));
        // console.log("dataSize : " + component.get("v.data").length);
    },

    fetchNextRecordSet : function(component, event){
        if(component.get("v.hasNext")){
            var pageNumber = component.get("v.pageNumber");
            pageNumber++;
            component.set("v.pageNumber", pageNumber);
            this.updateHasNextHasPrevious(component);
            var recordsOffset = component.get("v.recordsOffset");
            recordsOffset += parseInt(component.get("v.pageSize"));
            component.set("v.recordsOffset", recordsOffset);
            this.prepareDataToShow(component, event);
        }
    },

    fetchPreviousRecordSet : function(component, event){
        if(component.get("v.hasPrevious")){
            var pageNumber = component.get("v.pageNumber");
            pageNumber--;
            component.set("v.pageNumber", pageNumber);
            this.updateHasNextHasPrevious(component);
            var recordsOffset = component.get("v.recordsOffset");
            recordsOffset -= parseInt(component.get("v.pageSize"));
            component.set("v.recordsOffset", recordsOffset);
            this.prepareDataToShow(component, event);
        }
    },

    fetchFirstRecordSet : function(component, event){
        if(component.get("v.hasPrevious")){
            component.set("v.pageNumber", 1);
            this.updateHasNextHasPrevious(component);
            component.set("v.recordsOffset", 0);
            this.prepareDataToShow(component, event);
        }
    },

    fetchLastRecordSet : function(component, event){
        if(component.get("v.hasNext")){
            component.set("v.pageNumber", component.get("v.totalPages"));
            this.updateHasNextHasPrevious(component);
            var recordsOffset = parseInt(component.get("v.pageNumber") * component.get("v.pageSize") - component.get("v.pageSize"));
            component.set("v.recordsOffset", recordsOffset);
            this.prepareDataToShow(component, event);
        }
    },

    updateHasNextHasPrevious : function(component){
        var pageNumber = component.get("v.pageNumber");
        var hasNext = true;
        var hasPrevious = true;
        if(pageNumber == 1){
            hasPrevious = false;
        }
        if(pageNumber == component.get("v.totalPages")){
            hasNext = false;
        }
        component.set("v.hasNext", hasNext);
        component.set("v.hasPrevious", hasPrevious);
    },

    handleSchemaSectionEvent : function(component, event){
        var objectName = event.getParam("objectName");
        component.set("v.objectName", objectName);
        var fields = event.getParam("fields");
        // console.log(fields);
        if(fields && fields.length != 0 && objectName != 'none'){
            component.set("v.fields", fields);
            this.doInit(component, event);
            component.set("v.showComponent", true);
        }else{
            component.set("v.showComponent", false);
            return;
        }        
    },

    handleSort : function(component, event){
        var sortBy = component.get("v.sortBy");
        var sortDirection = component.get("v.sortDirection");
        var newSortBy = event.getParam("fieldName");
        if(!sortBy){
            sortBy = newSortBy;
        }
        if(sortBy == newSortBy){
             sortDirection = sortDirection == 'asc' ? 'desc' : 'asc';
        } else {
            sortDirection = 'asc';
            sortBy = newSortBy;
        }
        component.set("v.sortBy", sortBy);
        component.set("v.sortDirection", sortDirection);
        var allRecords = component.get("v.allRecords");
        allRecords = JSON.parse(allRecords).sort(function(a, b){
            var x = a[sortBy];
            var y = b[sortBy];
            if(x){
                x = x.toLowerCase();
            }
            if(y){
                y = y.toLowerCase();
            }
            if(x < y && sortDirection == 'desc'){
                return 1;
            }else if(x > y && sortDirection == 'asc'){
                return 1;
            }else{
                return 0;
            }
        });
        component.set("v.allRecords", JSON.stringify(allRecords));
        this.prepareDataToShow(component,event);
    }
})