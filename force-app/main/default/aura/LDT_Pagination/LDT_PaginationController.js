({
    doInit : function(component, event, helper) {
      helper.doInit(component,event);
    },

    changePageSize : function(component, event, helper){
        helper.changePageSize(component, event);
    },

    fetchNextRecordSet : function(component, event, helper){
        helper.fetchNextRecordSet(component, event);
    },

    fetchPreviousRecordSet : function(component, event, helper){
        helper.fetchPreviousRecordSet(component, event);
    },

    fetchFirstRecordSet : function(component, event, helper){
        helper.fetchFirstRecordSet(component, event);
    },

    fetchLastRecordSet : function(component, event, helper){
        helper.fetchLastRecordSet(component, event);
    },

    handleSchemaSectionEvent : function(component, event, helper){
        helper.handleSchemaSectionEvent(component, event);
    },

    handleSort : function(component, event, helper){
        helper.handleSort(component, event);
    }
})