({
    //QUERY FIVE RECORDS AS PER SEARCH TEXT
    queryRecords    : function(component, event){
        component.set("v.selectedRecordName", null);
        component.set("v.selectedRecordId", null);
        var id = component.get("v.id");
        component.set('v.focusedIndex', 0);
        component.set('v.isSearching', true);
        var searchKeyword = component.get('v.searchKeyword');
        var objectName = component.get("v.objectName");
        var action = component.get("c.handleQueryRecords");
        action.setParams({
            'sobjName' : objectName,
            'searchKeyword' : searchKeyword
        });
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var records = JSON.parse(response.getReturnValue());
                records.forEach( r =>{
                    if(objectName != 'case'){
                        r.nameField = r['Name'];
                    }else{
                        r.nameField = r['CaseNumber'];
                    }
                });
                if(records.length == 0){
                    $A.util.addClass( document.getElementById('dropDown' + id) , 'slds-hide');
                    if($A.util.hasClass( document.getElementById('error' + id), 'slds-hide')){
                        $A.util.removeClass( document.getElementById('noRecords' + id) , 'slds-hide');
                    }
                }else{
                    $A.util.addClass( document.getElementById('noRecords' + id) , 'slds-hide');
                    if($A.util.hasClass( document.getElementById('error' + id), 'slds-hide')){
                        $A.util.removeClass( document.getElementById('dropDown' + id), 'slds-hide');
                    }
                }
                component.set("v.records", records);
            }else{
                alert('Error in getting records');
            }
            component.set('v.isSearching', false);
        });
        $A.enqueueAction(action);
        var id = component.get("v.id");
        if($A.util.hasClass( document.getElementById('error' + id), 'slds-hide')){
            $A.util.removeClass( document.getElementById('dropDown' + id), 'slds-hide');
        }
    },

    //RETRIEVES FIRST FIVE RECORDS
    getAllRecords : function(component, event){
        var id = component.get("v.id");
        $A.util.addClass( document.getElementById('pill' + id), 'slds-hide');
        $A.util.removeClass( document.getElementById('mainDiv' + id), 'slds-hide');
        component.set('v.searchKeyword', '');
        this.queryRecords(component, event);
    },

    //FILLUP PILL WHEN INITIALLY A VALUE IS GIVE TO LOOKUP
    fillupPill : function(component, recordName){
        component.set("v.selectedRecordName", recordName);
        var id = component.get("v.id");
        $A.util.addClass( document.getElementById('mainDiv' + id), 'slds-hide');
        $A.util.removeClass( document.getElementById('pill' + id), 'slds-hide');
    }
})