({ 
    // INIT HANDLER TO FILL PREDEFINED VALUE TO LOOKUP IF EXISTS
    doInit : function(component, event, helper){
        var recordId = component.get('v.selectedRecordId');
        if(!recordId){
            return;
        } 
        var action = component.get('c.handleDoInit');
        action.setParams({
            "sobjName" : component.get('v.objectName'),
            "recordId" : recordId
        });
        action.setCallback(this, response => {
            if(response.getState() === 'SUCCESS'){
                helper.fillupPill(component, response.getReturnValue());
            }else{
                component.set('v.selectedRecordName', null);
                component.set('v.selectedRecordId', null);
            }
        });
        $A.enqueueAction(action);
    },

    // HANDLES CHNAGES IN LIGHTNING INPUT
    handleOnChange : function(component, event, helper){
        component.set('v.searchInput', event.getSource());
        var id = component.get("v.id");
        $A.util.addClass( document.getElementById('error' + id), 'slds-hide');
        $A.util.removeClass( document.getElementById('inputSpan' + id), 'slds-has-error');
        helper.queryRecords(component, event);
    },

    //HANDLES SELECTION OF NAME DIV INSIDE DROPDOWN
    handleSelect : function(component, event, helper){
        component.set("v.selectedRecordName", event.target.dataset.name);
        component.set("v.selectedRecordId", event.target.id);
        var id = component.get("v.id");
        $A.util.addClass( document.getElementById('mainDiv' + id), 'slds-hide');
        $A.util.removeClass( document.getElementById('pill' + id), 'slds-hide');
        component.set("v.isfocused", false);
    },

    //HANDLES PILL REMOVAL ACTION
    handleRemoveOnly : function(component, event, helper){
        var id = component.get("v.id");
        $A.util.addClass( document.getElementById('error' + id), 'slds-hide');
        $A.util.removeClass( document.getElementById('inputSpan' + id), 'slds-has-error');
        helper.getAllRecords(component, event);
        var searchInput = component.get('v.searchInput')
        if(searchInput){
            searchInput.focus();
        }
    },

    //HANDLES CLICK IN TEXT BOX (LIGHTNING INPUT)
    handleTextBoxClick : function(component, event, helper){
        component.set("v.isfocused", false);
        component.set('v.searchInput', event.getSource());
        var id = component.get("v.id");
        $A.util.addClass( document.getElementById('error' + id), 'slds-hide');
        $A.util.removeClass( document.getElementById('inputSpan' + id), 'slds-has-error');
        helper.queryRecords(component, event);
    },

    //HANDLES FOCUS OUT ACTION OF LIGHTNING INPUT
    handleFocusOut : function(component, event, helper){
        var id = component.get("v.id");
        if(!component.get("v.isfocused")){
            $A.util.addClass( document.getElementById('dropDown' + id), 'slds-hide');
            $A.util.addClass( document.getElementById('noRecords' + id), 'slds-hide');
        }
        if($A.util.hasClass( document.getElementById('dropDown' + id), 'slds-hide') && $A.util.hasClass( document.getElementById('noRecords' + id), 'slds-hide')){
            //check for validations
            var c2 = component.get('v.required');
            var c4 = component.get('v.searchKeyword') == '';
            if((!c4) || c2 ){
                var errorMsg;
                if(c4){
                    errorMsg = 'Field is required.';
                }else{
                    errorMsg = 'An invalid option has been chosen.';
                }
                var errorDiv = document.getElementById('error' + id);
                errorDiv.innerHTML = errorMsg;
                $A.util.removeClass( errorDiv, 'slds-hide');
                $A.util.addClass( document.getElementById('inputSpan' + id), 'slds-has-error');
            }
        }
        component.set('v.focusedIndex', 0);
    },

    //HANDLES MOUSE OVER EVENT OF DROPDOWN DIV
    handleDropdownMouseover : function(component, event){
        component.set("v.isfocused", true);
        component.get('v.records').forEach( r => {
            $A.util.removeClass( document.getElementById(r.Id), 'focusin');
        });
    },

    //HANDLES MOUSE OUT ACTION OF DROPDOWN DIV
    handleDropdownMouseOut : function(component, event, helper){
        component.set("v.isfocused", false);
    },

    //HANDLES KEYDOWN OF LIGHTNING INPUT'S WRAPPER SPAN
    handleKeyDown : function(component, event, helper){
        var records = component.get("v.records");
        if(records.length != 0){ 
            var focusedIndex = component.get("v.focusedIndex");  
            if(event.key == 'ArrowUp'){ //upArrow
                 //set focus on last child
                 for(var i=focusedIndex; i>=0; i--){
                    var currentElement = document.getElementById(records[i].Id);
                    if(!$A.util.hasClass(currentElement, 'focusin')){
                        $A.util.addClass( currentElement, 'focusin');
                        component.set('v.focusedIndex', i);
                        return;
                    }else{
                        $A.util.removeClass( currentElement, 'focusin');
                    }
                }
                $A.util.removeClass( document.getElementById(records[0].Id), 'focusin');
                $A.util.addClass( document.getElementById(records[records.length-1].Id), 'focusin');
                component.set('v.focusedIndex', records.length-1);
            }else if(event.key == 'ArrowDown'){ //downArrow
                //set focus on first child
                for(var i=focusedIndex; i<records.length; i++){
                    var currentElement = document.getElementById(records[i].Id);
                    if(!$A.util.hasClass(currentElement, 'focusin')){
                        $A.util.addClass( currentElement, 'focusin');
                        component.set('v.focusedIndex', i);
                        return;
                    }else{
                        $A.util.removeClass( currentElement, 'focusin');
                    }
                }
                $A.util.removeClass( document.getElementById(records[records.length-1].Id), 'focusin');
                $A.util.addClass( document.getElementById(records[0].Id), 'focusin');
                component.set('v.focusedIndex', 0);
            }else if(event.key == 'Enter'){ // ENTER WILL BE HANDLED AS SELECT
                var selectedDiv = document.getElementsByClassName('focusin')[0];
                if(selectedDiv){
                    component.set("v.selectedRecordName", selectedDiv.dataset.name);
                    component.set("v.selectedRecordId", selectedDiv.id);
                    var id = component.get("v.id");
                    $A.util.addClass( document.getElementById('mainDiv' + id), 'slds-hide');
                    $A.util.removeClass( document.getElementById('pill' + id), 'slds-hide');
                    component.set("v.isfocused", false);
                }
            }
        }
    },
})