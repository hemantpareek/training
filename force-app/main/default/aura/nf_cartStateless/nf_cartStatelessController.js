({
    /**
     * @name getAllTabInfo
     * @description Loads all tabs info and updates icon and title for required tab by matching its api name
     */
    getAllTabInfo : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getAllTabInfo().then(function(response) {
            console.log('response: ', JSON.parse(JSON.stringify(response)));
            if(response && response.length > 0){
                try{
                    for(let i=0; i<response.length; i++){
                        if(response[i].pageReference.attributes.apiName == component.get('v.tabApiName')){
                            let tabId = response[i].tabId;
                            workspaceAPI.setTabLabel({
                                'tabId' : tabId, 
                                'label' : component.get('v.tabLabel')
                            });
                            workspaceAPI.setTabIcon({
                                'tabId' : tabId,
                                'icon' : component.get('v.tabIcon'),
                                'iconAlt' : component.get('v.tabIconAlt')
                            });
                        }
                    }
                }catch(err){
                    console.log('err: ', err);
                }
            }
        })
        .catch(function(error) {
            console.log('error: ', error);
        });
    }
})