({
    openSubtab : function( component, itemNumber) {
        let libCmp = component.find('utils');
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openSubtab({
            pageReference: {
                "type": "standard__component",
                "attributes": { "componentName": 'c__nf_itemDetailAura' },
                "state": { 
                    c__itemNumber: itemNumber, 
                    c__recordId : component.get('v.recordId')
                }
            },
            focus: true
        }).then( function( subtabId ) {
            workspaceAPI.setTabLabel({
                tabId: subtabId,
                label: itemNumber
            });
            workspaceAPI.setTabIcon({
                tabId: subtabId,
                icon: "standard:webcart",
                iconAlt: "Cart"
            });
        }).catch(function(error) {
            libCmp.createLog('CartAura > openSubtab > error',  error);
        });
    }
})