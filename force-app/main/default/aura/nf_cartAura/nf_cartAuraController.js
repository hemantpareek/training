({
    getFocusedTabInfo : function(component, event, helper) {
        let libCmp = component.find('utils');

        let itemNumber = event.getParam('itemNumber');
        libCmp.createLog('CartAura > getFocusedTabInfo > itemNumber',  itemNumber);
        
        //Check wheater tab is already open or not
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            libCmp.createLog('CartAura > getFocusedTabInfo > response',  response);
            let tabId;
            if(response){
                try{
                    let subtabs = response.subtabs;
                    if(subtabs && subtabs.length>0){   
                        for(let j=0; j<subtabs.length; j++){
                            if(subtabs[j].pageReference.state.c__itemNumber == itemNumber){
                                tabId = subtabs[j].tabId;   
                                break;
                            }
                        }
                    }
                }catch(err){
                    libCmp.createLog('CartAura > getFocusedTabInfo > err',  err);
                }
            }
            //Open new tab or focus to existing one accordingly
            libCmp.createLog('CartAura > getFocusedTabInfo > tabId',  tabId);
            if(tabId){
                workspaceAPI.focusTab({tabId});
            } else {
                helper.openSubtab(component, itemNumber);
            }
        })
        .catch(function(error) {
            libCmp.createLog('CartAura > getFocusedTabInfo > error',  error);
        });
    }
})